﻿Imports System.Data.SqlClient
Imports System.Text

Module basCommon
    Public Clipped As Boolean           'コントロール状態保存フラグ
    Public ctls As Collection           '各コントロール状態の退避エリア
    Public clpScaleWidth As Double      'コントロールの幅
    Public clpScaleHeight As Double     'コントロールの高さ
    Public typSelData As New clsOrder
    Public G_LOGIN As Boolean = False

    '///部位明細格納用 ADD By Watanabe 2012.01.25
    Public aryBodyPartDetail() As clsBodyPart
    Public aryUpdBodyPartDetail() As clsBodyPart
    Public G_STUDY_DIV As String

    '---Ver.3.0.3.0 一般撮影受付時、CR、モバイルどちらで受け付けたかの判断 0=CR,1=モバイル 2025.01.17 ADD By Watanabe 
    Public intCrMobile As Integer = 0

#Region "DLL定義"
    Public Const DLL_NAME = "C:\Priall\Development\JINKOUKAIHP\PRISM_MWM_FORM\PRISM_MWM_FORM\dll\NRNDRT316.dll"

    'Setting of a DLL
    Public Declare Function USB_Device_Open Lib "NRNDRT316.dll" () As Long
    Public Declare Sub USB_Device_Close Lib "NRNDRT316.dll" ()
    Public Declare Function rs_CommSettings Lib "NRNDRT316.dll" (ByVal pBps As String, ByVal pBIT As String, ByVal pParity As String, ByVal pStop As String) As Long
    Public Declare Function rs_CommFormat_Set Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String, _
        ByVal Para3 As String, ByVal Para4 As String, ByVal Para5 As String, ByVal Para6 As String, ByVal Para7 As String) As Long
    Public Declare Function Connection_Model_Setup Lib "NRNDRT316.dll" (ByRef Model As Long) As Long

    Public Declare Function MagCard_Read Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String) As Long


    Public Declare Function Op_Prohibition Lib "NRNDRT316.dll" () As Long
    Public Declare Function Reset_Device Lib "NRNDRT316.dll" () As Long
    Public Declare Function StatusCheck Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal strData As String) As Long

    Public Declare Function BuzzerControl Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String, ByVal Para3 As String) As Long
    Public Declare Function LEDControl Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String, ByVal Para3 As String) As Long

    Public Declare Function Power_ON_OFF_Operation_Setup Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String, _
                                                                            ByVal Para3 As String, ByVal strData As String) As Long
    Public Declare Function Comm_Control_Set Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String, ByVal Para3 As String, _
        ByVal Para4 As String, ByVal Para5 As String, ByVal sPara6 As String, ByVal Para7 As String, ByVal strData As String) As Long
    Public Declare Function Comm_Format_Set Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String, _
        ByVal Para3 As String, ByVal Para4 As String, ByVal Para5 As String, ByVal Para6 As String, ByVal Para7 As String, ByVal strData As String) As Long
    Public Declare Function Read_Data_Send_Range_Set Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String, ByVal Para3 As String, ByVal strData As String) As Long

    Public Declare Function BUZ_Operation_setup Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String, _
        ByVal Para3 As String, ByVal Para4 As String, ByVal Para5 As String, ByVal strData As String) As Long
    Public Declare Function LED_Operation_setup Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String, _
        ByVal Para3 As String, ByVal Para4 As String, ByVal Para5 As String, ByVal strData As String) As Long

    Public Declare Function OperationTimingSet Lib "NRNDRT316.dll" (ByVal Para1 As String, ByVal Para2 As String, ByVal strData As String) As Long
    Public Declare Function LEDBuzzerOperationTimingSet Lib "NRNDRT316.dll" (ByVal Para As String, ByVal strData As String) As Long

    Public Declare Function BuzzerVolumeSet Lib "NRNDRT316.dll" (ByVal Para As String, ByVal strData As String) As Long


    Public Declare Function GetStatus Lib "NRNDRT316.dll" (ByRef Para1 As Long) As Long
    Public Declare Function GetCard_Result Lib "NRNDRT316.dll" (ByRef Para1 As Long, ByRef Para2 As Long, ByRef Para3 As Long, ByRef Tk1Len As Long, ByRef Tk2Len As Long, ByRef Tk3Len As Long) As Long
    Public Declare Function GetTrk1Data Lib "NRNDRT316.dll" (ByRef Para1 As Long, ByRef Para2 As Byte) As Long
    Public Declare Function GetTrk2Data Lib "NRNDRT316.dll" (ByRef Para1 As Long, ByRef Para2 As Byte) As Long
    Public Declare Function GetTrk3Data Lib "NRNDRT316.dll" (ByRef Para1 As Long, ByRef Para2 As Byte) As Long
#End Region

#Region "ログ出力"
    '*********************************************************************
    '* 関数名　　：subOutLog
    '* 関数概要　：所定フォルダログ出力
    '* 引　数　　：strMsg・・・出力メッセージ
    '* 　　　　　：intProc・・・メッセージ区分(0=Information、1=Error、2=Warning)
    '* 戻り値　　：無
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Sub subOutLog(ByVal strMsg As String, ByVal intProc As Integer)

        Dim path_LOG As String = String.Empty
        Dim strPath As String = String.Empty

        If My.Settings.LOG_PATH.EndsWith("\") Then
            path_LOG = My.Settings.LOG_PATH
        Else
            path_LOG = My.Settings.LOG_PATH + "\"
        End If

        '****************************************************
        '* ログ出力所定パス+ 年のフォルダが無ければ作成する *
        '****************************************************
        strPath = path_LOG & DateTime.Now.ToString("yyyy")
        If System.IO.File.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        '*********************************************************
        '* ログ出力所定パス+ 年 + 月のフォルダが無ければ作成する *
        '*********************************************************
        strPath &= "\" & DateTime.Now.ToString("MM")
        If System.IO.File.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        '**************************************************************
        '* ログ出力所定パス+ 年 + 月 + 日のフォルダが無ければ作成する *
        '**************************************************************
        'strPath &= "\" & DateTime.Now.ToString("dd")
        'If System.IO.File.Exists(strPath) = False Then
        '    System.IO.Directory.CreateDirectory(strPath)
        'End If

        Dim file_LOG As String = String.Empty

        file_LOG = strPath & "\" & _
                   DateTime.Now.ToString("yyyyMMdd") + ".log"

        Dim objsw As New System.IO.StreamWriter(file_LOG, _
                                                True, _
                                                System.Text.Encoding.GetEncoding(932))
        Dim strCate As String = ""

        Select Case intProc
            Case 0          'Information
                strCate = "[INF] " & DateTime.Now.ToString("yyyyMMdd HH:mm:ss") & " :"
            Case 1          'Error
                strCate = "[ERR] " & DateTime.Now.ToString("yyyyMMdd HH:mm:ss") & " :"
            Case 2          'Warning
                strCate = "[WAR] " & DateTime.Now.ToString("yyyyMMdd HH:mm:ss") & " :"
        End Select

        'Console.WriteLine(strCate & strMsg)
        'Console.ReadLine()

        objsw.WriteLine(strCate & strMsg)

        objsw.Close()

    End Sub
#End Region

#Region "SQLサーバConnection"
    Public Function fncDBConnect(ByRef strMsg As String) As Boolean

        Dim strDataSource As String = vbNullString
        Dim strCatalog As String = vbNullString
        Dim strDbUser As String = vbNullString
        Dim strDbPass As String = vbNullString
        'ﾛｸﾞ用ﾃｰﾌﾞﾙ接続
        Dim connectionString3 As New System.Text.StringBuilder

        Try
            strDataSource = My.Settings.DATASOURCE
            strCatalog = My.Settings.CATALOG
            strDbUser = My.Settings.DBUSER
            strDbPass = My.Settings.DBPASS
            connectionString3.Append("Data Source=")
            connectionString3.Append(strDataSource)
            connectionString3.Append(";")
            connectionString3.Append("Initial Catalog=")
            connectionString3.Append(strCatalog)
            connectionString3.Append(";")
            connectionString3.Append("UID=")
            connectionString3.Append(strDbUser)
            connectionString3.Append(";")
            connectionString3.Append("PWD=")
            connectionString3.Append(strDbPass)
            connectionString3.Append(";")
            connectionString3.Append("Integrated Security=false;")
            connectionString3.Append("Connect Timeout = 30;")

            'Call subOutLog("データベース接続 開始 (パラメータ = " & connectionString3.ToString & ") " & _
            '               "[basRepCommon.fncDBConnect]", 0)

            objConnect_LocalDB = New SqlConnection
            With objConnect_LocalDB
                .ConnectionString = connectionString3.ToString
                .Open()
            End With
            Return True

        Catch ex As Exception

            strMsg = ex.Message

            Call subOutLog("データベース接続 失敗 (エラー内容 = " & strMsg & ") " & _
                           "[basRepCommon.fncDBConnect]", 1)
            Return False

        End Try

    End Function
#End Region

#Region "SQLサーバDisConnect"
    Public Function fncDBDisConnect(ByRef strMsg As String) As Boolean

        Try

            If Not objConnect_LocalDB Is Nothing Then
                objConnect_LocalDB.Close()
                objConnect_LocalDB.Dispose()
            End If
            Return True
        Catch ex As Exception
            strMsg = ex.Message
            Return False
        End Try
    End Function
#End Region

#Region "SQL発行"
    Public Function fncAdptSQL(ByRef objCon As SqlConnection, ByVal strSQL As String, ByRef strMsg As String) As DataSet

        fncAdptSQL = New DataSet

        Try
            Dim objAdpt As SqlDataAdapter = New SqlDataAdapter(strSQL, objCon)

            objAdpt.SelectCommand.CommandTimeout = 0
            objAdpt.Fill(fncAdptSQL)

            objAdpt.Dispose()
            fncAdptSQL.Dispose()

        Catch ex As Exception

            strMsg = ex.Message
            subOutLog("fncAdptSQL Error:" & ex.Message, 1)

        End Try

    End Function


    Public Function fncExecuteNonQuery(ByRef objCon As SqlConnection, ByVal strSQL As String, Optional ByRef strMsg As String = "") As Integer

        Try

            Dim objCommand As SqlCommand

            objCommand = objCon.CreateCommand()
            objCommand.CommandText = strSQL

            Dim iResult As Integer = objCommand.ExecuteNonQuery()
            objCommand.Dispose()

            Return iResult

        Catch ex As Exception

            Debug.WriteLine(ex.Message)
            subOutLog(ex.Message, 1)
            Return 0

        End Try

    End Function

#End Region

#Region "fncChkLogin　ログイン認証"
    '************************************************************
    '* 関数名称　：fncChkLogin
    '* 機能概要　：ログイン認証
    '* 引　数　　：strUserID・・・ユーザID
    '* 　　　　　：strPass　・・・パスワード
    '* 　　　　　：strMsg　 ・・・エラーメッセージ
    '* 戻り値　　：無
    '* 作成日付　：2010/0716
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '************************************************************
    Public Function fncChkLogin(ByVal strUserID As String, ByVal strPass As String, ByRef strMsg As String) As Integer

        Dim strSQL As New System.Text.StringBuilder
        Dim dsUser As New DataSet

        Try
            '*************
            '* SQL文編集 *
            '*************
            strSQL.Append("SELECT " & vbCrLf)
            strSQL.Append(" USER_ANO," & vbCrLf)        'レポートユーザ番号(オートインクリメント)
            strSQL.Append(" USER_ID," & vbCrLf)         'レポートユーザID
            strSQL.Append(" USER_PASS," & vbCrLf)       'レポートユーザパスワード
            strSQL.Append(" USER_NAME," & vbCrLf)       'レポートユーザ名
            strSQL.Append(" USER_AUTH " & vbCrLf)      'レポートユーザ権限(0=システム管理者、1=最終確定者、2=1次確定者、3=参照権限)
            strSQL.Append("FROM " & vbCrLf)
            strSQL.Append(" MST_USER " & vbCrLf)
            strSQL.Append("WHERE " & vbCrLf)
            strSQL.Append(" USER_ID = '" & strUserID & "' ")

            '*************
            '* SQL文発行 *
            '*************
            dsUser = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '*********************************
            '* 該当データなしの場合、1を戻す *
            '*********************************
            If dsUser.Tables.Count = 0 Or dsUser.Tables(0).Rows.Count = 0 Then
                Return 1            '該当データなし
            End If

            '**********************************************************
            '* PassWordまで一致した場合は0、パスワード不一致は2を戻す *
            '**********************************************************
            If strPass.Trim = dsUser.Tables(0).Rows(0).Item("USER_PASS") Then

                With typUserInf
                    .USER_ANO = dsUser.Tables(0).Rows(0).Item("USER_ANO")
                    .USER_ID = dsUser.Tables(0).Rows(0).Item("USER_ID")
                    .USER_PASS = dsUser.Tables(0).Rows(0).Item("USER_PASS")
                    .USER_NAME = dsUser.Tables(0).Rows(0).Item("USER_NAME")
                    .REP_USER_AUTH = dsUser.Tables(0).Rows(0).Item("USER_AUTH")
                End With

                Return RET_NORMAL               '一致
            Else
                Return RET_WARNING              '不一致
            End If

        Catch ex As Exception

            '**************************************************
            '* 実行時エラーの場合、エラーメッセージと99を戻す *
            '**************************************************
            strMsg = ex.Message
            Return RET_ERROR

        Finally
            dsUser.Dispose()
            dsUser = Nothing
        End Try
    End Function

#End Region

#Region "fncInputNumeric　数値入力"
    '************************************************************
    '* 関数名称　：fncInputNumeric
    '* 機能概要　：数値専用入力エリア用
    '* 　　　　　：許容=0～9、タブ、BackSpace、←、→、↑、↓、.
    '* 　　　　　：許容以外はTrueを戻す。
    '* 引　数　　：chrKey・・・Char
    '* 戻り値　　：Boolean True=許容以外、False=許容
    '* 作成日付　：2010/07/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '************************************************************
    Public Function fncInputNumeric(ByVal chrKey As Char, Optional ByVal intProc As Integer = 0) As Boolean
        If chrKey < "0"c Or chrKey > "9"c Then
            If intProc = 0 Then
                If Not (chrKey = Chr(Keys.Tab)) And Not (chrKey = Chr(Keys.Back)) And _
                    Not (chrKey = Chr(Keys.Left)) And Not (chrKey = Chr(Keys.Right)) And _
                    Not (chrKey = Chr(Keys.Up)) And Not (chrKey = Chr(Keys.Down)) And _
                    Not (chrKey = "."c) Then

                    Return True

                End If
            Else
                If Not (chrKey = Chr(Keys.Tab)) And Not (chrKey = Chr(Keys.Back)) And _
                    Not (chrKey = Chr(Keys.Left)) And Not (chrKey = Chr(Keys.Right)) And _
                    Not (chrKey = Chr(Keys.Up)) And Not (chrKey = Chr(Keys.Down)) Then

                    Return True

                End If
            End If
        End If

        Return False
    End Function
#End Region

#Region "gncGetServerDate サーバ日付取得"
    '************************************************************
    '* 関数名称　：gncGetServerDate
    '* 機能概要　：サーバ日付取得
    '* 引　数　　：intProc・・・0=日付取得
    '* 　　　　　：       　　　1=時刻取得
    '* 　　　　　：             2=日付＋時刻
    '* 　　　　　：strMsg・・・エラーメッセージ
    '* 戻り値　　：String・・・サーバ日付
    '* 　　　　　：      　　　intProcが2の場合 日付 & "," & 時刻
    '* 作成日付　：2010/10/08
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '************************************************************
    Public Function fncGetServerDate(ByVal intProc As Integer, ByRef strMsg As String) As String
        Dim strDate As String = vbNullString
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL生成
            strSQL.Clear()

            If intProc = 0 Then
                strSQL.AppendLine("SELECT CONVERT(char(10), GETDATE(), 112) AS NOW_DATE ")
            ElseIf intProc = 1 Then
                strSQL.AppendLine("SELECT REPLACE(CONVERT(varchar,GETDATE(),108),':','') AS NOW_TIME")
            ElseIf intProc = 2 Then
                strSQL.AppendLine("SELECT ")
                strSQL.AppendLine(" CONVERT(char(10), GETDATE(), 112) AS NOW_DATE,")
                strSQL.AppendLine(" REPLACE(CONVERT(varchar,GETDATE(),108),':','') AS NOW_TIME ")

            End If

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If intProc = 0 Then
                '///取得データを退避
                strDate = dsData.Tables(0).Rows(0).Item("NOW_DATE")
            ElseIf intProc = 1 Then
                '///取得データを退避
                strDate = dsData.Tables(0).Rows(0).Item("NOW_TIME")
            ElseIf intProc = 2 Then
                strDate = dsData.Tables(0).Rows(0).Item("NOW_DATE")
                strDate &= "," & dsData.Tables(0).Rows(0).Item("NOW_TIME")
            End If

            Return strDate

        Catch ex As Exception
            strMsg = ex.Message
            Return vbNullString
        End Try
    End Function
#End Region

#Region "fncGetStringLength 文字列のバイト数取得"
    '************************************************************
    '* 関数名称　：fncGetStringLength
    '* 機能概要　：文字列のバイト数取得
    '* 引　数　　：strText・・・String
    '* 戻り値　　：Integer・・・渡された文字列のバイト数を戻す  
    '* 作成日付　：2010/07/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '************************************************************
    Public Function fncGetStringLength(ByVal strText As String) As Integer
        Dim strStr As String
        Dim sjisEnc As Encoding
        Dim intStrByte As Integer

        strStr = strText
        sjisEnc = Encoding.GetEncoding("Shift_JIS")

        intStrByte = sjisEnc.GetByteCount(strStr)

        Return intStrByte
    End Function
#End Region

#Region "subHightLight テキストボックスのハイライト表示"
    '************************************************************
    '* 関数名称　：subHightLight
    '* 機能概要　：テキストボックスのハイライト表示
    '* 引　数　　：objText・・・TextBox
    '* 戻り値　　：無  
    '* 作成日付　：2010/07/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '************************************************************
    Public Sub subHightLight(ByRef objText As Object)
        objText.SelectionStart = 0
        objText.SelectionLength = objText.TextLength
    End Sub
#End Region

End Module