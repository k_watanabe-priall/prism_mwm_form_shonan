﻿Module basNKF32

    '===== NKF32.DLL Declaration Module for Visual Basic(32bit) Ver 1.05 =====
    '(C)2000-2002 けるべ
    'MAIL : kelbe@geocities.co.jp
    'HOME : http://www.geocities.co.jp/SilkRoad/4511/
    '
    'このモジュールは、海人氏が NKF を Win32 用に移植された NKF32.DLL を、
    'Microsoft Visual Basic(32bit) から使用するための宣言モジュールです。
    'NKF32.DLL および NKF についての詳細は、NKF32.DLL に添付されている
    'nkf32.doc をお読み下さい。
    '
    '海人さんのウェブサイト : http://www2.tokai.or.jp/uminchu/
    '
    'なおこのモジュールには、NKF32.DLL の使用を簡単にするラッパー関数も
    '含めてあります。あくまでも「サンプル」として含めましたので、
    '広い範囲の用途で使用するには少々難があります。各自用途に応じた
    'ラッパー関数を自作して下さい(^^;
    '
    'このモジュールの使用・転載は自由に行っていただいて結構ですが、
    'NKF および NKF32.DLL については作成者の方々の条件に従って下さい。
    '
    '☆ NKF32.DLL 宣言モジュール更新履歴
    '
    'Ver 1.04 → 1.05
    '   EncodeSubject() を Alias とする ToMime() を追加。
    '   サンプル関数 NkfConvertCode() を修正し、JIS 用バッファを 4 倍、
    '   オプション文字比較をテキストモードに変更。


    '----- 2-1 NFK32 DLLのバージョンを返す -----
    '書式    void CALLBACK GetNkfVersion（LPSTR verStr） ;
    '解説    verStr  nkf移植元バージョン nkf32.dllバージョン（1.5k 1.03.32）を返します。
    Public Declare Sub GetNkfVersion Lib "Nkf32.dll" _
        (ByVal verStr As String)

    '----- 2-2 漢字変換オプションの指定 (NKF) -----
    '書式    int CALLBACK SetNkfOption（LPCSTR optStr） ;
    '戻値    エラー時は負値、正常終了時は０を戻します。
    '解説    optStr  変換オプション文字列（-j -e -s -i? -o? -r -m -l -Z -J -E -S -X -B -x）
    '        スペースとマイナスは無視しているので、連続したオプションの指定も可能です。
    '        なお、オプションの詳細は、後述の「ＮＫＦオリジナルドキュメント」を参照して下さい。
    '        例：　"-iB -oJ -s -m"  "iBoJsm"  "iB oJ s m"　（いずれも有効）
    Public Declare Function SetNkfOption Lib "Nkf32.dll" _
        (ByVal optStr As String) As Long

    '----- 2-3 漢字コード変換 (NKF)　行末コードの変換は行いません -----
    '書式    void CALLBACK NkfConvert（LPSTR outStr, LPCSTR inStr） ;
    '解説    outStr  出力文字列
    '        inStr   入力文字列
    Public Declare Sub NkfConvert Lib "Nkf32.dll" _
        (ByRef outStr As Object, _
         ByRef inStr As Object)

    '----- 2-4 全角英数字･記号文字列を半角文字列に変換 -----
    '書式    void CALLBACK ToHankaku（LPSTR inStr） ;
    '解説    inStr   入力文字列。変換結果はinStrに格納します。
    Public Declare Sub ToHankaku Lib "Nkf32.dll" _
        (ByRef inStr As Object)

    '----- 2-5 半角カタカナと一部の記号(｢｣ｰ等)を全角カタカナに変換 -----
    '書式    void CALLBACK ToZenkakuKana（LPSTR outStr ,LPCSTR inStr） ;
    '解説    outStr  出力文字列
    '        inStr   入力文字列
    Public Declare Sub ToZenkakuKana Lib "Nkf32.dll" _
        (ByRef outStr As Object, _
         ByRef inStr As Object)

    '----- 2-6 メールのサブジェクト（タイトル）をMIME（Base64）に変換 -----
    '書式    void CALLBACK ToMime（LPSTR outStr ,LPCSTR inStr） ;
    '解説    outStr  出力文字列
    '        inStr   入力文字列
    'SHIFT-JISのメールタイトルを変換する時は、JISに変換したあと、これをMIMEに変換して下さい。
    '
    '(※ 宣言モジュール作成者注)
    'nkf32.doc においては ToMime とされていますが、NKF32.DLL には
    'ToMime のエントリは存在せず、EncodeSubject となっています。
    'この宣言モジュールでは EncodeSubject を参照していますが、
    '今後 NKF32.DLL の仕様変更によっては ToMime に変更される可能性も
    'ありますのでご注意下さい。
    Public Declare Sub ToMime Lib "Nkf32.dll" Alias "EncodeSubject" _
        (ByRef outStr As Object, _
         ByRef inStr As Object)

    Public Declare Sub EncodeSubject Lib "Nkf32.dll" _
        (ByRef outStr As Object, _
         ByRef inStr As Object)
    '
    '===== 以下、サンプル関数群 =====
    '
    '----- NkfGetVersion -----
    'NKF32.DLL のバージョンを返します。
    '
    '戻り値
    '   NKF32.DLL のバージョンを含む文字列を返します。
    '
    Public Function NkfGetVersion() As String

        Dim strTemp As String                  '結果を受け取るバッファ

        strTemp = New String(vbNullChar, 255)  'バッファのサイズを確保
        Call GetNkfVersion(strTemp)         'バージョン情報文字列を取得
        NkfGetVersion = TrimNull(strTemp)   'NULL 文字を取り除いた文字列を返す

    End Function

    '----- NkfConvertCode -----
    '文字列を、指定されたオプションの文字コードに変換します。
    '使用方法がややこしいので、使い方がよくわからない人は下の
    'NkfConvertFile の方をご使用下さい(^^;
    '
    '引数 bytSource()
    '   文字コード変換元文字列が格納されたバイト型配列を指定します。
    '
    '引数 bytResult()
    '   文字コード変換後文字列を格納するバイト型配列を指定します。
    '   宣言は必要ですが、配列サイズはこの関数内で初期化するので、
    '   サイズを前もって決めておく必要はありません。
    '
    '引数 strOption
    '   省略可能です。NKF の変換オプションを指定します。
    '   省略した場合は "j" (JIS 変換)になります。
    '
    '戻り値
    '   引数 bytResult() で指定したバイト型配列のサイズを返します。
    '   正常な変換が出来なかった場合、0 を返します。
    '
    Public Function NkfConvertCode _
        (ByRef bytSource() As Byte, _
         ByRef bytResult() As Byte, _
         Optional ByRef strOption As String = "j") As Long

        Dim lngSourceSize As Long                                  '変換元文字列のサイズ
        Dim lngResultSize As Long                                  '変換後文字列のサイズ

        On Error GoTo ExitFunction                              'エラートラップを有効にする(UBound でエラーが発生する可能性)
        lngSourceSize = UBound(bytSource) + 1                   '変換元文字列のサイズを求める
        On Error GoTo 0                                         'エラートラップを無効にする
        If bytSource(lngSourceSize - 1) <> 0 Then               '最後の文字コードが NULL ではない場合
            ReDim Preserve bytSource(lngSourceSize)             '配列サイズを 1 バイト増やし、NULL を追加
            bytSource(lngSourceSize) = &H0
            lngSourceSize = lngSourceSize + 1
        End If

        If InStr(1, strOption, "j", vbTextCompare) Then         'JIS に変換する場合
            lngResultSize = lngSourceSize * 4                   '念のため変換元サイズの 4 倍のバッファを確保
        ElseIf InStr(1, strOption, "e", vbTextCompare) Or _
           InStr(1, strOption, "s", vbTextCompare) Then         'S-JIS, EUC に変換する場合
            lngResultSize = lngSourceSize * 2                   '念のため変換元サイズの 2 倍のバッファを確保
        Else
            lngResultSize = lngSourceSize
        End If

        Call SetNkfOption(strOption)                            'NKFのオプションを設定
        ReDim bytResult(lngResultSize)                          '変換後文字列を格納するバッファを確保
        Call NkfConvert(bytResult(0), bytSource(0))             'NKF で文字列を変換(それぞれの配列先頭アドレスを渡している)
        NkfConvertCode = TrimNullBytes(bytResult)             'bytResult() の余分な NULL を削り、そのサイズを返す

ExitFunction:

    End Function


    '----- NkfConvertStringToCode -----
    'VB 標準の Unicode 文字列を、指定されたオプションの文字コードに
    '変換します。こんな関数作るまでもないのですが・・・(^^;
    '
    '引数 strUnicode
    '   任意の文字コードに変換したい Unicode 文字列を指定します。
    '   VB の文字列型(String)は Unicode です。そのまま渡して下さい。
    '
    '引数 bytResult
    '   文字コード変換後文字列を格納するバイト型配列を指定します。
    '   宣言は必要ですが、配列サイズは関数内で初期化するので、
    '   サイズを前もって決めておく必要はありません。
    '
    '引数 strOption
    '   省略可能です。NKF の変換オプションを指定します。
    '   省略した場合は "j" (JIS 変換)になります。
    '
    '戻り値
    '   引数 bytResult() で指定したバイト型配列のサイズを返します。
    '   正常な変換が出来なかった場合、0 を返します。
    '
    Public Function NkfConvertStringToCode _
        (ByRef strUnicode As String, _
         ByRef bytResult() As Byte, _
         Optional ByVal strOption As String = "") As Long

        Dim bytSource(33) As Byte                                                            'S-JIS に変換した文字コードを格納する配列

        If UnicodeToSJIS(strUnicode, bytSource) = 0 Then Exit Function 'VB の Unicode 文字列を S-JIS に変換し、bytSource に格納する
        NkfConvertStringToCode = NkfConvertCode(bytSource, bytResult, strOption)    '任意の文字コードに変換し、サイズを返値とする。
        Erase bytSource                                                                 '配列を消去

    End Function

    '----- UnicodeToSJIS -----
    'VB 標準の Unicode 文字列を Shift-JIS に変換し、バイト型配列に格納します。
    '
    '引数 strUnicode
    '   Shift-JIS に変換したい Unicode 文字列を指定します。
    '
    '引数 bytSjis()
    '   Shift-JIS に変換した結果を格納するバイト型配列を指定します。
    '
    '戻り値
    '   bytSjis() に格納したサイズ(最大要素数 + 1)を返します。
    '   失敗した場合は 0 を返します。
    '
    Public Function UnicodeToSJIS(ByRef strUnicode As String, ByRef bytSjis() As Byte) As Long

        Dim lngSjisSize As Long                                    'S-JIS変換後のサイズ
        Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding(932)

        lngSjisSize = System.Text.Encoding.GetEncoding(932).GetByteCount(strUnicode)
        'いったん文字列を S-JIS に変換し、そのサイズを求める
        If lngSjisSize Then                                     'サイズの取得に成功した場合
            ReDim bytSjis(lngSjisSize - 1)                      'S-JIS 文字列を格納する領域を確保
            bytSjis = enc.GetBytes(strUnicode)                'バイト型配列 bytSjis() に S-JIS 変換文字列を格納する
            UnicodeToSJIS = lngSjisSize                         '変換後サイズを返す
        End If

    End Function

    '----- TrimNull -----
    '文字列より NULL 文字を検索し、最初に見つかった NULL より前までの
    '文字列を返します。
    '
    '引数 strTemp
    '   NULL 文字を含む文字列式を指定します。
    '
    '戻り値
    '   引数 strTemp に指定した文字列式から NULL 文字を取り除いた
    '   文字列が返されます。引数 strTemp に NULL が見つからなかった場合、
    '   引数として渡した文字列をそのまま返します。
    '
    Public Function TrimNull(ByRef strTemp As String) As String

        Dim lngRet As Long

        lngRet = InStr(strTemp, vbNullChar)
        If lngRet Then
            TrimNull = Left$(strTemp, lngRet - 1)
        Else
            TrimNull = strTemp
        End If

    End Function

    '----- TrimNullBytes -----
    'バイト型配列から NULL を検索し、NULL 以降を削ります。
    '
    '引数 bytArray()
    '   NULL を検索するバイト型配列を指定。結果もここに返されます。
    '
    '戻り値
    '   NULL を取り除いたByte型配列のサイズを返します。
    '   引数 bytArray() の最初の要素が NULL である場合は 0 を返します。
    '
    Public Function TrimNullBytes(ByRef bytArray() As Byte) As Long

        Dim lngSize As Long                            '配列 bytArray() の初期配列要素最大値
        Dim i As Long                                  'ループカウンタ

        lngSize = UBound(bytArray) + 1              'bytArray()のサイズを取得

        For i = 0 To (lngSize - 1) Step 1           '配列の最後の要素までループ
            If bytArray(i) = &H0 Then               'NULL が見つかった場合
                If i Then                           'カウンタ i が 0 以外の場合
                    ReDim Preserve bytArray(i - 1)  '配列サイズを NULL を発見した位置 -1 に削る
                    TrimNullBytes = i               '配列要素最大値を返す
                    Exit Function
                Else                                'カウンタ i が 0 の場合(いきなり NULL 発見)
                    Exit Function
                End If
            End If
        Next i

        TrimNullBytes = lngSize                     'NULL が見つからなかった場合、配列要素最大値のみ返す

    End Function
End Module
