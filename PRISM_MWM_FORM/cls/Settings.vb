﻿Imports System
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Windows.Forms
Imports Microsoft.Win32

<Serializable()> _
Public Class Settings
    '///一覧列幅/列Visible
#Region "定義"
    '///進捗
    Private _COL_STATE_WIDTH As Decimal
    Private _COL_STATE_VISIBLE As Boolean
    Private _COL_STATE_DISP_INDX As Integer
    Private _COL_STATE_HEADER As String

    '///検査区分
    Private _COL_STUDY_DIV_WIDTH As Decimal
    Private _COL_STUDY_DIV_VISIBLE As Boolean
    Private _COL_STUDY_DIV_DISP_INDX As Integer
    Private _COL_STUDY_DIV_HEADER As String

    '///検査名
    Private _COL_STUDY_NAME_WIDTH As Decimal
    Private _COL_STUDY_NAME_VISIBLE As Boolean
    Private _COL_STUDY_NAME_DISP_INDX As Integer
    Private _COL_STUDY_NAME_HEADER As String

    '///検査予定日
    Private _COL_ORDER_DATE_WIDTH As Decimal
    Private _COL_ORDER_DATE_VISIBLE As Boolean
    Private _COL_ORDER_DATE_DISP_INDX As Integer
    Private _COL_ORDER_DATE_HEADER As String

    '///検査予定時刻
    Private _COL_ORDER_TIME_WIDTH As Decimal
    Private _COL_ORDER_TIME_VISIBLE As Boolean
    Private _COL_ORDER_TIME_DISP_INDX As Integer
    Private _COL_ORDER_TIME_HEADER As String

    '///検査実施日
    Private _COL_STUDY_DATE_WIDTH As Decimal
    Private _COL_STUDY_DATE_VISIBLE As Boolean
    Private _COL_STUDY_DATE_DISP_INDX As Integer
    Private _COL_STUDY_DATE_HEADER As String

    '///検査実施時刻
    Private _COL_STUDY_TIME_WIDTH As Decimal
    Private _COL_STUDY_TIME_VISIBLE As Boolean
    Private _COL_STUDY_TIME_DISP_INDX As Integer
    Private _COL_STUDY_TIME_HEADER As String

    '///患者ID
    Private _COL_PATIENT_ID_WIDTH As Decimal
    Private _COL_PATIENT_ID_VISIBLE As Boolean
    Private _COL_PATIENT_ID_DISP_INDX As Integer
    Private _COL_PATIENT_ID_HEADER As String

    '///患者漢字氏名
    Private _COL_PATIENT_KANJI_WIDTH As Decimal
    Private _COL_PATIENT_KANJI_VISIBLE As Boolean
    Private _COL_PATIENT_KANJI_DISP_INDX As Integer
    Private _COL_PATIENT_KANJI_HEADER As String

    '///患者カナ氏名
    Private _COL_PATIENT_KANA_WIDTH As Decimal
    Private _COL_PATIENT_KANA_VISIBLE As Boolean
    Private _COL_PATIENT_KANA_DISP_INDX As Integer
    Private _COL_PATIENT_KANA_HEADER As String

    '///患者英字氏名
    Private _COL_PATIENT_EIJI_WIDTH As Decimal
    Private _COL_PATIENT_EIJI_VISIBLE As Boolean
    Private _COL_PATIENT_EIJI_DISP_INDX As Integer
    Private _COL_PATIENT_EIJI_HEADER As String

    '///生年月日
    Private _COL_PATIENT_BIRTH_WIDTH As Decimal
    Private _COL_PATIENT_BIRTH_VISIBLE As Boolean
    Private _COL_PATIENT_BIRTH_DISP_INDX As Integer
    Private _COL_PATIENT_BIRTH_HEADER As String

    '///性別
    Private _COL_PATIENT_SEX_WIDTH As Decimal
    Private _COL_PATIENT_SEX_VISIBLE As Boolean
    Private _COL_PATIENT_SEX_DISP_INDX As Integer
    Private _COL_PATIENT_SEX_HEADER As String

    '///撮影区分
    'Private _COL_SATSUEI_DIV_WIDTH As Decimal
    'Private _COL_SATSUEI_DIV_VISIBLE As Boolean
    'Private _COL_SATSUEI_DIV_DISP_INDX As Integer
    'Private _COL_SATSUEI_DIV_HEADER As String

    '///撮影部位
    Private _COL_BODY_PART_WIDTH As Decimal
    Private _COL_BODY_PART_VISIBLE As Boolean
    Private _COL_BODY_PART_DISP_INDX As Integer
    Private _COL_BODY_PART_HEADER As String

    '///モダリティ
    Private _COL_MODALITY_WIDTH As Decimal
    Private _COL_MODALITY_VISIBLE As Boolean
    Private _COL_MODALITY_DISP_INDX As Integer
    Private _COL_MODALITY_HEADER As String

    '///依頼科
    Private _COL_DEPT_WIDTH As Decimal
    Private _COL_DEPT_VISIBLE As Boolean
    Private _COL_DEPT_DISP_INDX As Integer
    Private _COL_DEPT_HEADER As String

    '///入外
    Private _COL_NYUGAI_WIDTH As Decimal
    Private _COL_NYUGAI_VISIBLE As Boolean
    Private _COL_NYUGAI_DISP_INDX As Integer
    Private _COL_NYUGAI_HEADER As String

    '///病棟
    Private _COL_WARD_WIDTH As Decimal
    Private _COL_WARD_VISIBLE As Boolean
    Private _COL_WARD_DISP_INDX As Integer
    Private _COL_WARD_HEADER As String

    '///病室
    Private _COL_ROOM_WIDTH As Decimal
    Private _COL_ROOM_VISIBLE As Boolean
    Private _COL_ROOM_DISP_INDX As Integer
    Private _COL_ROOM_HEADER As String

    '///依頼医
    Private _COL_REQ_DOC_WIDTH As Decimal
    Private _COL_REQ_DOC_VISIBLE As Boolean
    Private _COL_REQ_DOC_DISP_INDX As Integer
    Private _COL_REQ_DOC_HEADER As String

    '///削除フラグ
    Private _COL_DEL_FLG_WIDTH As Decimal
    Private _COL_DEL_FLG_VISIBLE As Boolean
    Private _COL_DEL_FLG_DISP_INDX As Integer
    Private _COL_DEL_FLG_HEADER As String

    '///StudyInstanceUID
    Private _COL_STUDY_INSTANCE_UID_WIDTH As Decimal
    Private _COL_STUDY_INSTANCE_UID_VISIBLE As Boolean
    Private _COL_STUDY_INSTANCE_UID_DISP_INDX As Integer
    Private _COL_STUDY_INSTANCE_UID_HEADER As String

    '///オーダ登録番号
    Private _COL_ORDER_ANO_WIDTH As Decimal
    Private _COL_ORDER_ANO_VISIBLE As Boolean
    Private _COL_ORDER_ANO_DISP_INDX As Integer
    Private _COL_ORDER_ANO_HEADER As String

    '///オーダ番号
    Private _COL_ORDER_NO_WIDTH As Decimal
    Private _COL_ORDER_NO_VISIBLE As Boolean
    Private _COL_ORDER_NO_DISP_INDX As Integer
    Private _COL_ORDER_NO_HEADER As String

    '///患者登録番号
    Private _COL_PATIENT_ANO_WIDTH As Decimal
    Private _COL_PATIENT_ANO_VISIBLE As Boolean
    Private _COL_PATIENT_ANO_DISP_INDX As Integer
    Private _COL_PATIENT_ANO_HEADER As String

    '///オーダコメント
    Private _COL_ORDER_COMMENT_WIDTH As Decimal
    Private _COL_ORDER_COMMENT_VISIBLE As Boolean
    Private _COL_ORDER_COMMENT_DISP_INDX As Integer
    Private _COL_ORDER_COMMENT_HEADER As String

    '///患者コメント
    Private _COL_PATIENT_COMMENT_WIDTH As Decimal
    Private _COL_PATIENT_COMMENT_VISIBLE As Boolean
    Private _COL_PATIENT_COMMENT_DISP_INDX As Integer
    Private _COL_PATIENT_COMMENT_HEADER As String

    '///実施コメント
    Private _COL_STUDY_COMMENT_WIDTH As Decimal
    Private _COL_STUDY_COMMENT_VISIBLE As Boolean
    Private _COL_STUDY_COMMENT_DISP_INDX As Integer
    Private _COL_STUDY_COMMENT_HEADER As String

    '///コメントフラグ
    Private _COL_COMMENT_FLG_WIDTH As Decimal
    Private _COL_COMMENT_FLG_VISIBLE As Boolean
    Private _COL_COMMENT_FLG_DISP_INDX As Integer
    Private _COL_COMMENT_FLG_HEADER As String

    '///オーダ登録日
    Private _COL_INS_DATE_WIDTH As Decimal
    Private _COL_INS_DATE_VISIBLE As Boolean
    Private _COL_INS_DATE_DISP_INDX As Integer
    Private _COL_INS_DATE_HEADER As String

    '///オーダ登録時刻
    Private _COL_INS_TIME_WIDTH As Decimal
    Private _COL_INS_TIME_VISIBLE As Boolean
    Private _COL_INS_TIME_DISP_INDX As Integer
    Private _COL_INS_TIME_HEADER As String

    '///オーダ更新日
    Private _COL_UPD_DATE_WIDTH As Decimal
    Private _COL_UPD_DATE_VISIBLE As Boolean
    Private _COL_UPD_DATE_DISP_INDX As Integer
    Private _COL_UPD_DATE_HEADER As String

    '///オーダ更新時刻
    Private _COL_UPD_TIME_WIDTH As Decimal
    Private _COL_UPD_TIME_VISIBLE As Boolean
    Private _COL_UPD_TIME_DISP_INDX As Integer
    Private _COL_UPD_TIME_HEADER As String

    '///年齢  ADD By Watanabe 2012.02.03
    Private _COL_AGE_WIDTH As Decimal
    Private _COL_AGE_VISIBLE As Boolean
    Private _COL_AGE_DISP_INDX As Integer
    Private _COL_AGE_HEADER As String

#End Region

#Region "設定のプロパティ"
    '設定のプロパティ

#Region "進捗(COL_STATE_WIDTH)"
    '///進捗
    Public Property COL_STATE_WIDTH() As Decimal
        Get
            Return _COL_STATE_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_STATE_WIDTH = Value
        End Set
    End Property
    Public Property COL_STATE_VISIBLE() As Boolean
        Get
            Return _COL_STATE_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_STATE_VISIBLE = Value
        End Set
    End Property
    Public Property COL_STATE_DISP_INDX() As Integer
        Get
            Return _COL_STATE_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_STATE_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_STATE_HEADER() As String
        Get
            Return _COL_STATE_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_STATE_HEADER = Value
        End Set
    End Property
#End Region

#Region "検査区分(COL_STUDY_DIV_WIDTH)"
    '///進捗
    Public Property COL_STUDY_DIV_WIDTH() As Decimal
        Get
            Return _COL_STUDY_DIV_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_STUDY_DIV_WIDTH = Value
        End Set
    End Property
    Public Property COL_STUDY_DIV_VISIBLE() As Boolean
        Get
            Return _COL_STUDY_DIV_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_STUDY_DIV_VISIBLE = Value
        End Set
    End Property
    Public Property COL_STUDY_DIV_DISP_INDX() As Integer
        Get
            Return _COL_STUDY_DIV_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_STUDY_DIV_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_STUDY_DIV_HEADER() As String
        Get
            Return _COL_STUDY_DIV_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_STUDY_DIV_HEADER = Value
        End Set
    End Property
#End Region

#Region "検査名(COL_STUDY_NAME_WIDTH)"
    '///撮影名
    Public Property COL_STUDY_NAME_WIDTH() As Decimal
        Get
            Return _COL_STUDY_NAME_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_STUDY_NAME_WIDTH = Value
        End Set
    End Property
    Public Property COL_STUDY_NAME_VISIBLE() As Boolean
        Get
            Return _COL_STUDY_NAME_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_STUDY_NAME_VISIBLE = Value
        End Set
    End Property
    Public Property COL_STUDY_NAME_DISP_INDX() As Integer
        Get
            Return _COL_STUDY_NAME_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_STUDY_NAME_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_STUDY_NAME_HEADER() As String
        Get
            Return _COL_STUDY_NAME_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_STUDY_NAME_HEADER = Value
        End Set
    End Property
#End Region

#Region "検査予定日(COL_ORDER_DATE_WIDTH)"
    '///検査予定日
    Public Property COL_ORDER_DATE_WIDTH() As Decimal
        Get
            Return _COL_ORDER_DATE_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_ORDER_DATE_WIDTH = Value
        End Set
    End Property
    Public Property COL_ORDER_DATE_VISIBLE() As Boolean
        Get
            Return _COL_ORDER_DATE_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_ORDER_DATE_VISIBLE = Value
        End Set
    End Property
    Public Property COL_ORDER_DATE_DISP_INDX() As Integer
        Get
            Return _COL_ORDER_DATE_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_ORDER_DATE_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_ORDER_DATE_HEADER() As String
        Get
            Return _COL_ORDER_DATE_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_ORDER_DATE_HEADER = Value
        End Set
    End Property
#End Region

#Region "検査予定時刻(COL_ORDER_TIME_WIDTH)"
    '///検査予定時刻
    Public Property COL_ORDER_TIME_WIDTH() As Decimal
        Get
            Return _COL_ORDER_TIME_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_ORDER_TIME_WIDTH = Value
        End Set
    End Property
    Public Property COL_ORDER_TIME_VISIBLE() As Boolean
        Get
            Return _COL_ORDER_TIME_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_ORDER_TIME_VISIBLE = Value
        End Set
    End Property
    Public Property COL_ORDER_TIME_DISP_INDX() As Integer
        Get
            Return _COL_ORDER_TIME_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_ORDER_TIME_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_ORDER_TIME_HEADER() As String
        Get
            Return _COL_ORDER_TIME_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_ORDER_TIME_HEADER = Value
        End Set
    End Property
#End Region

#Region "検査実施日(COL_STUDY_DATE_WIDTH)"
    '///検査実施日
    Public Property COL_STUDY_DATE_WIDTH() As Decimal
        Get
            Return _COL_STUDY_DATE_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_STUDY_DATE_WIDTH = Value
        End Set
    End Property
    Public Property COL_STUDY_DATE_VISIBLE() As Boolean
        Get
            Return _COL_STUDY_DATE_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_STUDY_DATE_VISIBLE = Value
        End Set
    End Property
    Public Property COL_STUDY_DATE_DISP_INDX() As Integer
        Get
            Return _COL_STUDY_DATE_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_STUDY_DATE_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_STUDY_DATE_HEADER() As String
        Get
            Return _COL_STUDY_DATE_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_STUDY_DATE_HEADER = Value
        End Set
    End Property
#End Region

#Region "検査実施時刻(COL_STUDY_TIME_WIDTH)"
    '///検査実施時刻
    Public Property COL_STUDY_TIME_WIDTH() As Decimal
        Get
            Return _COL_STUDY_TIME_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_STUDY_TIME_WIDTH = Value
        End Set
    End Property
    Public Property COL_STUDY_TIME_VISIBLE() As Boolean
        Get
            Return _COL_STUDY_TIME_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_STUDY_TIME_VISIBLE = Value
        End Set
    End Property
    Public Property COL_STUDY_TIME_DISP_INDX() As Integer
        Get
            Return _COL_STUDY_TIME_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_STUDY_TIME_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_STUDY_TIME_HEADER() As String
        Get
            Return _COL_STUDY_TIME_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_STUDY_TIME_HEADER = Value
        End Set
    End Property
#End Region

#Region "患者ID(COL_PATIENT_ID_WIDTH)"
    '///患者ID
    Public Property COL_PATIENT_ID_WIDTH() As Decimal
        Get
            Return _COL_PATIENT_ID_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_PATIENT_ID_WIDTH = Value
        End Set
    End Property
    Public Property COL_PATIENT_ID_VISIBLE() As Boolean
        Get
            Return _COL_PATIENT_ID_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_PATIENT_ID_VISIBLE = Value
        End Set
    End Property
    Public Property COL_PATIENT_ID_DISP_INDX() As Integer
        Get
            Return _COL_PATIENT_ID_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_PATIENT_ID_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_PATIENT_ID_HEADER() As String
        Get
            Return _COL_PATIENT_ID_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_PATIENT_ID_HEADER = Value
        End Set
    End Property
#End Region

#Region "患者漢字氏名(COL_PATIENT_KANJI_WIDTH)"
    '///患者漢字氏名
    Public Property COL_PATIENT_KANJI_WIDTH() As Decimal
        Get
            Return _COL_PATIENT_KANJI_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_PATIENT_KANJI_WIDTH = Value
        End Set
    End Property
    Public Property COL_PATIENT_KANJI_VISIBLE() As Boolean
        Get
            Return _COL_PATIENT_KANJI_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_PATIENT_KANJI_VISIBLE = Value
        End Set
    End Property
    Public Property COL_PATIENT_KANJI_DISP_INDX() As Integer
        Get
            Return _COL_PATIENT_KANJI_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_PATIENT_KANJI_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_PATIENT_KANJI_HEADER() As String
        Get
            Return _COL_PATIENT_KANJI_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_PATIENT_KANJI_HEADER = Value
        End Set
    End Property
#End Region

#Region "患者カナ氏名(COL_PATIENT_KANA_WIDTH)"
    '///患者カナ氏名
    Public Property COL_PATIENT_KANA_WIDTH() As Decimal
        Get
            Return _COL_PATIENT_KANA_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_PATIENT_KANA_WIDTH = Value
        End Set
    End Property
    Public Property COL_PATIENT_KANA_VISIBLE() As Boolean
        Get
            Return _COL_PATIENT_KANA_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_PATIENT_KANA_VISIBLE = Value
        End Set
    End Property
    Public Property COL_PATIENT_KANA_DISP_INDX() As Integer
        Get
            Return _COL_PATIENT_KANA_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_PATIENT_KANA_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_PATIENT_KANA_HEADER() As String
        Get
            Return _COL_PATIENT_KANA_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_PATIENT_KANA_HEADER = Value
        End Set
    End Property
#End Region

#Region "患者英字氏名(COL_PATIENT_EIJI_WIDTH)"
    '///患者英字氏名
    Public Property COL_PATIENT_EIJI_WIDTH() As Decimal
        Get
            Return _COL_PATIENT_EIJI_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_PATIENT_EIJI_WIDTH = Value
        End Set
    End Property
    Public Property COL_PATIENT_EIJI_VISIBLE() As Boolean
        Get
            Return _COL_PATIENT_EIJI_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_PATIENT_EIJI_VISIBLE = Value
        End Set
    End Property
    Public Property COL_PATIENT_EIJI_DISP_INDX() As Integer
        Get
            Return _COL_PATIENT_EIJI_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_PATIENT_EIJI_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_PATIENT_EIJI_HEADER() As String
        Get
            Return _COL_PATIENT_EIJI_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_PATIENT_EIJI_HEADER = Value
        End Set
    End Property
#End Region

#Region "患者生年月日(COL_PATIENT_BIRTH_WIDTH)"
    '///生年月日
    Public Property COL_PATIENT_BIRTH_WIDTH() As Decimal
        Get
            Return _COL_PATIENT_BIRTH_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_PATIENT_BIRTH_WIDTH = Value
        End Set
    End Property
    Public Property COL_PATIENT_BIRTH_VISIBLE() As Boolean
        Get
            Return _COL_PATIENT_BIRTH_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_PATIENT_BIRTH_VISIBLE = Value
        End Set
    End Property
    Public Property COL_PATIENT_BIRTH_DISP_INDX() As Integer
        Get
            Return _COL_PATIENT_BIRTH_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_PATIENT_BIRTH_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_PATIENT_BIRTH_HEADER() As String
        Get
            Return _COL_PATIENT_BIRTH_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_PATIENT_BIRTH_HEADER = Value
        End Set
    End Property
#End Region

#Region "患者性別(COL_PATIENT_SEX_WIDTH)"
    '///性別
    Public Property COL_PATIENT_SEX_WIDTH() As Decimal
        Get
            Return _COL_PATIENT_SEX_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_PATIENT_SEX_WIDTH = Value
        End Set
    End Property
    Public Property COL_PATIENT_SEX_VISIBLE() As Boolean
        Get
            Return _COL_PATIENT_SEX_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_PATIENT_SEX_VISIBLE = Value
        End Set
    End Property
    Public Property COL_PATIENT_SEX_DISP_INDX() As Integer
        Get
            Return _COL_PATIENT_SEX_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_PATIENT_SEX_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_PATIENT_SEX_HEADER() As String
        Get
            Return _COL_PATIENT_SEX_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_PATIENT_SEX_HEADER = Value
        End Set
    End Property
#End Region

    '#Region "検査区分(COL_SATSUEI_DIV_WIDTH)"
    '    '///撮影部位
    '    Public Property COL_SATSUEI_DIV_WIDTH() As Decimal
    '        Get
    '            Return _COL_SATSUEI_DIV_WIDTH
    '        End Get
    '        Set(ByVal Value As Decimal)
    '            _COL_SATSUEI_DIV_WIDTH = Value
    '        End Set
    '    End Property
    '    Public Property COL_SATSUEI_DIV_VISIBLE() As Boolean
    '        Get
    '            Return _COL_SATSUEI_DIV_VISIBLE
    '        End Get
    '        Set(ByVal Value As Boolean)
    '            _COL_SATSUEI_DIV_VISIBLE = Value
    '        End Set
    '    End Property
    '    Public Property COL_SATSUEI_DIV_DISP_INDX() As Integer
    '        Get
    '            Return _COL_SATSUEI_DIV_DISP_INDX
    '        End Get
    '        Set(ByVal Value As Integer)
    '            _COL_SATSUEI_DIV_DISP_INDX = Value
    '        End Set
    '    End Property
    '    Public Property COL_SATSUEI_DIV_HEADER() As String
    '        Get
    '            Return _COL_SATSUEI_DIV_HEADER
    '        End Get
    '        Set(ByVal Value As String)
    '            _COL_SATSUEI_DIV_HEADER = Value
    '        End Set
    '    End Property
    '#End Region

#Region "検査部位(COL_BODY_PART_WIDTH)"
    '///撮影部位
    Public Property COL_BODY_PART_WIDTH() As Decimal
        Get
            Return _COL_BODY_PART_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_BODY_PART_WIDTH = Value
        End Set
    End Property
    Public Property COL_BODY_PART_VISIBLE() As Boolean
        Get
            Return _COL_BODY_PART_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_BODY_PART_VISIBLE = Value
        End Set
    End Property
    Public Property COL_BODY_PART_DISP_INDX() As Integer
        Get
            Return _COL_BODY_PART_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_BODY_PART_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_BODY_PART_HEADER() As String
        Get
            Return _COL_BODY_PART_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_BODY_PART_HEADER = Value
        End Set
    End Property
#End Region

#Region "モダリティ(_COL_MODALITY_WIDTH)"
    '///モダリティ
    Public Property COL_MODALITY_WIDTH() As Decimal
        Get
            Return _COL_MODALITY_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_MODALITY_WIDTH = Value
        End Set
    End Property
    Public Property COL_MODALITY_VISIBLE() As Boolean
        Get
            Return _COL_MODALITY_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_MODALITY_VISIBLE = Value
        End Set
    End Property
    Public Property COL_MODALITY_DISP_INDX() As Integer
        Get
            Return _COL_MODALITY_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_MODALITY_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_MODALITY_HEADER() As String
        Get
            Return _COL_MODALITY_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_MODALITY_HEADER = Value
        End Set
    End Property
#End Region

#Region "依頼科(COL_DEPT_WIDTH)"
    '///生年月日
    Public Property COL_DEPT_WIDTH() As Decimal
        Get
            Return _COL_DEPT_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_DEPT_WIDTH = Value
        End Set
    End Property
    Public Property COL_DEPT_VISIBLE() As Boolean
        Get
            Return _COL_DEPT_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_DEPT_VISIBLE = Value
        End Set
    End Property
    Public Property COL_DEPT_DISP_INDX() As Integer
        Get
            Return _COL_DEPT_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_DEPT_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_DEPT_HEADER() As String
        Get
            Return _COL_DEPT_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_DEPT_HEADER = Value
        End Set
    End Property
#End Region

#Region "入外(COL_NYUGAI_WIDTH)"
    '///生年月日
    Public Property COL_NYUGAI_WIDTH() As Decimal
        Get
            Return _COL_NYUGAI_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_NYUGAI_WIDTH = Value
        End Set
    End Property
    Public Property COL_NYUGAI_VISIBLE() As Boolean
        Get
            Return _COL_NYUGAI_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_NYUGAI_VISIBLE = Value
        End Set
    End Property
    Public Property COL_NYUGAI_DISP_INDX() As Integer
        Get
            Return _COL_NYUGAI_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_NYUGAI_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_NYUGAI_HEADER() As String
        Get
            Return _COL_NYUGAI_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_NYUGAI_HEADER = Value
        End Set
    End Property
#End Region

#Region "病棟(COL_WARD_WIDTH)"
    '///生年月日
    Public Property COL_WARD_WIDTH() As Decimal
        Get
            Return _COL_WARD_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_WARD_WIDTH = Value
        End Set
    End Property
    Public Property COL_WARD_VISIBLE() As Boolean
        Get
            Return _COL_WARD_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_WARD_VISIBLE = Value
        End Set
    End Property
    Public Property COL_WARD_DISP_INDX() As Integer
        Get
            Return _COL_WARD_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_WARD_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_WARD_HEADER() As String
        Get
            Return _COL_WARD_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_WARD_HEADER = Value
        End Set
    End Property
#End Region

#Region "病室(COL_ROOM_WIDTH)"
    '///生年月日
    Public Property COL_ROOM_WIDTH() As Decimal
        Get
            Return _COL_ROOM_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_ROOM_WIDTH = Value
        End Set
    End Property
    Public Property COL_ROOM_VISIBLE() As Boolean
        Get
            Return _COL_ROOM_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_ROOM_VISIBLE = Value
        End Set
    End Property
    Public Property COL_ROOM_DISP_INDX() As Integer
        Get
            Return _COL_ROOM_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_ROOM_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_ROOM_HEADER() As String
        Get
            Return _COL_ROOM_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_ROOM_HEADER = Value
        End Set
    End Property
#End Region

#Region "依頼医(COL_REQ_DOC_WIDTH)"
    '///生年月日
    Public Property COL_REQ_DOC_WIDTH() As Decimal
        Get
            Return _COL_REQ_DOC_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_REQ_DOC_WIDTH = Value
        End Set
    End Property
    Public Property COL_REQ_DOC_VISIBLE() As Boolean
        Get
            Return _COL_REQ_DOC_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_REQ_DOC_VISIBLE = Value
        End Set
    End Property
    Public Property COL_REQ_DOC_DISP_INDX() As Integer
        Get
            Return _COL_REQ_DOC_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_REQ_DOC_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_REQ_DOC_HEADER() As String
        Get
            Return _COL_REQ_DOC_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_REQ_DOC_HEADER = Value
        End Set
    End Property
#End Region

#Region "削除フラグ(_COL_DEL_FLG_WIDTH)"
    '///削除フラグ
    Public Property COL_DEL_FLG_WIDTH() As Decimal
        Get
            Return _COL_DEL_FLG_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_DEL_FLG_WIDTH = Value
        End Set
    End Property
    Public Property COL_DEL_FLG_VISIBLE() As Boolean
        Get
            Return _COL_DEL_FLG_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_DEL_FLG_VISIBLE = Value
        End Set
    End Property
    Public Property COL_DEL_FLG_DISP_INDX() As Integer
        Get
            Return _COL_DEL_FLG_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_DEL_FLG_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_DEL_FLG_HEADER() As String
        Get
            Return _COL_DEL_FLG_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_DEL_FLG_HEADER = Value
        End Set
    End Property
#End Region

#Region "StudyInstanceUID(_COL_STUDY_INSTANCE_UID_WIDTH)"
    '///StudyInstanceUID
    Public Property COL_STUDY_INSTANCE_UID_WIDTH() As Decimal
        Get
            Return _COL_STUDY_INSTANCE_UID_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_STUDY_INSTANCE_UID_WIDTH = Value
        End Set
    End Property
    Public Property COL_STUDY_INSTANCE_UID_VISIBLE() As Boolean
        Get
            Return _COL_STUDY_INSTANCE_UID_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_STUDY_INSTANCE_UID_VISIBLE = Value
        End Set
    End Property
    Public Property COL_STUDY_INSTANCE_UID_DISP_INDX() As Integer
        Get
            Return _COL_STUDY_INSTANCE_UID_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_STUDY_INSTANCE_UID_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_STUDY_INSTANCE_UID_HEADER() As String
        Get
            Return _COL_STUDY_INSTANCE_UID_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_STUDY_INSTANCE_UID_HEADER = Value
        End Set
    End Property
#End Region

#Region "オーダ登録番号(_COL_ORDER_ANO_WIDTH)"
    '///オーダ登録番号
    Public Property COL_ORDER_ANO_WIDTH() As Decimal
        Get
            Return _COL_ORDER_ANO_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_ORDER_ANO_WIDTH = Value
        End Set
    End Property
    Public Property COL_ORDER_ANO_VISIBLE() As Boolean
        Get
            Return _COL_ORDER_ANO_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_ORDER_ANO_VISIBLE = Value
        End Set
    End Property
    Public Property COL_ORDER_ANO_DISP_INDX() As Integer
        Get
            Return _COL_ORDER_ANO_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_ORDER_ANO_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_ORDER_ANO_HEADER() As String
        Get
            Return _COL_ORDER_ANO_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_ORDER_ANO_HEADER = Value
        End Set
    End Property
#End Region

#Region "オーダ番号(_COL_ORDER_NO_WIDTH)"
    '///オーダ登録番号
    Public Property COL_ORDER_NO_WIDTH() As Decimal
        Get
            Return _COL_ORDER_NO_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_ORDER_NO_WIDTH = Value
        End Set
    End Property
    Public Property COL_ORDER_NO_VISIBLE() As Boolean
        Get
            Return _COL_ORDER_NO_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_ORDER_NO_VISIBLE = Value
        End Set
    End Property
    Public Property COL_ORDER_NO_DISP_INDX() As Integer
        Get
            Return _COL_ORDER_NO_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_ORDER_NO_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_ORDER_NO_HEADER() As String
        Get
            Return _COL_ORDER_NO_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_ORDER_NO_HEADER = Value
        End Set
    End Property
#End Region

#Region "患者登録番号(_COL_PATIENT_ANO_WIDTH)"
    '///患者登録番号
    Public Property COL_PATIENT_ANO_WIDTH() As Decimal
        Get
            Return _COL_PATIENT_ANO_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_PATIENT_ANO_WIDTH = Value
        End Set
    End Property
    Public Property COL_PATIENT_ANO_VISIBLE() As Boolean
        Get
            Return _COL_PATIENT_ANO_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_PATIENT_ANO_VISIBLE = Value
        End Set
    End Property
    Public Property COL_PATIENT_ANO_DISP_INDX() As Integer
        Get
            Return _COL_PATIENT_ANO_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_PATIENT_ANO_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_PATIENT_ANO_HEADER() As String
        Get
            Return _COL_PATIENT_ANO_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_PATIENT_ANO_HEADER = Value
        End Set
    End Property
#End Region

#Region "オーダコメント(COL_ORDER_COMMENT_WIDTH)"
    '///オーダコメント
    Public Property COL_ORDER_COMMENT_WIDTH() As Decimal
        Get
            Return _COL_ORDER_COMMENT_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_ORDER_COMMENT_WIDTH = Value
        End Set
    End Property
    Public Property COL_ORDER_COMMENT_VISIBLE() As Boolean
        Get
            Return _COL_ORDER_COMMENT_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_ORDER_COMMENT_VISIBLE = Value
        End Set
    End Property
    Public Property COL_ORDER_COMMENT_DISP_INDX() As Integer
        Get
            Return _COL_ORDER_COMMENT_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_ORDER_COMMENT_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_ORDER_COMMENT_HEADER() As String
        Get
            Return _COL_ORDER_COMMENT_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_ORDER_COMMENT_HEADER = Value
        End Set
    End Property
#End Region

#Region "患者コメント(COL_PATIENT_COMMENT_WIDTH)"
    '///患者コメント
    Public Property COL_PATIENT_COMMENT_WIDTH() As Decimal
        Get
            Return _COL_PATIENT_COMMENT_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_PATIENT_COMMENT_WIDTH = Value
        End Set
    End Property
    Public Property COL_PATIENT_COMMENT_VISIBLE() As Boolean
        Get
            Return _COL_PATIENT_COMMENT_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_PATIENT_COMMENT_VISIBLE = Value
        End Set
    End Property
    Public Property COL_PATIENT_COMMENT_DISP_INDX() As Integer
        Get
            Return _COL_PATIENT_COMMENT_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_PATIENT_COMMENT_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_PATIENT_COMMENT_HEADER() As String
        Get
            Return _COL_PATIENT_COMMENT_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_PATIENT_COMMENT_HEADER = Value
        End Set
    End Property
#End Region

#Region "実施コメント(COL_STUDY_COMMENT_WIDTH)"
    '///実施コメント
    Public Property COL_STUDY_COMMENT_WIDTH() As Decimal
        Get
            Return _COL_STUDY_COMMENT_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_STUDY_COMMENT_WIDTH = Value
        End Set
    End Property
    Public Property COL_STUDY_COMMENT_VISIBLE() As Boolean
        Get
            Return _COL_STUDY_COMMENT_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_STUDY_COMMENT_VISIBLE = Value
        End Set
    End Property
    Public Property COL_STUDY_COMMENT_DISP_INDX() As Integer
        Get
            Return _COL_STUDY_COMMENT_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_STUDY_COMMENT_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_STUDY_COMMENT_HEADER() As String
        Get
            Return _COL_STUDY_COMMENT_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_STUDY_COMMENT_HEADER = Value
        End Set
    End Property
#End Region

#Region "コメントフラグ(_COL_COMMENT_FLG_WIDTH)"
    '///コメントフラグ
    Public Property COL_COMMENT_FLG_WIDTH() As Decimal
        Get
            Return _COL_COMMENT_FLG_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_COMMENT_FLG_WIDTH = Value
        End Set
    End Property
    Public Property COL_COMMENT_FLG_VISIBLE() As Boolean
        Get
            Return _COL_COMMENT_FLG_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_COMMENT_FLG_VISIBLE = Value
        End Set
    End Property
    Public Property COL_COMMENT_FLG_DISP_INDX() As Integer
        Get
            Return _COL_COMMENT_FLG_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_COMMENT_FLG_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_COMMENT_FLG_HEADER() As String
        Get
            Return _COL_COMMENT_FLG_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_COMMENT_FLG_HEADER = Value
        End Set
    End Property
#End Region

#Region "オーダ登録日(_COL_INS_DATE_WIDTH)"
    '///オーダ登録日
    Public Property COL_INS_DATE_WIDTH() As Decimal
        Get
            Return _COL_INS_DATE_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_INS_DATE_WIDTH = Value
        End Set
    End Property
    Public Property COL_INS_DATE_VISIBLE() As Boolean
        Get
            Return _COL_INS_DATE_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_INS_DATE_VISIBLE = Value
        End Set
    End Property
    Public Property COL_INS_DATE_DISP_INDX() As Integer
        Get
            Return _COL_INS_DATE_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_INS_DATE_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_INS_DATE_HEADER() As String
        Get
            Return _COL_INS_DATE_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_INS_DATE_HEADER = Value
        End Set
    End Property
#End Region

#Region "オーダ登録時刻(_COL_INS_TIME_WIDTH)"
    '///オーダ登録時刻
    Public Property COL_INS_TIME_WIDTH() As Decimal
        Get
            Return _COL_INS_TIME_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_INS_TIME_WIDTH = Value
        End Set
    End Property
    Public Property COL_INS_TIME_VISIBLE() As Boolean
        Get
            Return _COL_INS_TIME_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_INS_TIME_VISIBLE = Value
        End Set
    End Property
    Public Property COL_INS_TIME_DISP_INDX() As Integer
        Get
            Return _COL_INS_TIME_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_INS_TIME_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_INS_TIME_HEADER() As String
        Get
            Return _COL_INS_TIME_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_INS_TIME_HEADER = Value
        End Set
    End Property
#End Region

#Region "オーダ更新日(_COL_UPD_DATE_WIDTH)"
    '///オーダ更新日
    Public Property COL_UPD_DATE_WIDTH() As Decimal
        Get
            Return _COL_UPD_DATE_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_UPD_DATE_WIDTH = Value
        End Set
    End Property
    Public Property COL_UPD_DATE_VISIBLE() As Boolean
        Get
            Return _COL_UPD_DATE_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_UPD_DATE_VISIBLE = Value
        End Set
    End Property
    Public Property COL_UPD_DATE_DISP_INDX() As Integer
        Get
            Return _COL_UPD_DATE_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_UPD_DATE_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_UPD_DATE_HEADER() As String
        Get
            Return _COL_UPD_DATE_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_UPD_DATE_HEADER = Value
        End Set
    End Property
#End Region

#Region "オーダ更新時刻(_COL_UPD_TIME_WIDTH)"
    '///オーダ更新時刻
    Public Property COL_UPD_TIME_WIDTH() As Decimal
        Get
            Return _COL_UPD_TIME_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_UPD_TIME_WIDTH = Value
        End Set
    End Property
    Public Property COL_UPD_TIME_VISIBLE() As Boolean
        Get
            Return _COL_UPD_TIME_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_UPD_TIME_VISIBLE = Value
        End Set
    End Property
    Public Property COL_UPD_TIME_DISP_INDX() As Integer
        Get
            Return _COL_UPD_TIME_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_UPD_TIME_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_UPD_TIME_HEADER() As String
        Get
            Return _COL_UPD_TIME_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_UPD_TIME_HEADER = Value
        End Set
    End Property
#End Region

#Region "年齢"
    '///年齢
    Public Property COL_AGE_WIDTH() As Decimal
        Get
            Return _COL_AGE_WIDTH
        End Get
        Set(ByVal Value As Decimal)
            _COL_AGE_WIDTH = Value
        End Set
    End Property
    Public Property COL_AGE_VISIBLE() As Boolean
        Get
            Return _COL_AGE_VISIBLE
        End Get
        Set(ByVal Value As Boolean)
            _COL_AGE_VISIBLE = Value
        End Set
    End Property
    Public Property COL_AGE_DISP_INDX() As Integer
        Get
            Return _COL_AGE_DISP_INDX
        End Get
        Set(ByVal Value As Integer)
            _COL_AGE_DISP_INDX = Value
        End Set
    End Property
    Public Property COL_AGE_HEADER() As String
        Get
            Return _COL_AGE_HEADER
        End Get
        Set(ByVal Value As String)
            _COL_AGE_HEADER = Value
        End Set
    End Property
#End Region

#End Region

#Region "コンストラクタ(New)"
    'コンストラクタ
    Public Sub New()
        '///画面背景色
        '       _FORM_BACK_COLOR()
        '///一覧 列幅/列Visible
        _COL_STATE_WIDTH = 0.0
        _COL_STATE_VISIBLE = False
        _COL_STATE_DISP_INDX = 0.0
        _COL_STATE_HEADER = vbNullString

        _COL_STUDY_NAME_WIDTH = 0.0
        _COL_STUDY_NAME_VISIBLE = False
        _COL_STUDY_NAME_DISP_INDX = 4
        _COL_STUDY_NAME_HEADER = vbNullString

        _COL_ORDER_DATE_WIDTH = 0.0
        _COL_ORDER_DATE_VISIBLE = False
        _COL_ORDER_DATE_DISP_INDX = 0.0
        _COL_ORDER_DATE_HEADER = vbNullString

        _COL_ORDER_TIME_WIDTH = 0.0
        _COL_ORDER_TIME_VISIBLE = False
        _COL_ORDER_TIME_DISP_INDX = 6
        _COL_ORDER_TIME_HEADER = vbNullString

        _COL_STUDY_DATE_WIDTH = 0.0
        _COL_STUDY_DATE_VISIBLE = False
        _COL_STUDY_DATE_DISP_INDX = 5
        _COL_STUDY_DATE_HEADER = vbNullString

        _COL_STUDY_TIME_WIDTH = 0.0
        _COL_STUDY_TIME_VISIBLE = False
        _COL_STUDY_TIME_DISP_INDX = 6
        _COL_STUDY_TIME_HEADER = vbNullString

        _COL_PATIENT_ID_WIDTH = 0.0
        _COL_PATIENT_ID_VISIBLE = False
        _COL_PATIENT_ID_DISP_INDX = 7
        _COL_PATIENT_ID_HEADER = vbNullString

        _COL_PATIENT_KANJI_WIDTH = 0.0
        _COL_PATIENT_KANJI_VISIBLE = False
        _COL_PATIENT_KANJI_DISP_INDX = 9
        _COL_PATIENT_KANJI_HEADER = vbNullString

        _COL_PATIENT_KANA_WIDTH = 171
        _COL_PATIENT_KANA_VISIBLE = False
        _COL_PATIENT_KANA_DISP_INDX = 11
        _COL_PATIENT_KANA_HEADER = vbNullString

        _COL_PATIENT_EIJI_WIDTH = 119
        _COL_PATIENT_EIJI_VISIBLE = False
        _COL_PATIENT_EIJI_DISP_INDX = 12
        _COL_PATIENT_EIJI_HEADER = vbNullString

        _COL_PATIENT_BIRTH_WIDTH = 0.0
        _COL_PATIENT_BIRTH_VISIBLE = False
        _COL_PATIENT_BIRTH_DISP_INDX = 13
        _COL_PATIENT_BIRTH_HEADER = vbNullString

        _COL_PATIENT_SEX_WIDTH = 0.0
        _COL_PATIENT_SEX_VISIBLE = False
        _COL_PATIENT_SEX_DISP_INDX = 14
        _COL_PATIENT_SEX_HEADER = vbNullString

        '_COL_SATSUEI_DIV_WIDTH = 0.0
        '_COL_SATSUEI_DIV_VISIBLE = False
        '_COL_SATSUEI_DIV_DISP_INDX = 15
        '_COL_SATSUEI_DIV_HEADER = vbNullString

        _COL_BODY_PART_WIDTH = 0.0
        _COL_BODY_PART_VISIBLE = False
        _COL_BODY_PART_DISP_INDX = 15
        _COL_BODY_PART_HEADER = vbNullString

        _COL_MODALITY_WIDTH = 0.0
        _COL_MODALITY_VISIBLE = False
        _COL_MODALITY_DISP_INDX = 16
        _COL_MODALITY_HEADER = vbNullString

        _COL_WARD_WIDTH = 0.0
        _COL_WARD_VISIBLE = False
        _COL_WARD_DISP_INDX = 16
        _COL_WARD_HEADER = vbNullString

        _COL_ROOM_WIDTH = 0.0
        _COL_ROOM_VISIBLE = False
        _COL_ROOM_DISP_INDX = 16
        _COL_ROOM_HEADER = vbNullString

        _COL_DEL_FLG_WIDTH = 0.0
        _COL_DEL_FLG_VISIBLE = False
        _COL_DEL_FLG_DISP_INDX = 18
        _COL_DEL_FLG_HEADER = vbNullString

        _COL_STUDY_INSTANCE_UID_WIDTH = 0.0
        _COL_STUDY_INSTANCE_UID_VISIBLE = False
        _COL_STUDY_INSTANCE_UID_DISP_INDX = 19
        _COL_STUDY_INSTANCE_UID_HEADER = vbNullString

        _COL_ORDER_ANO_WIDTH = 0.0
        _COL_ORDER_ANO_VISIBLE = False
        _COL_ORDER_ANO_DISP_INDX = 3
        _COL_ORDER_ANO_HEADER = vbNullString

        _COL_ORDER_NO_WIDTH = 0.0
        _COL_ORDER_NO_VISIBLE = False
        _COL_ORDER_NO_DISP_INDX = 8
        _COL_ORDER_NO_HEADER = vbNullString

        _COL_PATIENT_ANO_WIDTH = 0.0
        _COL_PATIENT_ANO_VISIBLE = False
        _COL_PATIENT_ANO_DISP_INDX = 21
        _COL_PATIENT_ANO_HEADER = vbNullString

        _COL_ORDER_COMMENT_WIDTH = 0.0
        _COL_ORDER_COMMENT_VISIBLE = False
        _COL_ORDER_COMMENT_DISP_INDX = 22
        _COL_ORDER_COMMENT_HEADER = vbNullString

        _COL_PATIENT_COMMENT_WIDTH = 0
        _COL_PATIENT_COMMENT_VISIBLE = False
        _COL_PATIENT_COMMENT_DISP_INDX = 0.0
        _COL_PATIENT_COMMENT_HEADER = vbNullString

        _COL_STUDY_COMMENT_WIDTH = 0
        _COL_STUDY_COMMENT_VISIBLE = False
        _COL_STUDY_COMMENT_DISP_INDX = 0.0
        _COL_STUDY_COMMENT_HEADER = vbNullString

        _COL_COMMENT_FLG_WIDTH = 0
        _COL_COMMENT_FLG_VISIBLE = False
        _COL_COMMENT_FLG_DISP_INDX = 0.0
        _COL_COMMENT_FLG_HEADER = vbNullString

        _COL_INS_DATE_WIDTH = 0
        _COL_INS_DATE_VISIBLE = False
        _COL_INS_DATE_DISP_INDX = 0.0
        _COL_INS_DATE_HEADER = vbNullString

        _COL_INS_TIME_WIDTH = 0
        _COL_INS_TIME_VISIBLE = False
        _COL_INS_TIME_DISP_INDX = 0.0
        _COL_INS_TIME_HEADER = vbNullString

        _COL_UPD_DATE_WIDTH = 0
        _COL_UPD_DATE_VISIBLE = False
        _COL_UPD_DATE_DISP_INDX = 0.0
        _COL_UPD_DATE_HEADER = vbNullString

        _COL_UPD_TIME_WIDTH = 0
        _COL_UPD_TIME_VISIBLE = False
        _COL_UPD_TIME_DISP_INDX = 0.0
        _COL_UPD_TIME_HEADER = vbNullString

        _COL_AGE_WIDTH = 0
        _COL_AGE_VISIBLE = False
        _COL_AGE_DISP_INDX = 0.0
        _COL_AGE_HEADER = vbNullString

    End Sub
#End Region

    'Settingsクラスのただ一つのインスタンス
    <NonSerialized()> _
    Private Shared _instance As Settings
    <System.Xml.Serialization.XmlIgnore()> _
    Public Shared Property Instance() As Settings
        Get
            If _instance Is Nothing Then
                _instance = New Settings
            End If
            Return _instance
        End Get
        Set(ByVal Value As Settings)
            _instance = Value
        End Set
    End Property

    ''' <summary>
    ''' 設定をXMLファイルから読み込み復元する
    ''' </summary>
    Public Shared Sub LoadFromXmlFile()
        'Dim p As String = GetSettingPath()
        Dim p As String = My.Settings.Setting_FILE_PATH

        Dim fs As New FileStream( _
            p, FileMode.Open, FileAccess.Read)
        Dim xs As New System.Xml.Serialization.XmlSerializer( _
            GetType(Settings))
        '読み込んで逆シリアル化する
        Dim obj As Object = xs.Deserialize(fs)
        fs.Close()

        Instance = CType(obj, Settings)
    End Sub

    ''' <summary>
    ''' 現在の設定をXMLファイルに保存する
    ''' </summary>
    Public Shared Sub SaveToXmlFile()
        'Dim p As String = GetSettingPath()
        Dim p As String = My.Settings.Setting_FILE_PATH

        Dim fs As New FileStream( _
            p, FileMode.Create, FileAccess.Write)
        Dim xs As New System.Xml.Serialization.XmlSerializer( _
            GetType(Settings))
        'シリアル化して書き込む
        xs.Serialize(fs, Instance)
        fs.Close()
    End Sub

    Private Shared Function GetSettingPath() As String
        Dim p As String = Path.Combine( _
            Environment.GetFolderPath( _
                Environment.SpecialFolder.ApplicationData), _
            Application.CompanyName + "\" + Application.ProductName _
            + "\" + Application.ProductName + ".config")
        Return p
    End Function

End Class
