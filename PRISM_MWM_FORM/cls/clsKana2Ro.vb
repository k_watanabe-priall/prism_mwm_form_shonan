﻿Public Class clsKana2Ro
    ' プロパティ
    Private iMode As Long        '　変換オプション 0 .. デフォルト
    Private strInput As String      ' 入力データ
    Private strOutput As String     ' 変換データ

    ' カナ->ローマ字(ヘボン式)変換テーブル
    Private Structure K2RTbl
        Dim sKana As String
        Dim sRome As String
    End Structure

    Private Const KANAMAX As Long = 100   ' 変換テーブル個数
    Private Const Kana_1st As String = "ァ"  ' 半角カナの最初の文字コード
    Private Const Kana_Last As String = "ヶ"  ' 半角カナの最後の文字コード
    Private Const KANAETC As Long = 90    ' 変換テーブル代替エントリの最初


    Private tK2R() As K2RTbl

    ' 変換テーブル検索
    Private Function hkanak2ro(ByVal c As String)
        Dim i As Long

        If c >= Kana_1st And c <= Kana_Last Then
            i = Asc(c) - Asc(Kana_1st)
            If tK2R(i).sKana = c Then
                hkanak2ro = tK2R(i).sRome   'テーブルにヒット
                Exit Function
            End If
        Else
            ' 長音その他
            For i = KANAETC To KANAMAX - 1
                If tK2R(i).sKana = c Then
                    hkanak2ro = tK2R(i).sRome   'テーブルにヒット
                    Exit Function
                End If
            Next
        End If

        hkanak2ro = ""  ' 変換テーブルになし nullセット
    End Function


    ' カナ変換１次処理
    Private Function kana2ro1(ByVal s As String)
        Dim str As String = String.Empty
        Dim i As Long
        Dim ilen As Long
        Dim c As String = String.Empty
        Dim sbuf As String = String.Empty

        '  ilen = Len(s)
        '
        '  For i = 1 To ilen
        '    c = Mid(s, i, 1)
        '    If Asc(c) >= 128 And Asc(c) <= 255 Then
        '      ' 半角カナ
        '      sbuf = sbuf & strcnv(c, vbWide)
        '    Else
        '      '半角英数/全角
        '      sbuf = sbuf & c
        '    End If
        '  Next

        sbuf = StrConv(s, vbWide)

        ilen = Len(sbuf)
        For i = 1 To ilen
            c = Mid(sbuf, i, 1)
            str = str + hkanak2ro(c)
        Next

        kana2ro1 = str
    End Function

    ' カナ変換２次処理
    Private Function kana2ro2(ByVal s As String)
        Dim i As Long
        Dim str As String = String.Empty
        Dim c As String = String.Empty
        Dim cc As String = String.Empty
        Dim ilen As Long
        Dim l As Long

        ilen = Len(s)

        For i = 1 To ilen
            c = Mid(s, i, 1)
            Select Case c

                Case "$"
                    ' $
                    cc = c
                    c = Mid(s, i + 1, 1)
                    Select Case c
                        Case "&"
                            ' 促音
                            c = Mid(s, i + 2, 1)
                            If c = "c" Then
                                ' chはtchにする
                                str = str & "t"
                            Else
                                If c <> "$" And c <> "-" Then
                                    str = str & c
                                End If
                            End If
                            i = i + 1

                        Case "?"
                            ' 変な文字
                            '  $ + ? -> 無視
                            i = i + 1

                        Case "y"
                            ' ャュョ
                            If i > 1 Then
                                If Mid(s, i - 1, 1) = "i" Then
                                    '拗音 ただし母音i以外は無視
                                    l = Len(str)
                                    str = Mid(str, 1, l - 1)  '前のiを消す
                                    cc = Right(str, 1)
                                    If cc = "j" Then
                                        ' ザ行　ダ行 は j+母音
                                        str = str & Mid(s, i + 2, 1)
                                    Else
                                        cc = Right(str, 2)
                                        If cc = "sh" Or cc = "ch" Then
                                            ' サ行 sh+母音
                                            ' タ行 ch+母音
                                            str = str & Mid(s, i + 2, 1)
                                        Else
                                            ' それ以外
                                            str = str & c & Mid(s, i + 2, 1)
                                        End If
                                    End If
                                End If
                            End If
                            i = i + 2

                        Case Else
                            ' ァｲゥェォ
                            cc = Mid(str, i - 1, 1)
                            If cc = "u" Then
                                ' ウァ -> wha whi whe who
                                l = Len(str)
                                str = Mid(str, 1, l - 1)  '前のuを消す
                                str = str & "wh" & c
                            Else
                                str = str & c
                            End If
                            i = i + 1

                    End Select

                Case "n"
                    '撥音
                    cc = c
                    c = Mid(s, i + 1, 1)
                    If c = "p" Or c = "b" Or c = "m" Then
                        ' 次の文字がp,b,m場合mを付加する
                        str = str & "m"
                    Else
                        str = str & cc
                    End If

                Case "-"
                    '　長音
                    If str <> "" Then
                        str = str & Right(str, 1)
                    End If

                Case Else
                    ' その他の文字
                    str = str & c
            End Select

        Next

        kana2ro2 = str
    End Function


    ' カナ変換処理
    Public Function kana2ro(ByVal s As String)
        Dim str As String = String.Empty
        Dim sbuf As String = String.Empty
        Dim i As Long
        Dim ilen As Long
        Dim kflg As Boolean
        Dim c As String = String.Empty


        ilen = Len(s)

        For i = 1 To ilen
            c = Mid(s, i, 1)
            If Asc(c) >= 128 And Asc(c) <= 255 Then
                ' 半角カナ
                If kflg = False Then
                    str = str & sbuf
                    sbuf = ""
                    kflg = True
                End If

                sbuf = sbuf & c
            Else
                '半角英数/全角
                If kflg = True Then
                    'バッファリングしたカナ文字を変換
                    sbuf = kana2ro1(sbuf)
                    str = str & kana2ro2(sbuf)
                    sbuf = ""
                    kflg = False
                End If

                sbuf = sbuf & c
            End If
        Next

        If kflg = True Then
            'バッファリングしたカナ文字を変換
            sbuf = kana2ro1(sbuf)
            str = str & kana2ro2(sbuf)
        Else
            str = str & sbuf
        End If

        kana2ro = str

    End Function


    '    ' modeプロパティ
    '    Public Property set Mode(ByVal newOption As Long)
    '   iMode = newOption
    '    End Property

    'Public Property Get Mode() As Long
    '   Mode = iMode
    '    End Property

    ' Inputプロパティ
    'Public Property Let sInput(newInput As String)
    '   strInput = newInput
    'End Property

    ' Outputプロパティ
    'Public Property Get sOutput() As String
    '   strOutput = kana2ro(strInput)
    '   sOutput = strOutput
    'End Property



    ' 初期化(コンストラクタ)
    Public Sub New()

        iMode = 0
        strInput = ""
        strOutput = ""

        ReDim tK2R(KANAMAX)

        '変換テーブルの並びはSJIS順
        ' あ行
        With tK2R(0)
            .sKana = "ァ"
            .sRome = "$a"
        End With

        With tK2R(1)
            .sKana = "ア"
            .sRome = "a"
        End With

        With tK2R(2)
            .sKana = "ィ"
            .sRome = "$i"
        End With

        With tK2R(3)
            .sKana = "イ"
            .sRome = "i"
        End With

        With tK2R(4)
            .sKana = "ゥ"
            .sRome = "$u"
        End With

        With tK2R(5)
            .sKana = "ウ"
            .sRome = "u"
        End With

        With tK2R(6)
            .sKana = "ェ"
            .sRome = "$e"
        End With

        With tK2R(7)
            .sKana = "エ"
            .sRome = "e"
        End With

        With tK2R(8)
            .sKana = "ォ"
            .sRome = "$o"
        End With

        With tK2R(9)
            .sKana = "オ"
            .sRome = "o"
        End With

        'カ行
        With tK2R(10)
            .sKana = "カ"
            .sRome = "ka"
        End With

        With tK2R(11)
            .sKana = "ガ"
            .sRome = "ga"
        End With

        With tK2R(12)
            .sKana = "キ"
            .sRome = "ki"
        End With

        With tK2R(13)
            .sKana = "ギ"
            .sRome = "gi"
        End With

        With tK2R(14)
            .sKana = "ク"
            .sRome = "ku"
        End With

        With tK2R(15)
            .sKana = "グ"
            .sRome = "gu"
        End With

        With tK2R(16)
            .sKana = "ケ"
            .sRome = "ke"
        End With

        With tK2R(17)
            .sKana = "ゲ"
            .sRome = "ge"
        End With

        With tK2R(18)
            .sKana = "コ"
            .sRome = "ko"
        End With

        With tK2R(19)
            .sKana = "ゴ"
            .sRome = "go"
        End With

        'サ行
        With tK2R(20)
            .sKana = "サ"
            .sRome = "sa"
        End With

        With tK2R(21)
            .sKana = "ザ"
            .sRome = "za"
        End With

        With tK2R(22)
            .sKana = "シ"
            .sRome = "shi"
        End With

        With tK2R(23)
            .sKana = "ジ"
            .sRome = "ji"
        End With

        With tK2R(24)
            .sKana = "ス"
            .sRome = "su"
        End With

        With tK2R(25)
            .sKana = "ズ"
            .sRome = "zu"
        End With

        With tK2R(26)
            .sKana = "セ"
            .sRome = "se"
        End With

        With tK2R(27)
            .sKana = "ゼ"
            .sRome = "ze"
        End With

        With tK2R(28)
            .sKana = "ソ"
            .sRome = "so"
        End With

        With tK2R(29)
            .sKana = "ゾ"
            .sRome = "zo"
        End With

        'タ行
        With tK2R(30)
            .sKana = "タ"
            .sRome = "ta"
        End With

        With tK2R(31)
            .sKana = "ダ"
            .sRome = "da"
        End With

        With tK2R(32)
            .sKana = "チ"
            .sRome = "chi"
        End With

        With tK2R(33)
            .sKana = "ヂ"
            .sRome = "ji"
        End With

        With tK2R(34)
            .sKana = "ッ"
            .sRome = "$&"   ' 詰める
        End With

        With tK2R(35)
            .sKana = "ツ"
            .sRome = "tsu"
        End With

        With tK2R(36)
            .sKana = "ヅ"
            .sRome = "zu"
        End With

        With tK2R(37)
            .sKana = "テ"
            .sRome = "te"
        End With

        With tK2R(38)
            .sKana = "デ"
            .sRome = "de"
        End With

        With tK2R(39)
            .sKana = "ト"
            .sRome = "to"
        End With

        With tK2R(40)
            .sKana = "ド"
            .sRome = "do"
        End With

        'ナ行
        With tK2R(41)
            .sKana = "ナ"
            .sRome = "na"
        End With

        With tK2R(42)
            .sKana = "ニ"
            .sRome = "ni"
        End With

        With tK2R(43)
            .sKana = "ヌ"
            .sRome = "nu"
        End With

        With tK2R(44)
            .sKana = "ネ"
            .sRome = "ne"
        End With

        With tK2R(45)
            .sKana = "ノ"
            .sRome = "no"
        End With

        'ハ行
        With tK2R(46)
            .sKana = "ハ"
            .sRome = "ha"
        End With

        With tK2R(47)
            .sKana = "バ"
            .sRome = "ba"
        End With

        With tK2R(48)
            .sKana = "パ"
            .sRome = "pa"
        End With

        With tK2R(49)
            .sKana = "ヒ"
            .sRome = "hi"
        End With

        With tK2R(50)
            .sKana = "ビ"
            .sRome = "bi"
        End With

        With tK2R(51)
            .sKana = "ピ"
            .sRome = "pi"
        End With

        With tK2R(52)
            .sKana = "フ"
            .sRome = "fu"
        End With

        With tK2R(53)
            .sKana = "ブ"
            .sRome = "bu"
        End With

        With tK2R(54)
            .sKana = "プ"
            .sRome = "pu"
        End With

        With tK2R(55)
            .sKana = "ヘ"
            .sRome = "he"
        End With

        With tK2R(56)
            .sKana = "ベ"
            .sRome = "be"
        End With

        With tK2R(57)
            .sKana = "ペ"
            .sRome = "pe"
        End With

        With tK2R(58)
            .sKana = "ホ"
            .sRome = "ho"
        End With

        With tK2R(59)
            .sKana = "ボ"
            .sRome = "bo"
        End With

        With tK2R(60)
            .sKana = "ポ"
            .sRome = "po"
        End With

        'マ行
        With tK2R(61)
            .sKana = "マ"
            .sRome = "ma"
        End With

        With tK2R(62)
            .sKana = "ミ"
            .sRome = "mi"
        End With

        With tK2R(64)
            .sKana = "ム"
            .sRome = "mu"
        End With

        With tK2R(65)
            .sKana = "メ"
            .sRome = "me"
        End With

        With tK2R(66)
            .sKana = "モ"
            .sRome = "mo"
        End With

        'ヤ行
        With tK2R(67)
            .sKana = "ャ"
            .sRome = "$ya"
        End With

        With tK2R(68)
            .sKana = "ヤ"
            .sRome = "ya"
        End With

        With tK2R(69)
            .sKana = "ュ"
            .sRome = "$yu"
        End With

        With tK2R(70)
            .sKana = "ユ"
            .sRome = "yu"
        End With

        With tK2R(71)
            .sKana = "ョ"
            .sRome = "$yo"
        End With

        With tK2R(72)
            .sKana = "ヨ"
            .sRome = "yo"
        End With

        'ラ行
        With tK2R(73)
            .sKana = "ラ"
            .sRome = "ra"
        End With

        With tK2R(74)
            .sKana = "リ"
            .sRome = "ri"
        End With

        With tK2R(75)
            .sKana = "ル"
            .sRome = "ru"
        End With

        With tK2R(76)
            .sKana = "レ"
            .sRome = "re"
        End With

        With tK2R(77)
            .sKana = "ロ"
            .sRome = "ro"
        End With

        'ワ行
        With tK2R(78)
            .sKana = "ヮ"
            .sRome = "$?"    ' 無視
        End With

        With tK2R(79)
            .sKana = "ワ"
            .sRome = "wa"
        End With

        '旧字
        With tK2R(80)
            .sKana = "ヰ"
            .sRome = "i"
        End With

        With tK2R(81)
            .sKana = "ヱ"
            .sRome = "e"
        End With

        'ヲ ン
        With tK2R(82)
            .sKana = "ヲ"
            .sRome = "wo"
        End With

        With tK2R(83)
            .sKana = "ン"
            .sRome = "n"
        End With

        'ヴ
        With tK2R(84)
            .sKana = "ヴ"
            .sRome = "v"
        End With

        'ヵ ヶ
        With tK2R(85)
            .sKana = "ヵ"
            .sRome = "$?"  ' 無視
        End With

        With tK2R(86)
            .sKana = "ヶ" ' 無視
            .sRome = "$?"
        End With

        ' 長音
        With tK2R(90)
            .sKana = "ー"
            .sRome = "-"
        End With

        ' 全角space
        With tK2R(91)
            .sKana = "　"
            .sRome = " "
        End With

        ' 半角カナにある文字
        With tK2R(92)
            .sKana = "。"
            .sRome = "."
        End With

        With tK2R(93)
            .sKana = "「"
            .sRome = "["
        End With

        With tK2R(94)
            .sKana = "」"
            .sRome = "]"
        End With

        With tK2R(95)
            .sKana = "、"
            .sRome = ","
        End With

        With tK2R(96)
            .sKana = "・"
            .sRome = ":"
        End With


    End Sub

    '' 終了処理
    'Private Sub Finalize()
    '    ReDim tK2R(0) As K2RTbl
    'End Sub



End Class
