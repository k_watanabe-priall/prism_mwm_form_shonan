﻿Public Class clsUser
    Public USER_ANO As Integer
    Public USER_ID As String = vbNullString
    Public USER_PASS As String = vbNullString
    Public USER_NAME As String = vbNullString
    Public REP_USER_AUTH As String = vbNullString
End Class

Public Class clsOrder
    Public ORDER_ANO As Integer
    Public ORDER_NO As String
    Public ORDER_DATE As String
    Public ORDER_TIME As String
    Public PATIENT_ANO As String
    Public STUDY_DATE As String
    Public STUDY_TIME As String
    Public STUDY_DIV As String
    Public MODALITY_NO As String
    Public MODALITY_NAME As String
    Public MODALITY_CD As String
    'Public SATSUEI_DIV As Integer       '///技師要望の為、撮影区分を追加　ADD By Watanabe 2011.01.24
    Public BODY_PART As String
    Public ORDER_COMMENT As String
    Public STATE As String
    Public STUDY_INSTANCE_UID As String
    Public TERMINAL As String
    Public STUDY_COMMENT As String
    Public INS_DATE As String
    Public INS_TIME As String
    Public INS_USER As String
    Public UPD_DATE As String
    Public UPD_TIME As String
    Public UPD_USER As String
    Public DEL_FLG As String
    '///患者属性
    Public PATIENT_ID As String
    Public KENSHIN_PATIENT_ID
    Public PATIENT_KANJI As String
    Public PATIENT_EIJI As String
    Public PATIENT_KANA As String
    Public PATIENT_BIRTH As String
    Public PATIENT_SEX As String
    Public PATIENT_COMMENT As String
    Public BEFOR_KANA As String
    Public BEFOR_KANJI As String
    Public BEFOR_EIJI As String
    Public BEFOR_BIRTH As String
    Public BEFOR_SEX As String

End Class

Public Class clsStudyInstanceUID
    Public CODE_ISO As String
    Public CODE_ISO_AFFIATION As String
    Public CODE_ISO_COUNTRY As String
    Public CODE_ORGANIZATION As String
    Public CODE_PRODUCT As String
    Public CODE_SYSTEM As String
    Public CODE_HOSPTAL As String
    Public CODE_SERIES As String
    Public CODE_SYSDATE As String
    Public CODE_SEQ As String
End Class

Public Class clsModality
    Public MODALITY_NO As String
    Public MODALITY_CD As String
    Public SCP_AE As String
    Public SCP_IP As String
    Public SCP_PORT As String
    Public SCU_AE As String
    Public SCU_IP As String
    Public SCU_PORT As String
    Public CHARCTERSET As String
    Public TERMINAL As String
    Public MODALITY_NAME As String
    Public ROOM_NAME As String
    Public HOME_PATH As String
End Class

Public Class clsPatinet
    Public PATIENT_ANO As String
    Public PATIENT_ID As String
    Public KENSHIN_PATIENT_ID As String
    Public PATIENT_KANJI As String
    Public PATIENT_EIJI As String
    Public PATIENT_KANA As String
    Public PATIENT_BIRTH As String
    Public PATIENT_SEX As String
    Public PATIENT_COMMENT As String
    Public BEFOR_KANA As String
    Public BEFOR_KANJI As String
    Public BEFOR_EIJI As String
    Public BEFOR_BIRTH As String
    Public BEFOR_SEX As String
End Class

Public Class clsBodyPart
    Public BODY_PART_NO As String
    Public BODY_PART_NM As String
    Public MODALITY_CD As String
    Public STUDY_DIV As String
End Class

Public Class clsSelOrder
    Public ORDER_ANO As Integer
    Public ORDER_NO As String
    Public ORDER_DATE As String
    Public ORDER_TIME As String
    Public PATIENT_ANO As String
    Public STUDY_DATE As String
    Public STUDY_TIME As String
    Public MODALITY_NO As String
    Public MODALITY_NAME As String
    Public MODALITY_CD As String
    Public BODY_PART As String
    Public ORDER_COMMENT As String
    Public STATE As String
    Public STUDY_INSTANCE_UID As String
    Public TERMINAL As String
End Class

Public Class TYPE_ORDER
    Public ORDER_ANO As String
    Public MWM_STATE As String
    Public MWM_ORDER_NO As String
    Public SEND_STATE As String
    Public PATIENT_ANO As String
    Public PATIENT_ID As String
    Public SHIJI_DATE As String
    Public SNONYM As String
    Public SHINRYO_KBN As String
    Public KUGIRI_NO As String
    Public SHINRYO_KBN_NAME As String
    Public SHINRYO_KBN_RNAME As String
    Public KINKYU_KBN As String
    Public KINKYU_KBN_NAME As String
    Public JIGO_NYURYOKU_FLG As String
    Public JIGO_NYURYOKU_FLG_NAME As String
    Public HOKEN_SHUBETSU As String
    Public HOKEN_SHUBETSU_NAME As String
    Public SHINKI_FLG As String
    Public SHINKI_FLG_NAME As String
    Public KENSA_SHUBETSU As String
    Public KENSA_SHUBETSU_NAME As String
    Public SHOHO_KBN As String
    Public SHOHO_KBN_NAME As String
    Public TEIRINJI_FLG As String
    Public TEIRINJI_FLG_NAME As String
    Public NICHI_KAISU As String
    Public JISSHIYOTEI_KAISHI_DATETIME As String
    Public JISSHIYOTEI_KAISHI_JIKAN_KBN As String
    Public JISSHIYOTEI_KAISHI_JIKAN_KBN_NAME As String
    Public JISSHIYOTEI_DATE_KANKAKU As String
    Public JISSHIYOTEI_SHURYO_JIKAN_KBN As String
    Public JISSHIYOTEI_SHURYO_JIKAN_KBN_NAME As String
    Public ORDER_NO As String
    Public CHUSHI_HAMBETSU_FLG As String
    Public CHUSHI_DATETIME As String
    Public CHUSHI_JIKAN_KBN As String
    Public CHUSHI_JIKAN_KBN_NAME As String
    Public BUMONKA_CODE As String
    Public BUMONKA_NAME As String
    Public FREE_COMMENT As String
    Public ORDER_STATUS As String
    Public HUJISSHI_JOHO_DATETIME As String
    Public HUJISSHI_JOHO_SOSASHA_ID As String
    Public HUJISSHI_JOHO_SOSASHA_NAME As String
    Public UKETSUKE_DATETIME As String
    Public UKETSUKE_SOSASHA_ID As String
    Public UKETSUKE_SOSASHA_NAME As String
    Public SAKUJO_DATETIME As String
    Public SAKUJO_SOSASHA_ID As String
    Public SAKUJO_SOSASHA_NAME As String
    Public CHUSHI_SOSA_DATETIME As String
    Public CHUSHI_SOSASHA_ID As String
    Public CHUSHI_SOSASHA_NAME As String
    Public CHUSHI_SHIJII_SOSASHA_ID As String
    Public CHUSHI_SHIJII_SOSASHA_NAME As String
    Public HOSOKU_NYURYOKU_DATETIME As String
    Public HOSOKU_NYURYOKU_SOSASHA_ID As String
    Public HOSOKU_NYURYOKU_SOSASHA_NAME As String
    Public KENSA_KEKKA_UMU As String
    Public KENSA_KEKKA_UMU_NAME As String
    Public SHOKUJI_SESSHU_YOHI As String
    Public SHOKUJI_SESSHU_YOHI_NAME As String
    Public KOHI_JOHO_REMBAN1 As String
    Public KOHI_JOHO_REMBAN1_NAME As String
    Public KOHI_JOHO_REMBAN2 As String
    Public KOHI_JOHO_REMBAN2_NAME As String
    Public NYUGAI_KBN As String
    Public NYUGAI_KBN_NAME As String
    Public SHINRYOKA_CODE As String
    Public SHINRYOKA_NAME As String
    Public BUMON_CODE As String
    Public BUMON_NAME As String
    Public SHINSATSUSHITSU_NO As String
    Public SHINSATSUSHITSU_NAME As String
    Public SOSASHA_ID As String
    Public SOSASHA_NAME As String
    Public SHIJII_ID As String
    Public SHIJII_NAME As String
    Public NYUIN_DATE As String
    Public TAIIN_DATE As String
    Public BYOTO_CODE As String
    Public BYOTO_NAME As String
    Public BYOSHITSU_NO As String
    Public BYOSHITSU_NAME As String
    Public NYUIN_KA As String
    Public NYUIN_KA_NAME As String
    Public SAISHU_TENTO_DATE As String
    Public SHUJII_ID As String
    Public SHUJII_NAME As String
    Public JISSHI_DATE As String
    Public KOUMOKU_HIMBAN As String
    Public KOUMOKU_NAME As String
    Public MODALITY_CD As String
    Public SEND_MODALITY_CD As String
    Public MODALITY_NAME As String
    Public DEL_FLG As String
    Public BODY_PART As String
    Public COMMENT As String
    Public JOKEN As String
End Class

Public Class TYPE_PATIENT
    Public PATIENT_ANO As String
    Public KANJA_NO As String
    Public KANJI_NAME As String
    Public KANA_NAME As String
    Public EIJI_NAME As String
    Public SEIBETSU As String
    Public SEIBETSU_NAME As String
    Public ADDRESS As String
    Public SHINCHO As String
    Public TAIJYU As String
    Public BIRTH_DATE As String
    Public YUBIN_NO As String
    Public TEL_NO As String
    Public RENRAKUSAKI_TEL_NO As String
    Public OLD_KANA_NAME As String
    Public OLD_KANJI_NAME As String
    Public OLD_NAME_YUKOKIGEN As String
    Public BLOOD_TYPE_ABO As String
    Public BLOOD_TYPE_RH As String
    Public BLOOD_TYPE_DATE As String
    Public NINSHIN_UMU As String
    Public BUNBEN_YOTEI_DATE As String
    Public NINSHIN_SHUSU As String
    Public HOKOU As String
    Public HOKOU_NAME As String
    Public ANSEIDO As String
    Public ANSEIDO_NAME As String
    Public KANGODO As String
    Public SHOKUJI_KAIJODO As String
    Public SHOKUJI_KAIJODO_NAME As String
    Public HUKUYAKU_SHIDO As String
    Public HUKUYAKU_SHIDO_NAME As String
    Public NINI_JOHO As String
    Public NINI_JOHO_NAME As String
    Public INS_DATE As String
    Public INS_TIME As String
    Public INS_USER As String
    Public UPD_DATE As String
    Public UPD_TIME As String
    Public UPD_USER As String
    Public DEL_FLG As String
End Class

Public Class TYPE_MODALITY
    Public ORDER_ANO As Long
    Public ORDER_MODALITY_NO As String
    Public ORDER_NO As String
    Public MWM_ORDER_NO As String
    Public KOUMOKU_HIMBAN As String
    Public MODALITY_CD As String
    Public MEISAI_NO As String
    Public KOUMOKU_NAME As String
End Class

