﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditPatient
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditPatient))
        Me.grpPatient = New System.Windows.Forms.GroupBox()
        Me.txtCancel = New System.Windows.Forms.TextBox()
        Me.txtKenshinID = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtStudyDiv = New System.Windows.Forms.TextBox()
        Me.txtStudyDate = New System.Windows.Forms.TextBox()
        Me.txtSex = New System.Windows.Forms.TextBox()
        Me.lblAge = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtPatientAno = New System.Windows.Forms.TextBox()
        Me.txtUpdFlg = New System.Windows.Forms.TextBox()
        Me.txtModalityNo = New System.Windows.Forms.TextBox()
        Me.txtModality = New System.Windows.Forms.TextBox()
        Me.txtPatientComment = New System.Windows.Forms.TextBox()
        Me.txtBirth = New System.Windows.Forms.TextBox()
        Me.cmbSex = New System.Windows.Forms.ComboBox()
        Me.txtEiji = New System.Windows.Forms.TextBox()
        Me.txtKana = New System.Windows.Forms.TextBox()
        Me.txtKanji = New System.Windows.Forms.TextBox()
        Me.txtPatientID = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grpProc = New System.Windows.Forms.GroupBox()
        Me.cmdClose = New VIBlend.WinForms.Controls.vButton()
        Me.cmdAddOrder = New VIBlend.WinForms.Controls.vButton()
        Me.cmdCancel = New VIBlend.WinForms.Controls.vButton()
        Me.cmbModality = New System.Windows.Forms.ComboBox()
        Me.cmdEdit = New VIBlend.WinForms.Controls.vButton()
        Me.grpPatient.SuspendLayout()
        Me.grpProc.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpPatient
        '
        Me.grpPatient.Controls.Add(Me.txtCancel)
        Me.grpPatient.Controls.Add(Me.txtKenshinID)
        Me.grpPatient.Controls.Add(Me.Label9)
        Me.grpPatient.Controls.Add(Me.txtStudyDiv)
        Me.grpPatient.Controls.Add(Me.txtStudyDate)
        Me.grpPatient.Controls.Add(Me.txtSex)
        Me.grpPatient.Controls.Add(Me.lblAge)
        Me.grpPatient.Controls.Add(Me.Label8)
        Me.grpPatient.Controls.Add(Me.txtPatientAno)
        Me.grpPatient.Controls.Add(Me.txtUpdFlg)
        Me.grpPatient.Controls.Add(Me.txtModalityNo)
        Me.grpPatient.Controls.Add(Me.txtModality)
        Me.grpPatient.Controls.Add(Me.txtPatientComment)
        Me.grpPatient.Controls.Add(Me.txtBirth)
        Me.grpPatient.Controls.Add(Me.cmbSex)
        Me.grpPatient.Controls.Add(Me.txtEiji)
        Me.grpPatient.Controls.Add(Me.txtKana)
        Me.grpPatient.Controls.Add(Me.txtKanji)
        Me.grpPatient.Controls.Add(Me.txtPatientID)
        Me.grpPatient.Controls.Add(Me.Label4)
        Me.grpPatient.Controls.Add(Me.Label7)
        Me.grpPatient.Controls.Add(Me.Label6)
        Me.grpPatient.Controls.Add(Me.Label5)
        Me.grpPatient.Controls.Add(Me.Label3)
        Me.grpPatient.Controls.Add(Me.Label2)
        Me.grpPatient.Controls.Add(Me.Label1)
        Me.grpPatient.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.grpPatient.ForeColor = System.Drawing.Color.DodgerBlue
        Me.grpPatient.Location = New System.Drawing.Point(4, 7)
        Me.grpPatient.Name = "grpPatient"
        Me.grpPatient.Size = New System.Drawing.Size(678, 272)
        Me.grpPatient.TabIndex = 0
        Me.grpPatient.TabStop = False
        Me.grpPatient.Text = "患者情報"
        '
        'txtCancel
        '
        Me.txtCancel.Location = New System.Drawing.Point(564, 110)
        Me.txtCancel.Name = "txtCancel"
        Me.txtCancel.Size = New System.Drawing.Size(35, 21)
        Me.txtCancel.TabIndex = 26
        Me.txtCancel.Visible = False
        '
        'txtKenshinID
        '
        Me.txtKenshinID.Location = New System.Drawing.Point(161, 27)
        Me.txtKenshinID.MaxLength = 10
        Me.txtKenshinID.Name = "txtKenshinID"
        Me.txtKenshinID.Size = New System.Drawing.Size(195, 21)
        Me.txtKenshinID.TabIndex = 25
        Me.txtKenshinID.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(9, 29)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(51, 16)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "健診ID"
        Me.Label9.Visible = False
        '
        'txtStudyDiv
        '
        Me.txtStudyDiv.Location = New System.Drawing.Point(564, 83)
        Me.txtStudyDiv.Name = "txtStudyDiv"
        Me.txtStudyDiv.Size = New System.Drawing.Size(35, 21)
        Me.txtStudyDiv.TabIndex = 23
        Me.txtStudyDiv.Visible = False
        '
        'txtStudyDate
        '
        Me.txtStudyDate.Location = New System.Drawing.Point(564, 56)
        Me.txtStudyDate.Name = "txtStudyDate"
        Me.txtStudyDate.Size = New System.Drawing.Size(35, 21)
        Me.txtStudyDate.TabIndex = 22
        Me.txtStudyDate.Visible = False
        '
        'txtSex
        '
        Me.txtSex.Location = New System.Drawing.Point(564, 29)
        Me.txtSex.Name = "txtSex"
        Me.txtSex.Size = New System.Drawing.Size(35, 21)
        Me.txtSex.TabIndex = 21
        Me.txtSex.Visible = False
        '
        'lblAge
        '
        Me.lblAge.AutoSize = True
        Me.lblAge.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lblAge.Location = New System.Drawing.Point(513, 101)
        Me.lblAge.Name = "lblAge"
        Me.lblAge.Size = New System.Drawing.Size(0, 15)
        Me.lblAge.TabIndex = 20
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(473, 100)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 16)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "年齢"
        '
        'txtPatientAno
        '
        Me.txtPatientAno.Location = New System.Drawing.Point(605, 104)
        Me.txtPatientAno.Name = "txtPatientAno"
        Me.txtPatientAno.Size = New System.Drawing.Size(35, 21)
        Me.txtPatientAno.TabIndex = 19
        Me.txtPatientAno.Visible = False
        '
        'txtUpdFlg
        '
        Me.txtUpdFlg.Location = New System.Drawing.Point(605, 79)
        Me.txtUpdFlg.Name = "txtUpdFlg"
        Me.txtUpdFlg.Size = New System.Drawing.Size(35, 21)
        Me.txtUpdFlg.TabIndex = 18
        Me.txtUpdFlg.Visible = False
        '
        'txtModalityNo
        '
        Me.txtModalityNo.Location = New System.Drawing.Point(605, 54)
        Me.txtModalityNo.Name = "txtModalityNo"
        Me.txtModalityNo.Size = New System.Drawing.Size(35, 21)
        Me.txtModalityNo.TabIndex = 17
        Me.txtModalityNo.Visible = False
        '
        'txtModality
        '
        Me.txtModality.Location = New System.Drawing.Point(605, 29)
        Me.txtModality.Name = "txtModality"
        Me.txtModality.Size = New System.Drawing.Size(35, 21)
        Me.txtModality.TabIndex = 16
        Me.txtModality.Visible = False
        '
        'txtPatientComment
        '
        Me.txtPatientComment.ImeMode = System.Windows.Forms.ImeMode.Hiragana
        Me.txtPatientComment.Location = New System.Drawing.Point(161, 137)
        Me.txtPatientComment.Multiline = True
        Me.txtPatientComment.Name = "txtPatientComment"
        Me.txtPatientComment.Size = New System.Drawing.Size(508, 130)
        Me.txtPatientComment.TabIndex = 15
        '
        'txtBirth
        '
        Me.txtBirth.Location = New System.Drawing.Point(355, 98)
        Me.txtBirth.Name = "txtBirth"
        Me.txtBirth.Size = New System.Drawing.Size(111, 21)
        Me.txtBirth.TabIndex = 11
        '
        'cmbSex
        '
        Me.cmbSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSex.FormattingEnabled = True
        Me.cmbSex.Items.AddRange(New Object() {"男", "女", "不明"})
        Me.cmbSex.Location = New System.Drawing.Point(161, 99)
        Me.cmbSex.Name = "cmbSex"
        Me.cmbSex.Size = New System.Drawing.Size(57, 23)
        Me.cmbSex.TabIndex = 9
        '
        'txtEiji
        '
        Me.txtEiji.ImeMode = System.Windows.Forms.ImeMode.Alpha
        Me.txtEiji.Location = New System.Drawing.Point(161, 73)
        Me.txtEiji.Name = "txtEiji"
        Me.txtEiji.Size = New System.Drawing.Size(391, 21)
        Me.txtEiji.TabIndex = 7
        '
        'txtKana
        '
        Me.txtKana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf
        Me.txtKana.Location = New System.Drawing.Point(161, 51)
        Me.txtKana.Name = "txtKana"
        Me.txtKana.Size = New System.Drawing.Size(391, 21)
        Me.txtKana.TabIndex = 5
        '
        'txtKanji
        '
        Me.txtKanji.ImeMode = System.Windows.Forms.ImeMode.Hiragana
        Me.txtKanji.Location = New System.Drawing.Point(161, 73)
        Me.txtKanji.Name = "txtKanji"
        Me.txtKanji.Size = New System.Drawing.Size(391, 21)
        Me.txtKanji.TabIndex = 3
        Me.txtKanji.Visible = False
        '
        'txtPatientID
        '
        Me.txtPatientID.Location = New System.Drawing.Point(161, 29)
        Me.txtPatientID.MaxLength = 10
        Me.txtPatientID.Name = "txtPatientID"
        Me.txtPatientID.Size = New System.Drawing.Size(195, 21)
        Me.txtPatientID.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(9, 139)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 16)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "患者コメント"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(289, 100)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 16)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "生年月日"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(9, 101)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 16)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "性　別"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(9, 75)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "患者英字氏名"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(9, 53)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "患者カナ氏名"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(9, 75)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "患者漢字氏名"
        Me.Label2.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(9, 31)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "患者ID"
        '
        'grpProc
        '
        Me.grpProc.Controls.Add(Me.cmdClose)
        Me.grpProc.Controls.Add(Me.cmdAddOrder)
        Me.grpProc.Controls.Add(Me.cmdCancel)
        Me.grpProc.Controls.Add(Me.cmbModality)
        Me.grpProc.Controls.Add(Me.cmdEdit)
        Me.grpProc.Location = New System.Drawing.Point(4, 277)
        Me.grpProc.Name = "grpProc"
        Me.grpProc.Size = New System.Drawing.Size(676, 64)
        Me.grpProc.TabIndex = 1
        Me.grpProc.TabStop = False
        '
        'cmdClose
        '
        Me.cmdClose.BackColor = System.Drawing.Color.Transparent
        Me.cmdClose.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClose.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdClose.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClose.Location = New System.Drawing.Point(186, 17)
        Me.cmdClose.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.PressedTextColor = System.Drawing.Color.Black
        Me.cmdClose.RoundedCornersMask = CType(15, Byte)
        Me.cmdClose.RoundedCornersRadius = 8
        Me.cmdClose.Size = New System.Drawing.Size(147, 34)
        Me.cmdClose.StretchImage = True
        Me.cmdClose.StyleKey = "Button"
        Me.cmdClose.TabIndex = 3
        Me.cmdClose.Text = "オーダ情報登録"
        Me.cmdClose.UseThemeTextColor = False
        Me.cmdClose.UseVisualStyleBackColor = False
        Me.cmdClose.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'cmdAddOrder
        '
        Me.cmdAddOrder.BackColor = System.Drawing.Color.Transparent
        Me.cmdAddOrder.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdAddOrder.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdAddOrder.Enabled = False
        Me.cmdAddOrder.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdAddOrder.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddOrder.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdAddOrder.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddOrder.Location = New System.Drawing.Point(355, 20)
        Me.cmdAddOrder.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAddOrder.Name = "cmdAddOrder"
        Me.cmdAddOrder.PressedTextColor = System.Drawing.Color.Black
        Me.cmdAddOrder.RoundedCornersMask = CType(15, Byte)
        Me.cmdAddOrder.RoundedCornersRadius = 8
        Me.cmdAddOrder.Size = New System.Drawing.Size(147, 34)
        Me.cmdAddOrder.StretchImage = True
        Me.cmdAddOrder.StyleKey = "Button"
        Me.cmdAddOrder.TabIndex = 2
        Me.cmdAddOrder.Text = "オーダ登録"
        Me.cmdAddOrder.UseThemeTextColor = False
        Me.cmdAddOrder.UseVisualStyleBackColor = False
        Me.cmdAddOrder.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        Me.cmdAddOrder.Visible = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.Color.Transparent
        Me.cmdCancel.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(516, 20)
        Me.cmdCancel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.PressedTextColor = System.Drawing.Color.Black
        Me.cmdCancel.RoundedCornersMask = CType(15, Byte)
        Me.cmdCancel.RoundedCornersRadius = 8
        Me.cmdCancel.Size = New System.Drawing.Size(147, 34)
        Me.cmdCancel.StretchImage = True
        Me.cmdCancel.StyleKey = "Button"
        Me.cmdCancel.TabIndex = 4
        Me.cmdCancel.Text = "キャンセル"
        Me.cmdCancel.UseThemeTextColor = False
        Me.cmdCancel.UseVisualStyleBackColor = False
        Me.cmdCancel.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'cmbModality
        '
        Me.cmbModality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbModality.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.cmbModality.FormattingEnabled = True
        Me.cmbModality.Location = New System.Drawing.Point(210, 23)
        Me.cmbModality.Name = "cmbModality"
        Me.cmbModality.Size = New System.Drawing.Size(138, 28)
        Me.cmbModality.TabIndex = 1
        Me.cmbModality.Visible = False
        '
        'cmdEdit
        '
        Me.cmdEdit.BackColor = System.Drawing.Color.Transparent
        Me.cmdEdit.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdEdit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdEdit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdEdit.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(11, 18)
        Me.cmdEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.PressedTextColor = System.Drawing.Color.Black
        Me.cmdEdit.RoundedCornersMask = CType(15, Byte)
        Me.cmdEdit.RoundedCornersRadius = 8
        Me.cmdEdit.Size = New System.Drawing.Size(147, 34)
        Me.cmdEdit.StretchImage = True
        Me.cmdEdit.StyleKey = "Button"
        Me.cmdEdit.TabIndex = 0
        Me.cmdEdit.Text = "患者情報のみ登録"
        Me.cmdEdit.UseThemeTextColor = False
        Me.cmdEdit.UseVisualStyleBackColor = False
        Me.cmdEdit.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'frmEditPatient
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(686, 343)
        Me.ControlBox = False
        Me.Controls.Add(Me.grpProc)
        Me.Controls.Add(Me.grpPatient)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmEditPatient"
        Me.Text = "患者情報登録/更新"
        Me.grpPatient.ResumeLayout(False)
        Me.grpPatient.PerformLayout()
        Me.grpProc.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpPatient As System.Windows.Forms.GroupBox
    Friend WithEvents txtPatientComment As System.Windows.Forms.TextBox
    Friend WithEvents txtBirth As System.Windows.Forms.TextBox
    Friend WithEvents cmbSex As System.Windows.Forms.ComboBox
    Friend WithEvents txtEiji As System.Windows.Forms.TextBox
    Friend WithEvents txtKana As System.Windows.Forms.TextBox
    Friend WithEvents txtKanji As System.Windows.Forms.TextBox
    Friend WithEvents txtPatientID As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents grpProc As System.Windows.Forms.GroupBox
    Friend WithEvents cmbModality As System.Windows.Forms.ComboBox
    Friend WithEvents cmdAddOrder As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdEdit As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdClose As VIBlend.WinForms.Controls.vButton
    Friend WithEvents txtModality As System.Windows.Forms.TextBox
    Friend WithEvents txtModalityNo As System.Windows.Forms.TextBox
    Friend WithEvents txtUpdFlg As System.Windows.Forms.TextBox
    Friend WithEvents txtPatientAno As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblAge As System.Windows.Forms.Label
    Friend WithEvents txtSex As System.Windows.Forms.TextBox
    Friend WithEvents txtStudyDate As System.Windows.Forms.TextBox
    Friend WithEvents txtStudyDiv As System.Windows.Forms.TextBox
    Friend WithEvents txtKenshinID As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCancel As System.Windows.Forms.TextBox
    Friend WithEvents cmdCancel As VIBlend.WinForms.Controls.vButton
End Class
