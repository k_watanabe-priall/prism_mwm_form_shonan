﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditCol
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbVisible = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtDispColName = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdUpd = New VIBlend.WinForms.Controls.vButton()
        Me.cmdClose = New VIBlend.WinForms.Controls.vButton()
        Me.cmdOK = New VIBlend.WinForms.Controls.vButton()
        Me.dtgListInf = New System.Windows.Forms.DataGridView()
        Me.COL_DISP_INDX = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_LIST_HEADER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_LIST_VISIBLE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_LIST_WIDTH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dtgListInf, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "列情報"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbVisible)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtDispColName)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 386)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(375, 61)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        '
        'cmbVisible
        '
        Me.cmbVisible.FormattingEnabled = True
        Me.cmbVisible.Items.AddRange(New Object() {"表示する", "表示しない"})
        Me.cmbVisible.Location = New System.Drawing.Point(76, 40)
        Me.cmbVisible.Name = "cmbVisible"
        Me.cmbVisible.Size = New System.Drawing.Size(100, 21)
        Me.cmbVisible.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 43)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "表示可否"
        '
        'txtDispColName
        '
        Me.txtDispColName.Location = New System.Drawing.Point(76, 11)
        Me.txtDispColName.Name = "txtDispColName"
        Me.txtDispColName.Size = New System.Drawing.Size(283, 20)
        Me.txtDispColName.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 14)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "表示名"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdUpd)
        Me.GroupBox2.Controls.Add(Me.cmdClose)
        Me.GroupBox2.Controls.Add(Me.cmdOK)
        Me.GroupBox2.Location = New System.Drawing.Point(14, 445)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(375, 42)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        '
        'cmdUpd
        '
        Me.cmdUpd.BackColor = System.Drawing.Color.Transparent
        Me.cmdUpd.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdUpd.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdUpd.Font = New System.Drawing.Font("ＭＳ 明朝", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cmdUpd.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdUpd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUpd.Location = New System.Drawing.Point(41, 8)
        Me.cmdUpd.Name = "cmdUpd"
        Me.cmdUpd.PressedTextColor = System.Drawing.Color.Black
        Me.cmdUpd.RoundedCornersMask = CType(15, Byte)
        Me.cmdUpd.RoundedCornersRadius = 8
        Me.cmdUpd.Size = New System.Drawing.Size(102, 34)
        Me.cmdUpd.StretchImage = True
        Me.cmdUpd.StyleKey = "Button"
        Me.cmdUpd.TabIndex = 8
        Me.cmdUpd.Text = "適　用"
        Me.cmdUpd.UseThemeTextColor = True
        Me.cmdUpd.UseVisualStyleBackColor = False
        Me.cmdUpd.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.RETRO
        '
        'cmdClose
        '
        Me.cmdClose.BackColor = System.Drawing.Color.Transparent
        Me.cmdClose.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdClose.Font = New System.Drawing.Font("ＭＳ 明朝", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cmdClose.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdClose.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClose.Location = New System.Drawing.Point(257, 8)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.PressedTextColor = System.Drawing.Color.Black
        Me.cmdClose.RoundedCornersMask = CType(15, Byte)
        Me.cmdClose.RoundedCornersRadius = 8
        Me.cmdClose.Size = New System.Drawing.Size(102, 34)
        Me.cmdClose.StretchImage = True
        Me.cmdClose.StyleKey = "Button"
        Me.cmdClose.TabIndex = 7
        Me.cmdClose.Text = "閉じる"
        Me.cmdClose.UseThemeTextColor = True
        Me.cmdClose.UseVisualStyleBackColor = False
        Me.cmdClose.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.RETRO
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.Color.Transparent
        Me.cmdOK.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdOK.Enabled = False
        Me.cmdOK.Font = New System.Drawing.Font("ＭＳ 明朝", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cmdOK.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(149, 8)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.PressedTextColor = System.Drawing.Color.Black
        Me.cmdOK.RoundedCornersMask = CType(15, Byte)
        Me.cmdOK.RoundedCornersRadius = 8
        Me.cmdOK.Size = New System.Drawing.Size(102, 34)
        Me.cmdOK.StretchImage = True
        Me.cmdOK.StyleKey = "Button"
        Me.cmdOK.TabIndex = 6
        Me.cmdOK.Text = "Ｏ　Ｋ"
        Me.cmdOK.UseThemeTextColor = True
        Me.cmdOK.UseVisualStyleBackColor = False
        Me.cmdOK.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.RETRO
        Me.cmdOK.Visible = False
        '
        'dtgListInf
        '
        Me.dtgListInf.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgListInf.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_DISP_INDX, Me.COL_LIST_HEADER, Me.COL_LIST_VISIBLE, Me.COL_LIST_WIDTH})
        Me.dtgListInf.Location = New System.Drawing.Point(14, 25)
        Me.dtgListInf.Name = "dtgListInf"
        Me.dtgListInf.RowTemplate.Height = 21
        Me.dtgListInf.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgListInf.Size = New System.Drawing.Size(374, 355)
        Me.dtgListInf.TabIndex = 10
        '
        'COL_DISP_INDX
        '
        Me.COL_DISP_INDX.HeaderText = "表示順"
        Me.COL_DISP_INDX.Name = "COL_DISP_INDX"
        Me.COL_DISP_INDX.ReadOnly = True
        Me.COL_DISP_INDX.Width = 75
        '
        'COL_LIST_HEADER
        '
        Me.COL_LIST_HEADER.HeaderText = "表示名"
        Me.COL_LIST_HEADER.Name = "COL_LIST_HEADER"
        Me.COL_LIST_HEADER.ReadOnly = True
        '
        'COL_LIST_VISIBLE
        '
        Me.COL_LIST_VISIBLE.HeaderText = "表示可否"
        Me.COL_LIST_VISIBLE.Name = "COL_LIST_VISIBLE"
        Me.COL_LIST_VISIBLE.ReadOnly = True
        '
        'COL_LIST_WIDTH
        '
        Me.COL_LIST_WIDTH.HeaderText = "幅"
        Me.COL_LIST_WIDTH.Name = "COL_LIST_WIDTH"
        Me.COL_LIST_WIDTH.ReadOnly = True
        Me.COL_LIST_WIDTH.Width = 50
        '
        'frmEditCol
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(400, 493)
        Me.ControlBox = False
        Me.Controls.Add(Me.dtgListInf)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("ＭＳ ゴシック", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmEditCol"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "一覧表示列設定画面"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dtgListInf, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbVisible As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtDispColName As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdClose As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdOK As VIBlend.WinForms.Controls.vButton
    Friend WithEvents dtgListInf As System.Windows.Forms.DataGridView
    Friend WithEvents cmdUpd As VIBlend.WinForms.Controls.vButton
    Friend WithEvents COL_DISP_INDX As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COL_LIST_HEADER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COL_LIST_VISIBLE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COL_LIST_WIDTH As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
