﻿Public Class frmOrderDetails

#Region "変数定義"
    Private typOrderPatient() As clsOrder
    Private clsResizer As New AutoResizer
#End Region

#Region "FormEvent"

#Region "FormLoad"
    Private Sub frmOrderDetails_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '///現在設定されているフォントをデフォルトとして退避
        G_FONT = My.Settings.FORM_FONT

        '///フォーム及び各コントロールの初期情報退避
        clsResizer = New AutoResizer(Me)

        Me.Width = My.Settings.FORM_DETAIL_WIDTH
        Me.Height = My.Settings.FORM_DETAIL_HEIGHT

        '///画面の初期化
        Call subInitForm()

        '///データ表示
        Call subDispData()

    End Sub
#End Region

#Region "FormResize"
    Private Sub frmOrderDetails_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        '///フォームと各コントロールのリサイズ
        clsResizer.subResize()

    End Sub
#End Region

#Region "FormResizeEnd"
    Private Sub frmOrderDetails_ResizeEnd(sender As Object, e As System.EventArgs) Handles Me.ResizeEnd
        My.Settings.FORM_DETAIL_HEIGHT = Me.Height
        My.Settings.FORM_DETAIL_WIDTH = Me.Width
        My.Settings.Save()
    End Sub
#End Region

#End Region

#Region "ControlEvents"

#Region "閉じるボタン押下時"
    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        With My.Settings
            .FORM_DETAIL_HEIGHT = Me.Height
            .FORM_DETAIL_WIDTH = Me.Width
            .Save()
        End With
        Me.Close()
    End Sub
#End Region

#Region "編集ボタン(患者コメント)押下時"
    Private Sub cmdPatientCommentEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPatientCommentEdit.Click
        Me.txtPatientComment.Enabled = True
    End Sub
#End Region

#Region "編集ボタン(実施時コメント)押下時"
    Private Sub cmdExcuteCommentEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExcuteCommentEdit.Click
        Me.txtExcuteComment.Enabled = True
    End Sub
#End Region

#Region "患者コメントEnabled変化時"
    Private Sub txtPatientComment_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPatientComment.EnabledChanged
        Me.cmdPatientCommentSave.Enabled = Me.txtPatientComment.Enabled
    End Sub
#End Region

#Region "実施時コメントEnabled変化時"
    Private Sub txtExcuteComment_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtExcuteComment.EnabledChanged
        Me.cmdOrderCommentSave.Enabled = Me.txtExcuteComment.Enabled
    End Sub
#End Region

#Region "過去検査一覧選択時"
    Private Sub lstHist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstHist.SelectedIndexChanged
        Dim strNaigai As String = vbNullString              '内外区分編集用
        Dim strMsg As String = vbNullString

        '///過去検査が無ければ処理を抜ける
        If typOrderPatient.Count = 0 Then Exit Sub

        '///選択された過去データを画面に展開する
        With typOrderPatient(lstHist.SelectedIndex)
            '///進捗
            Select Case .STATE
                Case "0", ""
                    Me.lblState.ForeColor = Color.Black
                    Me.lblState.Text = "未受付"
                Case "1"
                    Me.lblState.ForeColor = Color.Blue
                    Me.lblState.Text = "受付済"
                Case "2"
                    Me.lblState.ForeColor = Color.Green
                    Me.lblState.Text = "作成済"
                Case "3"
                    Me.lblState.ForeColor = Color.Orange
                    Me.lblState.Text = "実施済"
            End Select

            '///オーダ日
            Me.txtOrderDate.Text = .ORDER_DATE.Substring(0, 4) & "/" & .ORDER_DATE.Substring(4, 2) & "/" & .ORDER_DATE.Substring(6, 2)
            '///オーダ時刻
            Me.txtOrderTime.Text = .ORDER_TIME.Substring(0, 2) & ":" & .ORDER_TIME.Substring(2, 2) & ":" & .ORDER_TIME.Substring(4, 2)
            '///実施日
            If .STUDY_DATE = "" Then
                Me.txtExcuteDate.Text = vbNullString
            Else
                Me.txtExcuteDate.Text = .STUDY_DATE.Substring(0, 4) & "/" & .STUDY_DATE.Substring(4, 2) & "/" & .STUDY_DATE.Substring(6, 2)
            End If
            '///実施時刻
            If .STUDY_TIME = "" Then
                Me.txtExcuteTime.Text = vbNullString
            Else
                Me.txtExcuteTime.Text = .STUDY_TIME.Substring(0, 2) & ":" & .STUDY_TIME.Substring(2, 2) & ":" & .STUDY_TIME.Substring(4, 2)
            End If
            ''///検査名
            '           Me.txtStudyName.Text = fncGetStudyNm(.STUDY_NAME)
            ''///部位名
            'Me.txtBodyPart.Text = .BODY_PART
            Dim intRet As Integer = 0
            intRet = fncGetBodyPartDetail(strMsg)

            Dim lngCounter As Long = 0
            Me.txtBodyPart.Text = ""

            Select Case intRet
                Case RET_ERROR
                    MsgBox(strMsg & Space(1) & "lstHist_SelectedIndexChanged", MsgBoxStyle.Critical, "部位明細取得")
                    Exit Sub
                Case RET_NOTFOUND
                    Me.txtBodyPart.Text = ""
                Case Else
                    Dim strData As String = vbNullString
                    For lngCounter = 0 To aryBodyPartDetail.Length - 1
                        Select Case aryBodyPartDetail(lngCounter).STUDY_DIV
                            Case "0"
                                strData = "【単】" & aryBodyPartDetail(lngCounter).BODY_PART_NM
                            Case "1"
                                strData = "【造】" & aryBodyPartDetail(lngCounter).BODY_PART_NM
                            Case "2"
                                strData = "【単+造】" & aryBodyPartDetail(lngCounter).BODY_PART_NM
                            Case Else
                                strData = aryBodyPartDetail(lngCounter).BODY_PART_NM
                        End Select
                        If lngCounter = 0 Then
                            Me.txtBodyPart.Text &= strData
                        Else
                            Me.txtBodyPart.Text &= " + " & strData
                        End If
                    Next lngCounter
            End Select
            Me.toolchipDetail.SetToolTip(Me.txtBodyPart, .BODY_PART)

            '///モダリティ名
            Me.txtModality.Text = .MODALITY_NAME
            '///患者コメント
            If .PATIENT_COMMENT = "" Then
                Me.txtPatientComment.Text = vbNullString
            Else
                Me.txtPatientComment.Text = .PATIENT_COMMENT
                Me.toolchipDetail.SetToolTip(Me.txtPatientComment, .PATIENT_COMMENT)
            End If
            '///実施時コメント
            If .STUDY_COMMENT = "" Then
                Me.txtExcuteComment.Text = vbNullString
            Else
                Me.txtExcuteComment.Text = .STUDY_COMMENT
                Me.toolchipDetail.SetToolTip(Me.txtExcuteComment, .STUDY_COMMENT)
            End If
            '///オーダコメント
            If .ORDER_COMMENT = "" Then
                Me.txtOrderComment.Text = vbNullString
            Else
                Me.txtOrderComment.Text = .ORDER_COMMENT
                Me.toolchipDetail.SetToolTip(Me.txtOrderComment, .ORDER_COMMENT)
            End If
        End With
    End Sub
#End Region

#Region "実施時コメント保存ボタン押下時"
    Private Sub cmdOrderCommentSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOrderCommentSave.Click

        If MsgBox("実施時コメントを保存します。" & vbCrLf & "宜しいですか。" & "尚、過去検査一覧にて選択されている検査に対しての保存となります。", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "実施時コメント保存") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer
        Dim strMsg As String = vbNullString

        strSQL.Clear()
        strSQL.AppendLine("UPDATE TRAN_ORDER ")
        strSQL.AppendLine("SET STUDY_COMMENT = '" & Me.txtExcuteComment.Text.Trim.Replace("'", "''") & "',")
        strSQL.AppendLine("UPD_DATE = CONVERT(char(10), GETDATE(), 112),")
        strSQL.AppendLine("UPD_TIME = REPLACE(CONVERT(varchar,GETDATE(),108),':','') ")
        strSQL.AppendLine("WHERE ")
        strSQL.AppendLine("ORDER_ANO = " & typOrderPatient(Me.lstHist.SelectedIndex).ORDER_ANO & " ")

        intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

        MsgBox("実施時コメントを保存しました。", MsgBoxStyle.Information, "実施時コメント保存")
    End Sub
#End Region

#Region "患者コメント保存ボタン押下時"
    Private Sub cmdPatientCommentSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPatientCommentSave.Click
        If MsgBox("患者コメントを保存します。" & vbCrLf & "宜しいですか。", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "患者コメント保存") = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer
        Dim strMsg As String = vbNullString

        strSQL.Clear()
        strSQL.AppendLine("UPDATE TRAN_PATIENT ")
        strSQL.AppendLine("SET PATIENT_COMMENT = '" & Me.txtPatientComment.Text.Trim & "',")
        strSQL.AppendLine("UPD_DATE = CONVERT(char(10), GETDATE(), 112),")
        strSQL.AppendLine("UPD_TIME = REPLACE(CONVERT(varchar,GETDATE(),108),':','') ")
        strSQL.AppendLine("WHERE ")
        strSQL.AppendLine("PATIENT_ID = '" & Me.txtPatientID.Text & "' ")

        intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

        MsgBox("患者コメントを保存しました。", MsgBoxStyle.Information, "患者コメント保存")
    End Sub
#End Region

#End Region

#Region "Sub Routine"

#Region "画面初期処理(subInitForm)"
    '******************************************************************************************
    '* 機能概要　：画面初期処理
    '* 関数名称　：subInitForm
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/02/15
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '******************************************************************************************
    Private Sub subInitForm()
        '///患者情報エリア
        Me.txtPatientID.Text = vbNullString                 '患者ID
        Me.txtPatientName_Kanji.Text = vbNullString         '患者漢字氏名
        Me.txtPatientName_Half_Kana.Text = vbNullString     '患者半角カナ氏名
        'Me.txtPatientName_Full_Kana.Text = vbNullString     '患者全角カナ氏名
        Me.txtPatientName_Eiji.Text = vbNullString          '患者英字氏名
        Me.txtSex.Text = vbNullString                       '性別
        Me.txtAge.Text = vbNullString                       '受診時年齢
        Me.txtBirth.Text = vbNullString                     '生年月日
        Me.txtPatientComment.Text = vbNullString            '患者コメント

        '///オーダ情報
        'Me.txtOrderNumber.Text = vbNullString               'オーダ番号
        'Me.txtNaiGaiDiv.Text = vbNullString                 '内外区分
        'Me.txtDept.Text = vbNullString                      '診療科
        'Me.txtOrderDoc.Text = vbNullString                  '主治医(依頼医)
        'Me.txtDept.Text = vbNullString                      '依頼元病棟
        'Me.txtNowWard.Text = vbNullString                   '現在所在病棟
        Me.txtOrderDate.Text = vbNullString                 'オーダ日(検査予定日)
        Me.txtOrderTime.Text = vbNullString                 'オーダ時刻(検査予定時刻)
        Me.txtExcuteDate.Text = vbNullString                '検査実施日
        Me.txtExcuteTime.Text = vbNullString                '検査実施時刻
        'Me.txtStudyName.Text = vbNullString                 '検査種別(検査名)
        'Me.txtBodyPart.Text = vbNullString                  '部位名
        Me.lstHist.Items.Clear()                            '過去検査一覧
        Me.txtExcuteComment.Text = vbNullString             '実施時コメント

        '///各コメント入力欄の不活性化
        Me.txtPatientComment.Enabled = False
        Me.txtExcuteComment.Enabled = False

        Me.toolchipDetail.InitialDelay = 1000
        Me.toolchipDetail.ShowAlways = True
    End Sub
#End Region

#Region "画面表示(subDispData)"
    '******************************************************************************************
    '* 機能概要　：画面表示
    '* 関数名称　：subDispData
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/02/15
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '******************************************************************************************
    Private Sub subDispData()
        Dim strSex As String = vbNullString         '性別編集用
        Dim strBirth As String = vbNullString       '生年月日編集用
        Dim strMsg As String = vbNullString         'エラーメッセージ退避用

        With typSelData
            Me.txtPatientID.Text = .PATIENT_ID                      '患者ID
            Me.txtPatientName_Kanji.Text = .PATIENT_KANJI           '漢字氏名
            Me.txtPatientName_Half_Kana.Text = .PATIENT_KANA        'カナ氏名
            Me.txtPatientName_Eiji.Text = .PATIENT_EIJI

            '///性別編集
            If .PATIENT_SEX = "1" Then
                strSex = "男"
            ElseIf .PATIENT_SEX = "2" Then
                strSex = "女"
            ElseIf .PATIENT_SEX = "9" Then
                strSex = "不明"
            Else
                strSex = ""
            End If
            Me.txtSex.Text = strSex                                 '性別

            '///生年月日編集
            strBirth = .PATIENT_BIRTH.Substring(0, 4) & "/" & .PATIENT_BIRTH.Substring(4, 2) & "/" & .PATIENT_BIRTH.Substring(6, 2)
            Me.txtBirth.Text = strBirth                             '生年月日

            '            Me.txtAge.Text = .EXCUTE_AGE                            '受診時年齢
            Me.txtPatientComment.Text = .PATIENT_COMMENT            '患者コメント

            If .STUDY_DATE <> "" Then
                Dim strAge As String = (CLng(.STUDY_DATE.Replace("/", "")) - CLng(.PATIENT_BIRTH.Replace("/", ""))) / 10000
                strAge = strAge.Substring(0, InStr(strAge, ".") - 1)
                Me.txtAge.Text = strAge
            ElseIf .ORDER_DATE <> "" Then
                Dim strAge As String = (CLng(.ORDER_DATE.Replace("/", "")) - CLng(.PATIENT_BIRTH)) / 10000
                strAge = strAge.Substring(0, InStr(strAge, ".") - 1)
                Me.txtAge.Text = strAge
            End If

            '///検査情報表示
            Call subGetHistOrder(.PATIENT_ID, strMsg)

        End With
    End Sub
#End Region

#Region "検査名取得"
    '******************************************************************************************
    '* 機能概要　：検査名取得
    '* 関数名称　：fncGetStudyNm
    '* 引　数　　：strMsg　　　・・・エラーメッセージ退避用
    '* 戻り値　　：無
    '* 作成日付　：2011/02/15
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '******************************************************************************************
    Private Function fncGetStudyNm(ByRef strMsg As String) As String
        Dim strSQL As New System.Text.StringBuilder             'SQL文編集用
        Dim dsData As New DataSet                               'データセット
        Dim strStudyNm As String = vbNullString

        Try

            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("STUDY_TYPE_NM ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_STUDY ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("STUDY_TYPE_CD = '" & typSelData.MODALITY_NAME & "'")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///取得結果が0件時、""を戻す
            If dsData.Tables.Count = 0 Then Return ""
            If dsData.Tables(0).Rows.Count = 0 Then Return ""

            '///取得結果を戻す
            strStudyNm = dsData.Tables(0).Rows(0).Item("STUDY_TYPE_NM")
            Return strStudyNm

        Catch ex As Exception
            strMsg = ex.Message
            Return ""
        Finally
            dsData.Dispose()
            dsData = Nothing
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Function
#End Region

#Region "検査部位取得(fncGetBodyPartNm)"
    '******************************************************************************************
    '* 機能概要　：検査部位取得
    '* 関数名称　：fncGetBodyPartNm
    '* 引　数　　：strMsg　　　・・・エラーメッセージ退避用
    '* 戻り値　　：無
    '* 作成日付　：2011/02/15
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '******************************************************************************************
    Private Function fncGetBodyPartNm(ByRef strMsg As String) As String
        Dim strSQL As New System.Text.StringBuilder                 'SQL文編集用
        Dim dsData As New DataSet                                   'データセット
        Dim strWork() As String                                     '部位コード編集用
        Dim strBodyPart As String = vbNullString                    '部位名称編集用
        Dim intCounter As Integer                                   'カウンタ

        Try

            '///部位コードを分解
            strWork = typSelData.BODY_PART.Split(",")
            For intCounter = 0 To strWork.Length - 1
                If intCounter = strWork.Length - 1 Then
                    strBodyPart &= "'" & strWork(intCounter) & "'"
                Else
                    strBodyPart &= "'" & strWork(intCounter) & "',"
                End If
            Next intCounter

            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("BODY_PART_NM ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_STUDY ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("BODY_PART_CD IN (" & strBodyPart & ")")

            '///SQL文発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///取得件数が0件時は""を戻す
            If dsData.Tables.Count = 0 Then Return ""
            If dsData.Tables(0).Rows.Count = 0 Then Return ""

            strBodyPart = vbNullString

            '///取得結果の連結
            For intCounter = 0 To dsData.Tables(0).Rows.Count - 1
                If intCounter = dsData.Tables(0).Rows.Count - 1 Then
                    strBodyPart &= dsData.Tables(0).Rows(intCounter).Item("BODY_PART_NM")
                Else
                    strBodyPart &= dsData.Tables(0).Rows(intCounter).Item("BODY_PART_NM") & ","
                End If
            Next

            '///連結結果を戻す
            Return strBodyPart

        Catch ex As Exception
            strMsg = ex.Message
            Return ""
        Finally
            dsData.Dispose()
            dsData = Nothing
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Function

#End Region

#Region "過去検査一覧取得"
    '******************************************************************************************
    '* 機能概要　：過去検査一覧取得
    '* 関数名称　：subGetHistOrder
    '* 引　数　　：strPatientID・・・患者ID
    '* 　　　　　：strMsg　　　・・・エラーメッセージ退避用
    '* 戻り値　　：無
    '* 作成日付　：2011/02/15
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '******************************************************************************************
    Private Sub subGetHistOrder(ByVal strPatientID As String, ByRef strMsg As String)
        Dim strSQL As New System.Text.StringBuilder     'SQL文編集用
        Dim dsData As New DataSet                       'データセット
        Dim lngCounter As Long                          'カウンター
        Dim bolFlg As Boolean = False                   '判定フラグ

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("ORDER_ANO,")
            strSQL.AppendLine("ORDER_NO,")
            strSQL.AppendLine("ORDER_DATE,")
            strSQL.AppendLine("ORDER_TIME,")
            strSQL.AppendLine("PATIENT_ANO,")
            strSQL.AppendLine("STUDY_DATE,")
            strSQL.AppendLine("STUDY_TIME,")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("BODY_PART,")
            strSQL.AppendLine("ORDER_COMMENT,")
            strSQL.AppendLine("STATE,")
            strSQL.AppendLine("STUDY_INSTANCE_UID,")
            strSQL.AppendLine("INS_DATE,")
            strSQL.AppendLine("INS_TIME,")
            strSQL.AppendLine("INS_USER,")
            strSQL.AppendLine("UPD_DATE,")
            strSQL.AppendLine("UPD_TIME,")
            strSQL.AppendLine("UPD_USER,")
            strSQL.AppendLine("DEL_FLG,")
            strSQL.AppendLine("PATIENT_ID,")
            strSQL.AppendLine("PATIENT_KANA,")
            strSQL.AppendLine("PATIENT_KANJI,")
            strSQL.AppendLine("PATIENT_EIJI,")
            strSQL.AppendLine("PATIENT_BIRTH,")
            strSQL.AppendLine("PATIENT_SEX,")
            strSQL.AppendLine("BEFOR_KANA,")
            strSQL.AppendLine("BEFOR_KANJI,")
            strSQL.AppendLine("BEFOR_EIJI,")
            strSQL.AppendLine("BEFOR_BIRTH,")
            strSQL.AppendLine("BEFOR_SEX,")
            strSQL.AppendLine("PATIENT_COMMENT,")
            strSQL.AppendLine("MODALITY_NAME,")
            strSQL.AppendLine("MODALITY_CD,")
            strSQL.AppendLine("STUDY_COMMENT,")
            strSQL.AppendLine("TERMINAL ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine(" vwOrder ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine(" PATIENT_ID = '" & strPatientID & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0'")
            strSQL.AppendLine("ORDER BY ORDER_ANO DESC")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///配列構造体の初期化
            Erase typOrderPatient

            '///過去一覧用リストボックスの初期化
            lstHist.Items.Clear()

            '///取得件数が0件ならば処理を抜ける
            If dsData.Tables.Count = 0 Then Exit Sub
            If dsData.Tables(0).Rows.Count = 0 Then Exit Sub

            '///取得データを構造体へ退避
            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve typOrderPatient(lngCounter)

                typOrderPatient(lngCounter) = New clsOrder

                With typOrderPatient(lngCounter)
                    .ORDER_ANO = dsData.Tables(0).Rows(lngCounter).Item("ORDER_ANO")
                    .ORDER_NO = dsData.Tables(0).Rows(lngCounter).Item("ORDER_NO")
                    .ORDER_DATE = dsData.Tables(0).Rows(lngCounter).Item("ORDER_DATE")
                    .ORDER_TIME = dsData.Tables(0).Rows(lngCounter).Item("ORDER_TIME")
                    .PATIENT_ANO = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_ANO")
                    .STUDY_DATE = dsData.Tables(0).Rows(lngCounter).Item("STUDY_DATE")
                    .STUDY_TIME = dsData.Tables(0).Rows(lngCounter).Item("STUDY_TIME")
                    .MODALITY_NO = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_NO")
                    .BODY_PART = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART")
                    .ORDER_COMMENT = dsData.Tables(0).Rows(lngCounter).Item("ORDER_COMMENT")
                    .STATE = dsData.Tables(0).Rows(lngCounter).Item("STATE")
                    .STUDY_INSTANCE_UID = dsData.Tables(0).Rows(lngCounter).Item("STUDY_INSTANCE_UID")
                    .PATIENT_ID = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_ID")
                    .PATIENT_KANA = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_KANA")
                    .PATIENT_KANJI = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_KANJI")
                    .PATIENT_EIJI = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_EIJI")
                    .PATIENT_BIRTH = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_BIRTH")
                    .PATIENT_SEX = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_SEX")
                    .BEFOR_KANA = dsData.Tables(0).Rows(lngCounter).Item("BEFOR_KANA")
                    .BEFOR_KANJI = dsData.Tables(0).Rows(lngCounter).Item("BEFOR_KANJI")
                    .BEFOR_EIJI = dsData.Tables(0).Rows(lngCounter).Item("BEFOR_EIJI")
                    .BEFOR_BIRTH = dsData.Tables(0).Rows(lngCounter).Item("BEFOR_BIRTH")
                    .BEFOR_SEX = dsData.Tables(0).Rows(lngCounter).Item("BEFOR_SEX")
                    .PATIENT_COMMENT = dsData.Tables(0).Rows(lngCounter).Item("PATIENT_COMMENT")
                    .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")
                    .MODALITY_NAME = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_NAME")
                    .TERMINAL = dsData.Tables(0).Rows(lngCounter).Item("TERMINAL")
                    .STUDY_COMMENT = dsData.Tables(0).Rows(lngCounter).Item("STUDY_COMMENT")
                    '///過去一覧リストボックスに表示
                    lstHist.Items.Add("予約日=" & .ORDER_DATE & ",検査名=" & .MODALITY_NAME & ",部位名=" & .BODY_PART)
                End With
            Next lngCounter


        Catch ex As Exception
            strMsg = ex.Message

        Finally
            dsData.Dispose()
            dsData = Nothing
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Sub
#End Region

#Region "モダリティ名取得"
    '******************************************************************************************
    '* 機能概要　：モダリティ
    '* 関数名称　：fncGetModalityNm
    '* 引　数　　：strMsg　　　・・・エラーメッセージ退避用
    '* 戻り値　　：無
    '* 作成日付　：2011/02/15
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '******************************************************************************************
    Private Function fncGetModalityNm(ByRef strMsg As String) As String
        Dim strSQL As New System.Text.StringBuilder             'SQL文編集用
        Dim dsData As New DataSet                               'データセット
        Dim strModalityNm As String = vbNullString

        Try

            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT DISTINCT ")
            strSQL.AppendLine("MODALITY_NM ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_STUDY ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("STUDY_TYPE_CD = '" & typSelData.MODALITY_NAME & "'")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///取得結果が0件時、""を戻す
            If dsData.Tables.Count = 0 Then Return ""
            If dsData.Tables(0).Rows.Count = 0 Then Return ""

            '///取得結果を戻す
            strModalityNm = dsData.Tables(0).Rows(0).Item("MODALITY_NM")
            Return strModalityNm

        Catch ex As Exception
            strMsg = ex.Message
            Return ""
        Finally
            dsData.Dispose()
            dsData = Nothing
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Function
#End Region

#Region "部位明細取得(fncGetBodyPartDetail)"
    '********************************************************************************
    '* 機能概要　：部位明細取得
    '* 関数名称　：fncGetBodyPartDetail
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/25
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetBodyPartDetail(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim lngCounter As Long

        Try
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("BODY_PART_NO,")
            strSQL.AppendLine("BODY_PART_NM,")
            strSQL.AppendLine("MODALITY_CD,")
            strSQL.AppendLine("STUDY_DIV ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("TRAN_BODY_PART_DETAIL ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & typOrderPatient(Me.lstHist.SelectedIndex).ORDER_ANO & " ")
            strSQL.AppendLine("ORDER BY BODY_PART_DETAIL_SEQ ")

            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryBodyPartDetail(lngCounter)
                aryBodyPartDetail(lngCounter) = New clsBodyPart

                With aryBodyPartDetail(lngCounter)
                    .BODY_PART_NO = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NO")
                    .BODY_PART_NM = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NM")
                    .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")
                    .STUDY_DIV = dsData.Tables(0).Rows(lngCounter).Item("STUDY_DIV")
                End With
            Next lngCounter

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try
    End Function
#End Region

#End Region

End Class