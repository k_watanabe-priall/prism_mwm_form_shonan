﻿Public Class frmEditOrder

#Region "定義"
    Private aryModality() As clsModality
    Private typPatient As New clsPatinet
    Private aryBodyPart() As clsBodyPart
    Private typSelOrder As New clsSelOrder
    Private clsResizer As New AutoResizer
    Private strOrderNo As String = vbNullString
#End Region

#Region "Property"
    '///患者ID
    Public Property PATIENT_ID As String
        Get
            Return Me.txtPatientID.Text
        End Get
        Set(value As String)
            Me.txtPatientID.Text = value
        End Set
    End Property
    Public Property KENSHIN_PATIENT_ID As String
        Get
            Return Me.txtKenshinID.Text
        End Get
        Set(value As String)
            Me.txtKenshinID.Text = value
        End Set
    End Property

    '///モダリティ
    Public Property SELECTED_MODALITY As String
        Get
            Return Me.txtModality.Text
        End Get
        Set(value As String)
            Me.txtModality.Text = value
        End Set
    End Property
    '///モダリティ
    Public Property SELECTED_MODALITY_NO As String
        Get
            Return Me.txtModalityNo.Text
        End Get
        Set(value As String)
            Me.txtModalityNo.Text = value
        End Set
    End Property
    '///検査予定日
    Public Property ORDER_DATE As String
        Get
            Return Me.txtOrderDate.Text
        End Get
        Set(value As String)
            Me.txtOrderDate.Text = value
        End Set
    End Property
    '///検査予定時刻
    Public Property ORDER_TIME As String
        Get
            Return Me.txtOrderTime.Text
        End Get
        Set(value As String)
            Me.txtOrderTime.Text = value
            If value <> "" Then
                Me.cmbStudy_H.Text = value.Substring(0, 2)
                Me.cmbStudy_T.Text = value.Substring(2, 2)
            End If
        End Set
    End Property
    '///オーダ登録番号
    Public Property ORDER_ANO As String
        Get
            Return Me.txtOrderAno.Text
        End Get
        Set(value As String)
            Me.txtOrderAno.Text = value
        End Set
    End Property
    Public Property STUDY_DIV As Integer
        Get
            Return Me.txtStudyDiv.Text
        End Get
        Set(value As Integer)
            Me.txtStudyDiv.Text = value
        End Set
    End Property
#End Region

#Region "FormEvents"

#Region "FormLoad"

    Private Sub frmEditOrder_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.cmdAddOrder.Focus()

    End Sub
    Private Sub frmEditOrder_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strMsg As String = vbNullString

        '///現在設定されているフォントをデフォルトとして退避
        G_FONT = My.Settings.FORM_FONT

        '///フォーム及び各コントロールの初期情報退避
        clsResizer = New AutoResizer(Me)

        Me.Width = My.Settings.FORM_EDIT_ORDER_WIDTH
        Me.Height = My.Settings.FORM_EDIT_ORDER_HEIGHT

        '///モダリティ情報取得
        If fncGetModality(strMsg) = RET_ERROR Then
            MsgBox(strMsg & Space(1) & "FormLoad", MsgBoxStyle.Critical, "FormLoad")
            Exit Sub
        End If

        '///部位マスタ取得
        If fncGetBodyPart(strMsg) = RET_ERROR Then
            MsgBox(strMsg & Space(1) & "FormLoad", MsgBoxStyle.Critical, "FormLoad")
            Exit Sub
        End If

        '///画面初期処理
        Call subFormInit()


    End Sub
#End Region

#Region "FormResize"
    Private Sub frmEditOrder_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        '///フォームと各コントロールのリサイズ
        clsResizer.subResize()
    End Sub
#End Region

#Region "FormResizeEnd"
    Private Sub frmEditOrder_ResizeEnd(sender As Object, e As System.EventArgs) Handles Me.ResizeEnd
        My.Settings.FORM_EDIT_ORDER_HEIGHT = Me.Height
        My.Settings.FORM_EDIT_ORDER_WIDTH = Me.Width
        My.Settings.Save()
    End Sub
#End Region

#End Region

#Region "ControlEvents"

#Region "クリアボタン(患者情報)押下"
    Private Sub cmdPatientClear_Click(sender As System.Object, e As System.EventArgs) Handles cmdPatientClear.Click
        '///患者情報表示エリアの全クリア
        Me.txtPatientID.Text = vbNullString
        Me.txtKanji.Text = vbNullString
        Me.txtKana.Text = vbNullString
        Me.txtEiji.Text = vbNullString
        Me.txtBirth.Text = vbNullString
        Me.txtSex.Text = vbNullString
    End Sub
#End Region

#Region "クリアボタン(オーダ情報)押下"
    Private Sub cmdOrderClear_Click(sender As System.Object, e As System.EventArgs) Handles cmdOrderClear.Click
        '///オーダ情報入力エリアの全クリア
        Me.cmbModality.SelectedIndex = -1
        Me.dtOrderDate.Value = Date.Today.ToString("yyyy/MM/dd")
        Me.cmbStudy_H.Text = Now.Hour.ToString.PadLeft(2, "0"c)
        Me.cmbStudy_T.Text = Now.Minute.ToString.PadLeft(2, "0"c)
        'Me.cmbBodyPart.SelectedIndex = -1
        Me.txtOrderComment.Text = vbNullString
        Me.txtBodyPart.Text = vbNullString
        Me.lstBodyPart.Items.Clear()

        Me.txtOrderAno.Text = vbNullString
        Me.txtModality.Text = vbNullString
        Me.txtModalityNo.Text = vbNullString
        Me.txtOrderDate.Text = vbNullString
        Me.txtOrderTime.Text = vbNullString
    End Sub
#End Region

#Region "検査室変更時"
    Private Sub cmbModality_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbModality.SelectedIndexChanged
        Dim strMsg As String = vbNullString
        If Me.cmbModality.SelectedIndex = -1 Then Exit Sub
        Me.txtModality.Text = aryModality(Me.cmbModality.SelectedIndex).MODALITY_CD
        Me.txtModalityNo.Text = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO

        '///選択モダリティが変わったら部位プルダウンの中身も変える
        If fncGetBodyPart(strMsg) = RET_ERROR Then

        End If
    End Sub
#End Region

#Region "患者ID KeyDown"
    Private Sub txtPatientID_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtPatientID.KeyDown
        Dim strMsg As String = vbNullString
        Dim intRet As Integer = 0

        If e.KeyCode = Keys.Enter Then
            If Me.txtPatientID.TextLength < Me.txtPatientID.MaxLength Then
                'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength, "0"c)
                '///職員(先頭1桁"K")以外は先頭0埋め
                If Me.txtPatientID.Text <> "" And Me.txtPatientID.TextLength > 2 And IsNumeric(Me.txtPatientID.Text) = False Then
                    If Not Me.txtPatientID.Text.Substring(0, 1).ToUpper = "K" And _
                       Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "SA" And _
                       Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "TE" Then
                        Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength, "0"c)
                    End If
                ElseIf Me.txtPatientID.Text <> "" And IsNumeric(Me.txtPatientID.Text) = True Then
                    Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength, "0"c)
                Else
                    Me.txtPatientID.Text = Me.txtPatientID.Text.ToUpper
                End If

                intRet = fncGetPatient(strMsg)
                Select Case intRet
                    Case RET_ERROR
                        MsgBox(strMsg & Space(1) & "txtPatientID_KeyDown", MsgBoxStyle.Critical, "患者情報取得")
                        Me.Close()
                        Exit Sub
                    Case RET_NOTFOUND
                        MsgBox("入力された患者IDに該当するデータが見つかりませんでした。" & vbCrLf & _
                               "ご確認の上、再入力して下さい。", MsgBoxStyle.Exclamation, "患者情報取得")
                        Exit Sub
                    Case Else
                        With typPatient
                            Me.txtKanji.Text = .PATIENT_KANJI
                            Me.txtKana.Text = .PATIENT_KANA
                            Me.txtEiji.Text = .PATIENT_EIJI

                            Select Case .PATIENT_SEX
                                Case 1
                                    Me.txtSex.Text = "男"
                                Case 2
                                    Me.txtSex.Text = "女"
                                Case 9
                                    Me.txtSex.Text = "不明"
                                Case Else
                                    Me.txtSex.Text = ""
                            End Select

                            Me.txtBirth.Text = .PATIENT_BIRTH.Substring(0, 4) & "/" & _
                                             .PATIENT_BIRTH.Substring(4, 2) & "/" & _
                                             .PATIENT_BIRTH.Substring(6, 2)
                            Me.txtPatientComment.Text = .PATIENT_COMMENT
                        End With
                End Select
            Else
            End If
        End If
    End Sub
#End Region

#Region "患者ID KeyPress"
    Private Sub txtPatientID_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPatientID.KeyPress
        'If (e.KeyChar < "0"c Or e.KeyChar > "9"c) Then
        '    If e.KeyChar <> vbBack And e.KeyChar <> Chr(13) Then
        '        e.Handled = True
        '    End If
        'End If

    End Sub
#End Region

#Region "オーダ登録ボタン押下"
    Private Sub cmdAddOrder_Click(sender As System.Object, e As System.EventArgs) Handles cmdAddOrder.Click
        Dim strMsg As String = vbNullString

        '/////////////// 入力チェック ///////////////
        If Me.cmbModality.SelectedIndex = -1 Then
            MsgBox("検査室を選択して下さい。", vbExclamation, "入力チェック")
            Me.cmbModality.Focus()
            Exit Sub
        End If

        If Me.cmbStudy_H.Text = "" Or Me.cmbStudy_T.Text = "" Then
            MsgBox("検査時刻を選択して下さい。", vbExclamation, "入力チェック")
            Me.cmbStudy_H.Focus()
            Exit Sub
        End If

        'If Me.cmbBodyPart.Text = "" Then
        '    MsgBox("検査部位を選択して下さい。", vbExclamation, "入力チェック")
        '    Me.cmbBodyPart.Focus()
        'End If
        'If Me.txtBodyPart.Text = "" Then
        '    MsgBox("検査部位を入力して下さい。", vbExclamation, "入力チェック")
        '    Me.txtBodyPart.Focus()
        '    Exit Sub
        'End If
        Erase aryUpdBodyPartDetail
        Dim lngCounter As Long = 0

        aryUpdBodyPartDetail = aryBodyPartDetail

        If Me.txtOrderAno.Text = "" Then
            If CLng(Me.dtOrderDate.Value.ToString("yyyyMMdd")) < Now.ToString("yyyyMMdd") Then
                MsgBox("過去のオーダは登録できません。", vbExclamation, "入力チェック")
                Me.dtOrderDate.Focus()
                Exit Sub
            End If

            '///オーダ登録
            If fncInsOrder(strMsg) = RET_ERROR Then
                MsgBox(strMsg & Space(1) & "オーダ登録処理(cmdAddOrder_Click)", MsgBoxStyle.Critical, "オーダ登録")
                Exit Sub
            End If

            If fncgetOrderAno(strMsg) = RET_ERROR Then
                MsgBox(strMsg & Space(1) & "オーダ登録番号取得処理(cmdAddOrder_Click)", MsgBoxStyle.Critical, "登録番号取得処理")
                Exit Sub
            End If

            If aryUpdBodyPartDetail IsNot Nothing Then
                If fncUpdBodyPartDetail(strMsg) = RET_ERROR Then
                    MsgBox(strMsg & Space(1) & "部位明細更新処理(cmdAddOrder_Click)", MsgBoxStyle.Critical, "部位明細更新処理")
                    Exit Sub
                End If
            End If
            'MsgBox("オーダ情報の登録が完了しました。", MsgBoxStyle.Information, "オーダ情報登録")

        Else
            Dim intRet As Integer

            intRet = fncUpdOrder(strMsg)

            Select Case intRet
                Case RET_ERROR
                    MsgBox(strMsg & Space(1) & "オーダ更新処理(cmdAddOrder_Click)", MsgBoxStyle.Critical, "オーダ情報更新")
                    Exit Sub
                Case RET_NOTFOUND
                    '///オーダ登録
                    If fncInsOrder(strMsg) = RET_ERROR Then
                        MsgBox(strMsg & Space(1) & "オーダ登録処理(cmdAddOrder_Click)", MsgBoxStyle.Critical, "オーダ登録")
                        Exit Sub
                    End If
            End Select

            If aryUpdBodyPartDetail IsNot Nothing Then
                intRet = fncUpdBodyPartDetail(strMsg)
                If intRet = RET_ERROR Then
                    MsgBox(strMsg & Space(1) & "部位明細更新処理(cmdAddOrder_Click)", MsgBoxStyle.Critical, "部位明細更新処理")
                    Exit Sub
                End If
            End If
            'MsgBox("オーダ情報の更新が完了しました。", MsgBoxStyle.Information, "オーダ情報更新")
        End If

        G_STUDY_DIV = vbNullString
        Me.Close()
    End Sub
#End Region

#Region "時刻入力時"
    Private Sub cmbStudy_H_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cmbStudy_H.KeyPress, cmbStudy_T.KeyPress
        If (e.KeyChar < "0"c Or e.KeyChar > "9"c) Then
            If e.KeyChar <> vbBack And e.KeyChar <> Chr(13) Then
                e.Handled = True
            End If
        End If

    End Sub
#End Region

#Region "閉じるボタン押下"
    Private Sub cmdClose_Click(sender As System.Object, e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub
#End Region

#Region "部位選択ボタン押下"
    Private Sub cmdBodyPart_Click(sender As System.Object, e As System.EventArgs) Handles cmdBodyPart.Click
        Dim lngCounter As Long = 0
        Dim strBodyPart As String = vbNullString

        If Me.lstBodyPart.Items.Count = 0 Then
            Erase aryBodyPartDetail
        End If
        frmSelectBodyPart.SELECTED_MODALITY = Me.txtModalityNo.Text
        frmSelectBodyPart.ShowDialog()

        If aryBodyPartDetail IsNot Nothing Then
            Me.lstBodyPart.Items.Clear()
            For lngCounter = 0 To aryBodyPartDetail.Length - 1
                Dim strData As String = vbNullString

                If aryBodyPartDetail(lngCounter).STUDY_DIV = "0" Then
                    strData = "【単】" & aryBodyPartDetail(lngCounter).BODY_PART_NM
                ElseIf aryBodyPartDetail(lngCounter).STUDY_DIV = "1" Then
                    strData = "【造】" & aryBodyPartDetail(lngCounter).BODY_PART_NM
                ElseIf aryBodyPartDetail(lngCounter).STUDY_DIV = "2" Then
                    strData = "【単+造】" & aryBodyPartDetail(lngCounter).BODY_PART_NM
                ElseIf aryBodyPartDetail(lngCounter).STUDY_DIV = "3" Then
                    strData = "【Dynamic】" & aryBodyPartDetail(lngCounter).BODY_PART_NM
                Else
                    strData = aryBodyPartDetail(lngCounter).BODY_PART_NM
                End If
                Me.lstBodyPart.Items.Add(strData)
                If strBodyPart <> "" Then
                    strBodyPart &= " + " & strData
                Else
                    strBodyPart = strData
                End If
            Next
        End If

        Me.txtBodyPart.Text = strBodyPart
    End Sub
#End Region

#End Region

#Region "Sub Routine"

#Region "画面初期処理(subFormInit)"
    '********************************************************************************
    '* 機能概要　：画面初期処理
    '* 関数名称　：subFormInit
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日　　：2012/01/20
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Sub subFormInit()
        Dim strMsg As String = vbNullString
        Dim sender As New System.Object
        Dim e As New System.EventArgs

        '///患者情報エリア
        If PATIENT_ID = "" Then

            Call cmdPatientClear_Click(sender, e)
        Else
            If fncGetPatient(strMsg) = RET_ERROR Then
                MsgBox(strMsg & Space(1) & "subFormInit", MsgBoxStyle.Critical, "subFormInit")
                Exit Sub
            End If
        End If

        If Me.txtModalityNo.Text = "" Then
            Me.cmbModality.SelectedIndex = -1
        Else
            Dim intCounter As Integer
            For intCounter = 0 To aryModality.Length - 1
                If aryModality(intCounter).MODALITY_NO = Me.txtModalityNo.Text Then

                    Me.cmbModality.SelectedIndex = intCounter
                    Exit For
                End If
            Next intCounter
        End If
        '///オーダ情報
        Erase aryBodyPartDetail     '///ADD By Watanabe 2012.02.04

        If Me.txtOrderAno.Text = "" Then
            'If Me.txtModalityNo.Text = "" Then
            '    Me.cmbModality.SelectedIndex = -1
            'Else
            '    Dim intCounter As Integer
            '    For intCounter = 0 To aryModality.Length - 1
            '        If aryModality(intCounter).MODALITY_NO = Me.txtModalityNo.Text Then

            '            Me.cmbModality.SelectedIndex = intCounter
            '            Exit For
            '        End If
            '    Next intCounter
            'End If

            If Me.txtOrderDate.Text = "" Then
                Me.dtOrderDate.Value = Now
            Else
                Me.dtOrderDate.Value = Me.txtOrderDate.Text.Substring(0, 4) & "/" & Me.txtOrderDate.Text.Substring(4, 2) & "/" & Me.txtOrderDate.Text.Substring(6, 2)
            End If

            If Me.txtOrderTime.Text = "" Then
                Me.cmbStudy_H.Text = Now.Hour
                Me.cmbStudy_T.Text = Now.Minute
            Else
                Me.cmbStudy_H.Text = Me.txtOrderTime.Text.Substring(0, 2)
                Me.cmbStudy_T.Text = Me.txtOrderTime.Text.Substring(2, 2)
            End If

            Me.cmbBodyPart.SelectedIndex = -1
            Me.txtOrderComment.Text = vbNullString

            '///隠しコントロール
            'Me.txtModality.Text = vbNullString
            'Me.txtModalityNo.Text = vbNullString
            'Me.txtOrderDate.Text = vbNullString
            'Me.txtOrderTime.Text = vbNullString

            Dim lngCounter As Long = 0

            For lngCounter = 0 To aryModality.Length - 1
                If aryModality(lngCounter).MODALITY_NO = Me.txtModalityNo.Text Then
                    Me.cmbModality.SelectedIndex = lngCounter
                    Exit For
                End If
            Next lngCounter

            Me.lstBodyPart.Items.Clear()

        Else
            Me.lstBodyPart.Items.Clear()

            If fncGetOrder(strMsg) = RET_ERROR Then
                MsgBox(strMsg & Space(1) & "subFormInit", MsgBoxStyle.Critical, "subFormInit")
                Exit Sub
            End If
            If fncGetBodyPartDetail(strMsg) = RET_ERROR Then
                MsgBox(strMsg & Space(1) & "subFormInit", MsgBoxStyle.Critical, "subFormInit")
                Exit Sub
            End If
        End If
    End Sub
#End Region

#Region "モダリティ情報取得(fncGetModality)"
    '********************************************************************************
    '* 機能概要　：モダリティ情報取得
    '* 関数名称　：subGetModality
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetModality(ByRef strMsg As String, Optional intProc As Integer = 0) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try

            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("MODALITY_CD,")
            strSQL.AppendLine("MODALITY_NAME,")
            strSQL.AppendLine("TERMINAL,")
            strSQL.AppendLine("ROOM_NAME ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_MODALITY ")

            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            Dim lngCounter As Integer = 0
            Erase aryModality
            Me.cmbModality.Items.Clear()

            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryModality(lngCounter)

                aryModality(lngCounter) = New clsModality

                With aryModality(lngCounter)
                    .MODALITY_NO = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_NO")
                    .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")
                    .MODALITY_NAME = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_NAME")
                    .TERMINAL = dsData.Tables(0).Rows(lngCounter).Item("TERMINAL")
                    .ROOM_NAME = dsData.Tables(0).Rows(lngCounter).Item("ROOM_NAME")

                    Me.cmbModality.Items.Add(.ROOM_NAME)
                End With
            Next lngCounter

            '///メイン画面での検査室を初期選択させる
            If Me.txtModality.Text <> "" Then
                For lngCounter = 0 To aryModality.Length - 1
                    If Me.txtModalityNo.Text = aryModality(lngCounter).MODALITY_NO Then
                        Me.cmbModality.SelectedIndex = lngCounter
                        Exit For
                    End If
                Next lngCounter
            End If

            Return RET_NORMAL
        Catch ex As Exception
            Call subOutLog(ex.Message & Space(1) & "モダリティ情報取得(subGetModality)", 1)
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try

    End Function

#End Region

#Region "患者情報取得(fncGetPatient)"
    '********************************************************************************
    '* 機能概要　：患者情報取得
    '* 関数名称　：fncGetPatient
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetPatient(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("PATIENT_ANO,")
            strSQL.AppendLine("PATIENT_ID,")
            strSQL.AppendLine("PATIENT_KANA,")
            strSQL.AppendLine("PATIENT_KANJI,")
            strSQL.AppendLine("PATIENT_EIJI,")
            strSQL.AppendLine("PATIENT_BIRTH,")
            strSQL.AppendLine("PATIENT_SEX,")
            strSQL.AppendLine("ISNULL(PATIENT_COMMENT,'') AS PATIENT_COMMENT ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("TRAN_PATIENT ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("PATIENT_ID = '" & Me.txtPatientID.Text.Trim & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")
            '///----- START ----- 同一患者IDの患者情報変更時の不具合対応 Yanagihori 2012.02.23
            strSQL.AppendLine("ORDER BY PATIENT_ANO DESC ")
            '///----- E N D ----- 同一患者IDの患者情報変更時の不具合対応 Yanagihori 2012.02.23

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///未存在チェック
            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            '///取得データを構造体に展開
            With typPatient
                .PATIENT_ANO = dsData.Tables(0).Rows(0).Item("PATIENT_ANO")                 '患者登録番号
                .PATIENT_KANA = dsData.Tables(0).Rows(0).Item("PATIENT_KANA")               'カナ氏名
                .PATIENT_KANJI = dsData.Tables(0).Rows(0).Item("PATIENT_KANJI")             '漢字氏名
                .PATIENT_EIJI = dsData.Tables(0).Rows(0).Item("PATIENT_EIJI")               '英字氏名
                .PATIENT_SEX = dsData.Tables(0).Rows(0).Item("PATIENT_SEX")                 '性別
                .PATIENT_BIRTH = dsData.Tables(0).Rows(0).Item("PATIENT_BIRTH")             '生年月日


                '///取得データを各表示エリアに展開
                Me.txtKana.Text = .PATIENT_KANA                                             'カナ氏名
                Me.txtKanji.Text = .PATIENT_KANJI                                           '漢字氏名
                Me.txtEiji.Text = .PATIENT_EIJI                                             '英字氏名
                Dim strBirth As String = .PATIENT_BIRTH
                Me.txtBirth.Text = strBirth.Substring(0, 4) & "/" & _
                                   strBirth.Substring(4, 2) & "/" & _
                                   strBirth.Substring(6, 2)                                 '生年月日

                Select Case .PATIENT_SEX                                                    '性別
                    Case 0, ""
                        Me.txtSex.Text = ""
                    Case 1
                        Me.txtSex.Text = "男"
                    Case 2
                        Me.txtSex.Text = "女"
                    Case 9
                        Me.txtSex.Text = "不明"
                End Select
            End With

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
        End Try
    End Function
#End Region

#Region "部位マスタ取得(fncGetBodyPart)"
    '********************************************************************************
    '* 機能概要　：部位マスタ取得
    '* 関数名称　：fncGetBodyPart
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetBodyPart(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim lngCounter As Long

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("BODY_PART_NO,")
            strSQL.AppendLine("BODY_PART_NM,")
            strSQL.AppendLine("MODALITY_CD ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_BODY_PART ")
            strSQL.AppendLine("WHERE ")

            If Me.txtModality.Text <> "" Then
                strSQL.AppendLine("MODALITY_CD = '" & Me.txtModality.Text & "' ")
                strSQL.AppendLine("AND ")
            End If

            strSQL.AppendLine("DEL_FLG = '0' ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///取得データチェック
            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            '///取得データ構造体退避
            Erase aryBodyPart
            Me.cmbBodyPart.Items.Clear()
            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryBodyPart(lngCounter)
                aryBodyPart(lngCounter) = New clsBodyPart

                With aryBodyPart(lngCounter)
                    .BODY_PART_NO = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NO")
                    .BODY_PART_NM = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NM")
                    .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")

                    Me.cmbBodyPart.Items.Add(.BODY_PART_NM)
                End With
            Next lngCounter

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try
    End Function
#End Region

#Region "オーダ情報取得(fncGetOrder)"
    '********************************************************************************
    '* 機能概要　：オーダ情報取得
    '* 関数名称　：fncGetOrder
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetOrder(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("ORDER_ANO,")
            strSQL.AppendLine("ORDER_NO,")
            strSQL.AppendLine("ORDER_DATE,")
            strSQL.AppendLine("ORDER_TIME,")
            strSQL.AppendLine("PATIENT_ANO,")
            strSQL.AppendLine("STUDY_DATE,")
            strSQL.AppendLine("STUDY_TIME,")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("BODY_PART,")
            strSQL.AppendLine("ISNULL(ORDER_COMMENT,'') AS ORDER_COMMENT,")
            strSQL.AppendLine("STATE,")
            strSQL.AppendLine("STUDY_INSTANCE_UID,")
            strSQL.AppendLine("TERMINAL ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("TRAN_ORDER ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & CInt(Me.txtOrderAno.Text))

            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            With typSelOrder
                .ORDER_ANO = dsData.Tables(0).Rows(0).Item("ORDER_ANO")
                .ORDER_NO = dsData.Tables(0).Rows(0).Item("ORDER_NO")
                .ORDER_DATE = dsData.Tables(0).Rows(0).Item("ORDER_DATE")
                .ORDER_TIME = dsData.Tables(0).Rows(0).Item("ORDER_TIME")
                .PATIENT_ANO = dsData.Tables(0).Rows(0).Item("PATIENT_ANO")
                .STUDY_DATE = dsData.Tables(0).Rows(0).Item("STUDY_DATE")
                .STUDY_TIME = dsData.Tables(0).Rows(0).Item("STUDY_TIME")
                .MODALITY_NO = dsData.Tables(0).Rows(0).Item("MODALITY_NO")
                .BODY_PART = dsData.Tables(0).Rows(0).Item("BODY_PART")
                .ORDER_COMMENT = dsData.Tables(0).Rows(0).Item("ORDER_COMMENT")
                .STATE = dsData.Tables(0).Rows(0).Item("STATE")
                .STUDY_INSTANCE_UID = dsData.Tables(0).Rows(0).Item("STUDY_INSTANCE_UID")
                .TERMINAL = dsData.Tables(0).Rows(0).Item("TERMINAL")

                Me.cmbStudy_H.Text = ""
                Me.cmbStudy_T.Text = ""
                Me.cmbBodyPart.Text = ""

                Me.dtOrderDate.Value = .ORDER_DATE.Substring(0, 4) & "/" & .ORDER_DATE.Substring(4, 2) & "/" & .ORDER_DATE.Substring(6, 2)
                Me.cmbStudy_H.Text = .ORDER_TIME.Substring(0, 2)
                Me.cmbStudy_T.Text = .ORDER_TIME.Substring(2, 2)
                Me.txtOrderComment.Text = dsData.Tables(0).Rows(0).Item("ORDER_COMMENT")
                Me.txtBodyPart.Text = .BODY_PART
                Me.cmbBodyPart.Text = .BODY_PART
                Dim intCounter As Integer

                For intCounter = 0 To aryModality.Length - 1
                    If .MODALITY_NO = aryModality(intCounter).MODALITY_NO Then
                        Me.cmbModality.SelectedIndex = intCounter
                        Exit For
                    End If
                Next
            End With

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
        End Try
    End Function
#End Region

#Region "オーダ情報登録処理(fncInsOrder)"
    '********************************************************************************
    '* 機能概要　：オーダ情報登録処理
    '* 関数名称　：fncInsOrder
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncInsOrder(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer
        Dim strWork As String = vbNullString
        Dim strDate As String()
        Dim dsData As New DataSet

        Try
            strWork = fncGetServerDate(2, strMsg)

            strDate = strWork.Split(",")

            If G_STUDY_DIV = 0 Then
                strOrderNo = "S" & strDate(0).Trim.Substring(2, 6) & strDate(1).Trim & aryModality(Me.cmbModality.SelectedIndex).MODALITY_CD
            Else
                strOrderNo = "K" & strDate(0).Trim.Substring(2, 6) & strDate(1).Trim & aryModality(Me.cmbModality.SelectedIndex).MODALITY_CD
            End If
            strSQL.Clear()
            strSQL.AppendLine("INSERT INTO TRAN_ORDER ")
            strSQL.AppendLine("(")
            strSQL.AppendLine("ORDER_NO,")
            strSQL.AppendLine("ORDER_DATE,")
            strSQL.AppendLine("ORDER_TIME,")
            strSQL.AppendLine("PATIENT_ANO,")
            strSQL.AppendLine("STUDY_DATE,")
            strSQL.AppendLine("STUDY_TIME,")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("BODY_PART,")
            strSQL.AppendLine("ORDER_COMMENT,")
            strSQL.AppendLine("STATE,")
            strSQL.AppendLine("STUDY_INSTANCE_UID,")
            strSQL.AppendLine("TERMINAL,")
            strSQL.AppendLine("INS_DATE,")
            strSQL.AppendLine("INS_TIME,")
            strSQL.AppendLine("INS_USER,")
            strSQL.AppendLine("STUDY_DIV,")
            strSQL.AppendLine("DEL_FLG ")
            'strSQL.AppendLine("FROM ")
            'strSQL.AppendLine("TRAN_ORDER ")
            strSQL.AppendLine(")")
            strSQL.AppendLine("VALUES ")
            strSQL.AppendLine("(")
            strSQL.AppendLine("'" & strOrderNo & "',")
            strSQL.AppendLine("'" & Me.dtOrderDate.Value.ToString("yyyyMMdd") & "',")
            strSQL.AppendLine("'" & Me.cmbStudy_H.Text.PadLeft(2, "0"c) & Me.cmbStudy_T.Text.PadLeft(2, "0"c) & "00',")
            strSQL.AppendLine(typPatient.PATIENT_ANO & ",")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'" & aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO & "',")
            strSQL.AppendLine("'" & Me.txtBodyPart.Text & "',")
            strSQL.AppendLine("'" & Me.txtOrderComment.Text.Replace("'", "''") & "',")
            strSQL.AppendLine("'0',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'" & My.Settings.TERMINAL & "',")
            strSQL.AppendLine("'" & strDate(0).Trim & "',")
            strSQL.AppendLine("'" & strDate(1).Trim & "',")
            strSQL.AppendLine("'" & typUserInf.USER_ID & "',")
            strSQL.AppendLine("'" & G_STUDY_DIV & "',")
            strSQL.AppendLine("'0'")
            strSQL.AppendLine(")")

            strOrderNo = strOrderNo

            '///SQL文発行
            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Function
#End Region

#Region "オーダ情報更新処理(fncUpdOrder)"
    '********************************************************************************
    '* 機能概要　：オーダ情報登録処理
    '* 関数名称　：fncInsOrder
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncUpdOrder(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim intRet As Integer = 0

        Try
            '///存在チェック用SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("COUNT(ORDER_ANO) AS CNT ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("TRAN_ORDER ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & Me.txtOrderAno.Text & " ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///データが未存在ならば処理を抜ける
            If dsData.Tables(0).Rows(0).Item("CNT") = 0 Then Return RET_NOTFOUND

            '///更新用SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("UPDATE TRAN_ORDER ")
            strSQL.AppendLine("SET ")
            strSQL.AppendLine("MODALITY_NO = " & aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO & ",")
            strSQL.AppendLine("ORDER_DATE = '" & Me.dtOrderDate.Value.ToString("yyyyMMdd") & "',")
            strSQL.AppendLine("ORDER_TIME = '" & Me.cmbStudy_H.Text.PadLeft(2, "0"c) & Me.cmbStudy_T.Text.PadLeft(2, "0"c) & "00',")
            strSQL.AppendLine("BODY_PART = '" & Me.txtBodyPart.Text & "',")
            strSQL.AppendLine("ORDER_COMMENT = '" & Me.txtOrderComment.Text.Replace("'", "''") & "',")
            strSQL.AppendLine("TERMINAL = '" & My.Settings.TERMINAL & "',")
            strSQL.AppendLine("UPD_DATE = '" & Now.ToString("yyyyMMdd") & "',")
            strSQL.AppendLine("UPD_TIME = '" & Now.ToString("HHmmss") & "',")
            strSQL.AppendLine("UPD_USER = '" & typUserInf.USER_ID & "' ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & Me.txtOrderAno.Text & " ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")

            '///SQL発行
            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try
    End Function
#End Region

#Region "部位明細取得(fncGetBodyPartDetail)"
    '********************************************************************************
    '* 機能概要　：部位明細取得
    '* 関数名称　：fncGetBodyPartDetail
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/25
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetBodyPartDetail(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim lngCounter As Long

        Try
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("BODY_PART_NO,")
            strSQL.AppendLine("BODY_PART_NM,")
            strSQL.AppendLine("MODALITY_CD,")
            strSQL.AppendLine("STUDY_DIV ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("TRAN_BODY_PART_DETAIL ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & Me.txtOrderAno.Text & " ")
            strSQL.AppendLine("ORDER BY BODY_PART_DETAIL_SEQ ")

            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            Me.lstBodyPart.Items.Clear()
            Me.txtBodyPart.Text = vbNullString
            Erase aryBodyPartDetail     '///ADD By Watanabe 2012.02.01

            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryBodyPartDetail(lngCounter)
                aryBodyPartDetail(lngCounter) = New clsBodyPart

                With aryBodyPartDetail(lngCounter)
                    .BODY_PART_NO = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NO")
                    .BODY_PART_NM = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NM")
                    .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")
                    .STUDY_DIV = dsData.Tables(0).Rows(lngCounter).Item("STUDY_DIV")

                    Dim strData As String = vbNullString

                    Select Case .STUDY_DIV
                        Case "0"
                            strData = "【単】" & .BODY_PART_NM
                        Case "1"
                            strData = "【造】" & .BODY_PART_NM
                        Case "2"
                            strData = "【単+造】" & .BODY_PART_NM
                        Case "3"
                            strData = "【Dynamic】" & .BODY_PART_NM
                        Case Else
                            strData = .BODY_PART_NM
                    End Select
                    If Me.txtBodyPart.Text = "" Then
                        Me.txtBodyPart.Text = strData
                    Else
                        Me.txtBodyPart.Text &= "," & strData
                    End If

                    Me.lstBodyPart.Items.Add(strData)
                End With
            Next lngCounter

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try
    End Function
#End Region

#Region "部位明細更新(fncUpdBodyPartDetail)"
    '********************************************************************************
    '* 機能概要　：部位明細更新
    '* 関数名称　：fncUpdBodyPartDetail
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/25
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncUpdBodyPartDetail(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim bolFlg As Boolean = False
        Dim intRet As Integer = 0
        Dim lngCounter As Long = 0

        Try
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("COUNT(ORDER_ANO) AS CNT ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("TRAN_BODY_PART_DETAIL ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & txtOrderAno.Text & " ")

            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables(0).Rows(0).Item("CNT") > 0 Then
                bolFlg = True
            Else
                bolFlg = False
            End If

            If bolFlg = True Then          'データ存在時は、既存データを削除(更新方法：DELETE INSERT)
                '///削除用SQL文生成
                strSQL.Clear()
                strSQL.AppendLine("DELETE ")
                strSQL.AppendLine("FROM ")
                strSQL.AppendLine("TRAN_BODY_PART_DETAIL ")
                strSQL.AppendLine("WHERE ")
                strSQL.AppendLine("ORDER_ANO = " & txtOrderAno.Text & " ")

                '///SQL発行
                intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            End If

            If aryUpdBodyPartDetail IsNot Nothing Then
                For lngCounter = 0 To aryUpdBodyPartDetail.Length - 1

                    With aryUpdBodyPartDetail(lngCounter)
                        strSQL.Clear()
                        strSQL.AppendLine("INSERT INTO TRAN_BODY_PART_DETAIL ")
                        strSQL.AppendLine("( ")
                        strSQL.AppendLine("ORDER_ANO,")
                        strSQL.AppendLine("BODY_PART_DETAIL_SEQ,")
                        strSQL.AppendLine("BODY_PART_NO,")
                        strSQL.AppendLine("BODY_PART_NM,")
                        strSQL.AppendLine("STUDY_DIV,")
                        strSQL.AppendLine("MODALITY_CD ")
                        strSQL.AppendLine(") ")
                        strSQL.AppendLine("VALUES ")
                        strSQL.AppendLine("( ")
                        strSQL.AppendLine("" & Me.txtOrderAno.Text & ",")
                        strSQL.AppendLine("" & lngCounter + 1 & ",")
                        strSQL.AppendLine("" & .BODY_PART_NO & ",")
                        strSQL.AppendLine("'" & .BODY_PART_NM & "',")
                        strSQL.AppendLine("'" & .STUDY_DIV & "',")
                        strSQL.AppendLine("'" & .MODALITY_CD & "' ")
                        strSQL.AppendLine(") ")

                    End With
                    intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)
                Next lngCounter

            End If
            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try
    End Function
#End Region

#Region "オーダ登録番号取得(fncGetOrderAno)"
    Private Function fncgetOrderAno(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            strSQL.Clear()
            strSQL.AppendLine("SELECT ORDER_ANO ")
            strSQL.AppendLine("FROM TRAN_ORDER ")
            strSQL.AppendLine("WHERE ORDER_NO = '" & strOrderNo & "'")

            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            Me.txtOrderAno.Text = dsData.Tables(0).Rows(0).Item("ORDER_ANO")
            Me.ORDER_ANO = Me.txtOrderAno.Text

            strOrderNo = vbNullString

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        End Try
    End Function
#End Region

#End Region

End Class