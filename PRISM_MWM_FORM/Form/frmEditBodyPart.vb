﻿Public Class frmEditBodyPart

    '///当該画面が2012.03.01に追加
    '///技師長より一覧画面から部位のみを修正する旨要望有の為
#Region "Property"
    '///患者ID
    Public Property PATIENT_ID As String
        Get
            Return Me.txtPatientID.Text
        End Get
        Set(value As String)
            Me.txtPatientID.Text = value
        End Set
    End Property

    '///モダリティ
    Public Property SELECTED_MODALITY As String
        Get
            Return Me.txtModality.Text
        End Get
        Set(value As String)
            Me.txtModality.Text = value
        End Set
    End Property
    '///モダリティ
    Public Property SELECTED_MODALITY_NO As String
        Get
            Return Me.txtModalityNo.Text
        End Get
        Set(value As String)
            Me.txtModalityNo.Text = value
        End Set
    End Property
    '///検査予定日
    Public Property ORDER_DATE As String
        Get
            Return Me.txtOrderDate.Text
        End Get
        Set(value As String)
            Me.txtOrderDate.Text = value
        End Set
    End Property
    '///検査予定時刻
    Public Property ORDER_TIME As String
        Get
            Return Me.txtOrderTime.Text
        End Get
        Set(value As String)
            Me.txtOrderTime.Text = value
            If value <> "" Then
                Me.cmbStudy_H.Text = value.Substring(0, 2)
                Me.cmbStudy_T.Text = value.Substring(2, 2)
            End If
        End Set
    End Property
    '///オーダ登録番号
    Public Property ORDER_ANO As String
        Get
            Return Me.txtOrderAno.Text
        End Get
        Set(value As String)
            Me.txtOrderAno.Text = value
        End Set
    End Property

#End Region

#Region "定義"
    Private typMstModality As New clsModality
    Private aryMstBodyPart() As clsBodyPart
    Private typOrder As New clsOrder
    Private clsResizer As New AutoResizer
#End Region

#Region "FormEvents"

#Region "FormLoad"
    Private Sub frmEditBodyPart_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        '///現在設定されているフォントをデフォルトとして退避
        G_FONT = My.Settings.FORM_FONT

        '///フォーム及び各コントロールの初期情報退避
        clsResizer = New AutoResizer(Me)

        Me.Width = My.Settings.FORM_BODY_PART_WIDTH
        Me.Height = My.Settings.FORM_BODY_PART_HEGHT
        Me.dtgList.AllowUserToAddRows = False

        '///画面初期処理
        Call subInitForm()

    End Sub
#End Region

#Region "FormResize"
    Private Sub frmEditBodyPart_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        '///フォームと各コントロールのリサイズ
        clsResizer.subResize()
    End Sub
#End Region

#Region "FormResizeEnd"
    Private Sub frmEditBodyPartt_ResizeEnd(sender As Object, e As System.EventArgs) Handles Me.ResizeEnd
        My.Settings.FORM_BODY_PART_HEGHT = Me.Height
        My.Settings.FORM_BODY_PART_WIDTH = Me.Width
        My.Settings.Save()
    End Sub
#End Region

#End Region

#Region "ControlEvents"

#Region "キャンセルボタン押下時"
    Private Sub cmdCancel_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

#End Region

#Region "更新ボタン押下時"
    Private Sub cmdUpdOrder_Click(sender As System.Object, e As System.EventArgs) Handles cmdUpdOrder.Click
        Dim lngCounter As Long = 0
        Dim lngChkCnt As Long = 0
        Dim strMsg As String = vbNullString
        Dim strBodyPart As String = vbNullString

        '///部位選択結果を構造体へ退避
        Erase aryUpdBodyPartDetail
        For lngCounter = 0 To Me.dtgList.Rows.Count - 1

            If dtgList.Rows(lngCounter).Cells("COL_SELECT").Value = True Then
                ReDim Preserve aryUpdBodyPartDetail(lngChkCnt)
                aryUpdBodyPartDetail(lngChkCnt) = New clsBodyPart

                With aryUpdBodyPartDetail(lngChkCnt)
                    .BODY_PART_NM = dtgList.Rows(lngCounter).Cells("COL_BODY_PART").Value
                    .BODY_PART_NO = dtgList.Rows(lngCounter).Cells("COL_BODY_PART_NO").Value

                    Select Case dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value
                        Case "【単】"
                            .STUDY_DIV = 0
                        Case "【造】"
                            .STUDY_DIV = 1
                        Case "【単+造】"
                            .STUDY_DIV = 2
                        Case "【Dynamic】"
                            .STUDY_DIV = 3
                    End Select
                    strBodyPart &= dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value & .BODY_PART_NM & "+"
                    lngChkCnt += 1
                End With
            End If
        Next lngCounter

        strBodyPart = strBodyPart.Substring(0, strBodyPart.Length - 1)

        '///更新処理実施
        If fncUpdBodyPartDetail(strMsg) = RET_ERROR Then
            Call MsgBox(strMsg & Space(1) & "部位明細更新処理", MsgBoxStyle.Critical, "更新処理")
            Exit Sub
        End If

        If fncUpdOrder(strBodyPart, strMsg) = RET_ERROR Then
            Call MsgBox(strMsg & Space(1) & "オーダ情報更新処理", MsgBoxStyle.Critical, "更新処理")
            Exit Sub
        End If

        '///更新処理を終えると自画面を閉じる
        Me.cmdCancel_Click(sender, e)
    End Sub
#End Region

#Region "部位一覧CellEnter"
    Private Sub dtgList_CellEnter(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgList.CellEnter
        Dim wkDataGridView As DataGridView
        wkDataGridView = CType(sender, DataGridView)

        If TypeOf wkDataGridView.Columns(e.ColumnIndex) _
          Is DataGridViewComboBoxColumn Then

            SendKeys.Send("{F4}")
            'ElseIf TypeOf wkDataGridView.Columns(e.ColumnIndex) _
            '  Is DataGridViewCheckBoxColumn Then

            '    SendKeys.Send("{F4}")
        End If
    End Sub
#End Region

#End Region

#Region "Sub Routine"

#Region "画面初期処理(subInitForm)"
    '********************************************************************************
    '* 機能概要　：画面初期処理
    '* 関数名称　：subInitForm
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日　　：2011/03/01
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Sub subInitForm()
        Dim strMsg As String = vbNullString

        '///患者情報表示エリアの初期化
        Me.txtKana.Text = vbNullString
        Me.txtEiji.Text = vbNullString
        Me.txtKanji.Text = vbNullString
        Me.txtBirth.Text = vbNullString
        Me.txtSex.Text = vbNullString

        '///オーダ情報表示エリアの初期化
        Me.txtOrderDate.Text = vbNullString
        Me.txtOrderTime.Text = vbNullString
        Me.txtOrderComment.Text = vbNullString
        Me.txtModality.Text = vbNullString

        '///モダリティマスタ取得
        If fncGetMstModality(strMsg) = RET_ERROR Then
            Call MsgBox(strMsg & Space(1) & "モダリティマスタ取得", MsgBoxStyle.Critical, "画面初期処理")
            Exit Sub
        End If

        '///部位マスタ取得
        If fncGetMstBodyPart(strMsg) = RET_ERROR Then
            Call MsgBox(strMsg & Space(1) & "部位マスタ取得", MsgBoxStyle.Critical, "画面初期処理")
            Exit Sub
        End If

        '///オーダ情報取得
        If fncGetOrder(strMsg) = RET_ERROR Then
            Call MsgBox(strMsg & Space(1) & "オーダ情報取得", MsgBoxStyle.Critical, "画面初期処理")
            Exit Sub
        End If

    End Sub
#End Region

#Region "部位明細更新(fncUpdBodyPartDetail)"
    '********************************************************************************
    '* 機能概要　：部位明細更新
    '* 関数名称　：fncUpdBodyPartDetail
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日　　：2011/03/01
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncUpdBodyPartDetail(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer = 0
        Dim lngCounter As Long = 0

        Try
            '///DELETE/INSERTの為、先にDELETEを実施
            strSQL.Clear()
            strSQL.AppendLine("DELETE ")
            strSQL.AppendLine("TRAN_BODY_PART_DETAIL ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & typOrder.ORDER_ANO)

            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///INSERT実施
            If aryUpdBodyPartDetail IsNot Nothing Then

                With aryUpdBodyPartDetail(lngCounter)
                    For lngCounter = 0 To aryUpdBodyPartDetail.Length - 1
                        '///SQL文生成
                        strSQL.Clear()
                        strSQL.AppendLine("INSERT INTO ")
                        strSQL.AppendLine("TRAN_BODY_PART_DETAIL ")
                        strSQL.AppendLine("(")
                        strSQL.AppendLine("ORDER_ANO,")
                        strSQL.AppendLine("BODY_PART_DETAIL_SEQ,")
                        strSQL.AppendLine("BODY_PART_NO,")
                        strSQL.AppendLine("BODY_PART_NM,")
                        strSQL.AppendLine("STUDY_DIV,")
                        strSQL.AppendLine("MODALITY_CD")
                        strSQL.AppendLine(") ")
                        strSQL.AppendLine("VALUES ")
                        strSQL.AppendLine("(")
                        strSQL.AppendLine("" & typOrder.ORDER_ANO & ",")
                        strSQL.AppendLine("" & lngCounter + 1 & ",")
                        strSQL.AppendLine("" & .BODY_PART_NO & ",")
                        strSQL.AppendLine("'" & .BODY_PART_NM & "',")
                        strSQL.AppendLine("'" & .STUDY_DIV & "',")
                        strSQL.AppendLine("'" & typOrder.MODALITY_CD & "'")
                        strSQL.AppendLine(")")

                        '///SQL発行
                        intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

                    Next lngCounter
                End With
            End If

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog(strMsg & Space(1) & "部位明細更新(fncUpdBodyPartDetail)", 1)
            Return RET_ERROR
        Finally
        End Try
    End Function

#End Region

#Region "オーダ明細取得(fncGetBodyPartDetail)"
    '********************************************************************************
    '* 機能概要　：部位明細取得
    '* 関数名称　：fncGetBodyPartDetail
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日　　：2011/03/01
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetBodyPartDetail(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim lngCounter As Long
        Dim lngLstCounter As Long

        Try
            '///SQL生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("ORDER_ANO,")
            strSQL.AppendLine("BODY_PART_DETAIL_SEQ,")
            strSQL.AppendLine("BODY_PART_NO,")
            strSQL.AppendLine("BODY_PART_NM,")
            strSQL.AppendLine("STUDY_DIV,")
            strSQL.AppendLine("MODALITY_CD ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("TRAN_BODY_PART_DETAIL ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & typOrder.ORDER_ANO & " ")
            strSQL.AppendLine("ORDER BY BODY_PART_DETAIL_SEQ ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            Erase aryBodyPartDetail
            Erase aryUpdBodyPartDetail

            '///データ未取得時
            If dsData.Tables.Count = 0 Then
                Return RET_NOTFOUND
                Exit Function
            End If
            If dsData.Tables(0).Rows.Count = 0 Then
                Return RET_NOTFOUND
                Exit Function
            End If

            '///取得データを構造体へ退避
            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryBodyPartDetail(lngCounter)
                aryBodyPartDetail(lngCounter) = New clsBodyPart

                With aryBodyPartDetail(lngCounter)
                    .STUDY_DIV = dsData.Tables(0).Rows(lngCounter).Item("STUDY_DIV")
                    .BODY_PART_NO = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NO")
                    .BODY_PART_NM = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NM")

                    '///取得データを画面に展開
                    For lngLstCounter = 0 To dtgList.Rows.Count - 1
                        If dtgList.Rows(lngLstCounter).Cells("COL_BODY_PART_NO").Value = .BODY_PART_NO Then
                            dtgList.Rows(lngLstCounter).Cells("COL_SELECT").Value = True
                            dtgList.Rows(lngLstCounter).Cells("COL_STUDY_DIV").Value = ""

                            Select Case .STUDY_DIV
                                Case "0"
                                    dtgList.Rows(lngLstCounter).Cells("COL_STUDY_DIV").Value = "【単】"
                                Case "1"
                                    dtgList.Rows(lngLstCounter).Cells("COL_STUDY_DIV").Value = "【造】"
                                Case "2"
                                    dtgList.Rows(lngLstCounter).Cells("COL_STUDY_DIV").Value = "【単+造】"
                                Case "3"
                                    dtgList.Rows(lngLstCounter).Cells("COL_STUDY_DIV").Value = "【Dynamic】"
                            End Select
                        End If
                    Next lngLstCounter

                End With

            Next lngCounter

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog(strMsg & Space(1) & "部位明細取得(fncGetBodyPartDetail", 1)
            Return RET_ERROR
        Finally
        End Try
    End Function
#End Region

#Region "オーダ情報取得(fncGetOrder)"
    '********************************************************************************
    '* 機能概要　：オーダ情報取得
    '* 関数名称　：fncGetOrder
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日　　：2011/03/01
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetOrder(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("ORDER_ANO,")
            strSQL.AppendLine("ORDER_NO,")
            strSQL.AppendLine("ORDER_DATE,")
            strSQL.AppendLine("ORDER_TIME,")
            strSQL.AppendLine("PATIENT_ANO,")
            strSQL.AppendLine("STUDY_DATE,")
            strSQL.AppendLine("STUDY_TIME,")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("BODY_PART,")
            strSQL.AppendLine("ORDER_COMMENT,")
            strSQL.AppendLine("STUDY_COMMENT,")
            strSQL.AppendLine("STATE,")
            strSQL.AppendLine("STUDY_INSTANCE_UID,")
            strSQL.AppendLine("INS_DATE,")
            strSQL.AppendLine("INS_TIME,")
            strSQL.AppendLine("INS_USER,")
            strSQL.AppendLine("UPD_DATE,")
            strSQL.AppendLine("UPD_TIME,")
            strSQL.AppendLine("UPD_USER,")
            strSQL.AppendLine("DEL_FLG,")
            strSQL.AppendLine("PATIENT_ID,")
            strSQL.AppendLine("PATIENT_KANA,")
            strSQL.AppendLine("PATIENT_KANJI,")
            strSQL.AppendLine("PATIENT_EIJI,")
            strSQL.AppendLine("PATIENT_BIRTH,")
            strSQL.AppendLine("PATIENT_SEX,")
            strSQL.AppendLine("BEFOR_KANA,")
            strSQL.AppendLine("BEFOR_KANJI,")
            strSQL.AppendLine("BEFOR_EIJI,")
            strSQL.AppendLine("BEFOR_BIRTH,")
            strSQL.AppendLine("BEFOR_SEX,")
            strSQL.AppendLine("PATIENT_COMMENT,")
            strSQL.AppendLine("MODALITY_NAME,")
            strSQL.AppendLine("MODALITY_CD,")
            strSQL.AppendLine("TERMINAL ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine(" vwOrder ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & CLng(Me.txtOrderAno.Text))

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            typOrder = Nothing
            typOrder = New clsOrder

            '///データ未取得時
            If dsData.Tables.Count = 0 Then
                Return RET_NOTFOUND
                Exit Function
            End If
            If dsData.Tables(0).Rows.Count = 0 Then
                Return RET_NOTFOUND
                Exit Function
            End If

            '///取得データを構造体へ退避
            With typOrder
                .ORDER_ANO = dsData.Tables(0).Rows(0).Item("ORDER_ANO")
                .ORDER_NO = dsData.Tables(0).Rows(0).Item("ORDER_NO")
                .ORDER_DATE = dsData.Tables(0).Rows(0).Item("ORDER_DATE")
                .ORDER_TIME = dsData.Tables(0).Rows(0).Item("ORDER_TIME")
                .PATIENT_ANO = dsData.Tables(0).Rows(0).Item("PATIENT_ANO")
                .STUDY_DATE = dsData.Tables(0).Rows(0).Item("STUDY_DATE")
                .STUDY_TIME = dsData.Tables(0).Rows(0).Item("STUDY_TIME")
                .MODALITY_NO = dsData.Tables(0).Rows(0).Item("MODALITY_NO")
                .BODY_PART = dsData.Tables(0).Rows(0).Item("BODY_PART")
                .ORDER_COMMENT = dsData.Tables(0).Rows(0).Item("ORDER_COMMENT")
                .STATE = dsData.Tables(0).Rows(0).Item("STATE")
                .STUDY_INSTANCE_UID = dsData.Tables(0).Rows(0).Item("STUDY_INSTANCE_UID")
                .PATIENT_ID = dsData.Tables(0).Rows(0).Item("PATIENT_ID")
                .PATIENT_KANA = dsData.Tables(0).Rows(0).Item("PATIENT_KANA")
                .PATIENT_KANJI = dsData.Tables(0).Rows(0).Item("PATIENT_KANJI")
                .PATIENT_EIJI = dsData.Tables(0).Rows(0).Item("PATIENT_EIJI")
                .PATIENT_BIRTH = dsData.Tables(0).Rows(0).Item("PATIENT_BIRTH")
                .PATIENT_SEX = dsData.Tables(0).Rows(0).Item("PATIENT_SEX")
                .BEFOR_KANA = dsData.Tables(0).Rows(0).Item("BEFOR_KANA")
                .BEFOR_KANJI = dsData.Tables(0).Rows(0).Item("BEFOR_KANJI")
                .BEFOR_EIJI = dsData.Tables(0).Rows(0).Item("BEFOR_EIJI")
                .BEFOR_BIRTH = dsData.Tables(0).Rows(0).Item("BEFOR_BIRTH")
                .BEFOR_SEX = dsData.Tables(0).Rows(0).Item("BEFOR_SEX")
                .PATIENT_COMMENT = dsData.Tables(0).Rows(0).Item("PATIENT_COMMENT")
                .MODALITY_CD = dsData.Tables(0).Rows(0).Item("MODALITY_CD")
                .MODALITY_NAME = dsData.Tables(0).Rows(0).Item("MODALITY_NAME")
                .TERMINAL = dsData.Tables(0).Rows(0).Item("TERMINAL")
                .DEL_FLG = dsData.Tables(0).Rows(0).Item("DEL_FLG")
                .STUDY_COMMENT = dsData.Tables(0).Rows(0).Item("STUDY_COMMENT")

                '///オーダの進捗状況により検査予約日か検査実施日を切り替える
                Select Case .STATE
                    Case "", "0", "1"
                        Me.dtOrderDate.Value = .ORDER_DATE.Substring(0, 4) & "/" & .ORDER_DATE.Substring(4, 2) & "/" & .ORDER_DATE.Substring(6, 2)
                        Me.cmbStudy_H.Text = .ORDER_TIME.Substring(0, 2)
                        Me.cmbStudy_T.Text = .ORDER_TIME.Substring(2, 2)
                    Case Else
                        Me.dtOrderDate.Value = .STUDY_DATE.Substring(0, 4) & "/" & .STUDY_DATE.Substring(4, 2) & "/" & .STUDY_DATE.Substring(6, 2)
                        Me.cmbStudy_H.Text = .STUDY_TIME.Substring(0, 2)
                        Me.cmbStudy_T.Text = .STUDY_TIME.Substring(2, 2)
                End Select

                '///患者情報を画面に展開
                Me.txtKana.Text = .PATIENT_KANA                 '患者カナ氏名
                Me.txtEiji.Text = .PATIENT_EIJI                 '患者英字氏名
                Me.txtKanji.Text = .PATIENT_KANJI               '患者漢字氏名(非表示)
                Me.txtBirth.Text = .PATIENT_BIRTH               '患者生年月日
                Select Case .PATIENT_SEX                        '患者性別
                    Case "1"
                        Me.txtSex.Text = "男"
                    Case "2"
                        Me.txtSex.Text = "女"
                    Case "3"
                        Me.txtSex.Text = "不明"
                    Case Else
                        Me.txtSex.Text = ""
                End Select

                Me.txtPatientComment.Text = .PATIENT_COMMENT    '患者コメント
                Me.txtOrderComment.Text = .ORDER_COMMENT        'オーダコメント
            End With

            '///オーダ明細情報取得
            If fncGetBodyPartDetail(strMsg) = RET_ERROR Then
                Call MsgBox("オーダ明細情報取得にてエラーが発生しました。(" & strMsg & ")", MsgBoxStyle.Critical, "オーダ情報取得(fncGetOrder)")
                Return RET_ERROR
            End If

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog(strMsg & Space(1) & "オーダ情報取得(fncGetOrder)", 1)
            Return RET_ERROR
        Finally
        End Try
    End Function
#End Region

#Region "部位マスタ取得(fncGetMstBodyPart)"
    '********************************************************************************
    '* 機能概要　：部位マスタ取得
    '* 関数名称　：fncGetMstBodyPart
    '* 引　数　　：strMsg ・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・正常　異常
    '* 作成日　　：2011/03/01
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetMstBodyPart(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim lngCounter As Long

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("BODY_PART_NO,")
            strSQL.AppendLine("DISP_SORT,")
            strSQL.AppendLine("BODY_PART_NM,")
            strSQL.AppendLine("MODALITY_CD ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_BODY_PART ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("MODALITY_CD = '" & typMstModality.MODALITY_CD & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///データ未取得時
            If dsData.Tables.Count = 0 Then
                Return RET_NOTFOUND
                Exit Function
            End If
            If dsData.Tables(0).Rows.Count = 0 Then
                Return RET_NOTFOUND
                Exit Function
            End If

            '///部位一覧初期化
            Me.dtgList.Rows.Clear()
            Erase aryMstBodyPart

            '///取得データを構造体へ退避。且つ部位一覧に表示
            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryMstBodyPart(lngCounter)
                aryMstBodyPart(lngCounter) = New clsBodyPart

                With aryMstBodyPart(lngCounter)
                    .BODY_PART_NO = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NO")
                    .BODY_PART_NM = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NM")
                    .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")

                    Me.dtgList.Rows.Add()
                    dtgList.Rows(lngCounter).Cells("COL_BODY_PART").Value = .BODY_PART_NM
                    dtgList.Rows(lngCounter).Cells("COL_BODY_PART_NO").Value = .BODY_PART_NO
                    dtgList.Rows(lngCounter).Cells("COL_SELECT").Value = False
                    dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = ""

                End With

            Next lngCounter

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog(strMsg & Space(1) & "部位マスタ取得(fncGetMstBodyPart", 1)
            Return RET_ERROR
        Finally
        End Try
    End Function
#End Region

#Region "モダリティマスタ取得(fncGetMstModality)"
    '********************************************************************************
    '* 機能概要　：モダリティマスタ取得
    '* 関数名称　：fncGetMstModality
    '* 引　数　　：strMsg ・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・正常　異常
    '* 作成日　　：2011/03/01
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetMstModality(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("MODALITY_CD,")
            strSQL.AppendLine("SCP_AE,")
            strSQL.AppendLine("SCP_IP,")
            strSQL.AppendLine("SCU_AE,")
            strSQL.AppendLine("SCU_IP,")
            strSQL.AppendLine("SCU_PORT,")
            strSQL.AppendLine("CHARCTERSET,")
            strSQL.AppendLine("TERMINAL,")
            strSQL.AppendLine("MODALITY_NAME,")
            strSQL.AppendLine("ROOM_NAME ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_MODALITY ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("MODALITY_NO = " & CLng(Me.txtModalityNo.Text))

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///データ未取得時
            If dsData.Tables.Count = 0 Then
                Return RET_NOTFOUND
                Exit Function
            End If
            If dsData.Tables(0).Rows.Count = 0 Then
                Return RET_NOTFOUND
                Exit Function
            End If

            typMstModality = Nothing
            typMstModality = New clsModality

            '///取得データを構造体へ退避
            With typMstModality
                .MODALITY_NO = dsData.Tables(0).Rows(0).Item("MODALITY_NO")
                .MODALITY_CD = dsData.Tables(0).Rows(0).Item("MODALITY_CD")
                .SCP_AE = dsData.Tables(0).Rows(0).Item("SCP_AE")
                .SCP_IP = dsData.Tables(0).Rows(0).Item("SCP_IP")
                .SCU_AE = dsData.Tables(0).Rows(0).Item("SCU_AE")
                .SCU_IP = dsData.Tables(0).Rows(0).Item("SCU_IP")
                .SCU_PORT = dsData.Tables(0).Rows(0).Item("SCU_PORT")
                .CHARCTERSET = dsData.Tables(0).Rows(0).Item("CHARCTERSET")
                .TERMINAL = dsData.Tables(0).Rows(0).Item("TERMINAL")
                .MODALITY_NAME = dsData.Tables(0).Rows(0).Item("MODALITY_NAME")
                .ROOM_NAME = dsData.Tables(0).Rows(0).Item("ROOM_NAME")

                '///検査室コンボボックスへセット
                Me.cmbModality.Items.Add(.ROOM_NAME)
                Me.cmbModality.SelectedIndex = 0
            End With

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog(strMsg & Space(1) & "モダリティマスタ取得(fncGetMstModality", 1)
            Return RET_ERROR
        Finally
        End Try
    End Function
#End Region

#Region "オーダ情報更新(fncUpdOrder)"
    '********************************************************************************
    '* 機能概要　：オーダ情報更新
    '* 関数名称　：fncUpdOrder
    '* 引　数　　：strMsg ・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・正常　異常
    '* 作成日　　：2011/03/01
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncUpdOrder(strBodyPart As String, ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer = 0
        Dim strWork As String = vbNullString
        Dim strDate As String()

        Try
            '///サーバ日付の取得
            strWork = fncGetServerDate(2, strMsg)
            strDate = strWork.Split(",")

            strSQL.Clear()
            strSQL.AppendLine("UPDATE ")
            strSQL.AppendLine("TRAN_ORDER ")
            strSQL.AppendLine("SET ")
            strSQL.AppendLine("BODY_PART = '" & strBodyPart & "',")
            strSQL.AppendLine("ORDER_COMMENT = '" & Me.txtOrderComment.Text.Trim.Replace("'", "''") & "',")
            strSQL.AppendLine("UPD_DATE = '" & strDate(0).Trim & "',")
            strSQL.AppendLine("UPD_TIME = '" & strDate(1).Trim & "'")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & typOrder.ORDER_ANO & "")

            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog(strMsg & Space(1) & "オーダ情報更新(fncUpdOrder)", 1)
            Return RET_ERROR
        Finally
        End Try
    End Function
#End Region

#End Region

End Class