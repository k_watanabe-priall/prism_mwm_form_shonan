﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStudyDiv
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdGairai = New System.Windows.Forms.Button()
        Me.cmdKenshin = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cmdGairai
        '
        Me.cmdGairai.Font = New System.Drawing.Font("MS UI Gothic", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdGairai.Location = New System.Drawing.Point(12, 12)
        Me.cmdGairai.Name = "cmdGairai"
        Me.cmdGairai.Size = New System.Drawing.Size(220, 165)
        Me.cmdGairai.TabIndex = 0
        Me.cmdGairai.Text = "診療"
        Me.cmdGairai.UseVisualStyleBackColor = True
        '
        'cmdKenshin
        '
        Me.cmdKenshin.Font = New System.Drawing.Font("MS UI Gothic", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdKenshin.Location = New System.Drawing.Point(248, 15)
        Me.cmdKenshin.Name = "cmdKenshin"
        Me.cmdKenshin.Size = New System.Drawing.Size(220, 165)
        Me.cmdKenshin.TabIndex = 1
        Me.cmdKenshin.Text = "健診"
        Me.cmdKenshin.UseVisualStyleBackColor = True
        '
        'frmStudyDiv
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(486, 195)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdKenshin)
        Me.Controls.Add(Me.cmdGairai)
        Me.Name = "frmStudyDiv"
        Me.Text = "検査区分選択"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdGairai As System.Windows.Forms.Button
    Friend WithEvents cmdKenshin As System.Windows.Forms.Button
End Class
