﻿Public Class frmMain

#Region "変数定義"

    '-------------------------------------------------------------------------------------
    '   デリゲート型の宣言
    '-------------------------------------------------------------------------------------
    Delegate Sub ShowEventDelegate(ByVal ev As String)             '受信イベントのデータ表示用
    Delegate Sub ShowStatusDelegate(ByVal st As String)            '装置の接続状態の表示用
    Delegate Sub ShowMonitorDelegate(ByVal dir As String, ByVal str As String)   '送受信データのモニター表示用

    Private clsResizer As New AutoResizer
    Private G_FONT As String = vbNullString
    Private aryOrderPatinet() As clsOrder
    Private typPatient As New clsPatinet
    Private lngSelRow As Long = -1
    Private typUID As New clsStudyInstanceUID
    Private bolFormClose As Boolean = False
    Private bolInit As Boolean = False
    Private typModality As New clsModality
    Private aryModality() As clsModality
    Private bolActivate As Boolean = False
    Private typIjiPatient As New clsPatinet

    Private g_PatientID As String = vbNullString
    Private g_Kana As String = vbNullString
    Private g_Kanji As String = vbNullString
    Private g_Birth As String = vbNullString
    Private g_Sex As String = vbNullString
    Private g_Card As Boolean = False
    Private g_JushinNo As String = vbNullString

    Private objOrder() As TYPE_ORDER
    Private objModality As New TYPE_MODALITY
    Private objPatient As New TYPE_PATIENT

    Private G_SORT_COL As Integer = 0
    Private G_SORT_DIV As Integer = 0

    Private g_e As DataGridViewColumnEventArgs
#End Region

#Region "FormEvents"

#Region "FormActivated"
    Private Sub frmMain_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'Me.Width = My.Settings.FORM_SIZE_WIDTH
        'Me.Height = My.Settings.FORM_SIZE_HEIGHT
        bolActivate = True
        Me.txtPatientID.Focus()
    End Sub
#End Region

#Region "FormClosing"
    Private Sub frmMain_FormClosing(ByVal sender As System.Object, _
        ByVal e As System.Windows.Forms.FormClosingEventArgs) _
        Handles MyBase.FormClosing

        Call subSaveColWidth()

        Select Case e.CloseReason
            Case CloseReason.ApplicationExitCall
                Console.WriteLine("Application.Exitによる")
            Case CloseReason.FormOwnerClosing
                Console.WriteLine("所有側のフォームが閉じられようとしている")
            Case CloseReason.MdiFormClosing
                Console.WriteLine("MDIの親フォームが閉じられようとしている")
            Case CloseReason.TaskManagerClosing
                Console.WriteLine("タスクマネージャによる")
            Case CloseReason.UserClosing
                Console.WriteLine("ユーザーインターフェイスによる")
            Case CloseReason.WindowsShutDown
                Console.WriteLine("OSのシャットダウンによる")
            Case CloseReason.None
                Console.WriteLine("未知の理由")
            Case Else
                Console.WriteLine("それ以外")
        End Select

        My.Settings.FORM_SIZE_HEIGHT = Me.Height
        My.Settings.FORM_SIZE_WIDTH = Me.Width
        My.Settings.Save()

        If bolFormClose = False Then End
    End Sub

#End Region

#Region "FormClick"
    Private Sub frmMain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Click
        Me.txtPatientID.Focus()
    End Sub
#End Region

#Region "FormLoad"
    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strMsg As String = vbNullString

        '///現在設定されているフォントをデフォルトとして退避
        G_FONT = My.Settings.FORM_FONT

        Timer1.Enabled = False
        tmrProc.Enabled = False

        Me.SetStyle(ControlStyles.ResizeRedraw, True)
        Me.SetStyle(ControlStyles.DoubleBuffer, True)
        Me.SetStyle(ControlStyles.UserPaint, True)
        Me.SetStyle(ControlStyles.AllPaintingInWmPaint, True)

        '///フォーム及び各コントロールの初期情報退避
        clsResizer = New AutoResizer(Me)
        Me.Width = My.Settings.FORM_SIZE_WIDTH
        Me.Height = My.Settings.FORM_SIZE_HEIGHT

        '///DB Conection
        Call fncDBConnect(strMsg)

        '///モダリティマスタ取得処理
        If fncGetModality(strMsg, 1) <> RET_NORMAL Then
            MsgBox("モダリティマスタ取得に失敗しました。" & vbCrLf & "システム管理者に連絡して下さい。" & vbCrLf & "エラー内容(" & strMsg.ToString & ")", MsgBoxStyle.Critical, "モダリティマスタ取得")
            Call subOutLog("モダリティマスタ取得に失敗しました。" & Space(1) & "システム管理者に連絡して下さい。" & Space(1) & "エラー内容(" & strMsg.ToString & ")", 1)
            End
        End If

        Dim lngCounter As Long = 0

        For lngCounter = 0 To aryModality.Length - 1
            If My.Settings.TERMINAL = aryModality(lngCounter).TERMINAL Then
                Exit For
            End If
        Next lngCounter

        Select Case My.Settings.XRAY_DIV
            Case "1"
                Me.chkPorterble.Visible = True
                '---START Ver.3.0.2.7 未使用だったエコー室1をAplioに変更 2024.07.18
                '---エコー室１が使用していない為非表示 UpDated By Watanabe 2021.09.14
                Me.chkModality1.Visible = True
                'Me.chkModality1.Location = New Point(370, 219)
                'Me.chkModality1.Text = "訪問"
                'Me.chkModality1.Visible = False
                Me.chkModality1.Text = "Aplio"
                '---表示位置の変更 ADD By Watanabe 2021.09.14
                'Me.chkModality2.Location = New Point(74, 219)
                '---END Ver.3.0.2.7 未使用だったエコー室1をAplioに変更 2024.07.18
                Me.chkModality2.Visible = True
                '---施設要望により名称変更 UpDated By Watanabe 2021.09.14
                'Me.chkModality2.Text = "エコー室２"
                Me.chkModality2.Text = "VIVID"
                '---表示位置の変更 ADD By Watanabe 2021.09.14
                'Me.chkTmscPorterble.Location = New Point(160, 219)
                Me.chkTmscPorterble.Visible = True
                '---施設要望により名称変更 ADD By Watanabe 2021.09.14
                Me.chkTmscPorterble.Text = "Xario"
                Me.cmdJisshi.Visible = False
                Me.cmdCanJisshi.Visible = False
                If My.Settings.TERMINAL = My.Settings.US1_TERMINAL Then Me.chkModality2.Checked = True
                If My.Settings.TERMINAL = My.Settings.US2_TERMINAL Then Me.chkTmscPorterble.Checked = True
            Case "2"
                '---表示位置の変更 ADD By Watanabe 2021.09.14
                'Me.chkModality1.Location = New Point(74, 219)
                'Me.chkModality2.Location = New Point(180, 219)
                Me.chkPorterble.Visible = False
                Me.chkModality1.Visible = True
                Me.chkModality1.Text = "内視鏡１"
                Me.chkModality2.Visible = True
                Me.chkModality2.Text = "内視鏡２"
                Me.chkTmscPorterble.Visible = False

                If My.Settings.TERMINAL = My.Settings.ES1_TERMINAL Then Me.chkModality1.Checked = True
                If My.Settings.TERMINAL = My.Settings.ES2_TERMINAL Then Me.chkModality2.Checked = True
            Case "0"
                Me.chkPorterble.Visible = False
                Me.chkModality1.Visible = False
                Me.chkModality2.Visible = False
                Me.chkTmscPorterble.Visible = False
        End Select

        'Call subPortOpen()
        'Call subComPortOpen()
        '///フォームの初期化
        Call subInitForm()


        'If lngCounter > (aryModality.Length - 1) Then
        '    Me.cmbModality.SelectedIndex = 0
        'Else
        '    Me.cmbModality.SelectedIndex = lngCounter
        'End If
        Select Case My.Settings.TERMINAL
            Case My.Settings.CR_TERMINAL
                Me.cmbModality.SelectedIndex = 0
            Case My.Settings.PORTA_TERMINAL     '---放射線科 ポータブル対応 2022.02.18 ADD By Watanabe
                Me.cmbModality.SelectedIndex = 1
            Case My.Settings.SITEHEALTH_TERMINAL     '---放射線科 施設健診対応 2022.04.22 ADD By Watanabe
                Me.cmbModality.SelectedIndex = 1
            Case My.Settings.CT_TERMINAL
                Me.cmbModality.SelectedIndex = 2
            Case My.Settings.RF_TERMINAL
                Me.cmbModality.SelectedIndex = 3
            Case My.Settings.ES1_TERMINAL, My.Settings.ES2_TERMINAL
                Me.cmbModality.SelectedIndex = 0
            Case My.Settings.US1_TERMINAL, My.Settings.US2_TERMINAL, My.Settings.US3_TERMINAL
                Me.cmbModality.SelectedIndex = 1
        End Select

        '///設定情報反映
        Call subSetColWidth()

        Me.dtgList.MultiSelect = False
        Me.dtgList.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        Me.dtgList.AllowUserToAddRows = False

        '///強制的に日付変更を呼び出す
        Call dtStudyDate_ValueChanged(sender, e)

        Me.txtPatientID.Focus()

        Timer1.Enabled = My.Settings.AUTO_REFRESH
        tmrProc.Enabled = True
    End Sub
#End Region

#Region "FormResize"
    Private Sub frmMain_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        '///フォームと各コントロールのリサイズ
        clsResizer.subResize()
    End Sub
#End Region

#Region "FormResizeEnd"
    Private Sub frmMain_ResizeEnd(sender As Object, e As System.EventArgs) Handles Me.ResizeEnd
        My.Settings.FORM_SIZE_HEIGHT = Me.Height
        My.Settings.FORM_SIZE_WIDTH = Me.Width
        My.Settings.Save()
    End Sub
#End Region

#End Region

#Region "ControlEvents"

#Region "＜ボタン押下時"
    Private Sub cmdBeforDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBeforDate.Click
        dtStudyDate.Value = DateAdd(DateInterval.Day, -1, dtStudyDate.Value)
    End Sub
#End Region

#Region "＜＜ボタン押下時"
    Private Sub cmdBeforMonth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBeforMonth.Click
        dtStudyDate.Value = DateAdd(DateInterval.Month, -1, dtStudyDate.Value)
    End Sub
#End Region

#Region "＞ボタン押下時"
    Private Sub cmdNextDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNextDate.Click
        dtStudyDate.Value = DateAdd(DateInterval.Day, 1, dtStudyDate.Value)
    End Sub
#End Region

#Region "＞＞ボタン押下時"
    Private Sub cmdNextMonth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNextMonth.Click
        dtStudyDate.Value = DateAdd(DateInterval.Month, 1, dtStudyDate.Value)
    End Sub
#End Region

#Region "本日ボタン押下時"
    Private Sub cmdToDay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdToDay.Click
        dtStudyDate.Value = Date.Now
    End Sub
#End Region

#Region "日付の入力値が変わったとき"
    Private Sub dtStudyDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtStudyDate.ValueChanged
        Dim strMsg As String = vbNullString

        If bolInit = True Then Exit Sub

        Me.Timer1.Enabled = False

        '---Ver.3.0.2.2 処理中のカーソルを変更 ADD By Watanabe 2021.10.12
        Me.Cursor = Cursors.WaitCursor

        If fncGetOrder(strMsg, Me.chkNonAccept.Checked, Me.chkAccept.Checked, Me.chkStudyAccept.Checked, Me.chkEnd.Checked, Me.chkStudyEnd.Checked, Me.chkNonDelete.Checked, Me.chkDelete.Checked) = RET_ERROR Then
            MsgBox("オーダ情報取得にてエラーが発生しました。" & vbCrLf & "エラー内容：" & strMsg, MsgBoxStyle.Critical, "オーダ情報取得")
            Call subOutLog("オーダ情報取得にてエラーが発生しました。" & Space(1) & "エラー内容(" & strMsg.ToString & ")", 1)
            Exit Sub
        End If

        Call subDispList(strMsg)

        '---START Ver.3.0.2.2 SORT状態の維持 ADD By Watanabe 2021.10.12
        If dtgList.SortedColumn IsNot Nothing Then

            '並び替える列を決める
            Dim sortColumn As DataGridViewColumn =
                Me.dtgList.SortedColumn

            '並び替えの方向（昇順か降順か）を決める
            Dim sortDirection As System.ComponentModel.ListSortDirection

            If Me.dtgList.SortOrder = SortOrder.Ascending Then
                sortDirection = System.ComponentModel.ListSortDirection.Ascending
            Else
                sortDirection = System.ComponentModel.ListSortDirection.Descending
            End If

            '並び替えを行う
            dtgList.Sort(sortColumn, sortDirection)
            '---SORT
            'Else
            '    Return
        End If
        '---END Ver.3.0.2.2 SORT状態の維持 ADD By Watanabe 2021.10.12

        If lngSelRow >= 0 Then
            If dtgList.Rows.Count > lngSelRow Then
                dtgList.Rows(lngSelRow).Selected = True
                dtgList_Click(sender, e)
            End If
        Else
            If dtgList.Rows.Count > 0 Then
                dtgList.Rows(0).Selected = True
                dtgList_Click(sender, e)
            End If
        End If


        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH

        '---Ver.3.0.2.2 処理中のカーソルを変更 ADD By Watanabe 2021.10.12
        Me.Cursor = Cursors.Default

        Me.txtPatientID.Focus()
    End Sub

#End Region

#Region "患者IDフォーカス取得時"
    Private Sub txtPatientID_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPatientID.GotFocus
        Me.txtPatientID.SelectionStart = 0
        Me.txtPatientID.SelectionLength = Me.txtPatientID.TextLength
    End Sub
#End Region

#Region "患者ID入力時"
    Private Sub txtPatientID_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPatientID.KeyDown
        Dim lngCounter As Long = 0
        Dim bolFlg As Boolean = False
        Dim strMsg As String = vbNullString
        Dim bolMatchFlg As Boolean = False
        Dim strPatientID As String = vbNullString
        Dim strState As String = vbNullString
        Dim strTxtPatientID As String = vbNullString

        '///一覧自動更新用タイマーの停止
        Me.Timer1.Enabled = False
        Me.tmrProc.Enabled = False

        '///Enter押下時は患者情報登録処理(オーダ登録)を行う。
        If e.KeyData = Keys.Enter Then
            '///入力値より改行コードを外し代入 Created By Watanabe 2014.03.31
            strTxtPatientID = Me.txtPatientID.Text.Replace(Chr(Keys.Enter), "")
            If strTxtPatientID.Trim = "" Then Exit Sub
            g_Card = False
            'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(6).Replace(Space(1), "0")
            For lngCounter = 0 To Me.dtgList.RowCount - 1
                strPatientID = Me.dtgList.Rows(lngCounter).Cells("COL_PATIENT_ID").Value
                strState = Me.dtgList.Rows(lngCounter).Cells("COL_STATE").Value

                If (strPatientID.Trim = strTxtPatientID.Trim) And (strState.Trim = "未受付") Then

                    dtgList.Rows(lngCounter).Selected = True
                    bolMatchFlg = True
                    lngSelRow = lngCounter
                    Call cmdAccept.PerformClick()
                    '///1件のみ処理するよう処理後にループを抜ける ADD By Watanabe 2014.04.11
                    Exit For
                End If
            Next lngCounter

            'Call cmdEditPatient_Click(sender, e)
            If bolMatchFlg = False Then
                Call MsgBox("入力された患者IDのデータは一覧にありませんでした。" & vbCrLf & "もう一度ご確認下さい。", MsgBoxStyle.Exclamation)
                Call subOutLog("入力された患者IDのデータは一覧にありませんでした。" & vbCrLf & "もう一度ご確認下さい。", 2)
            End If
        End If

        '///一覧自動更新用タイマーを元に戻す
        If Me.Timer1.Enabled <> My.Settings.AUTO_REFRESH Then
            Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
        End If
        Me.tmrProc.Enabled = True

    End Sub
#End Region

#Region "患者ID LostFocus"
    Private Sub txtPatientID_LostFocus(sender As Object, e As System.EventArgs) Handles txtPatientID.LostFocus
        '///職員(先頭1桁"K")以外は先頭0埋め
        If Me.txtPatientID.Text <> "" And Me.txtPatientID.TextLength > 2 And IsNumeric(Me.txtPatientID.Text) = False Then
            If Not Me.txtPatientID.Text.Substring(0, 1).ToUpper = "K" And _
               Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "SA" And _
               Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "TE" Then
                'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
            End If
        ElseIf Me.txtPatientID.Text <> "" And IsNumeric(Me.txtPatientID.Text) = True Then
            'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
        Else
            Me.txtPatientID.Text = Me.txtPatientID.Text.ToUpper
        End If
    End Sub
#End Region

#Region "受付ボタン押下時"
    Private Sub cmdAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAccept.Click
        Dim strMsg As String = vbNullString
        Dim intCounter As Integer = 0
        Dim strOrderNumber As String = vbNullString

        Me.Timer1.Enabled = False

        '---START Ver.3.0.2.2 受付データのオーダ番号を退避 ADD By Watanabe 2021.10.12
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next r
        strOrderNumber = Me.dtgList.Rows(lngSelRow).Cells("COL_ORDER_NO").Value
        '---END Ver.3.0.2.2 受付データのオーダ番号を退避 ADD By Watanabe 2021.10.12

        '///受付処理開始
        Call subUpdAccept(1, strMsg)

        '///検査日のValueChanged Eventsを呼び出し、表示更新を行う。
        Call dtStudyDate_ValueChanged(sender, e)


        '---START Ver.3.0.2.2 内視鏡室要望 受付後一覧の選択が受け付けた患者にして欲しい ADD By Watanabe 2021.10.12
        If My.Settings.XRAY_DIV = "2" Then
            For Each r As DataGridViewRow In dtgList.Rows
                If strOrderNumber = Me.dtgList.Rows(intCounter).Cells("COL_ORDER_NO").Value Then
                    Exit For
                End If
                intCounter += 1
            Next
            Me.dtgList.Rows(intCounter).Selected = True
            Me.dtgList_Click(sender, e)
        End If
        '---END Ver.3.0.2.2 内視鏡室要望 受付後一覧の選択が受け付けた患者にして欲しい ADD By Watanabe 2021.10.12

        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "一覧クリック時"
    Private Sub dtgList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtgList.Click

        Timer1.Enabled = False

        If Me.dtgList.Rows.Count = 0 Then
            Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
            Exit Sub
        End If

        '///選択行のIndexを退避
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next r

        If lngSelRow < 0 Then Exit Sub

        Me.txtPatientID.Text = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_ID").Value
        Me.txtPatientKana.Text = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_KANA").Value

        If dtgList.Rows(lngSelRow).Cells("COL_STATE").Value = "実施済" Then
            '///実施済でも受付取り消し、削除が行えるようにする UpDated By Watanabe 2012.02.02
            'Me.cmdAccept.Enabled = False
            'Me.cmdCanAccept.Enabled = False
            'Me.cmdDelete.Enabled = False
            'Me.cmdCanDelete.Enabled = False
            'Me.オーダ修正ToolStripMenuItem1.Enabled = False
            Me.cmdAccept.Enabled = False
            Me.cmdCanAccept.Enabled = False
            Me.cmdDelete.Enabled = True
            Me.cmdCanDelete.Enabled = False
            Me.cmdJisshi.Enabled = True
            Me.cmdCanJisshi.Enabled = False

            Me.部位情報修正ToolStripMenuItem1.Enabled = True
            If My.Settings.VIEWER_DIV <> "1" Then
                Me.画像表示ToolStripMenuItem.Enabled = True
                Me.画像表示ToolStripMenuItem1.Enabled = True
            End If
        Else
            Select Case dtgList.Rows(lngSelRow).Cells("COL_STATE").Value
                Case "", "未受付"
                    Me.cmdAccept.Enabled = True
                    Me.cmdCanAccept.Enabled = False
                    Me.cmdDelete.Enabled = True
                    Me.cmdCanDelete.Enabled = False
                    Me.cmdJisshi.Enabled = False
                    Me.cmdCanJisshi.Enabled = False
                    Me.部位情報修正ToolStripMenuItem1.Enabled = True
                    If My.Settings.VIEWER_DIV <> "1" Then
                        Me.画像表示ToolStripMenuItem.Enabled = False
                        Me.画像表示ToolStripMenuItem1.Enabled = False
                    End If
                Case "受付済"
                    Me.cmdAccept.Enabled = False
                    Me.cmdCanAccept.Enabled = True
                    Me.cmdDelete.Enabled = True
                    Me.cmdCanDelete.Enabled = False
                    Me.cmdJisshi.Enabled = False
                    Me.cmdCanJisshi.Enabled = False
                    Me.部位情報修正ToolStripMenuItem1.Enabled = True
                    If My.Settings.VIEWER_DIV <> "1" Then
                        Me.画像表示ToolStripMenuItem.Enabled = True
                        Me.画像表示ToolStripMenuItem1.Enabled = True
                    End If
                Case "検査受付済"
                    'Me.cmdAccept.Enabled = False
                    'Me.cmdCanAccept.Enabled = False
                    'Me.cmdDelete.Enabled = True
                    'Me.cmdCanDelete.Enabled = False
                    'Me.cmdJisshi.Enabled = True
                    'Me.cmdCanJisshi.Enabled = False
                    Me.cmdAccept.Enabled = False
                    Me.cmdCanAccept.Enabled = True
                    Me.cmdDelete.Enabled = True
                    Me.cmdCanDelete.Enabled = False
                    Me.cmdJisshi.Enabled = False
                    Me.cmdCanJisshi.Enabled = False
                    Me.部位情報修正ToolStripMenuItem1.Enabled = True
                    If My.Settings.VIEWER_DIV <> "1" Then
                        Me.画像表示ToolStripMenuItem.Enabled = True
                        Me.画像表示ToolStripMenuItem1.Enabled = True
                    End If
                Case "実施済"
                    Me.cmdAccept.Enabled = False
                    Me.cmdCanAccept.Enabled = False
                    Me.cmdDelete.Enabled = True
                    Me.cmdCanDelete.Enabled = False
                    Me.cmdJisshi.Enabled = False
                    Me.cmdCanJisshi.Enabled = True
                    Me.部位情報修正ToolStripMenuItem1.Enabled = True
                    If My.Settings.VIEWER_DIV <> "1" Then
                        Me.画像表示ToolStripMenuItem.Enabled = True
                        Me.画像表示ToolStripMenuItem1.Enabled = True
                    End If
                Case "削除"
                    Me.cmdAccept.Enabled = False
                    Me.cmdCanAccept.Enabled = False
                    Me.cmdDelete.Enabled = False
                    Me.cmdCanDelete.Enabled = True
                    Me.cmdJisshi.Enabled = False
                    Me.cmdCanJisshi.Enabled = False
                    Me.部位情報修正ToolStripMenuItem1.Enabled = False
                    If My.Settings.VIEWER_DIV <> "1" Then
                        Me.画像表示ToolStripMenuItem.Enabled = False
                        Me.画像表示ToolStripMenuItem1.Enabled = False
                    End If
            End Select
        End If

        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH

    End Sub
#End Region

#Region "一覧ダブルクリック時"

    Private Sub dtgList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtgList.DoubleClick
        Dim strOrderNumber As String = vbNullString
        Dim strValue As String = vbNullString
        Dim intCounter As Integer = 0

        Me.Timer1.Enabled = False

        '///START 一覧ダブルクリック時、画像表示して欲しいとの要望の為 UpDated By Watanabe 2012.03.06
        ''///選択行のIndexを退避
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next r
        strOrderNumber = Me.dtgList.Rows(lngSelRow).Cells("COL_ORDER_NO").Value

        strValue = Me.dtgList.Rows(lngSelRow).Cells("COL_STATE").Value

        '---START Ver.3.0.3.0 CRコンソール、モバイルコンソールの両方で受付できるようにするため ADD By Watanabe 2025.01.28
        If cmbModality.SelectedItem = "一般撮影" Then
            Select Case dtgList.Rows(lngSelRow).Cells("COL_STATE").Value
                Case "", "未受付"
                    frmCrMenu.ShowDialog()
                    Call cmdAccept.PerformClick()
            End Select
        End If

        '---END Ver.3.0.3.1 ADD By Watanabe 2025.01.28

        '///
        Select Case strValue
            Case "未受付"
                Call Me.cmdAccept_Click(sender, e)
            Case "受付済"
            Case "検査受付済"
            Case "実施済"
        End Select
        '///END 一覧ダブルクリック時、画像表示して欲しいとの要望の為 UpDated By Watanabe 2012.03.06

        '---START Ver.3.0.2.1 内視鏡室からの要望。受け付けた後に受け付けた以外の患者情報が画面に表示されるので受け付けた患者が表示されるようにして欲しいとの事。
        If My.Settings.XRAY_DIV = "2" Then
            For Each r As DataGridViewRow In dtgList.Rows
                If strOrderNumber = Me.dtgList.Rows(intCounter).Cells("COL_ORDER_NO").Value Then
                    Exit For
                End If
                intCounter += 1
            Next
            Me.dtgList.Rows(intCounter).Selected = True
            Me.dtgList_Click(sender, e)
        End If
        '---END Ver.3.0.2.1 内視鏡室からの要望。受け付けた後に受け付けた以外の患者情報が画面に表示されるので受け付けた患者が表示されるようにして欲しいとの事。

        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "一覧列幅変更時"
    Private Sub dtgList_ColumnWidthChanged(sender As Object, e As System.Windows.Forms.DataGridViewColumnEventArgs) Handles dtgList.ColumnWidthChanged
        If bolActivate = False Then Exit Sub

        '///設定保存
        Call subSaveColWidth()

        '///設定情報反映
        Call subSetColWidth()
    End Sub
#End Region

#Region "一覧列移動時"
    Private Sub dtgList_ColumnDisplayIndexChanged(sender As Object, e As System.Windows.Forms.DataGridViewColumnEventArgs) Handles dtgList.ColumnDisplayIndexChanged
        If bolActivate = False Then Exit Sub

        '///設定保存
        Call subSaveColWidth()

        '///設定情報反映
        Call subSetColWidth()
    End Sub
#End Region

#Region "受付取消ボタン押下時"
    Private Sub cmdCanAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCanAccept.Click
        Dim strMsg As String = vbNullString
        Me.Timer1.Enabled = False

        Dim strAE As String = vbNullString
        Dim strWLPath As String = "\\" & My.Settings.DATASOURCE & "\Priall\SHONANDAIICHI\MWM\home\"
        Dim strPatientID As String = vbNullString
        Dim strWLFullPath As String = vbNullString
        Dim aryFiles As New ArrayList

        Dim strOrderNumber As String = vbNullString
        Dim strValue As String = vbNullString
        Dim intCounter As Integer = 0

        '---2021.10.08 ADD By Watanabe
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next r
        strOrderNumber = Me.dtgList.Rows(lngSelRow).Cells("COL_ORDER_NO").Value

        '///受付取消処理開始
        Call subUpdAccept(0, strMsg)

        '-------------------------------------------------------------------------
        '///WLファイルの削除
        Select Case Me.cmbModality.SelectedItem
            Case "一般撮影"
                strAE = "PMSCR1"
            Case "ポータブル"        '---放射線科 ポータブル対応 2022.02.18 ADD By Watanabe
                strAE = "PMSCR2"
            Case "施設健診 "        '---放射線科 施設健診対応 2022.04.22 ADD By Watanabe
                strAE = "PMSCR2"
            Case "CT検査"
                strAE = "PMSCT1"
            Case "透視検査"
                strAE = "PMSRF1"
            Case "超音波"
                If Me.chkModality1.Checked = True Then          '未使用
                    strAE = "PMSUS4"
                ElseIf Me.chkModality2.Checked = True Then      'VIVID_S60
                    strAE = "PMSUS1"
                ElseIf Me.chkPorterble.Checked = True Then      'ポータブル
                    strAE = "PMSUS3"
                ElseIf Me.chkTmscPorterble.Checked = True Then  'XARIO
                    strAE = "PMSUS2"
                End If
            Case "内視鏡"
                If Me.chkModality1.Checked = True Then
                    strAE = "PMSES1"
                ElseIf Me.chkModality2.Checked = True Then
                    strAE = "PMSES2"
                End If
        End Select
        strWLFullPath = strWLPath & strAE
        strPatientID = Me.dtgList.Rows(lngSelRow).Cells("COL_PATIENT_ID").Value
        Call subGetAllFiles(strWLFullPath, "*.*", aryFiles, 0)

        '***********************************************
        '* MWM作成用フォルダ内の不要ファイルを移動する *
        '***********************************************
        For lngCounter = 0 To aryFiles.Count - 1
            If InStr(aryFiles(lngCounter).ToString, "lockfile") = 0 Then
                If InStr(aryFiles(lngCounter).ToString, strPatientID) > 0 Then
                    System.IO.File.Delete(aryFiles(lngCounter).ToString)
                    Call subOutLog("削除ファイル (" & aryFiles(lngCounter).ToString & ")", 0)
                End If
            End If

        Next lngCounter
        '-------------------------------------------------------------------------

        '///検査日のValueChanged Eventsを呼び出し、表示更新を行う。
        Call dtStudyDate_ValueChanged(sender, e)

        If My.Settings.XRAY_DIV = "2" Then
            For Each r As DataGridViewRow In dtgList.Rows
                If strOrderNumber = Me.dtgList.Rows(intCounter).Cells("COL_ORDER_NO").Value Then
                    Exit For
                End If
                intCounter += 1
            Next
            Me.dtgList.Rows(intCounter).Selected = True
            Me.dtgList_Click(sender, e)
        End If

        '---START Ver.3.0.2.3 CommentOut By Watanabe SortロジックはdtStudyDate_ValueChangedに移動 2021.10.15
        ''---START Ver.3.0.2.2 SORT状態の維持 ADD By Watanabe 2021.10.12
        'If dtgList.SortedColumn IsNot Nothing Then

        '    '並び替える列を決める
        '    Dim sortColumn As DataGridViewColumn =
        '        Me.dtgList.SortedColumn

        '    '並び替えの方向（昇順か降順か）を決める
        '    Dim sortDirection As System.ComponentModel.ListSortDirection

        '    If Me.dtgList.SortOrder = SortOrder.Ascending Then
        '        sortDirection = System.ComponentModel.ListSortDirection.Ascending
        '    Else
        '        sortDirection = System.ComponentModel.ListSortDirection.Descending
        '    End If

        '    '並び替えを行う
        '    dtgList.Sort(sortColumn, sortDirection)
        '    '---SORT
        '    'Else
        '    '    Return
        'End If
        ''---END Ver.3.0.2.2 SORT状態の維持 ADD By Watanabe 2021.10.12
        '---END Ver.3.0.2.3 CommentOut By Watanabe SortロジックはdtStudyDate_ValueChangedに移動 2021.10.15

        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "削除ボタン押下時"
    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim strMsg As String = vbNullString

        Me.Timer1.Enabled = False

        '///削除処理
        Call subUpdDelOrder(1, strMsg)

        '///検査日のValueChanged Eventsを呼び出し、表示更新を行う。
        Call dtStudyDate_ValueChanged(sender, e)

        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "削除取消ボタン押下時"
    Private Sub cmdCanDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCanDelete.Click
        Dim strMsg As String = vbNullString

        Me.Timer1.Enabled = False

        '///削除取消処理
        Call subUpdDelOrder(0, strMsg)

        '///検査日のValueChanged Eventsを呼び出し、表示更新を行う。
        Call dtStudyDate_ValueChanged(sender, e)

        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "メニュー"

#Region "バージョン情報クリック時"
    Private Sub バージョン情報ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles バージョン情報ToolStripMenuItem.Click
        Dim frmInfor As New frmSplash
        Me.Timer1.Enabled = False

        g_SystemStart = 1
        frmInfor.ShowDialog()
        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "ログアウトクリック時"
    Private Sub ログアウトToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ログアウトToolStripMenuItem.Click
        Call Me.cmdLogOut.PerformClick()
    End Sub
#End Region

#Region "自動更新クリック時"
    Private Sub Interval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Interval.Click
        '///自動更新のチェック状態によるIntervalメニューの制御
        If Interval.Checked = True Then
            Interval.Checked = False
            Interval1.Enabled = False
            Interval2.Enabled = False
            Interval3.Enabled = False
            Interval4.Enabled = False
            Interval5.Enabled = False
            Interval6.Enabled = False
        Else
            Interval.Checked = True
            Interval1.Enabled = True
            Interval2.Enabled = True
            Interval3.Enabled = True
            Interval4.Enabled = True
            Interval5.Enabled = True
            Interval6.Enabled = True
        End If

    End Sub
#End Region

#Region "自動更新チェック状態変化時"
    Private Sub Interval_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Interval.CheckedChanged
        Me.Timer1.Enabled = False

        '///自動更新のチェック状態によるTimerの制御
        My.Settings.AUTO_REFRESH = Interval.Checked
        My.Settings.Save()
        Me.Timer1.Enabled = Interval.Checked
    End Sub
#End Region

#Region "各Intervalクリック時"
    Private Sub Interval1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Interval1.Click, _
                                                                                                    Interval2.Click, _
                                                                                                    Interval3.Click, _
                                                                                                    Interval4.Click, _
                                                                                                    Interval5.Click, _
                                                                                                    Interval6.Click

        Dim intInterval As Integer

        '///不活性状態なら処理を抜ける
        If sender.Enabled = False Then Exit Sub

        '///クリックされた際のチェック状態での制御
        If sender.Checked = True Then
            sender.Checked = False
        Else
            sender.Checked = True

            Select Case sender.Name
                Case "Interval1"
                    intInterval = 5000
                    Interval2.Checked = False
                    Interval3.Checked = False
                    Interval4.Checked = False
                    Interval5.Checked = False
                    Interval6.Checked = False
                Case "Interval2"
                    intInterval = 10000
                    Interval1.Checked = False
                    Interval3.Checked = False
                    Interval4.Checked = False
                    Interval5.Checked = False
                    Interval6.Checked = False
                Case "Interval3"
                    intInterval = 15000
                    Interval1.Checked = False
                    Interval2.Checked = False
                    Interval4.Checked = False
                    Interval5.Checked = False
                    Interval6.Checked = False
                Case "Interval4"
                    intInterval = 30000
                    Interval1.Checked = False
                    Interval2.Checked = False
                    Interval3.Checked = False
                    Interval5.Checked = False
                    Interval6.Checked = False
                Case "Interval5"
                    intInterval = 60000
                    Interval1.Checked = False
                    Interval2.Checked = False
                    Interval3.Checked = False
                    Interval4.Checked = False
                    Interval6.Checked = False
                Case "Interval6"
                    intInterval = 90000
                    Interval1.Checked = False
                    Interval2.Checked = False
                    Interval3.Checked = False
                    Interval4.Checked = False
                    Interval5.Checked = False
                Case Else
                    intInterval = 30000
            End Select

            Timer1.Enabled = False

            '///IntervalをConfigに保存
            My.Settings.INTERVAL = intInterval
            My.Settings.Save()

            '///Intervalの再設定
            Timer1.Interval = intInterval
        End If


        Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "終了クリック時"
    Private Sub 終了ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 終了ToolStripMenuItem.Click
        Me.Timer1.Enabled = False

        If MsgBox("システムを終了します。" & vbCrLf & "宜しいですか。", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "システム終了") = MsgBoxResult.No Then
            Me.txtPatientID.Focus()
            Exit Sub
        End If

        My.Settings.FORM_SIZE_HEIGHT = Me.Height
        My.Settings.FORM_SIZE_WIDTH = Me.Width
        My.Settings.Save()

        End
    End Sub
#End Region

#Region "受付クリック時"
    Private Sub 受付ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 受付ToolStripMenuItem.Click
        Call cmdAccept_Click(sender, e)
    End Sub
#End Region

#Region "受付取消クリック時"
    Private Sub 受付取消ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 受付取消ToolStripMenuItem.Click
        Call cmdCanAccept_Click(sender, e)
    End Sub
#End Region

#Region "削除クリック時"
    Private Sub 削除ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 削除ToolStripMenuItem.Click
        Call cmdDelete_Click(sender, e)
    End Sub
#End Region

#Region "削除取消クリック時"
    Private Sub 削除取消ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 削除取消ToolStripMenuItem.Click
        Call cmdCanDelete_Click(sender, e)
    End Sub
#End Region

#Region "オーダ明細クリック時"
    Private Sub オーダ明細ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles オーダ明細ToolStripMenuItem.Click
        Call cmdOrderDetail_Click(sender, e)
    End Sub
#End Region

#Region "画像表示クリック"
    Private Sub 画像表示ToolStripMenuItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles 画像表示ToolStripMenuItem.Click
        Call subViewer()
    End Sub
#End Region

#Region "オーダ情報登録/変更クリック時"
    Private Sub mnuEditOrder_Click(sender As System.Object, e As System.EventArgs) Handles mnuEditOrder.Click
        Me.Timer1.Enabled = False
        Call cmdEditOrder_Click(sender, e)
        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub

#End Region

#Region "患者情報登録/変更クリック時"
    Private Sub mnuEditPatient_Click(sender As System.Object, e As System.EventArgs) Handles mnuEditPatient.Click
        Me.Timer1.Enabled = False
        Call cmdEditPatient_Click(sender, e)
        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#End Region

#Region "コンテキストメニュー"

#Region "オーダ明細クリック"
    Private Sub オーダ明細ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles オーダ明細ToolStripMenuItem1.Click
        Call cmdOrderDetail_Click(sender, e)
    End Sub
#End Region

#Region "部位修正選択"
    Private Sub 部位情報修正ToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles 部位情報修正ToolStripMenuItem1.Click
        Dim lngCounter As Long = 0
        Dim bolFlg As Boolean = False
        Dim strMsg As String = vbNullString
        Dim strOrderAno As String = vbNullString

        Me.Timer1.Enabled = False

        'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
        '///職員(先頭1桁"K")以外は先頭0埋め
        If Me.txtPatientID.Text <> "" And Me.txtPatientID.TextLength > 2 And IsNumeric(Me.txtPatientID.Text) = False Then
            If Not Me.txtPatientID.Text.Substring(0, 1).ToUpper = "K" And _
               Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "SA" And _
               Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "TE" Then
                'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
            End If
        ElseIf Me.txtPatientID.Text <> "" And IsNumeric(Me.txtPatientID.Text) = True Then
            'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
        Else
            Me.txtPatientID.Text = Me.txtPatientID.Text.ToUpper
        End If

        For lngCounter = 0 To dtgList.RowCount - 1
            If (dtgList.Rows(lngCounter).Cells("COL_PATIENT_ID").Value = Me.txtPatientID.Text.Trim) And _
               (dtgList.Rows(lngCounter).Cells("COL_DEL_FLG").Value <> "1") Then
                'If (dtgList.Rows(lngCounter).Cells("COL_PATIENT_ID").Value = Me.txtPatientID.Text.Trim) And _
                '   (dtgList.Rows(lngCounter).Cells("COL_STATE").Value = "未受付") And _
                '   (dtgList.Rows(lngCounter).Cells("COL_DEL_FLG").Value <> "1") Then

                strOrderAno = dtgList.Rows(lngCounter).Cells("COL_ORDER_ANO").Value
                dtgList.Rows(lngCounter).Selected = True
                bolFlg = True
                Exit For
            End If
        Next lngCounter

        If bolFlg = True Then

            '---START--- オーダ情報修正から部位情報修正に変更の為 UpDated By Watanabe 2012.03.01
            'frmEditOrder.PATIENT_ID = Me.txtPatientID.Text.Trim
            'frmEditOrder.SELECTED_MODALITY = Me.cmbModality.SelectedIndex
            'frmEditOrder.ORDER_DATE = aryOrderPatinet(lngCounter).ORDER_DATE
            'frmEditOrder.ORDER_TIME = aryOrderPatinet(lngCounter).ORDER_TIME
            'frmEditOrder.ORDER_ANO = strOrderAno
            'frmEditOrder.SELECTED_MODALITY_NO = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO
            frmEditBodyPart.PATIENT_ID = Me.txtPatientID.Text.Trim
            frmEditBodyPart.SELECTED_MODALITY = Me.cmbModality.SelectedIndex
            frmEditBodyPart.ORDER_DATE = aryOrderPatinet(lngCounter).ORDER_DATE
            frmEditBodyPart.ORDER_TIME = aryOrderPatinet(lngCounter).ORDER_TIME
            frmEditBodyPart.ORDER_ANO = strOrderAno
            frmEditBodyPart.SELECTED_MODALITY_NO = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO
            'Me.cmdEditOrder_Click(sender, e)

            frmEditBodyPart.ShowDialog()

            '---END--- オーダ情報修正から部位情報修正に変更の為 UpDated By Watanabe 2012.03.01

        End If

        '///最新データを取得し、一覧表示
        Call dtStudyDate_ValueChanged(sender, e)

        '///入力された患者IDデータを選択
        For lngCounter = 0 To dtgList.RowCount - 1
            If (dtgList.Rows(lngCounter).Cells("COL_PATIENT_ID").Value = Me.txtPatientID.Text.Trim) And _
               (dtgList.Rows(lngCounter).Cells("COL_STATE").Value = "0") And _
               (dtgList.Rows(lngCounter).Cells("COL_DEL_FLG").Value <> "1") Then

                dtgList.Rows(lngCounter).Selected = True
                Exit For
            End If
        Next lngCounter

        Call dtStudyDate_ValueChanged(sender, e)

        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "受付クリック"
    Private Sub 受付ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 受付ToolStripMenuItem1.Click
        Timer1.Enabled = False
        '///カードリーダ用タイマの停止 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = False

        '///選択行位置を取り直す ADD Watanabe 2014.05.27
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next

        Call cmdAccept_Click(sender, e)

        '///カードリーダ用タイマの再開 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = True
    End Sub
#End Region

#Region "受付取消クリック"
    Private Sub 受付取消ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 受付取消ToolStripMenuItem1.Click
        Timer1.Enabled = False
        '///カードリーダ用タイマの停止 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = False

        '///選択行位置を取り直す ADD Watanabe 2014.05.27
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next

        Call cmdCanAccept_Click(sender, e)

        '///カードリーダ用タイマの再開 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = True

    End Sub
#End Region

#Region "削除クリック"
    Private Sub 削除ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 削除ToolStripMenuItem1.Click
        Timer1.Enabled = False
        '///カードリーダ用タイマの停止 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = False

        '///選択行位置を取り直す ADD Watanabe 2014.05.27
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next

        Call cmdDelete_Click(sender, e)

        '///カードリーダ用タイマの再開 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = True
    End Sub
#End Region

#Region "削除取消クリック"
    Private Sub 削除取消ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles 削除取消ToolStripMenuItem1.Click
        Timer1.Enabled = False
        '///カードリーダ用タイマの停止 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = False

        '///選択行位置を取り直す ADD Watanabe 2014.05.27
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next

        Call cmdCanDelete_Click(sender, e)

        '///カードリーダ用タイマの再開 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = True
    End Sub
#End Region

#Region "実施クリック"
    Private Sub 実施ToolStripMenuItem1_Click(sender As Object, e As System.EventArgs) Handles 実施ToolStripMenuItem1.Click
        Timer1.Enabled = False
        '///カードリーダ用タイマの停止 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = False

        '///選択行位置を取り直す ADD Watanabe 2014.05.27
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next

        Call cmdJisshi_Click(sender, e)

        '///カードリーダ用タイマの再開 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = True
    End Sub
#End Region

#Region "実施取消クリック"
    Private Sub 実施取消ToolStripMenuItem1_Click(sender As Object, e As System.EventArgs) Handles 実施取消ToolStripMenuItem1.Click
        Timer1.Enabled = False
        '///カードリーダ用タイマの停止 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = False

        '///選択行位置を取り直す ADD Watanabe 2014.05.27
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next

        Call cmdCanJisshi_Click(sender, e)

        '///カードリーダ用タイマの再開 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = True
    End Sub
#End Region

#Region "画像表示クリック"
    Private Sub 画像表示ToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles 画像表示ToolStripMenuItem1.Click
        Call subViewer()
    End Sub
#End Region

#Region "患者情報修正"
    Private Sub 患者情報修正ToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles 患者情報修正ToolStripMenuItem.Click
        Timer1.Enabled = False
        '///カードリーダ用タイマの停止 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = False

        '///選択行のIndexを退避
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next r
        'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
        '///職員(先頭1桁"K")以外は先頭0埋め
        If Me.txtPatientID.Text <> "" And Me.txtPatientID.TextLength > 2 And IsNumeric(Me.txtPatientID.Text) = False Then
            If Not Me.txtPatientID.Text.Substring(0, 1).ToUpper = "K" And _
               Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "SA" And _
               Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "TE" Then
                'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
            End If
        ElseIf Me.txtPatientID.Text <> "" And IsNumeric(Me.txtPatientID.Text) = True Then
            'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
        Else
            Me.txtPatientID.Text = Me.txtPatientID.Text.ToUpper
        End If

        If lngSelRow < 0 Then Exit Sub
        With aryOrderPatinet(lngSelRow)
            frmEditPatient.PATIENT_ANO = .PATIENT_ANO
            If dtgList.Rows(lngSelRow).Cells("COL_STUDY_DIV").Value = "外来" Then
                frmEditPatient.STUDY_DIV = 0
            ElseIf dtgList.Rows(lngSelRow).Cells("COL_STUDY_DIV").Value = "健診" Then
                frmEditPatient.STUDY_DIV = 1
            Else
                frmEditPatient.STUDY_DIV = 0
            End If
            frmEditPatient.PATIENT_ANO = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_ANO").Value
            frmEditPatient.PATIENT_ID = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_ID").Value
            frmEditPatient.KENSHIN_PATIENT_ID = dtgList.Rows(lngSelRow).Cells("COL_KENSHIN_PATIENT_ID").Value
            frmEditPatient.PATIENT_KANA = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_KANA").Value
            frmEditPatient.PATIENT_KANJI = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_KANJI").Value
            frmEditPatient.PATIENT_EIJI = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_EIJI").Value
            frmEditPatient.PATIENT_BIRTH = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_BIRTH").Value
            frmEditPatient.PATIENT_SEX = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_SEX").Value
            frmEditPatient.PATIENT_COMMENT = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_COMMENT").Value
            frmEditPatient.STUDY_DATE = Me.dtStudyDate.Value.ToString("yyyyMMdd")
            frmEditPatient.SELECTED_MODALITY = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO

            'frmEditPatient.PATIENT_ID = .PATIENT_ID
            'frmEditPatient.PATIENT_KANA = .PATIENT_KANA
            'frmEditPatient.PATIENT_KANJI = .PATIENT_KANJI
            'frmEditPatient.PATIENT_EIJI = .PATIENT_EIJI
            'frmEditPatient.PATIENT_BIRTH = .PATIENT_BIRTH
            'frmEditPatient.PATIENT_SEX = .PATIENT_SEX
            'frmEditPatient.PATIENT_COMMENT = .PATIENT_COMMENT
            frmEditPatient.UPD_FLG = "1"
        End With

        frmEditPatient.ShowDialog()

        Call dtStudyDate_ValueChanged(sender, e)

        Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "画像参照選択"
    Private Sub 画像ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call subViewer()
    End Sub
#End Region

#End Region

#Region "Timer起動時"
    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        '///処理中にTickEventに入らないように不活性化する。
        Timer1.Enabled = False
        '///カードリーダ用タイマの停止 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = False

        '///検査日のValueChanged Eventsを呼び出し、表示更新を行う。
        Call dtStudyDate_ValueChanged(sender, e)

        '///処理後、活性化する。
        Timer1.Enabled = True
        '///カードリーダ用タイマの再開 ADD By Watanabe 2014.05.27
        tmrProc.Enabled = True
    End Sub
#End Region

#Region "各表示切替チェック状態変化時"
    Private Sub chkNonAccept_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNonAccept.CheckedChanged, _
                                                                                                                chkAccept.CheckedChanged, _
                                                                                                                chkNonDelete.CheckedChanged, _
                                                                                                                chkDelete.CheckedChanged
        'Select Case sender.Name
        '    Case "chkNonAccept"
        '        If sender.Checked = False Then
        '            Me.chkAccept.Checked = True
        '        End If
        '    Case "chkAccept"
        '        If sender.Checked = False Then
        '            Me.chkNonAccept.Checked = True
        '        End If
        '    Case "chkNonDelete"
        '        If sender.Checked = False Then
        '            Me.chkDelete.Checked = True
        '        End If
        '    Case "chkDelete"
        '        If sender.Checked = False Then
        '            Me.chkNonDelete.Checked = True
        '        End If
        'End Select

        Dim strMsg As String = vbNullString

        If bolInit = True Then Exit Sub

        Me.Timer1.Enabled = False

        '///検査日のValueChanged Eventsを呼び出し、表示更新を行う。
        Call dtStudyDate_ValueChanged(sender, e)

        Me.txtPatientID.Focus()

        '///表示切替チェック状態を保存
        With My.Settings
            .ACCEPT = Me.chkAccept.Checked
            .NON_ACCEPT = Me.chkNonAccept.Checked
            .DELETE = Me.chkDelete.Checked
            .NON_DELETE = Me.chkNonDelete.Checked
            '.Finish = Me.chkEnd.Checked
            .STUDY_ACCEPT = Me.chkStudyAccept.Checked
            .STUDY_END = Me.chkStudyEnd.Checked
            .Save()
        End With

        If Me.Timer1.Enabled <> My.Settings.AUTO_REFRESH Then
            Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
        End If
    End Sub
#End Region

#Region "ログアウトボタン押下"
    Private Sub cmdLogOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLogOut.Click
        'Dim frmLongin As New frmLogin
        'Me.Timer1.Enabled = False

        'bolFormClose = True

        'If MsgBox("ログアウトします。" & vbCrLf & "宜しいですか。", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "ログアウト") = MsgBoxResult.No Then
        '    Me.txtPatientID.Focus()
        '    Exit Sub
        'End If

        'Call subOutLog("ログアウト frmMain.cmdLogOut_Click", 0)

        'My.Settings.FORM_SIZE_HEIGHT = Me.Height
        'My.Settings.FORM_SIZE_WIDTH = Me.Width
        'My.Settings.Save()

        'Me.Close()

        'frmLogin.Show()


        If My.Settings.LOGIN_DIV = "0" Then

            If MsgBox("システムをログアウトします。" & vbCrLf & "宜しいですか。", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "システムログアウト") = MsgBoxResult.No Then
                Exit Sub
            End If

            Dim frmLogin As New frmLogin

            frmLogin.Show()

            Me.Close()
            'Call frmMain_FormClosing(sender, e)
            Me.Hide()
        Else
            Me.Close()
            'Call frmMain_FormClosing(sender, e)
            End
        End If
    End Sub
#End Region

#Region "オーダ明細ボタン押下 "
    Private Sub cmdOrderDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOrderDetail.Click
        Dim frmDeatail As New frmOrderDetails

        Me.Timer1.Enabled = False

        With typSelData
            .ORDER_ANO = Me.dtgList.Rows(lngSelRow).Cells("COL_ORDER_ANO").Value
            '            .ORDER_NO = Me.dtgList.Rows(lngSelRow).Cells("COL_ORDER_NO").Value
            .ORDER_DATE = Me.dtgList.Rows(lngSelRow).Cells("COL_ORDER_DATE").Value
            .ORDER_TIME = Me.dtgList.Rows(lngSelRow).Cells("COL_ORDER_TIME").Value
            .STUDY_DATE = Me.dtgList.Rows(lngSelRow).Cells("COL_STUDY_DATE").Value
            .STUDY_TIME = Me.dtgList.Rows(lngSelRow).Cells("COL_STUDY_TIME").Value
            .MODALITY_CD = Me.dtgList.Rows(lngSelRow).Cells("COL_MODALITY").Value
            .MODALITY_NAME = Me.dtgList.Rows(lngSelRow).Cells("COL_STUDY_NAME").Value
            .BODY_PART = Me.dtgList.Rows(lngSelRow).Cells("COL_BODY_PART").Value

            .PATIENT_ID = Me.dtgList.Rows(lngSelRow).Cells("COL_PATIENT_ID").Value
            .PATIENT_KANJI = Me.dtgList.Rows(lngSelRow).Cells("COL_PATIENT_KANJI").Value
            .PATIENT_KANA = Me.dtgList.Rows(lngSelRow).Cells("COL_PATIENT_KANA").Value
            .PATIENT_EIJI = Me.dtgList.Rows(lngSelRow).Cells("COL_PATIENT_EIJI").Value

            Select Case Me.dtgList.Rows(lngSelRow).Cells("COL_PATIENT_SEX").Value
                Case "男"
                    .PATIENT_SEX = "1"
                Case "女"
                    .PATIENT_SEX = "2"
                Case "不明"
                    .PATIENT_SEX = "9"
                Case ""
                    .PATIENT_SEX = "0"
            End Select
            .PATIENT_BIRTH = Me.dtgList.Rows(lngSelRow).Cells("COL_PATIENT_BIRTH").Value
            .PATIENT_BIRTH = .PATIENT_BIRTH.Replace("/", "")
            .PATIENT_COMMENT = Me.dtgList.Rows(lngSelRow).Cells("COL_PATIENT_COMMENT").Value
            .ORDER_COMMENT = Me.dtgList.Rows(lngSelRow).Cells("COL_ORDER_COMMENT").Value
        End With

        frmDeatail.ShowDialog()

        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "患者情報登録/更新ボタン押下 "
    Private Sub cmdEditPatient_Click(sender As System.Object, e As System.EventArgs) Handles cmdEditPatient.Click
        Dim lngCounter As Long = 0
        Dim bolFlg As Boolean = False
        Dim strMsg As String = vbNullString
        Dim strOrderAno As String = vbNullString
        Dim lngOrderAno As Long = 0

        Me.Timer1.Enabled = False

        'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
        '///職員(先頭1桁"K")以外は先頭0埋め
        If Me.txtPatientID.Text <> "" And Me.txtPatientID.TextLength > 2 And IsNumeric(Me.txtPatientID.Text) = False Then
            If Not Me.txtPatientID.Text.Substring(0, 1).ToUpper = "K" And _
               Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "SA" And _
               Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "TE" Then
                'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
            End If
        ElseIf Me.txtPatientID.Text <> "" And IsNumeric(Me.txtPatientID.Text) = True Then
            'Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength)
        Else
            Me.txtPatientID.Text = Me.txtPatientID.Text.ToUpper
        End If

        If g_Card = False Then
            frmEditPatient.PATIENT_ID = Me.txtPatientID.Text
            frmEditPatient.STUDY_DATE = Me.dtStudyDate.Value.ToString("yyyyMMdd")
            frmEditPatient.SELECTED_MODALITY = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO
            If fncChkPatient(strMsg) = RET_NOTFOUND Then
                frmEditPatient.UPD_FLG = "0"
                frmEditPatient.PATIENT_KANA = vbNullString
                frmEditPatient.PATIENT_BIRTH = vbNullString
                frmEditPatient.PATIENT_EIJI = vbNullString
                frmEditPatient.PATIENT_SEX = vbNullString
                frmEditPatient.PATIENT_COMMENT = vbNullString

                frmEditPatient.ShowDialog()
            Else
                frmEditPatient.UPD_FLG = "1"
                frmEditPatient.PATIENT_ANO = typPatient.PATIENT_ANO
                frmEditPatient.PATIENT_KANA = typPatient.PATIENT_KANA
                frmEditPatient.PATIENT_BIRTH = typPatient.PATIENT_BIRTH
                frmEditPatient.PATIENT_EIJI = typPatient.PATIENT_EIJI
                frmEditPatient.PATIENT_SEX = typPatient.PATIENT_SEX
                frmEditPatient.PATIENT_COMMENT = typPatient.PATIENT_COMMENT
                frmEditPatient.ShowDialog()
            End If
            If frmEditPatient.CANCEL_FLG = "1" Then Exit Sub

        Else
            frmEditOrder.STUDY_DIV = 0
            frmEditPatient.STUDY_DIV = 0
        End If

        '---START--- 入力された患者IDのオーダが既に存在するかチェックしない Comment By Watanabe 2012.03.01
        'For lngCounter = 0 To dtgList.RowCount - 1
        '    If (dtgList.Rows(lngCounter).Cells("COL_PATIENT_ID").Value = Me.txtPatientID.Text.Trim) And _
        '       (dtgList.Rows(lngCounter).Cells("COL_STATE").Value = "未受付") And _
        '       (dtgList.Rows(lngCounter).Cells("COL_DEL_FLG").Value <> "1") Then

        '        strOrderAno = dtgList.Rows(lngCounter).Cells("COL_ORDER_ANO").Value
        '        dtgList.Rows(lngCounter).Selected = True
        '        bolFlg = True
        '        Exit For
        '    End If
        'Next lngCounter
        '---END--- 入力された患者IDのオーダが既に存在するかチェックしない Comment By Watanabe 2012.03.01

        '///表示一覧上に入力された患者IDが存在するかチェック
        If bolFlg = True Then

            'frmEditOrder.PATIENT_ID = Me.txtPatientID.Text.Trim
            'frmEditOrder.SELECTED_MODALITY = Me.cmbModality.SelectedIndex
            'frmEditOrder.ORDER_DATE = Me.dtStudyDate.Value.ToString
            'frmEditOrder.ORDER_ANO = strOrderAno

            'Me.cmdEditOrder_Click(sender, e)

            '---START--- 入力された患者IDと同一のオーダがあっても受付しない Comment By Watanabe 2012.03.01
            '///存在時、受付処理を行う。
            'Call Me.cmdAccept_Click(sender, e)
            '---END--- 入力された患者IDと同一のオーダがあっても受付しない Comment By Watanabe 2012.03.01

        Else
            '///未存在時、入力された患者IDが患者情報に存在するかチェック
            'If fncChkIjiDB(strMsg) = RET_NOTFOUND Then                '未存在時

            '///医事側DB参照
            ' ''If fncChkPatient(strMsg) = RET_NOTFOUND Then
            ' ''    '///入力された患者IDが患者情報に存在しない場合、新規登録を促す
            ' ''    If MsgBox("入力された患者IDは患者情報に存在しませんでした。" & vbCrLf & "新規に患者情報を登録しますか。", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "受付処理") = MsgBoxResult.No Then Exit Sub
            ' ''    frmEditPatient.PATIENT_ID = Me.txtPatientID.Text.Trim
            ' ''    frmEditPatient.KENSHIN_PATIENT_ID = ""
            ' ''    frmEditPatient.PATIENT_ANO = ""
            ' ''    frmEditPatient.PATIENT_KANA = Me.txtPatientKana.Text.Trim
            ' ''    frmEditPatient.PATIENT_KANJI = ""
            ' ''    frmEditPatient.PATIENT_EIJI = ""
            ' ''    frmEditPatient.PATIENT_BIRTH = ""
            ' ''    frmEditPatient.PATIENT_SEX = ""
            ' ''    frmEditPatient.UPD_FLG = "0"
            ' ''    frmEditPatient.STUDY_DATE = Me.dtStudyDate.Value.ToString("yyyyMMdd")
            ' ''    frmEditPatient.SELECTED_MODALITY = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO
            ' ''    frmEditPatient.ShowDialog()
            ' ''End If
            'Else                                                        '存在時
            '///医事患者情報ローカルDB存在チェック
            If fncChkPatient(strMsg) = RET_NOTFOUND Then

                '///患者情報登録処理
                If fncInsPatient(strMsg) = RET_ERROR Then
                    Call MsgBox("患者情報登録にてエラーが発生しました。")
                    Exit Sub
                End If
            Else
                '///患者情報更新処理
                If fncUpdPatient(strMsg) = RET_ERROR Then
                    Call MsgBox("患者情報更新にてエラーが発生しました。")
                    Exit Sub
                End If
            End If

            '///ローカルDB患者情報取得
            Call fncChkPatient(strMsg)

            Me.txtPatientKana.Text = typPatient.PATIENT_KANA
            'Me.txtPatientKana.Update()
            strOrderAno = ""

            '---START--- 確認メッセージを表示しない Comment By Watanabe 2012.03.01
            '///入力された患者IDが患者情報に存在する場合、オーダ情報の新規登録を促す
            'If MsgBox("入力された患者情報でのオーダ情報がありません。" & vbCrLf & "新規にオーダ情報を登録しますか。", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "受付処理") = MsgBoxResult.No Then Exit Sub
            '---END--- 確認メッセージを表示しない Comment By Watanabe 2012.03.01

            '---START--- オーダ登録画面に遷移させず、そのままオーダ登録し、自動受付を行う Comment By Watanabe 2012.03.01 
            frmEditOrder.PATIENT_ID = Me.txtPatientID.Text
            frmEditOrder.SELECTED_MODALITY = Me.cmbModality.SelectedIndex
            frmEditOrder.ORDER_DATE = Me.dtStudyDate.Value.ToString("yyyyMMdd")
            frmEditOrder.ORDER_TIME = Me.dtStudyDate.Value.ToString("HHmmss")
            frmEditOrder.ORDER_ANO = strOrderAno
            frmEditOrder.SELECTED_MODALITY_NO = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO
            frmEditOrder.KENSHIN_PATIENT_ID = typPatient.KENSHIN_PATIENT_ID
            'Me.cmdEditOrder_Click(sender, e)

            If IsNumeric(frmEditOrder.ORDER_ANO) = True Then
                lngOrderAno = frmEditOrder.ORDER_ANO
            End If

            frmEditOrder.PATIENT_ID = ""
            frmEditOrder.SELECTED_MODALITY = ""
            frmEditOrder.ORDER_DATE = Me.dtStudyDate.Value.ToString("yyyyMMdd")
            frmEditOrder.ORDER_TIME = Me.dtStudyDate.Value.ToString("HHmmss")
            frmEditOrder.ORDER_ANO = ""
            frmEditOrder.KENSHIN_PATIENT_ID = ""
            frmEditOrder.SELECTED_MODALITY_NO = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO
            '---END--- オーダ登録画面に遷移させず、そのままオーダ登録し、自動受付を行う Comment By Watanabe 2012.03.01

            '---START--- オーダ登録機能を追加 ADD By Watanabe 2012.03.01
            If fncInsOrder(strMsg) = RET_ERROR Then
                MsgBox(strMsg, MsgBoxStyle.Critical, "オーダ登録")
                Exit Sub
            End If
            '---END--- オーダ登録機能を追加 ADD By Watanabe 2012.03.01
            'End If
        End If

        '///最新データを取得し、一覧表示
        Call dtStudyDate_ValueChanged(sender, e)

        '///オーダ登録画面から戻った後、対称オーダを一覧から検索し、選択行とする。 UpDated By Watanabe 2012.02.03
        'For lngCounter = 0 To dtgList.RowCount - 1
        '    If strOrderAno <> "" Then
        '        If (dtgList.Rows(lngCounter).Cells("COL_ORDER_ANO").Value = strOrderAno) Then

        '            dtgList.Rows(lngCounter).Selected = True
        '            Exit For
        '        End If
        '    End If
        'Next lngCounter
        Dim lngRowCounter As Long = 0
        Dim strPatientID As String = vbNullString
        Dim strKana As String = vbNullString

        If lngOrderAno > 0 Then
            For lngRowCounter = 0 To Me.dtgList.Rows.Count - 1
                If lngOrderAno = Me.dtgList.Rows(lngRowCounter).Cells("COL_ORDER_ANO").Value Then
                    Me.dtgList.Rows(lngRowCounter).Selected = True
                    strPatientID = Me.dtgList.Rows(lngRowCounter).Cells("COL_PATIENT_ID").Value
                    strKana = Me.dtgList.Rows(lngRowCounter).Cells("COL_PATIENT_KANA").Value
                    lngSelRow = lngRowCounter
                    Exit For
                End If
            Next lngRowCounter

            '///START 患者ID、カナ氏名が消えてしまうので条件を付与 UpDated By Watanabe 2012.03.07
            'Me.txtPatientID.Text = strPatientID
            'Me.txtPatientKana.Text = strKana
            If strPatientID <> "" Then
                Me.txtPatientID.Text = strPatientID
            End If
            If strKana <> "" Then
                Me.txtPatientKana.Text = strKana
            End If
            '///END 患者ID、カナ氏名が消えてしまうので条件を付与 UpDated By Watanabe 2012.03.07

        Else
            '///入力された患者IDデータを選択
            For lngCounter = 0 To dtgList.RowCount - 1
                If strOrderAno <> "" Then
                    If (dtgList.Rows(lngCounter).Cells("COL_ORDER_ANO").Value = strOrderAno) Then

                        dtgList.Rows(lngCounter).Selected = True
                        strPatientID = Me.dtgList.Rows(lngCounter).Cells("COL_PATIENT_ID").Value
                        strKana = Me.dtgList.Rows(lngCounter).Cells("COL_PATIENT_KANA").Value
                        'lngSelRow = lngCounter
                        Exit For
                    End If
                End If
            Next lngCounter

            '///オーダ登録後は常に先頭行を選択 ADD By Watanae 2012.04.03
            If dtgList.Rows.Count > 0 Then
                dtgList.Rows(0).Selected = True
            End If

            '///START 患者ID、カナ氏名が消えてしまうので条件を付与 UpDated By Watanabe 2012.03.07
            'Me.txtPatientID.Text = strPatientID
            'Me.txtPatientKana.Text = strKana
            If strPatientID <> "" Then
                Me.txtPatientID.Text = strPatientID
            End If
            If strKana <> "" Then
                Me.txtPatientKana.Text = strKana
            End If
            '///END 患者ID、カナ氏名が消えてしまうので条件を付与 UpDated By Watanabe 2012.03.07

        End If

        'Call dtStudyDate_ValueChanged(sender, e)
        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "オーダ情報登録/更新ボタン押下"
    Private Sub cmdEditOrder_Click(sender As System.Object, e As System.EventArgs) Handles cmdEditOrder.Click

        'If Me.cmbModality.SelectedIndex = -1 Then
        '    frmEditOrder.SELECTED_MODALITY = ""
        'Else
        '    frmEditOrder.SELECTED_MODALITY = Me.cmbModality.SelectedIndex
        'End If
        'frmEditOrder.SELECTED_MODALITY_NO = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO
        'frmEditOrder.ORDER_DATE = dtStudyDate.Value.ToString("yyyyMMdd")
        'frmEditOrder.ORDER_TIME = dtStudyDate.Value.ToString("HHmm")
        frmEditOrder.ORDER_TIME = Now.ToString("HHmm")

        frmEditOrder.ShowDialog()
        Dim lngOrderAno As Long = 0
        Dim strPatientID As String = vbNullString
        Dim strKana As String = vbNullString

        If IsNumeric(frmEditOrder.ORDER_ANO) = True Then
            lngOrderAno = frmEditOrder.ORDER_ANO
        End If

        Call dtStudyDate_ValueChanged(sender, e)

        '///オーダ登録画面から戻った後、対称オーダを一覧から検索し、選択行とする。 ADD By Watanabe 2012.02.03
        Dim lngCounter As Long = 0

        If lngOrderAno > 0 Then
            For lngCounter = 0 To Me.dtgList.Rows.Count - 1
                If lngOrderAno = Me.dtgList.Rows(lngCounter).Cells("COL_ORDER_ANO").Value Then
                    Me.dtgList.Rows(lngCounter).Selected = True
                    strPatientID = Me.dtgList.Rows(lngCounter).Cells("COL_PATIENT_ID").Value
                    strKana = Me.dtgList.Rows(lngCounter).Cells("COL_PATIENT_KANA").Value
                    Exit For
                End If
            Next lngCounter

            Me.txtPatientID.Text = strPatientID
            Me.txtPatientKana.Text = strKana
        End If

    End Sub
#End Region

#Region "受付ボタンEnabled変化時"
    Private Sub cmdAccept_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAccept.EnabledChanged, _
                                                                                                      cmdCanAccept.EnabledChanged, _
                                                                                                      cmdDelete.EnabledChanged, _
                                                                                                      cmdCanDelete.EnabledChanged, _
                                                                                                      cmdJisshi.EnabledChanged, _
                                                                                                      cmdCanJisshi.EnabledChanged

        Select Case sender.Name
            Case "cmdAccept"
                Me.受付ToolStripMenuItem.Enabled = sender.Enabled
                Me.受付ToolStripMenuItem1.Enabled = sender.Enabled
            Case "cmdCanAccept"
                Me.受付取消ToolStripMenuItem.Enabled = sender.Enabled
                Me.受付取消ToolStripMenuItem1.Enabled = sender.Enabled
            Case "cmdDelete"
                Me.削除ToolStripMenuItem.Enabled = sender.Enabled
                Me.削除ToolStripMenuItem1.Enabled = sender.Enabled
            Case "cmdCanDelete"
                Me.削除取消ToolStripMenuItem.Enabled = sender.Enabled
                Me.削除取消ToolStripMenuItem1.Enabled = sender.Enabled
            Case "cmdJisshi"
                Me.実施ToolStripMenuItem.Enabled = sender.Enabled
                Me.実施ToolStripMenuItem1.Enabled = sender.Enabled
            Case "cmdCanJisshi"
                Me.実施取消ToolStripMenuItem.Enabled = sender.Enabled
                Me.実施取消ToolStripMenuItem1.Enabled = sender.Enabled
            Case ""
        End Select

    End Sub
#End Region

#Region "モダリティ選択時"
    Private Sub cmbModality_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbModality.SelectedIndexChanged
        '---START 放射線科 ポータブル対応 2022.02.18 UpDated By Watanabe
        'If Me.cmbModality.SelectedItem = "一般撮影"  Then
        '---END 放射線科 ポータブル対応 2022.02.18 UpDated By Watanabe
        If Me.cmbModality.SelectedItem = "一般撮影" Or Me.cmbModality.SelectedItem = "ポータブル" Or Me.cmbModality.SelectedItem = "施設健診" Then
            Me.cmdPortable.Visible = True
            Me.cmdPortable.Text = "ファイル一括削除"
        Else
            Me.cmdPortable.Visible = False
        End If
        Timer1.Enabled = False

        '///強制的に日付変更を呼び出す
        Call dtStudyDate_ValueChanged(sender, e)

        Timer1.Enabled = My.Settings.AUTO_REFRESH

    End Sub
#End Region

#Region "患者ID変化時"
    Private Sub txtPatientID_TextChanged(sender As Object, e As System.EventArgs) Handles txtPatientID.TextChanged
        '///カナ氏名をクリアする。
        Me.txtPatientKana.Text = vbNullString
    End Sub
#End Region

#Region "CSV出力ボタン押下"

    Private Sub cmdOutCSV_Click(sender As System.Object, e As System.EventArgs) Handles cmdOutCSV.Click
        Dim lngCounter As Long
        Dim strData As String = String.Empty
        Dim lngColCounter As Long = 0
        Dim FileNM As String = vbNullString
        Dim file_LOG As String = String.Empty
        Dim bolFlg As Boolean = False
        Dim lngZouei As Long = 0
        Dim lngTanjun As Long = 0

        saveCSV.InitialDirectory = My.Settings.CSV_PATH
        FileNM = DateTime.Now.ToString("yyyyMMdd") + ".csv"
        saveCSV.FileName = FileNM
        If saveCSV.ShowDialog() = DialogResult.Cancel Then Exit Sub

        file_LOG = saveCSV.FileName

        'file_LOG = My.Settings.CSV_PATH & _
        '           DateTime.Now.ToString("yyyyMMdd") + ".csv"

        Dim objsw As New System.IO.StreamWriter(file_LOG, _
                                               False, _
                                               System.Text.Encoding.GetEncoding(932))
        'strData = "進捗" & vbTab
        'strData &= "撮影室" & vbTab
        'strData &= "検査日" & vbTab
        'strData &= "検査時刻" & vbTab
        'strData &= "患者ID" & vbTab
        'strData &= "患者漢字氏名" & vbTab
        'strData &= "患者カナ氏名" & vbTab
        'strData &= "患者英字氏名" & vbTab
        'strData &= "生年月日" & vbTab
        'strData &= "性別" & vbTab
        'strData &= "撮影項目" & vbTab
        'strData &= "撮影部位" & vbTab
        'objsw.WriteLine(strData)


        '///START 技師長 ご要望によるカスタマイズ ADD By Watanabe 2012.03.06
        objsw.WriteLine("検査日：" & Me.dtStudyDate.Value.ToString("yyyy/MM/dd"))
        objsw.WriteLine("検査室：" & Me.cmbModality.SelectedItem)
        objsw.WriteLine("")
        '///END 技師長 ご要望によるカスタマイズ ADD By Watanabe 2012.03.06

        For lngColCounter = 0 To Me.dtgList.Columns.Count - 1

            If Me.dtgList.Columns(CInt(lngColCounter)).Visible = True Then
                If strData = "" Then
                    strData = Me.dtgList.Columns(CInt(lngColCounter)).HeaderText
                Else
                    strData &= vbTab & Me.dtgList.Columns(CInt(lngColCounter)).HeaderText
                End If
            End If
        Next lngColCounter
        objsw.WriteLine(strData)

        strData = vbNullString
        For lngCounter = 0 To Me.dtgList.Rows.Count - 1
            strData = vbNullString
            For lngColCounter = 0 To Me.dtgList.Columns.Count - 1

                If Me.dtgList.Columns(CInt(lngColCounter)).Visible = True Then
                    If strData = "" Then
                        strData = Me.dtgList.Rows(lngCounter).Cells(CInt(lngColCounter)).Value
                    Else
                        If InStr(Me.dtgList.Rows(lngCounter).Cells(CInt(lngColCounter)).Value, "コメント有") <= 0 Then
                            strData &= vbTab & Me.dtgList.Rows(lngCounter).Cells(CInt(lngColCounter)).Value

                        Else
                            If Me.dtgList.Rows(lngCounter).Cells("COL_PATIENT_COMMENT").Value <> "" Then
                                strData &= vbTab & "○患者コメント:" & Me.dtgList.Rows(lngCounter).Cells("COL_PATIENT_COMMENT").Value
                                bolFlg = True
                            End If
                            If Me.dtgList.Rows(lngCounter).Cells("COL_ORDER_COMMENT").Value <> "" Then
                                If bolFlg = False Then
                                    strData &= vbTab & "●"
                                Else
                                    strData &= " ●"
                                End If
                                strData &= "オーダコメント:" & Me.dtgList.Rows(lngCounter).Cells("COL_ORDER_COMMENT").Value
                            End If
                            If Me.dtgList.Rows(lngCounter).Cells("COL_STUDY_COMMENT").Value <> "" Then
                                If bolFlg = False Then
                                    strData &= vbTab & "◎"
                                Else
                                    strData &= " ◎"
                                End If

                                strData &= "実施コメント:" & Me.dtgList.Rows(lngCounter).Cells("COL_STUDY_COMMENT").Value
                            End If
                            'strData &= vbTab & "患者コメント:" & Me.dtgList.Rows(lngCounter).Cells("COL_PATIENT_COMMENT").Value & Space(1) & _
                            '            "オーダコメント：" & Me.dtgList.Rows(lngCounter).Cells("COL_ORDER_COMMENT").Value & Space(1) & _
                            '            "実施コメント：" & Me.dtgList.Rows(lngCounter).Cells("COL_STUDY_COMMENT").Value

                        End If
                    End If

                End If
            Next lngColCounter

            '///START 技師長 ご要望によるカスタマイズ ADD By Watanabe 2012.03.06
            '///撮影区分ごとの件数カウント
            If InStr(Me.dtgList.Rows(lngCounter).Cells("COL_BODY_PART").Value, "造") > 0 Then
                lngZouei += 1
            ElseIf InStr(Me.dtgList.Rows(lngCounter).Cells("COL_BODY_PART").Value, "【単】") > 0 Then
                lngTanjun += 1
            ElseIf InStr(Me.dtgList.Rows(lngCounter).Cells("COL_BODY_PART").Value, "造") = 0 And InStr(Me.dtgList.Rows(lngCounter).Cells("COL_BODY_PART").Value, "【単】") = 0 Then
                lngTanjun += 1
            End If
            '///END 技師長 ご要望によるカスタマイズ ADD By Watanabe 2012.03.06

            objsw.WriteLine(strData)

        Next lngCounter

        '///START 技師長 ご要望によるカスタマイズ ADD By Watanabe 2012.03.06
        objsw.WriteLine("")
        objsw.WriteLine("")
        objsw.WriteLine("")

        objsw.WriteLine(vbTab & vbTab & vbTab & vbTab & "患者数：" & Me.lblPatientCnt.Text & "人" & vbTab & "検査件数：" & lngZouei + lngTanjun & Space(1) & "件")
        '///END 技師長 ご要望によるカスタマイズ ADD By Watanabe 2012.03.06

        objsw.Close()

        MsgBox("CSVファイル" & vbCrLf & "(" & file_LOG & ")を作成しました。", MsgBoxStyle.Information, "CSV出力")
    End Sub

#End Region

#Region "実施ボタン押下"
    Private Sub cmdJisshi_Click(sender As System.Object, e As System.EventArgs) Handles cmdJisshi.Click
        Dim strMsg As String = vbNullString

        Me.Timer1.Enabled = False

        '///実施処理開始
        Call subUpdAccept(3, strMsg)

        '///検査日のValueChanged Eventsを呼び出し、表示更新を行う。
        Call dtStudyDate_ValueChanged(sender, e)

        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "実施取消ボタン押下"
    Private Sub cmdCanJisshi_Click(sender As System.Object, e As System.EventArgs) Handles cmdCanJisshi.Click
        Dim strMsg As String = vbNullString

        Me.Timer1.Enabled = False

        '///実施処理開始
        Call subUpdAccept(2, strMsg)

        '///検査日のValueChanged Eventsを呼び出し、表示更新を行う。
        Call dtStudyDate_ValueChanged(sender, e)

        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "ポータブル押下(cmdPortable_Click)"
    '********************************************************************************
    '* 機能概要　：ポータブル押下
    '* 関数名称　：cmdPortable_Click
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日　　：2021.09.29
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    ''' <summary>
    ''' ポータブル押下
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>

    Private Sub cmdPortable_Click(sender As System.Object, e As System.EventArgs) Handles cmdPortable.Click
        Dim intRet As Integer = 0
        Dim strPathSV As String = vbNullString
        Dim strBkPathSV As String = vbNullString
        Dim strTitle As String = vbNullString
        Dim lngFileCount As Long = 0
        Dim intCount As Integer = 0             'ポータブル2台対応 ADD By Watanabe 2021.09.28
        Dim intCounter As Integer = 0           'ポータブル2台対応 ADD By Watanabe 2021.09.28
        Dim strTerminal As String = vbNullString

        'If My.Settings.XRAY_DIV = "1" Then

        '    '///東芝ポータブル追加対応 UpDated By Watanabe 2013.04.15
        '    '---START
        '    'strPathSV = "\\" & My.Settings.DATASOURCE & "\Priall\SHONANDAIICHI\MWM\home\PMSUS3\"
        '    'strBkPathSV = "\\" & My.Settings.DATASOURCE & "\Priall\SHONANDAIICHI\MWM\home\PMSUS3\BK\"
        '    'strTitle = "ポータブルエコー用"
        '    If Me.chkPorterble.Checked = True Then
        '        strPathSV = "\\" & My.Settings.DATASOURCE & "\Priall\SHONANDAIICHI\MWM\home\PMSUS5\"
        '        strBkPathSV = "\\" & My.Settings.DATASOURCE & "\Priall\SHONANDAIICHI\MWM\home\PMSUS5\BK\"
        '        strTitle = "ポータブルエコー用"
        '        intCount = 1
        '    ElseIf Me.chkTmscPorterble.Checked = True Then
        '        strPathSV = "\\" & My.Settings.DATASOURCE & "\Priall\SHONANDAIICHI\MWM\home\PMSUS2\"
        '        strBkPathSV = "\\" & My.Settings.DATASOURCE & "\Priall\SHONANDAIICHI\MWM\home\PMSUS2\BK\"
        '        strTitle = "XARIO用"
        '    End If
        '    '---END
        'ElseIf My.Settings.XRAY_DIV = "0" Then
        '    strPathSV = "\\" & My.Settings.DATASOURCE & "\Priall\SHONANDAIICHI\MWM\home\PMSCR1\"
        '    strBkPathSV = "\\" & My.Settings.DATASOURCE & "\Priall\SHONANDAIICHI\MWM\home\PMSCR1\BK\"
        '    strTitle = "一般撮影用"
        'End If

        If My.Settings.XRAY_DIV = "1" Then
            If Me.chkModality2.Checked = True Then
                strTerminal = My.Settings.US1_TERMINAL
                strTitle = Me.chkModality2.Text & "用 "
            ElseIf Me.chkTmscPorterble.Checked = True Then
                strTerminal = My.Settings.US2_TERMINAL
                strTitle = Me.chkTmscPorterble.Text & "用 "
            ElseIf Me.chkPorterble.Checked = True Then
                strTerminal = My.Settings.US3_TERMINAL
                strTitle = Me.chkPorterble.Text & "用 "
            End If
        ElseIf My.Settings.XRAY_DIV = "0" Then
            Select Case Me.cmbModality.SelectedItem
                Case "一般撮影"
                    strTerminal = My.Settings.TERMINAL
                Case "施設健診"
                    strTerminal = My.Settings.SITEHEALTH_TERMINAL
            End Select
            strTitle = Me.cmbModality.SelectedItem & "用"

            'strTerminal = My.Settings.TERMINAL
            'strTitle = "一般撮影用 "
        End If

        'ポータブル2台対応のためモダリティ情報を取得 ADD By Watanabe 2021.09.28
        Call subGetModalityInfo(strTerminal)

        'ポータブル2台対応のためLoop ADD By Watanabe 2021.09.28
        For intCounter = 0 To aryModality.Length - 1

            'ポータブル2台対応 ADD By Watanabe 2021.09.28
            strPathSV = aryModality(intCount).HOME_PATH
            strPathSV = strPathSV.Replace("D:", "\\" & My.Settings.DATASOURCE)

            strBkPathSV = aryModality(intCount).HOME_PATH & "BK\"
            strBkPathSV = strBkPathSV.Replace("D:", "\\" & My.Settings.DATASOURCE)

            If MsgBox(strTitle & "のデータファイルを削除します。", MsgBoxStyle.OkCancel + MsgBoxStyle.Question, Me.cmdPortable.Text) = MsgBoxResult.Cancel Then Exit Sub

            Dim fs As String() = System.IO.Directory.GetFiles(strPathSV, "*.*", IO.SearchOption.TopDirectoryOnly)

            lngFileCount = fs.Count

            For lngCounter = 0 To lngFileCount - 1
                If InStr(fs(lngCounter).ToString, "lockfile") = 0 Then
                    '///ファイル移動前に移動先に同名ファイルが存在するかチェック ADD By Watanabe 2014.02.18
                    If System.IO.File.Exists(fs(lngCounter).ToString.Replace(strPathSV,
                                                                             strBkPathSV)) = True Then
                        '///移動先に同名ファイルが存在した場合、移動先の同名ファイルを削除する。
                        System.IO.File.Delete(fs(lngCounter).ToString.Replace(strPathSV,
                                                                              strBkPathSV))
                    End If

                    Threading.Thread.Sleep(500)

                    System.IO.File.Move(fs(lngCounter).ToString,
                                        fs(lngCounter).ToString.Replace(strPathSV,
                                                                        strBkPathSV))
                    Call subOutLog("移動ファイル (" & fs(lngCounter).ToString & ")", 0)
                End If
            Next lngCounter

            Call MsgBox(lngFileCount & "件の" & strTitle & "データファイルの削除が完了しました。", MsgBoxStyle.Information, strTitle & "データファイル削除")
            Call subOutLog(lngFileCount & "件の" & strTitle & "データファイルの削除が完了しました。", 0)
        Next intCounter
    End Sub
#End Region

#Region "トグルボタンクリック"
    Private Sub chkModality1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkModality1.CheckedChanged
        If Me.chkModality1.Checked = True Then
            Me.chkModality2.Checked = False
            '///東芝ポータブル追加対応 ADD By Watanabe 2013.04.15
            Me.chkTmscPorterble.Checked = False
            Me.chkPorterble.Checked = False
            Me.cmdJisshi.Visible = False
            Me.cmdCanJisshi.Visible = False

            Me.cmdPortable.Visible = False
        End If
    End Sub

    Private Sub chkModality2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkModality2.CheckedChanged
        If Me.chkModality2.Checked = True Then
            Me.chkModality1.Checked = False
            '///東芝ポータブル追加対応 ADD By Watanabe 2013.04.15
            Me.chkTmscPorterble.Checked = False
            Me.chkPorterble.Checked = False
            Me.cmdJisshi.Visible = False
            Me.cmdCanJisshi.Visible = False

            Me.cmdPortable.Visible = False
        End If

    End Sub

    Private Sub chkTmscPorterble_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkTmscPorterble.CheckedChanged
        If Me.chkTmscPorterble.Checked = True Then
            Me.chkModality1.Checked = False
            Me.chkModality2.Checked = False
            Me.chkPorterble.Checked = False
            Me.cmdJisshi.Visible = False
            Me.cmdCanJisshi.Visible = False

            Me.cmdPortable.Visible = True
        End If

    End Sub

    Private Sub chkPorterble_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkPorterble.CheckedChanged
        If Me.chkPorterble.Checked = True Then
            Me.chkModality1.Checked = False
            Me.chkModality2.Checked = False
            '///東芝ポータブル追加対応 ADD By Watanabe 2013.04.15
            Me.chkTmscPorterble.Checked = False
            Me.cmdJisshi.Visible = False
            Me.cmdCanJisshi.Visible = False

            Me.cmdPortable.Visible = True
        End If

    End Sub

#End Region

#Region "一覧表示列選択"
    Private Sub 一覧表示列ToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles 一覧表示列ToolStripMenuItem.Click
        Dim frmColEdit As New frmEditCol
        Me.Timer1.Enabled = False

        tmrProc.Enabled = False
        '///表示列設定画面表示
        frmColEdit.ShowDialog()

        '///一覧表示再設定
        Call subSetColWidth()
        tmrProc.Enabled = True
        Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
    End Sub
#End Region

#Region "進捗絞込み関連"
    Private Sub chkStudyAccept_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkStudyAccept.CheckedChanged
        Dim strMsg As String = vbNullString

        If bolInit = True Then Exit Sub

        Me.Timer1.Enabled = False

        '///検査日のValueChanged Eventsを呼び出し、表示更新を行う。
        Call dtStudyDate_ValueChanged(sender, e)

        Me.txtPatientID.Focus()

        '///表示切替チェック状態を保存
        With My.Settings
            .ACCEPT = Me.chkAccept.Checked
            .NON_ACCEPT = Me.chkNonAccept.Checked
            .DELETE = Me.chkDelete.Checked
            .NON_DELETE = Me.chkNonDelete.Checked
            '.Finish = Me.chkEnd.Checked
            .STUDY_ACCEPT = Me.chkStudyAccept.Checked
            .STUDY_END = Me.chkStudyEnd.Checked
            .Save()
        End With

        If Me.Timer1.Enabled <> My.Settings.AUTO_REFRESH Then
            Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
        End If
    End Sub

    Private Sub chkStudyEnd_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkStudyEnd.CheckedChanged
        Dim strMsg As String = vbNullString

        If bolInit = True Then Exit Sub

        Me.Timer1.Enabled = False

        '///検査日のValueChanged Eventsを呼び出し、表示更新を行う。
        Call dtStudyDate_ValueChanged(sender, e)

        Me.txtPatientID.Focus()

        '///表示切替チェック状態を保存
        With My.Settings
            .ACCEPT = Me.chkAccept.Checked
            .NON_ACCEPT = Me.chkNonAccept.Checked
            .DELETE = Me.chkDelete.Checked
            .NON_DELETE = Me.chkNonDelete.Checked
            '.Finish = Me.chkEnd.Checked
            .STUDY_ACCEPT = Me.chkStudyAccept.Checked
            .STUDY_END = Me.chkStudyEnd.Checked
            .Save()
        End With

        If Me.Timer1.Enabled <> My.Settings.AUTO_REFRESH Then
            Me.Timer1.Enabled = My.Settings.AUTO_REFRESH
        End If
    End Sub
#End Region

#End Region

#Region "Sub Routine"

#Region "オーダ情報取得(fncGetOrder)"
    '********************************************************************************
    '* 機能概要　：オーダ情報取得
    '* 関数名称　：fncGetOrder
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・0=正常、-1=データ無、-9=エラー
    '* 作成日　　：2011/02/09
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetOrder(ByRef strMsg As String, Optional ByVal intNonAccept As Integer = 0, Optional ByVal intAccept As Integer = 0, Optional ByVal intStudyAccept As Integer = 0, Optional ByVal intEnd As Integer = 0, Optional ByVal intStudyEnd As Integer = 0, Optional ByVal intNonDel As Integer = 0, Optional ByVal intDel As Integer = 0) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim lngCounter As Long = 0
        Dim lngDataCnt As Long = 0
        Dim strData As String = vbNullString
        Dim strWkData As String = vbNullString

        Try

            Erase objOrder
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine(" ORDER_ANO,")
            strSQL.AppendLine(" MWM_STATE,")
            strSQL.AppendLine(" ISNULL(MWM_ORDER_NO,'') AS MWM_ORDER_NO,")
            strSQL.AppendLine(" SEND_STATE,")
            strSQL.AppendLine(" PATIENT_ANO,")
            strSQL.AppendLine(" PATIENT_ID,")
            strSQL.AppendLine(" SHIJI_DATE,")
            strSQL.AppendLine(" SNONYM,")
            strSQL.AppendLine(" SHINRYO_KBN,")
            strSQL.AppendLine(" KUGIRI_NO,")
            strSQL.AppendLine(" SHINRYO_KBN_NAME,")
            strSQL.AppendLine(" SHINRYO_KBN_RNAME,")
            strSQL.AppendLine(" KINKYU_KBN,")
            strSQL.AppendLine(" KINKYU_KBN_NAME,")
            strSQL.AppendLine(" JIGO_NYURYOKU_FLG,")
            strSQL.AppendLine(" JIGO_NYURYOKU_FLG_NAME,")
            strSQL.AppendLine(" HOKEN_SHUBETSU,")
            strSQL.AppendLine(" HOKEN_SHUBETSU_NAME,")
            strSQL.AppendLine(" SHINKI_FLG,")
            strSQL.AppendLine(" SHINKI_FLG_NAME,")
            strSQL.AppendLine(" KENSA_SHUBETSU,")
            strSQL.AppendLine(" KENSA_SHUBETSU_NAME,")
            strSQL.AppendLine(" SHOHO_KBN,")
            strSQL.AppendLine(" SHOHO_KBN_NAME,")
            strSQL.AppendLine(" TEIRINJI_FLG,")
            strSQL.AppendLine(" TEIRINJI_FLG_NAME,")
            strSQL.AppendLine(" NICHI_KAISU,")
            strSQL.AppendLine(" JISSHIYOTEI_KAISHI_DATETIME,")
            strSQL.AppendLine(" JISSHIYOTEI_KAISHI_JIKAN_KBN,")
            strSQL.AppendLine(" JISSHIYOTEI_KAISHI_JIKAN_KBN_NAME,")
            strSQL.AppendLine(" JISSHIYOTEI_DATE_KANKAKU,")
            strSQL.AppendLine(" JISSHIYOTEI_SHURYO_JIKAN_KBN,")
            strSQL.AppendLine(" JISSHIYOTEI_SHURYO_JIKAN_KBN_NAME,")
            strSQL.AppendLine(" ORDER_NO,")
            strSQL.AppendLine(" CHUSHI_HAMBETSU_FLG,")
            strSQL.AppendLine(" CHUSHI_DATETIME,")
            strSQL.AppendLine(" CHUSHI_JIKAN_KBN,")
            strSQL.AppendLine(" CHUSHI_JIKAN_KBN_NAME,")
            strSQL.AppendLine(" BUMONKA_CODE,")
            strSQL.AppendLine(" BUMONKA_NAME,")
            strSQL.AppendLine(" FREE_COMMENT,")
            strSQL.AppendLine(" ORDER_STATUS,")
            strSQL.AppendLine(" HUJISSHI_JOHO_DATETIME,")
            strSQL.AppendLine(" HUJISSHI_JOHO_SOSASHA_ID,")
            strSQL.AppendLine(" HUJISSHI_JOHO_SOSASHA_NAME,")
            strSQL.AppendLine(" UKETSUKE_DATETIME,")
            strSQL.AppendLine(" UKETSUKE_SOSASHA_ID,")
            strSQL.AppendLine(" UKETSUKE_SOSASHA_NAME,")
            strSQL.AppendLine(" SAKUJO_DATETIME,")
            strSQL.AppendLine(" SAKUJO_SOSASHA_ID,")
            strSQL.AppendLine(" SAKUJO_SOSASHA_NAME,")
            strSQL.AppendLine(" CHUSHI_SOSA_DATETIME,")
            strSQL.AppendLine(" CHUSHI_SOSASHA_ID,")
            strSQL.AppendLine(" CHUSHI_SOSASHA_NAME,")
            strSQL.AppendLine(" CHUSHI_SHIJII_SOSASHA_ID,")
            strSQL.AppendLine(" CHUSHI_SHIJII_SOSASHA_NAME,")
            strSQL.AppendLine(" HOSOKU_NYURYOKU_DATETIME,")
            strSQL.AppendLine(" HOSOKU_NYURYOKU_SOSASHA_ID,")
            strSQL.AppendLine(" HOSOKU_NYURYOKU_SOSASHA_NAME,")
            strSQL.AppendLine(" KENSA_KEKKA_UMU,")
            strSQL.AppendLine(" KENSA_KEKKA_UMU_NAME,")
            strSQL.AppendLine(" SHOKUJI_SESSHU_YOHI,")
            strSQL.AppendLine(" SHOKUJI_SESSHU_YOHI_NAME,")
            strSQL.AppendLine(" KOHI_JOHO_REMBAN1,")
            strSQL.AppendLine(" KOHI_JOHO_REMBAN1_NAME,")
            strSQL.AppendLine(" KOHI_JOHO_REMBAN2,")
            strSQL.AppendLine(" KOHI_JOHO_REMBAN2_NAME,")
            strSQL.AppendLine(" NYUGAI_KBN,")
            strSQL.AppendLine(" NYUGAI_KBN_NAME,")
            strSQL.AppendLine(" SHINRYOKA_CODE,")
            strSQL.AppendLine(" SHINRYOKA_NAME,")
            strSQL.AppendLine(" BUMON_CODE,")
            strSQL.AppendLine(" BUMON_NAME,")
            strSQL.AppendLine(" SHINSATSUSHITSU_NO,")
            strSQL.AppendLine(" SHINSATSUSHITSU_NAME,")
            strSQL.AppendLine(" SOSASHA_ID,")
            strSQL.AppendLine(" SOSASHA_NAME,")
            strSQL.AppendLine(" SHIJII_ID,")
            strSQL.AppendLine(" SHIJII_NAME,")
            strSQL.AppendLine(" NYUIN_DATE,")
            strSQL.AppendLine(" TAIIN_DATE,")
            strSQL.AppendLine(" BYOTO_CODE,")
            strSQL.AppendLine(" BYOTO_NAME,")
            strSQL.AppendLine(" BYOSHITSU_NO,")
            strSQL.AppendLine(" BYOSHITSU_NAME,")
            strSQL.AppendLine(" NYUIN_KA,")
            strSQL.AppendLine(" NYUIN_KA_NAME,")
            strSQL.AppendLine(" SAISHU_TENTO_DATE,")
            strSQL.AppendLine(" SHUJII_ID,")
            strSQL.AppendLine(" SHUJII_NAME,")
            strSQL.AppendLine(" ISNULL(CONVERT(VARCHAR,JISSHI_DATE,112),'') AS JISSHI_DATE,")
            strSQL.AppendLine(" KOUMKU_HIMBAN,")
            strSQL.AppendLine(" KOUMKU_NAME,")
            strSQL.AppendLine(" MODALITY_CD,")
            strSQL.AppendLine(" SEND_MODALITY_CD,")
            strSQL.AppendLine(" MODALITY_NAME,")
            strSQL.AppendLine(" DEL_FLG ")

            strSQL.AppendLine("FROM ")

            Select Case My.Settings.XRAY_DIV
                Case "0"            '放射線オーダ
                    strSQL.AppendLine(" vwTRAN_ORDER_XRAY ")
                Case "1"            '超音波オーダ
                    strSQL.AppendLine(" vwTRAN_ORDER_US ")
                Case "2"            '内視鏡オーダ
                    strSQL.AppendLine(" vwTRAN_ORDER_ES ")
            End Select
            'Select Case My.Settings.XRAY_DIV
            '    Case "0"            '放射線オーダ
            '        strSQL.AppendLine(" vwMWM_XRAY ")
            '    Case "1"            '超音波オーダ
            '        strSQL.AppendLine(" vwMWM_US ")
            '    Case "2"            '内視鏡オーダ
            '        strSQL.AppendLine(" vwMWM_ES")
            'End Select
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine(" CONVERT(varchar,JISSHIYOTEI_KAISHI_DATETIME,112) = '" & dtStudyDate.Value.ToString("yyyyMMdd") & "' ")

            If Me.cmbModality.SelectedIndex > -1 Then
                Select Case Me.cmbModality.SelectedItem
                    Case "一般撮影"
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" MODALITY_CD = 'CR' ")
                        strSQL.AppendLine(" AND")
                        '---Ver.3.0.2.5 PORTABLEに<> '1'の条件を追加 UpDated By Watanabe 2022.04.25
                        '                        strSQL.AppendLine(" PORTABLE IS NULL")
                        strSQL.AppendLine(" (PORTABLE IS NULL OR PORTABLE <> '1')")
                        'strSQL.AppendLine(" AND")
                        '---START Ver.3.0.3.0 一般撮影をポータブルCRで受付対応 CommentOut By Watanabe 2025.01.28
                        '---Ver.3.0.2.5 SITEHEALTH<> '1'の条件を追加 UpDated By Watanabe 2022.04.25
                        'strSQL.AppendLine(" SITEHEALTH IS NULL")
                        'strSQL.AppendLine(" (SITEHEALTH IS NULL OR SITEHEALTH <> '1')")
                        '---END Ver.3.0.3.0 一般撮影をポータブルCRで受付対応 CommentOut By Watanabe 2025.01.28
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" MODALITY_NAME = '一般撮影' ")
                    Case "ポータブル"
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" MODALITY_CD = 'CR' ")
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" PORTABLE = '1' ")
                        '---START Ver.3.0.3.0 一般撮影をポータブルCRで受付対応 ADD By Watanabe 2025.01.28
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" (SITEHEALTH IS NULL OR SITEHEALTH <> '1')")
                        '---END Ver.3.0.3.0 一般撮影をポータブルCRで受付対応 ADD By Watanabe 2025.01.28
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" MODALITY_NAME = 'ポータブル' ")
                    Case "施設健診"
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" MODALITY_CD = 'CR' ")
                        '---START Ver.3.0.3.0 一般撮影をポータブルCRで受付対応 ADD By Watanabe 2025.01.28
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" PORTABLE = '1' ")
                        '---END Ver.3.0.3.0 一般撮影をポータブルCRで受付対応 ADD By Watanabe 2025.01.28
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" SITEHEALTH = '1' ")
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" MODALITY_NAME = '施設健診' ")
                    Case "CT検査"
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" MODALITY_CD = 'CT' ")
                    Case "透視撮影"
                        strSQL.AppendLine(" AND")
                        strSQL.AppendLine(" MODALITY_CD = 'RF' ")
                End Select
            End If

            If intNonAccept = -1 And intAccept = 0 And intStudyAccept = 0 And intEnd = 0 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('0') ")
            ElseIf intNonAccept = -1 And intAccept = -1 And intStudyAccept = 0 And intEnd = 0 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('0','1') ")
            ElseIf intNonAccept = -1 And intAccept = -1 And intStudyAccept = -1 And intEnd = 0 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('0','1','2') ")
            ElseIf intNonAccept = -1 And intAccept = -1 And intStudyAccept = -1 And intEnd = -1 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('0','1','2','3') ")
            ElseIf intNonAccept = -1 And intAccept = 0 And intStudyAccept = -1 And intEnd = -1 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('0','2','3') ")
            ElseIf intNonAccept = -1 And intAccept = 0 And intStudyAccept = 0 And intEnd = -1 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('0','3') ")
            ElseIf intNonAccept = -1 And intAccept = -1 And intStudyAccept = 0 And intEnd = -1 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('0','1','3') ")
            ElseIf intNonAccept = -1 And intAccept = 0 And intStudyAccept = -1 And intEnd = -1 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('0','2','3') ")
            ElseIf intNonAccept = -1 And intAccept = 0 And intStudyAccept = -1 And intEnd = 0 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('0','2') ")


            ElseIf intNonAccept = 0 And intAccept = -1 And intStudyAccept = 0 And intEnd = 0 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('1') ")
            ElseIf intNonAccept = 0 And intAccept = -1 And intStudyAccept = -1 And intEnd = 0 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('1','2') ")
            ElseIf intNonAccept = 0 And intAccept = -1 And intStudyAccept = -1 And intEnd = -1 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('1','2','3') ")
            ElseIf intNonAccept = 0 And intAccept = -1 And intStudyAccept = 0 And intEnd = -1 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('1','3') ")
            ElseIf intNonAccept = 0 And intAccept = 0 And intStudyAccept = -1 And intEnd = 0 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('2') ")
            ElseIf intNonAccept = 0 And intAccept = 0 And intStudyAccept = -1 And intEnd = -1 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('2','3') ")


            ElseIf intNonAccept = 0 And intAccept = 0 And intStudyAccept = -1 And intEnd = 0 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('2') ")
            ElseIf intNonAccept = 0 And intAccept = 0 And intStudyAccept = -1 And intEnd = -1 Then
                strSQL.AppendLine("AND ")
                strSQL.AppendLine(" MWM_STATE IN ('2','3') ")
            ElseIf intNonAccept = 0 And intAccept = 0 And intStudyAccept = 0 And intEnd = -1 Then
                strSQL.AppendLine("AND ")
                'strSQL.AppendLine(" MWM_STATE IN ('2','3') ")
                strSQL.AppendLine(" MWM_STATE IN ('2','3') ")
            End If

            '---Ver.3.0.2.3 削除済みを追加 UpDated By Watanabe 2021.10.15
            'If Me.chkDelete.Checked = False And Me.chkNonDelete.Checked = True Then
            '    strSQL.AppendLine(" AND")
            '    strSQL.AppendLine(" DEL_FLG IN ('0') ")
            'End If
            If Me.chkDelete.Checked = False And Me.chkNonDelete.Checked = True Then
                strSQL.AppendLine(" AND")
                strSQL.AppendLine(" DEL_FLG IN ('0') ")
            ElseIf Me.chkDelete.Checked = True And Me.chkNonDelete.Checked = False Then
                strSQL.AppendLine(" AND")
                strSQL.AppendLine(" DEL_FLG IN ('1') ")
            End If

            '---Ver.3.0.2.3 Sort条件にORDER_ANOを追加 ADD By Watanabe 2021.10.15
            'strSQL.AppendLine("ORDER BY DEL_FLG,MWM_STATE,JISSHIYOTEI_KAISHI_DATETIME")
            strSQL.AppendLine("ORDER BY DEL_FLG,MWM_STATE,JISSHIYOTEI_KAISHI_DATETIME,ORDER_ANO")
            'strSQL.AppendLine(" SEND_STATE = '0' ")
            'strSQL.AppendLine(" AND ")
            'strSQL.AppendLine(" MWM_STATE = '0' ")
            'strSQL.AppendLine(" AND ")
            'strSQL.AppendLine(" DEL_FLG = '0' ")
            'strSQL.AppendLine("ORDER BY JISSHIYOTEI_KAISHI_DATETIME,MWM_STATE")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///データ無時は処理を抜ける
            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            '///取得データを構造体へ退避
            lngDataCnt = dsData.Tables(0).Rows.Count

            Call subOutLog("オーダ送信対象データ検知：" & lngDataCnt & "件", 0)

            '///検知数分繰り返し
            For lngCounter = 0 To lngDataCnt - 1
                ReDim Preserve objOrder(lngCounter)
                objOrder(lngCounter) = New TYPE_ORDER
                With dsData.Tables(0).Rows(lngCounter)
                    objOrder(lngCounter).ORDER_ANO = .Item("ORDER_ANO")
                    objOrder(lngCounter).MWM_STATE = .Item("MWM_STATE")
                    objOrder(lngCounter).MWM_ORDER_NO = .Item("MWM_ORDER_NO")
                    objOrder(lngCounter).MWM_ORDER_NO = Date.Now.ToString("yyMMdd") & objOrder(lngCounter).ORDER_ANO.PadLeft(10).Replace(" ", "0")
                    objOrder(lngCounter).SEND_STATE = .Item("SEND_STATE")
                    objOrder(lngCounter).PATIENT_ANO = .Item("PATIENT_ANO")
                    objOrder(lngCounter).PATIENT_ID = .Item("PATIENT_ID")
                    objOrder(lngCounter).SHIJI_DATE = .Item("SHIJI_DATE")
                    objOrder(lngCounter).SNONYM = .Item("SNONYM")
                    objOrder(lngCounter).SHINRYO_KBN = .Item("SHINRYO_KBN")
                    objOrder(lngCounter).KUGIRI_NO = .Item("KUGIRI_NO")
                    objOrder(lngCounter).SHINRYO_KBN_NAME = .Item("SHINRYO_KBN_NAME")
                    objOrder(lngCounter).SHINRYO_KBN_RNAME = .Item("SHINRYO_KBN_RNAME")
                    objOrder(lngCounter).KINKYU_KBN = .Item("KINKYU_KBN")
                    objOrder(lngCounter).KINKYU_KBN_NAME = .Item("KINKYU_KBN_NAME")
                    objOrder(lngCounter).JIGO_NYURYOKU_FLG = .Item("JIGO_NYURYOKU_FLG")
                    objOrder(lngCounter).JIGO_NYURYOKU_FLG_NAME = .Item("JIGO_NYURYOKU_FLG_NAME")
                    objOrder(lngCounter).HOKEN_SHUBETSU = .Item("HOKEN_SHUBETSU")
                    objOrder(lngCounter).HOKEN_SHUBETSU_NAME = .Item("HOKEN_SHUBETSU_NAME")
                    objOrder(lngCounter).SHINKI_FLG = .Item("SHINKI_FLG")
                    objOrder(lngCounter).SHINKI_FLG_NAME = .Item("SHINKI_FLG_NAME")
                    objOrder(lngCounter).KENSA_SHUBETSU = .Item("KENSA_SHUBETSU")
                    objOrder(lngCounter).KENSA_SHUBETSU_NAME = .Item("KENSA_SHUBETSU_NAME")
                    objOrder(lngCounter).SHOHO_KBN = .Item("SHOHO_KBN")
                    objOrder(lngCounter).SHOHO_KBN_NAME = .Item("SHOHO_KBN_NAME")
                    objOrder(lngCounter).TEIRINJI_FLG = .Item("TEIRINJI_FLG")
                    objOrder(lngCounter).TEIRINJI_FLG_NAME = .Item("TEIRINJI_FLG_NAME")
                    objOrder(lngCounter).NICHI_KAISU = .Item("NICHI_KAISU")
                    objOrder(lngCounter).JISSHIYOTEI_KAISHI_DATETIME = .Item("JISSHIYOTEI_KAISHI_DATETIME")
                    objOrder(lngCounter).JISSHIYOTEI_KAISHI_JIKAN_KBN = .Item("JISSHIYOTEI_KAISHI_JIKAN_KBN")
                    objOrder(lngCounter).JISSHIYOTEI_KAISHI_JIKAN_KBN_NAME = .Item("JISSHIYOTEI_KAISHI_JIKAN_KBN_NAME")
                    objOrder(lngCounter).JISSHIYOTEI_DATE_KANKAKU = .Item("JISSHIYOTEI_DATE_KANKAKU")
                    objOrder(lngCounter).JISSHIYOTEI_SHURYO_JIKAN_KBN = .Item("JISSHIYOTEI_SHURYO_JIKAN_KBN")
                    objOrder(lngCounter).JISSHIYOTEI_SHURYO_JIKAN_KBN_NAME = .Item("JISSHIYOTEI_SHURYO_JIKAN_KBN_NAME")
                    objOrder(lngCounter).ORDER_NO = .Item("ORDER_NO")
                    objOrder(lngCounter).CHUSHI_HAMBETSU_FLG = .Item("CHUSHI_HAMBETSU_FLG")
                    objOrder(lngCounter).CHUSHI_DATETIME = .Item("CHUSHI_DATETIME")
                    objOrder(lngCounter).CHUSHI_JIKAN_KBN = .Item("CHUSHI_JIKAN_KBN")
                    objOrder(lngCounter).CHUSHI_JIKAN_KBN_NAME = .Item("CHUSHI_JIKAN_KBN_NAME")
                    objOrder(lngCounter).BUMONKA_CODE = .Item("BUMONKA_CODE")
                    objOrder(lngCounter).BUMONKA_NAME = .Item("BUMONKA_NAME")
                    objOrder(lngCounter).FREE_COMMENT = .Item("FREE_COMMENT")
                    objOrder(lngCounter).ORDER_STATUS = .Item("ORDER_STATUS")
                    objOrder(lngCounter).HUJISSHI_JOHO_DATETIME = .Item("HUJISSHI_JOHO_DATETIME")
                    objOrder(lngCounter).HUJISSHI_JOHO_SOSASHA_ID = .Item("HUJISSHI_JOHO_SOSASHA_ID")
                    objOrder(lngCounter).HUJISSHI_JOHO_SOSASHA_NAME = .Item("HUJISSHI_JOHO_SOSASHA_NAME")
                    objOrder(lngCounter).UKETSUKE_DATETIME = .Item("UKETSUKE_DATETIME")
                    objOrder(lngCounter).UKETSUKE_SOSASHA_ID = .Item("UKETSUKE_SOSASHA_ID")
                    objOrder(lngCounter).UKETSUKE_SOSASHA_NAME = .Item("UKETSUKE_SOSASHA_NAME")
                    objOrder(lngCounter).SAKUJO_DATETIME = .Item("SAKUJO_DATETIME")
                    objOrder(lngCounter).SAKUJO_SOSASHA_ID = .Item("SAKUJO_SOSASHA_ID")
                    objOrder(lngCounter).SAKUJO_SOSASHA_NAME = .Item("SAKUJO_SOSASHA_NAME")
                    objOrder(lngCounter).CHUSHI_SOSA_DATETIME = .Item("CHUSHI_SOSA_DATETIME")
                    objOrder(lngCounter).CHUSHI_SOSASHA_ID = .Item("CHUSHI_SOSASHA_ID")
                    objOrder(lngCounter).CHUSHI_SOSASHA_NAME = .Item("CHUSHI_SOSASHA_NAME")
                    objOrder(lngCounter).CHUSHI_SHIJII_SOSASHA_ID = .Item("CHUSHI_SHIJII_SOSASHA_ID")
                    objOrder(lngCounter).CHUSHI_SHIJII_SOSASHA_NAME = .Item("CHUSHI_SHIJII_SOSASHA_NAME")
                    objOrder(lngCounter).HOSOKU_NYURYOKU_DATETIME = .Item("HOSOKU_NYURYOKU_DATETIME")
                    objOrder(lngCounter).HOSOKU_NYURYOKU_SOSASHA_ID = .Item("HOSOKU_NYURYOKU_SOSASHA_ID")
                    objOrder(lngCounter).HOSOKU_NYURYOKU_SOSASHA_NAME = .Item("HOSOKU_NYURYOKU_SOSASHA_NAME")
                    objOrder(lngCounter).KENSA_KEKKA_UMU = .Item("KENSA_KEKKA_UMU")
                    objOrder(lngCounter).KENSA_KEKKA_UMU_NAME = .Item("KENSA_KEKKA_UMU_NAME")
                    objOrder(lngCounter).SHOKUJI_SESSHU_YOHI = .Item("SHOKUJI_SESSHU_YOHI")
                    objOrder(lngCounter).SHOKUJI_SESSHU_YOHI_NAME = .Item("SHOKUJI_SESSHU_YOHI_NAME")
                    objOrder(lngCounter).KOHI_JOHO_REMBAN1 = .Item("KOHI_JOHO_REMBAN1")
                    objOrder(lngCounter).KOHI_JOHO_REMBAN1_NAME = .Item("KOHI_JOHO_REMBAN1_NAME")
                    objOrder(lngCounter).KOHI_JOHO_REMBAN2 = .Item("KOHI_JOHO_REMBAN2")
                    objOrder(lngCounter).KOHI_JOHO_REMBAN2_NAME = .Item("KOHI_JOHO_REMBAN2_NAME")
                    objOrder(lngCounter).NYUGAI_KBN = .Item("NYUGAI_KBN")
                    objOrder(lngCounter).NYUGAI_KBN_NAME = .Item("NYUGAI_KBN_NAME")
                    objOrder(lngCounter).SHINRYOKA_CODE = .Item("SHINRYOKA_CODE")
                    objOrder(lngCounter).SHINRYOKA_NAME = .Item("SHINRYOKA_NAME")
                    objOrder(lngCounter).BUMON_CODE = .Item("BUMON_CODE")
                    objOrder(lngCounter).BUMON_NAME = .Item("BUMON_NAME")
                    objOrder(lngCounter).SHINSATSUSHITSU_NO = .Item("SHINSATSUSHITSU_NO")
                    objOrder(lngCounter).SHINSATSUSHITSU_NAME = .Item("SHINSATSUSHITSU_NAME")
                    objOrder(lngCounter).SOSASHA_ID = .Item("SOSASHA_ID")
                    objOrder(lngCounter).SOSASHA_NAME = .Item("SOSASHA_NAME")
                    objOrder(lngCounter).SHIJII_ID = .Item("SHIJII_ID")
                    objOrder(lngCounter).SHIJII_NAME = .Item("SHIJII_NAME")
                    objOrder(lngCounter).NYUIN_DATE = .Item("NYUIN_DATE")
                    objOrder(lngCounter).TAIIN_DATE = .Item("TAIIN_DATE")
                    objOrder(lngCounter).BYOTO_CODE = .Item("BYOTO_CODE")
                    objOrder(lngCounter).BYOTO_NAME = .Item("BYOTO_NAME")
                    objOrder(lngCounter).BYOSHITSU_NO = .Item("BYOSHITSU_NO")
                    objOrder(lngCounter).BYOSHITSU_NAME = .Item("BYOSHITSU_NAME")
                    objOrder(lngCounter).NYUIN_KA = .Item("NYUIN_KA")
                    objOrder(lngCounter).NYUIN_KA_NAME = .Item("NYUIN_KA_NAME")
                    objOrder(lngCounter).SAISHU_TENTO_DATE = .Item("SAISHU_TENTO_DATE")
                    objOrder(lngCounter).SHUJII_ID = .Item("SHUJII_ID")
                    objOrder(lngCounter).SHUJII_NAME = .Item("SHUJII_NAME")
                    objOrder(lngCounter).JISSHI_DATE = .Item("JISSHI_DATE")
                    objOrder(lngCounter).KOUMOKU_HIMBAN = .Item("KOUMKU_HIMBAN")
                    objOrder(lngCounter).KOUMOKU_NAME = .Item("KOUMKU_NAME")
                    objOrder(lngCounter).MODALITY_CD = .Item("MODALITY_CD")
                    objOrder(lngCounter).SEND_MODALITY_CD = .Item("SEND_MODALITY_CD")
                    objOrder(lngCounter).MODALITY_NAME = .Item("MODALITY_NAME")
                    objOrder(lngCounter).DEL_FLG = .Item("DEL_FLG")

                    '///部位情報
                    Call fncGetOrderPart(objOrder(lngCounter), strMsg)

                    Call subOutLog(lngCounter & "/" & lngDataCnt & "件 ：オーダ番号=" & objOrder(lngCounter).ORDER_NO & Space(1) & "患者ID=" & objOrder(lngCounter).PATIENT_ID, 0)

                End With
            Next lngCounter
            Call subDispList(strMsg)

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("新着オーダチェック(fncChkSendData) エラー：" & strMsg, 1)
            Return RET_ERROR
        Finally
        End Try
    End Function
#End Region

#Region "一覧表示(subDispList)"
    '********************************************************************************
    '* 機能概要　：一覧表示
    '* 関数名称　：subDispList
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：無
    '* 作成日　　：2011/02/09
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Sub subDispList(ByRef strMsg As String)
        Dim lngCounter As Long
        Dim backColor As New Color
        Dim foreColor As New Color
        Dim strBirth As String
        '///件数表示 ADD By Watanabe 2012.02.02
        Dim lngNonAccept As Long = 0
        Dim lngAccept As Long = 0
        Dim lngEnd As Long = 0
        Dim lngNonDel As Long = 0
        Dim lngDel As Long = 0
        Dim lblTotal As Long = 0
        Dim lngStudyAccept As Long = 0
        Dim lngStudyEnd As Long = 0

        Try
            Me.Cursor = Cursors.WaitCursor

            Me.dtgList.Rows.Clear()

            Me.cmdAccept.Enabled = False
            Me.cmdCanAccept.Enabled = False
            Me.cmdDelete.Enabled = False
            Me.cmdCanDelete.Enabled = False
            Me.cmdJisshi.Enabled = False
            Me.cmdCanJisshi.Enabled = False
            Me.cmdOrderDetail.Enabled = False
            Me.lblNonAccept.Text = lngNonAccept
            Me.lblAccept.Text = lngAccept
            Me.lblEnd.Text = lngEnd
            Me.lblNonDelete.Text = lngNonDel
            Me.lblDelete.Text = lngDel
            Me.lblStudyAccept.Text = lngStudyAccept
            Me.lblStudyEnd.Text = lngStudyEnd

            Me.lblTotal.Text = CLng(lngNonAccept + lngAccept + lngStudyAccept + lngEnd + lngStudyEnd + lngDel).ToString

            Me.dtgList.Rows.Clear()
            If objOrder Is Nothing Then Exit Sub

            For lngCounter = 0 To objOrder.Length - 1

                With objOrder(lngCounter)

                    Call fncGetPatient(.PATIENT_ANO, strMsg)

                    dtgList.Rows.Add()
                    'dtgList.Rows(lngCounter).Cells("COL_PATIENT_COMMENT").Value = .PATIENT_COMMENT
                    'dtgList.Rows(lngCounter).Cells("COL_ORDER_COMMENT").Value = .ORDER_COMMENT
                    dtgList.Rows(lngCounter).Cells("COL_STATE").Value = .MWM_STATE
                    dtgList.Rows(lngCounter).Cells("COL_PATIENT_ANO").Value = .PATIENT_ANO
                    dtgList.Rows(lngCounter).Cells("COL_PATIENT_ID").Value = .PATIENT_ID
                    'dtgList.Rows(lngCounter).Cells("COL_KENSHIN_PATIENT_ID").Value = .KENSHIN_PATIENT_ID
                    dtgList.Rows(lngCounter).Cells("COL_PATIENT_KANJI").Value = objPatient.KANJI_NAME
                    dtgList.Rows(lngCounter).Cells("COL_PATIENT_KANA").Value = objPatient.KANA_NAME
                    dtgList.Rows(lngCounter).Cells("COL_PATIENT_EIJI").Value = objPatient.EIJI_NAME
                    Select Case objPatient.SEIBETSU
                        Case "1", "M", "男"
                            dtgList.Rows(lngCounter).Cells("COL_PATIENT_SEX").Value = "男"
                        Case "2", "F", "女"
                            dtgList.Rows(lngCounter).Cells("COL_PATIENT_SEX").Value = "女"
                        Case "9", "O", "不明"
                            dtgList.Rows(lngCounter).Cells("COL_PATIENT_SEX").Value = "不明"
                        Case Else
                            dtgList.Rows(lngCounter).Cells("COL_PATIENT_SEX").Value = ""
                    End Select
                    dtgList.Rows(lngCounter).Cells("COL_PATIENT_BIRTH").Value = objPatient.BIRTH_DATE
                    If .SHINRYOKA_CODE <> "20" Then
                        dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = "外来"
                    ElseIf .SHINRYOKA_CODE = "20" Then
                        dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = "健診"
                    Else
                        dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = ""
                    End If
                    '////////////////////////////////////////
                    If .JISSHI_DATE = "" Then
                        Dim strAge As String = (CLng(.JISSHIYOTEI_KAISHI_DATETIME.Substring(0, 10).Replace("/", "")) - CLng(objPatient.BIRTH_DATE.Replace("/", ""))) / 10000
                        '///"."がない場合の対処 2012.02.09
                        If InStr(strAge, ".") > 0 Then
                            strAge = strAge.Substring(0, InStr(strAge, ".") - 1)
                        End If
                        'strAge = strAge.Substring(0, InStr(strAge, ".") - 1)
                        dtgList.Rows(lngCounter).Cells("COL_AGE").Value = strAge
                    ElseIf CLng(.JISSHI_DATE.Replace("/", "")) > 20000101 Then
                        Dim strAge As String = (CLng(.JISSHI_DATE.Replace("/", "")) - CLng(objPatient.BIRTH_DATE.Replace("/", ""))) / 10000
                        '///"."がない場合の対処 2012.02.09
                        If InStr(strAge, ".") > 0 Then
                            strAge = strAge.Substring(0, InStr(strAge, ".") - 1)
                        End If
                        'strAge = strAge.Substring(0, InStr(strAge, ".") - 1)
                        dtgList.Rows(lngCounter).Cells("COL_AGE").Value = strAge
                    ElseIf .JISSHIYOTEI_KAISHI_DATETIME <> "" Then
                        Dim strAge As String = (CLng(.JISSHIYOTEI_KAISHI_DATETIME.Substring(0, 10).Replace("/", "")) - CLng(objPatient.BIRTH_DATE.Replace("/", ""))) / 10000
                        '///"."がない場合の対処 2012.02.09
                        If InStr(strAge, ".") > 0 Then
                            strAge = strAge.Substring(0, InStr(strAge, ".") - 1)
                        End If
                        'strAge = strAge.Substring(0, InStr(strAge, ".") - 1)
                        dtgList.Rows(lngCounter).Cells("COL_AGE").Value = strAge
                    End If

                    '////////////////////////////////////////
                    dtgList.Rows(lngCounter).Cells("COL_ORDER_ANO").Value = .ORDER_ANO
                    '---オーダ番号をMWM時のオーダ番号に置き換え UpDated By Watanabe 2021.10.08
                    'dtgList.Rows(lngCounter).Cells("COL_ORDER_NO").Value = .ORDER_NO
                    dtgList.Rows(lngCounter).Cells("COL_ORDER_NO").Value = .MWM_ORDER_NO
                    dtgList.Rows(lngCounter).Cells("COL_DEPT").Value = .SHINRYOKA_NAME
                    dtgList.Rows(lngCounter).Cells("COL_REQ_DOC").Value = .SHIJII_NAME
                    dtgList.Rows(lngCounter).Cells("COL_WARD").Value = .BYOTO_NAME
                    '---病室表示項目をコード＋病室名に変更 UpDated By Watanabe 2014.04.16
                    'dtgList.Rows(lngCounter).Cells("COL_ROOM").Value = .BYOSHITSU_NAME
                    dtgList.Rows(lngCounter).Cells("COL_ROOM").Value = .BYOSHITSU_NO & .BYOSHITSU_NAME
                    dtgList.Rows(lngCounter).Cells("COL_NYUGAI").Value = .NYUGAI_KBN_NAME
                    dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = .KINKYU_KBN_NAME




                    'dtgList.Rows(lngCounter).Cells("COL_PATIENT_COMMENT").Value = .PATIENT_COMMENT
                    'If .PATIENT_COMMENT <> "" Then
                    '    dtgList.Rows(lngCounter).Cells("COL_COMMENT_FLG").Value = "患者コメント有"
                    '    dtgList("COL_COMMENT_FLG", CInt(lngCounter)).ToolTipText = "患者コメント：" & .PATIENT_COMMENT
                    'End If
                    dtgList.Rows(lngCounter).Cells("COL_ORDER_COMMENT").Value = .FREE_COMMENT
                    'If .ORDER_COMMENT <> "" Then
                    '    If .PATIENT_COMMENT <> "" Then
                    '        dtgList.Rows(lngCounter).Cells("COL_COMMENT_FLG").Value &= ",オーダコメント有"
                    '        dtgList("COL_COMMENT_FLG", CInt(lngCounter)).ToolTipText &= vbCrLf & "オーダコメント：" & .ORDER_COMMENT
                    '    Else
                    '        dtgList.Rows(lngCounter).Cells("COL_COMMENT_FLG").Value &= "オーダコメント有"
                    '        dtgList("COL_COMMENT_FLG", CInt(lngCounter)).ToolTipText &= "オーダコメント：" & .ORDER_COMMENT
                    '    End If
                    'End If


                    'dtgList.Rows(lngCounter).Cells("COL_STUDY_COMMENT").Value = .STUDY_COMMENT
                    'If .STUDY_COMMENT <> "" Then
                    '    If .PATIENT_COMMENT <> "" Or .ORDER_COMMENT <> "" Then
                    '        dtgList.Rows(lngCounter).Cells("COL_COMMENT_FLG").Value &= ",実施コメント有"
                    '        dtgList("COL_COMMENT_FLG", CInt(lngCounter)).ToolTipText &= vbCrLf & "実施コメント：" & .STUDY_COMMENT
                    '    Else
                    '        dtgList.Rows(lngCounter).Cells("COL_COMMENT_FLG").Value &= "実施コメント有"
                    '        dtgList("COL_COMMENT_FLG", CInt(lngCounter)).ToolTipText &= "実施コメント：" & .STUDY_COMMENT
                    '    End If
                    'End If
                    strBirth = objPatient.BIRTH_DATE
                    If strBirth.Length = 8 Then
                        strBirth = strBirth.Substring(0, 4) & "/" & strBirth.Substring(4, 2) & "/" & strBirth.Substring(6, 2)
                    End If
                    dtgList.Rows(lngCounter).Cells("COL_PATIENT_BIRTH").Value = strBirth
                    strBirth = ""
                    strBirth = .JISSHIYOTEI_KAISHI_DATETIME.Substring(0, 10)
                    If strBirth.Length = 8 Then
                        strBirth = strBirth.Substring(0, 4) & "/" & strBirth.Substring(4, 2) & "/" & strBirth.Substring(6, 2)
                    End If
                    dtgList.Rows(lngCounter).Cells("COL_ORDER_DATE").Value = strBirth
                    strBirth = ""
                    strBirth = .JISSHIYOTEI_KAISHI_DATETIME.Substring(10, 8)
                    If strBirth.Length = 6 Then
                        strBirth = strBirth.Substring(0, 2) & ":" & strBirth.Substring(2, 2) & ":" & strBirth.Substring(4, 2)
                    End If
                    dtgList.Rows(lngCounter).Cells("COL_ORDER_TIME").Value = strBirth
                    strBirth = ""
                    strBirth = .JISSHI_DATE
                    If strBirth.Length = 8 Then
                        strBirth = strBirth.Substring(0, 4) & "/" & strBirth.Substring(4, 2) & "/" & strBirth.Substring(6, 2)
                    End If
                    dtgList.Rows(lngCounter).Cells("COL_STUDY_DATE").Value = strBirth
                    strBirth = ""
                    'strBirth = .STUDY_TIME
                    'If strBirth.Length = 6 Then
                    '    strBirth = strBirth.Substring(0, 2) & ":" & strBirth.Substring(2, 2) & ":" & strBirth.Substring(4, 2)
                    'End If
                    'dtgList.Rows(lngCounter).Cells("COL_STUDY_TIME").Value = strBirth
                    dtgList.Rows(lngCounter).Cells("COL_MODALITY").Value = .MODALITY_CD
                    dtgList.Rows(lngCounter).Cells("COL_STUDY_NAME").Value = .MODALITY_NAME
                    dtgList.Rows(lngCounter).Cells("COL_BODY_PART").Value = .BODY_PART
                    Select Case .MWM_STATE
                        Case "0", ""
                            backColor = Color.White
                            foreColor = Color.Black
                            dtgList.Rows(lngCounter).Cells("COL_STATE").Value = "未受付"
                            If Not .DEL_FLG = 1 Then
                                lngNonAccept += 1
                            End If
                        Case "1"
                            backColor = Color.Yellow
                            'foreColor = Color.Red
                            foreColor = Color.Black
                            dtgList.Rows(lngCounter).Cells("COL_STATE").Value = "受付済"
                            If Not .DEL_FLG = 1 Then
                                lngAccept += 1
                            End If
                        Case "2"
                            backColor = Color.LightGreen
                            foreColor = Color.Black
                            dtgList.Rows(lngCounter).Cells("COL_STATE").Value = "検査受付済"
                            If Not .DEL_FLG = 1 Then
                                lngStudyAccept += 1
                            End If
                        Case "3"
                            backColor = Color.Yellow
                            foreColor = Color.Black
                            dtgList.Rows(lngCounter).Cells("COL_STATE").Value = "実施済"
                            If Not .DEL_FLG = 1 Then
                                lngEnd += 1
                            End If
                        Case "4"
                            backColor = Color.LightGray
                            foreColor = Color.Black
                            dtgList.Rows(lngCounter).Cells("COL_STATE").Value = "検査済"
                            If Not .DEL_FLG = 1 Then
                                lngStudyEnd += 1
                            End If
                    End Select

                    If .DEL_FLG = 1 Then
                        dtgList.Rows(lngCounter).Cells("COL_STATE").Value = "削除"
                        backColor = Color.Red
                        foreColor = Color.White
                        lngDel += 1
                    Else
                        lngNonDel += 1
                    End If

                    dtgList.Rows(lngCounter).Cells("COL_DEL_FLG").Value = .DEL_FLG
                    'dtgList.Rows(lngCounter).Cells("COL_STUDY_INSTANCE_UID").Value = .STUDY_INSTANCE_UID
                    'dtgList.Rows(lngCounter).Cells("COL_INS_DATE").Value = .INS_DATE
                    'dtgList.Rows(lngCounter).Cells("COL_INS_TIME").Value = .INS_TIME
                    'dtgList.Rows(lngCounter).Cells("COL_UPD_DATE").Value = .UPD_DATE
                    'dtgList.Rows(lngCounter).Cells("COL_UPD_TIME").Value = .UPD_TIME
                    dtgList.Rows(lngCounter).DefaultCellStyle.BackColor = backColor
                    dtgList.Rows(lngCounter).DefaultCellStyle.ForeColor = foreColor
                End With
            Next lngCounter

            '///リストのちらつき防止の為、RefreshをComment化 Deleted By Watanabe 2012.01.24
            '            dtgList.Refresh()
            Me.lblNonAccept.Text = lngNonAccept
            Me.lblAccept.Text = lngAccept
            Me.lblEnd.Text = lngEnd
            Me.lblNonDelete.Text = lngNonDel
            Me.lblDelete.Text = lngDel
            Me.lblStudyAccept.Text = lngStudyAccept
            Me.lblStudyEnd.Text = lngStudyEnd

            Me.lblTotal.Text = CLng(lngNonAccept + lngAccept + lngStudyAccept + lngEnd + lngStudyEnd + lngDel).ToString

        Catch ex As Exception
            strMsg = ex.Message
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
#End Region

#Region "フォームの初期化(subInitForm)"
    '********************************************************************************
    '* 機能概要　：フォームの初期化
    '* 関数名称　：subInitForm
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日　　：2011/02/09
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Sub subInitForm()
        bolInit = True

        '---画面タイトルの表示 ADD By Watanabe 2021.10.08
        Me.Text = "検査一覧" & Space(1) & "Ver." & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Build

        If My.Settings.VIEWER_DIV <> "1" Then
            Me.画像表示ToolStripMenuItem.Visible = False
            Me.画像表示ToolStripMenuItem1.Visible = False
        Else
            Me.画像表示ToolStripMenuItem.Visible = True
            Me.画像表示ToolStripMenuItem1.Visible = True
        End If

        Me.txtPatientID.Text = vbNullString
        Me.Interval.Checked = My.Settings.AUTO_REFRESH

        Me.Timer1.Interval = My.Settings.INTERVAL

        Me.lblPatientCnt.Text = "0"
        Me.chkNonAccept.Checked = My.Settings.NON_ACCEPT
        Me.chkAccept.Checked = My.Settings.ACCEPT
        Me.chkNonDelete.Checked = My.Settings.NON_DELETE
        Me.chkDelete.Checked = My.Settings.DELETE
        Me.chkEnd.Checked = My.Settings.Finish
        Me.chkStudyAccept.Checked = My.Settings.STUDY_ACCEPT
        Me.chkStudyEnd.Checked = My.Settings.STUDY_END

        Me.dtgList.AllowUserToAddRows = False
        Me.dtgList.MultiSelect = False
        Me.dtgList.Rows.Clear()
        Me.dtStudyDate.Value = Date.Now
        Me.txtPatientKana.Text = vbNullString

        Me.cmdAccept.Enabled = True
        Me.cmdCanAccept.Enabled = True
        Me.cmdDelete.Enabled = True
        Me.cmdCanDelete.Enabled = True
        Me.cmdJisshi.Enabled = True
        Me.cmdCanJisshi.Enabled = True

        bolInit = False
    End Sub
#End Region

#Region "受付処理/受付取消処理(subUpdAccept)"
    '********************************************************************************
    '* 機能概要　：受付処理
    '* 関数名称　：subUpdAccept
    '* 引　数　　：intProc・・・0=受付取消処理、1=受付処理
    '* 　　　　　：str_Msg・・・エラーメッセージ退避用
    '* 戻り値　　：無
    '* 作成日　　：2011/02/15
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Sub subUpdAccept(ByVal intProc As Integer, ByRef strMsg As String)
        Dim intRet As Integer = 0
        Dim strSQL As New System.Text.StringBuilder
        Dim strTerminal As String = vbNullString
        Dim strPortable As String
        Dim strSiteHealth As String = vbNullString

        Try
            '///端末名の設定
            strPortable = "0"
            strSiteHealth = "0"                     '---放射線科 施設健診対応 2022.04.22 ADD By Watanabe

            Select Case My.Settings.XRAY_DIV
                Case "0"
                    If Me.cmbModality.SelectedIndex > -1 Then
                        Select Case Me.cmbModality.SelectedItem
                            Case "一般撮影"
                                strTerminal = My.Settings.CR_TERMINAL
                                '---Ver.3.0.3.0 一般撮影でCRで受け付けられたかモバイルで受け付けられたかを判断　ADD By Watanabe 2025.01.17
                                If intCrMobile = 1 Then
                                    strSiteHealth = "1"
                                    strTerminal = My.Settings.SITEHEALTH_TERMINAL
                                Else
                                    strSiteHealth = "0"
                                End If

                                strPortable = "0"
                            Case "ポータブル"                        '---放射線科 ポータブル対応 2022.02.18 ADD By Watanabe
                                strTerminal = My.Settings.PORTA_TERMINAL
                                strPortable = "1"
                            Case "施設健診"                        '---放射線科 施設健診対応 2022.04.22 ADD By Watanabe
                                strTerminal = My.Settings.SITEHEALTH_TERMINAL
                                strSiteHealth = "1"
                                strPortable = "1"
                            Case "CT検査"
                                strTerminal = My.Settings.CT_TERMINAL
                            Case "透視撮影"
                                strTerminal = My.Settings.RF_TERMINAL
                        End Select
                    End If
                Case "1"
                    If Me.chkPorterble.Checked = True Then          'ポータブル(2台)
                        strPortable = "1"
                        strTerminal = My.Settings.US3_TERMINAL
                    ElseIf Me.chkModality1.Checked = True Then      ' Ver.3.0.2.7 Aplio 未使用だったエコー室1をAplioに変更 2024.07.18
                        strPortable = "1"
                        strTerminal = My.Settings.US4_TERMINAL
                    ElseIf Me.chkModality2.Checked = True Then      'VIVID VIVD_S60×2台
                        strTerminal = My.Settings.US1_TERMINAL
                    ElseIf Me.chkTmscPorterble.Checked = True Then  '東芝ポータブル追加対応 ADD By Watanabe 2013.04.15 XARIO
                        strPortable = "1"
                        strTerminal = My.Settings.US2_TERMINAL
                    Else
                        strTerminal = My.Settings.TERMINAL
                    End If
                Case "2"
                    If Me.chkModality1.Checked = True Then
                        strTerminal = My.Settings.ES1_TERMINAL
                    ElseIf Me.chkModality2.Checked = True Then
                        strTerminal = My.Settings.ES2_TERMINAL
                    Else
                        strTerminal = My.Settings.TERMINAL
                    End If
            End Select

            strSQL.Clear()
            strSQL.AppendLine("UPDATE ")
            strSQL.AppendLine(" TRAN_ORDER ")
            strSQL.AppendLine("SET ")
            strSQL.AppendLine(" MWM_STATE = '" & intProc & "',")
            strSQL.AppendLine(" PORTABLE = '" & strPortable & "',")
            strSQL.AppendLine(" SITEHEALTH = '" & strSiteHealth & "',") '---放射線科 施設健診対応 2022.04.22 ADD By Watanabe
            strSQL.AppendLine(" USE_TERMINAL = '" & My.Settings.TERMINAL & "',")
            If intProc = 1 Then
                strSQL.AppendLine(" TERMINAL = '" & strTerminal & "',")
                strSQL.AppendLine(" UKETSUKE_DATETIME = '" & Date.Now & "' ")
            Else
                strSQL.AppendLine(" TERMINAL = '',")
                strSQL.AppendLine(" UKETSUKE_DATETIME = '' ")
            End If

            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine(" ORDER_ANO = " & dtgList.Rows(lngSelRow).Cells("COL_ORDER_ANO").Value & " ")
            strSQL.AppendLine("AND ")
            If intProc = 1 Then
                strSQL.AppendLine(" (MWM_STATE = '0' OR MWM_STATE = '') ")
            Else
                strSQL.AppendLine(" MWM_STATE <> '0' ")
            End If
            strSQL.AppendLine("AND ")
            strSQL.AppendLine(" DEL_FLG = '0' ")

            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

        Catch ex As Exception
            strMsg = ex.Message
        Finally
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Sub
#End Region

#Region "オーダ削除処理/オーダ削除取消処理(subUpdDelOrder)"
    '********************************************************************************
    '* 機能概要　：オーダ削除処理
    '* 関数名称　：subUpdDelOrder
    '* 引　数　　：intProc・・・0=削除取消処理、1=削除処理
    '* 　　　　　：str_Msg・・・エラーメッセージ退避用
    '* 戻り値　　：無
    '* 作成日　　：2011/02/15
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Sub subUpdDelOrder(ByVal intProc As Integer, ByRef strMsg As String)
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer = 0

        Try
            strSQL.Clear()
            strSQL.AppendLine("UPDATE ")
            strSQL.AppendLine(" TRAN_ORDER ")
            strSQL.AppendLine("SET ")
            If intProc = 1 Then
                strSQL.AppendLine(" DEL_FLG = '1' ")
            Else
                strSQL.AppendLine(" DEL_FLG = '0' ")
            End If

            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine(" ORDER_ANO = " & dtgList.Rows(lngSelRow).Cells("COL_ORDER_ANO").Value & " ")
            strSQL.AppendLine("AND ")

            If intProc = 1 Then
                strSQL.AppendLine(" DEL_FLG = '0' ")
            Else
                strSQL.AppendLine(" DEL_FLG = '1' ")
            End If

            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

        Catch ex As Exception
            strMsg = ex.Message
        Finally
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Sub
#End Region

#Region "画像表示(subViewer)"
    '********************************************************************************
    '* 機能概要　：画像表示
    '* 関数名称　：subViewer
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日　　：2011/02/15
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Sub subViewer()
        Dim strURL As String = vbNullString

        'Shell(My.Settings.VIEWER_EXEC & Space(1) & My.Settings.VIEWER_END_URL)
        '///選択行のIndexを退避
        For Each r As DataGridViewRow In dtgList.SelectedRows
            lngSelRow = r.Index
        Next r

        frmEditPatient.PATIENT_ANO = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_ANO").Value
        frmEditPatient.PATIENT_ID = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_ID").Value
        frmEditPatient.PATIENT_KANA = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_KANA").Value
        frmEditPatient.PATIENT_KANJI = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_KANJI").Value
        frmEditPatient.PATIENT_EIJI = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_EIJI").Value
        frmEditPatient.PATIENT_BIRTH = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_BIRTH").Value
        frmEditPatient.PATIENT_SEX = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_SEX").Value
        frmEditPatient.PATIENT_COMMENT = dtgList.Rows(lngSelRow).Cells("COL_PATIENT_COMMENT").Value

        If lngSelRow < 0 Then Exit Sub
        strURL = My.Settings.VIEWER_URL
        strURL = strURL.Replace("$Patient", dtgList.Rows(lngSelRow).Cells("COL_PATIENT_ID").Value)
        'strURL = Replace(strURL, "$studydate", typSelData.EXCUTE_DATE)
        strURL = strURL.Replace("$modality", dtgList.Rows(lngSelRow).Cells("COL_MODALITY").Value)

        'Shell(My.Settings.VIEWER_EXEC & Space(1) & strURL)
        Process.Start(My.Settings.VIEWER_EXEC, strURL)

    End Sub

#End Region

#Region "患者情報存在チェック(fncChkPatient)"
    '********************************************************************************
    '* 機能概要　：患者情報存在チェック
    '* 関数名称　：fncChkPatient
    '* 引　数　　：無
    '* 戻り値　　：Boolean・・・True=存在、False=未存在
    '* 作成日　　：2012/01/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncChkPatient(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            typPatient = New clsPatinet

            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("PATIENT_ANO,")
            strSQL.AppendLine("PATIENT_ID,")
            strSQL.AppendLine("ISNULL(KENSHIN_PATIENT_ID,'') AS KENSHIN_PATIENT_ID,")
            strSQL.AppendLine("PATIENT_KANA,")
            strSQL.AppendLine("PATIENT_KANJI,")
            strSQL.AppendLine("PATIENT_EIJI,")
            strSQL.AppendLine("PATIENT_BIRTH,")
            strSQL.AppendLine("PATIENT_SEX,")
            strSQL.AppendLine("ISNULL(PATIENT_COMMENT,'') AS PATIENT_COMMENT ")
            strSQL.AppendLine("FROM TRAN_PATIENT ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("PATIENT_ID = '" & Me.txtPatientID.Text.Trim & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")
            strSQL.AppendLine("ORDER BY PATIENT_ANO DESC ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            With typPatient
                .PATIENT_ANO = dsData.Tables(0).Rows(0).Item("PATIENT_ANO")
                .PATIENT_ID = dsData.Tables(0).Rows(0).Item("PATIENT_ID")
                .KENSHIN_PATIENT_ID = dsData.Tables(0).Rows(0).Item("KENSHIN_PATIENT_ID")
                .PATIENT_KANA = dsData.Tables(0).Rows(0).Item("PATIENT_KANA")
                .PATIENT_KANJI = dsData.Tables(0).Rows(0).Item("PATIENT_KANJI")
                .PATIENT_EIJI = dsData.Tables(0).Rows(0).Item("PATIENT_EIJI")
                .PATIENT_BIRTH = dsData.Tables(0).Rows(0).Item("PATIENT_BIRTH")
                .PATIENT_SEX = dsData.Tables(0).Rows(0).Item("PATIENT_SEX")
                .PATIENT_COMMENT = dsData.Tables(0).Rows(0).Item("PATIENT_COMMENT")
            End With
            typIjiPatient = typPatient

            Return RET_NORMAL

        Catch ex As Exception
            strMsg = ex.Message & Space(1) & "fncChkPatient"
            Return RET_ERROR
        Finally
            dsData.Dispose()
            dsData = Nothing
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Function
#End Region

#Region "モダリティ情報取得(subGetModality)"
    '********************************************************************************
    '* 機能概要　：モダリティ情報取得
    '* 関数名称　：subGetModality
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetModality(ByRef strMsg As String, Optional intProc As Integer = 0) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try

            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT DISTINCT")
            'strSQL.AppendLine(" ORDER_MODALITY_NO,")
            strSQL.AppendLine(" MODALITY_NAME,")
            strSQL.AppendLine(" MODALITY_CD,")
            strSQL.AppendLine(" MODALITY_SORT,")
            strSQL.AppendLine(" TERMINAL ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine(" MST_MODALITY ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine(" STUDY_DIV = '" & My.Settings.XRAY_DIV & "' ")
            strSQL.AppendLine("AND")
            strSQL.AppendLine(" DEL_FLG = 'False'")
            strSQL.AppendLine("GROUP BY MODALITY_CD, MODALITY_NAME, TERMINAL,MODALITY_SORT ")
            strSQL.AppendLine("ORDER BY MODALITY_SORT ")

            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            If intProc = 0 Then
                With typModality
                    '.MODALITY_NO = dsData.Tables(0).Rows(0).Item("ORDER_MODALITY_NO")
                    .MODALITY_CD = dsData.Tables(0).Rows(0).Item("MODALITY_CD")
                    .MODALITY_NAME = dsData.Tables(0).Rows(0).Item("MODALITY_NAME")
                    .TERMINAL = dsData.Tables(0).Rows(0).Item("TERMINAL")
                End With
            Else
                Dim lngCounter As Integer = 0
                Erase aryModality
                Me.cmbModality.Items.Clear()

                For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                    ReDim Preserve aryModality(lngCounter)

                    aryModality(lngCounter) = New clsModality

                    With aryModality(lngCounter)
                        '.MODALITY_NO = dsData.Tables(0).Rows(lngCounter).Item("ORDER_MODALITY_NO")
                        .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")
                        .MODALITY_NAME = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_NAME")

                        Me.cmbModality.Items.Add(.MODALITY_NAME)
                    End With
                Next lngCounter
            End If

            Return RET_NORMAL
        Catch ex As Exception
            Call subOutLog(ex.Message & Space(1) & "モダリティ情報取得(subGetModality)", 1)
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try

    End Function

#End Region

#Region "一覧項目幅保存(subSaveColWidth)"
    '**********************************************************************
    '* 関数名称　：subSaveColWidth
    '* 機能概要　：一覧項目幅保存
    '* 引　数　　：
    '* 戻り値　　：無
    '* 作成日　　：2010/08/17
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************
    Private Sub subSaveColWidth()

        Call subOutLog("一覧項目幅保存 開始 [frmList.subSaveColWidth]", 0)

        With Settings.Instance
            .COL_STATE_WIDTH = dtgList.Columns("COL_STATE").Width
            .COL_STUDY_NAME_WIDTH = dtgList.Columns("COL_STUDY_NAME").Width
            .COL_ORDER_DATE_WIDTH = dtgList.Columns("COL_ORDER_DATE").Width
            .COL_ORDER_TIME_WIDTH = dtgList.Columns("COL_ORDER_TIME").Width
            .COL_STUDY_DATE_WIDTH = dtgList.Columns("COL_STUDY_DATE").Width
            .COL_STUDY_TIME_WIDTH = dtgList.Columns("COL_STUDY_TIME").Width
            .COL_PATIENT_ID_WIDTH = dtgList.Columns("COL_PATIENT_ID").Width
            .COL_PATIENT_KANJI_WIDTH = dtgList.Columns("COL_PATIENT_KANJI").Width
            .COL_PATIENT_KANA_WIDTH = dtgList.Columns("COL_PATIENT_KANA").Width
            .COL_PATIENT_EIJI_WIDTH = dtgList.Columns("COL_PATIENT_EIJI").Width
            .COL_PATIENT_BIRTH_WIDTH = dtgList.Columns("COL_PATIENT_BIRTH").Width
            .COL_PATIENT_SEX_WIDTH = dtgList.Columns("COL_PATIENT_SEX").Width
            '.COL_SATSUEI_DIV_WIDTH = dtgList.Columns("COL_SATSUEI_DIV").Width
            .COL_BODY_PART_WIDTH = dtgList.Columns("COL_BODY_PART").Width
            .COL_MODALITY_WIDTH = dtgList.Columns("COL_MODALITY").Width
            .COL_DEL_FLG_WIDTH = dtgList.Columns("COL_DEL_FLG").Width
            '.COL_STUDY_INSTANCE_UID_WIDTH = dtgList.Columns("COL_STUDY_INSTANCE_UID").Width
            .COL_ORDER_ANO_WIDTH = dtgList.Columns("COL_ORDER_ANO").Width
            .COL_ORDER_NO_WIDTH = dtgList.Columns("COL_ORDER_NO").Width
            .COL_PATIENT_ANO_WIDTH = dtgList.Columns("COL_PATIENT_ANO").Width
            .COL_ORDER_COMMENT_WIDTH = dtgList.Columns("COL_ORDER_COMMENT").Width
            .COL_PATIENT_COMMENT_WIDTH = dtgList.Columns("COL_PATIENT_COMMENT").Width
            .COL_COMMENT_FLG_WIDTH = dtgList.Columns("COL_COMMENT_FLG").Width
            .COL_DEPT_WIDTH = dtgList.Columns("COL_DEPT").Width
            .COL_WARD_WIDTH = dtgList.Columns("COL_WARD").Width
            .COL_ROOM_WIDTH = dtgList.Columns("COL_ROOM").Width
            .COL_NYUGAI_WIDTH = dtgList.Columns("COL_NYUGAI").Width
            .COL_REQ_DOC_WIDTH = dtgList.Columns("COL_REQ_DOC").Width
            '.COL_INS_DATE_WIDTH = dtgList.Columns("COL_INS_DATE").Width
            '.COL_INS_TIME_WIDTH = dtgList.Columns("COL_INS_TIME").Width
            '.COL_UPD_DATE_WIDTH = dtgList.Columns("COL_UPD_DATE").Width
            '.COL_UPD_TIME_WIDTH = dtgList.Columns("COL_UPD_TIME").Width
            .COL_AGE_WIDTH = dtgList.Columns("COL_AGE").Width

            .COL_STATE_VISIBLE = dtgList.Columns("COL_STATE").Visible
            .COL_STUDY_NAME_VISIBLE = dtgList.Columns("COL_STUDY_NAME").Visible
            .COL_ORDER_DATE_VISIBLE = dtgList.Columns("COL_ORDER_DATE").Visible
            .COL_ORDER_TIME_VISIBLE = dtgList.Columns("COL_ORDER_TIME").Visible
            .COL_STUDY_DATE_VISIBLE = dtgList.Columns("COL_STUDY_DATE").Visible
            .COL_STUDY_TIME_VISIBLE = dtgList.Columns("COL_STUDY_TIME").Visible
            .COL_PATIENT_ID_VISIBLE = dtgList.Columns("COL_PATIENT_ID").Visible
            .COL_PATIENT_KANJI_VISIBLE = dtgList.Columns("COL_PATIENT_KANJI").Visible
            .COL_PATIENT_KANA_VISIBLE = dtgList.Columns("COL_PATIENT_KANA").Visible
            .COL_PATIENT_EIJI_VISIBLE = dtgList.Columns("COL_PATIENT_EIJI").Visible
            .COL_PATIENT_BIRTH_VISIBLE = dtgList.Columns("COL_PATIENT_BIRTH").Visible
            .COL_PATIENT_SEX_VISIBLE = dtgList.Columns("COL_PATIENT_SEX").Visible
            '.COL_SATSUEI_DIV_VISIBLE = dtgList.Columns("COL_SATSUEI_DIV").Visible
            .COL_BODY_PART_VISIBLE = dtgList.Columns("COL_BODY_PART").Visible
            .COL_MODALITY_VISIBLE = dtgList.Columns("COL_MODALITY").Visible
            .COL_DEL_FLG_VISIBLE = dtgList.Columns("COL_DEL_FLG").Visible
            '.COL_STUDY_INSTANCE_UID_VISIBLE = dtgList.Columns("COL_STUDY_INSTANCE_UID").Visible
            .COL_ORDER_ANO_VISIBLE = dtgList.Columns("COL_ORDER_ANO").Visible
            .COL_ORDER_NO_VISIBLE = dtgList.Columns("COL_ORDER_NO").Visible
            .COL_PATIENT_ANO_VISIBLE = dtgList.Columns("COL_PATIENT_ANO").Visible
            .COL_ORDER_COMMENT_VISIBLE = dtgList.Columns("COL_ORDER_COMMENT").Visible
            .COL_PATIENT_COMMENT_VISIBLE = dtgList.Columns("COL_PATIENT_COMMENT").Visible
            .COL_COMMENT_FLG_VISIBLE = dtgList.Columns("COL_COMMENT_FLG").Visible
            .COL_DEPT_VISIBLE = dtgList.Columns("COL_DEPT").Visible
            .COL_WARD_VISIBLE = dtgList.Columns("COL_WARD").Visible
            .COL_ROOM_VISIBLE = dtgList.Columns("COL_ROOM").Visible
            .COL_NYUGAI_VISIBLE = dtgList.Columns("COL_NYUGAI").Visible
            .COL_REQ_DOC_VISIBLE = dtgList.Columns("COL_REQ_DOC").Visible
            '.COL_INS_DATE_VISIBLE = dtgList.Columns("COL_INS_DATE").Visible
            '.COL_INS_TIME_VISIBLE = dtgList.Columns("COL_INS_TIME").Visible
            '.COL_UPD_DATE_VISIBLE = dtgList.Columns("COL_UPD_DATE").Visible
            '.COL_UPD_TIME_VISIBLE = dtgList.Columns("COL_UPD_TIME").Visible
            .COL_AGE_VISIBLE = dtgList.Columns("COL_AGE").Visible

            .COL_STATE_DISP_INDX = dtgList.Columns("COL_STATE").DisplayIndex
            .COL_STUDY_NAME_DISP_INDX = dtgList.Columns("COL_STUDY_NAME").DisplayIndex
            .COL_ORDER_DATE_DISP_INDX = dtgList.Columns("COL_ORDER_DATE").DisplayIndex
            .COL_ORDER_TIME_DISP_INDX = dtgList.Columns("COL_ORDER_TIME").DisplayIndex
            .COL_STUDY_DATE_DISP_INDX = dtgList.Columns("COL_STUDY_DATE").DisplayIndex
            .COL_STUDY_TIME_DISP_INDX = dtgList.Columns("COL_STUDY_TIME").DisplayIndex
            .COL_PATIENT_ID_DISP_INDX = dtgList.Columns("COL_PATIENT_ID").DisplayIndex
            .COL_PATIENT_KANJI_DISP_INDX = dtgList.Columns("COL_PATIENT_KANJI").DisplayIndex
            .COL_PATIENT_KANA_DISP_INDX = dtgList.Columns("COL_PATIENT_KANA").DisplayIndex
            .COL_PATIENT_EIJI_DISP_INDX = dtgList.Columns("COL_PATIENT_EIJI").DisplayIndex
            .COL_PATIENT_BIRTH_DISP_INDX = dtgList.Columns("COL_PATIENT_BIRTH").DisplayIndex
            .COL_PATIENT_SEX_DISP_INDX = dtgList.Columns("COL_PATIENT_SEX").DisplayIndex
            '.COL_SATSUEI_DIV_DISP_INDX = dtgList.Columns("COL_SATSUEI_DIV").DisplayIndex
            .COL_BODY_PART_DISP_INDX = dtgList.Columns("COL_BODY_PART").DisplayIndex
            .COL_MODALITY_DISP_INDX = dtgList.Columns("COL_MODALITY").DisplayIndex
            .COL_DEL_FLG_DISP_INDX = dtgList.Columns("COL_DEL_FLG").DisplayIndex
            '.COL_STUDY_INSTANCE_UID_DISP_INDX = dtgList.Columns("COL_STUDY_INSTANCE_UID").DisplayIndex
            .COL_ORDER_ANO_DISP_INDX = dtgList.Columns("COL_ORDER_ANO").DisplayIndex
            .COL_ORDER_NO_DISP_INDX = dtgList.Columns("COL_ORDER_NO").DisplayIndex
            .COL_PATIENT_ANO_DISP_INDX = dtgList.Columns("COL_PATIENT_ANO").DisplayIndex
            .COL_ORDER_COMMENT_DISP_INDX = dtgList.Columns("COL_ORDER_COMMENT").DisplayIndex
            .COL_PATIENT_COMMENT_DISP_INDX = dtgList.Columns("COL_PATIENT_COMMENT").DisplayIndex
            .COL_COMMENT_FLG_DISP_INDX = dtgList.Columns("COL_COMMENT_FLG").DisplayIndex
            .COL_DEPT_DISP_INDX = dtgList.Columns("COL_DEPT").DisplayIndex
            .COL_WARD_DISP_INDX = dtgList.Columns("COL_WARD").DisplayIndex
            .COL_ROOM_DISP_INDX = dtgList.Columns("COL_ROOM").DisplayIndex
            .COL_NYUGAI_DISP_INDX = dtgList.Columns("COL_NYUGAI").DisplayIndex
            .COL_REQ_DOC_DISP_INDX = dtgList.Columns("COL_REQ_DOC").DisplayIndex
            '.COL_INS_DATE_DISP_INDX = dtgList.Columns("COL_INS_DATE").DisplayIndex
            '.COL_INS_TIME_DISP_INDX = dtgList.Columns("COL_INS_TIME").DisplayIndex
            '.COL_UPD_DATE_DISP_INDX = dtgList.Columns("COL_UPD_DATE").DisplayIndex
            '.COL_UPD_TIME_DISP_INDX = dtgList.Columns("COL_UPD_TIME").DisplayIndex
            .COL_AGE_DISP_INDX = dtgList.Columns("COL_AGE").DisplayIndex

            .COL_STATE_HEADER = dtgList.Columns("COL_STATE").HeaderText
            .COL_STUDY_NAME_HEADER = dtgList.Columns("COL_STUDY_NAME").HeaderText
            .COL_ORDER_DATE_HEADER = dtgList.Columns("COL_ORDER_DATE").HeaderText
            .COL_ORDER_TIME_HEADER = dtgList.Columns("COL_ORDER_TIME").HeaderText
            .COL_STUDY_DATE_HEADER = dtgList.Columns("COL_STUDY_DATE").HeaderText
            .COL_STUDY_TIME_HEADER = dtgList.Columns("COL_STUDY_TIME").HeaderText
            .COL_PATIENT_ID_HEADER = dtgList.Columns("COL_PATIENT_ID").HeaderText
            .COL_PATIENT_KANJI_HEADER = dtgList.Columns("COL_PATIENT_KANJI").HeaderText
            .COL_PATIENT_KANA_HEADER = dtgList.Columns("COL_PATIENT_KANA").HeaderText
            .COL_PATIENT_EIJI_HEADER = dtgList.Columns("COL_PATIENT_EIJI").HeaderText
            .COL_PATIENT_BIRTH_HEADER = dtgList.Columns("COL_PATIENT_BIRTH").HeaderText
            .COL_PATIENT_SEX_HEADER = dtgList.Columns("COL_PATIENT_SEX").HeaderText
            '.COL_SATSUEI_DIV_HEADER = dtgList.Columns("COL_SATSUEI_DIV").HeaderText
            .COL_BODY_PART_HEADER = dtgList.Columns("COL_BODY_PART").HeaderText
            .COL_MODALITY_HEADER = dtgList.Columns("COL_MODALITY").HeaderText
            .COL_DEL_FLG_HEADER = dtgList.Columns("COL_DEL_FLG").HeaderText
            '.COL_STUDY_INSTANCE_UID_HEADER = dtgList.Columns("COL_STUDY_INSTANCE_UID").HeaderText
            .COL_ORDER_ANO_HEADER = dtgList.Columns("COL_ORDER_ANO").HeaderText
            .COL_ORDER_NO_HEADER = dtgList.Columns("COL_ORDER_NO").HeaderText
            .COL_PATIENT_ANO_HEADER = dtgList.Columns("COL_PATIENT_ANO").HeaderText
            .COL_ORDER_COMMENT_HEADER = dtgList.Columns("COL_ORDER_COMMENT").HeaderText
            .COL_PATIENT_COMMENT_HEADER = dtgList.Columns("COL_PATIENT_COMMENT").HeaderText
            .COL_COMMENT_FLG_HEADER = dtgList.Columns("COL_COMMENT_FLG").HeaderText
            .COL_DEPT_HEADER = dtgList.Columns("COL_DEPT").HeaderText
            .COL_WARD_HEADER = dtgList.Columns("COL_WARD").HeaderText
            .COL_ROOM_HEADER = dtgList.Columns("COL_ROOM").HeaderText
            .COL_NYUGAI_HEADER = dtgList.Columns("COL_NYUGAI").HeaderText
            .COL_REQ_DOC_HEADER = dtgList.Columns("COL_REQ_DOC").HeaderText
            '.COL_INS_DATE_HEADER = dtgList.Columns("COL_INS_DATE").HeaderText
            '.COL_INS_TIME_HEADER = dtgList.Columns("COL_INS_TIME").HeaderText
            '.COL_UPD_DATE_HEADER = dtgList.Columns("COL_UPD_DATE").HeaderText
            '.COL_UPD_TIME_HEADER = dtgList.Columns("COL_UPD_TIME").HeaderText
            .COL_AGE_HEADER = dtgList.Columns("COL_AGE").HeaderText
        End With

        Settings.SaveToXmlFile()


        Call subOutLog("一覧項目幅保存 終了 [frmList.subSaveColWidth]", 0)
    End Sub
#End Region

#Region "一覧項目幅設定(subSetColWidth)"
    '**********************************************************************
    '* 関数名称　：subSetColWidth
    '* 機能概要　：一覧項目幅設定
    '* 引　数　　：
    '* 戻り値　　：無
    '* 作成日　　：2010/08/17
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************
    Private Sub subSetColWidth()
        Settings.LoadFromXmlFile()

        '        Call subOutLog("一覧項目幅設定 開始 [frmList.subSetColWidth]", 0)
        Try

            With Settings.Instance
                dtgList.Columns("COL_STATE").Width = .COL_STATE_WIDTH                   '進捗
                dtgList.Columns("COL_STUDY_NAME").Width = .COL_STUDY_NAME_WIDTH         '検査名
                dtgList.Columns("COL_ORDER_DATE").Width = .COL_ORDER_DATE_WIDTH         '検査予定日
                dtgList.Columns("COL_ORDER_TIME").Width = .COL_ORDER_TIME_WIDTH         '検査予定時刻
                dtgList.Columns("COL_STUDY_DATE").Width = .COL_STUDY_DATE_WIDTH         '検査実施日
                dtgList.Columns("COL_STUDY_TIME").Width = .COL_STUDY_TIME_WIDTH         '検査実施時刻
                dtgList.Columns("COL_PATIENT_ID").Width = .COL_PATIENT_ID_WIDTH         '患者ID
                dtgList.Columns("COL_PATIENT_KANJI").Width = .COL_PATIENT_KANJI_WIDTH   '患者漢字氏名
                dtgList.Columns("COL_PATIENT_KANA").Width = .COL_PATIENT_KANA_WIDTH     '患者カナ氏名
                dtgList.Columns("COL_PATIENT_EIJI").Width = .COL_PATIENT_EIJI_WIDTH     '患者英字氏名
                dtgList.Columns("COL_PATIENT_BIRTH").Width = .COL_PATIENT_BIRTH_WIDTH   '生年月日
                dtgList.Columns("COL_PATIENT_SEX").Width = .COL_PATIENT_SEX_WIDTH       '性別
                'dtgList.Columns("COL_SATSUEI_DIV").Width = .COL_SATSUEI_DIV_WIDTH
                dtgList.Columns("COL_BODY_PART").Width = .COL_BODY_PART_WIDTH           '検査部位
                dtgList.Columns("COL_MODALITY").Width = .COL_MODALITY_WIDTH             'モダリティ
                dtgList.Columns("COL_DEL_FLG").Width = .COL_DEL_FLG_WIDTH               '削除フラグ
                'dtgList.Columns("COL_STUDY_INSTANCE_UID").Width = .COL_STUDY_INSTANCE_UID_WIDTH
                dtgList.Columns("COL_ORDER_ANO").Width = .COL_ORDER_ANO_WIDTH           'オーダ登録番号
                dtgList.Columns("COL_ORDER_NO").Width = .COL_ORDER_NO_WIDTH             'オーダ番号
                dtgList.Columns("COL_PATIENT_ANO").Width = .COL_PATIENT_ANO_WIDTH       '患者登録番号
                dtgList.Columns("COL_ORDER_COMMENT").Width = .COL_ORDER_COMMENT_WIDTH   'オーダコメント
                dtgList.Columns("COL_PATIENT_COMMENT").Width = .COL_PATIENT_COMMENT_WIDTH   '患者コメント
                dtgList.Columns("COL_COMMENT_FLG").Width = .COL_COMMENT_FLG_WIDTH           'コメントフラグ
                dtgList.Columns("COL_WARD").Width = .COL_WARD_WIDTH
                dtgList.Columns("COL_ROOM").Width = .COL_ROOM_WIDTH
                dtgList.Columns("COL_REQ_DOC").Width = .COL_REQ_DOC_WIDTH
                dtgList.Columns("COL_DEPT").Width = .COL_DEPT_WIDTH
                dtgList.Columns("COL_NYUGAI").Width = .COL_NYUGAI_WIDTH

                dtgList.Columns("COL_INS_DATE").Width = .COL_INS_DATE_WIDTH
                dtgList.Columns("COL_INS_TIME").Width = .COL_INS_TIME_WIDTH
                dtgList.Columns("COL_UPD_DATE").Width = .COL_UPD_DATE_WIDTH
                dtgList.Columns("COL_UPD_TIME").Width = .COL_UPD_TIME_WIDTH
                dtgList.Columns("COL_AGE").Width = .COL_AGE_WIDTH

                dtgList.Columns("COL_STATE").Visible = .COL_STATE_VISIBLE
                dtgList.Columns("COL_STUDY_NAME").Visible = .COL_STUDY_NAME_VISIBLE
                dtgList.Columns("COL_ORDER_DATE").Visible = .COL_ORDER_DATE_VISIBLE
                dtgList.Columns("COL_ORDER_TIME").Visible = .COL_ORDER_TIME_VISIBLE
                dtgList.Columns("COL_STUDY_DATE").Visible = .COL_STUDY_DATE_VISIBLE
                dtgList.Columns("COL_STUDY_TIME").Visible = .COL_STUDY_TIME_VISIBLE
                dtgList.Columns("COL_PATIENT_ID").Visible = .COL_PATIENT_ID_VISIBLE
                dtgList.Columns("COL_PATIENT_KANJI").Visible = .COL_PATIENT_KANJI_VISIBLE
                dtgList.Columns("COL_PATIENT_KANA").Visible = .COL_PATIENT_KANA_VISIBLE
                dtgList.Columns("COL_PATIENT_EIJI").Visible = .COL_PATIENT_EIJI_VISIBLE
                dtgList.Columns("COL_PATIENT_BIRTH").Visible = .COL_PATIENT_BIRTH_VISIBLE
                dtgList.Columns("COL_PATIENT_SEX").Visible = .COL_PATIENT_SEX_VISIBLE
                'dtgList.Columns("COL_SATSUEI_DIV").Visible = .COL_SATSUEI_DIV_VISIBLE
                dtgList.Columns("COL_BODY_PART").Visible = .COL_BODY_PART_VISIBLE
                dtgList.Columns("COL_MODALITY").Visible = .COL_MODALITY_VISIBLE
                dtgList.Columns("COL_DEL_FLG").Visible = .COL_DEL_FLG_VISIBLE
                'dtgList.Columns("COL_STUDY_INSTANCE_UID").Visible = .COL_STUDY_INSTANCE_UID_VISIBLE
                dtgList.Columns("COL_ORDER_ANO").Visible = .COL_ORDER_ANO_VISIBLE
                dtgList.Columns("COL_ORDER_NO").Visible = .COL_ORDER_NO_VISIBLE
                dtgList.Columns("COL_PATIENT_ANO").Visible = .COL_PATIENT_ANO_VISIBLE
                dtgList.Columns("COL_ORDER_COMMENT").Visible = .COL_ORDER_COMMENT_VISIBLE
                dtgList.Columns("COL_PATIENT_COMMENT").Visible = .COL_PATIENT_COMMENT_VISIBLE
                dtgList.Columns("COL_COMMENT_FLG").Visible = .COL_COMMENT_FLG_VISIBLE
                dtgList.Columns("COL_INS_DATE").Visible = .COL_INS_DATE_VISIBLE
                dtgList.Columns("COL_INS_TIME").Visible = .COL_INS_TIME_VISIBLE
                dtgList.Columns("COL_UPD_DATE").Visible = .COL_UPD_DATE_VISIBLE
                dtgList.Columns("COL_UPD_TIME").Visible = .COL_UPD_TIME_VISIBLE
                dtgList.Columns("COL_AGE").Visible = .COL_AGE_VISIBLE

                dtgList.Columns("COL_WARD").Visible = .COL_WARD_VISIBLE
                dtgList.Columns("COL_ROOM").Visible = .COL_ROOM_VISIBLE
                dtgList.Columns("COL_REQ_DOC").Visible = .COL_REQ_DOC_VISIBLE
                dtgList.Columns("COL_DEPT").Visible = .COL_DEPT_VISIBLE
                dtgList.Columns("COL_NYUGAI").Visible = .COL_NYUGAI_VISIBLE

                dtgList.Columns("COL_STATE").DisplayIndex = .COL_STATE_DISP_INDX
                dtgList.Columns("COL_STUDY_NAME").DisplayIndex = .COL_STUDY_NAME_DISP_INDX
                dtgList.Columns("COL_ORDER_DATE").DisplayIndex = .COL_ORDER_DATE_DISP_INDX
                dtgList.Columns("COL_ORDER_TIME").DisplayIndex = .COL_ORDER_TIME_DISP_INDX
                dtgList.Columns("COL_STUDY_DATE").DisplayIndex = .COL_STUDY_DATE_DISP_INDX
                dtgList.Columns("COL_STUDY_TIME").DisplayIndex = .COL_STUDY_TIME_DISP_INDX
                dtgList.Columns("COL_PATIENT_ID").DisplayIndex = .COL_PATIENT_ID_DISP_INDX
                dtgList.Columns("COL_PATIENT_KANJI").DisplayIndex = .COL_PATIENT_KANJI_DISP_INDX
                dtgList.Columns("COL_PATIENT_KANA").DisplayIndex = .COL_PATIENT_KANA_DISP_INDX
                dtgList.Columns("COL_PATIENT_EIJI").DisplayIndex = .COL_PATIENT_EIJI_DISP_INDX
                dtgList.Columns("COL_PATIENT_BIRTH").DisplayIndex = .COL_PATIENT_BIRTH_DISP_INDX
                dtgList.Columns("COL_PATIENT_SEX").DisplayIndex = .COL_PATIENT_SEX_DISP_INDX
                'dtgList.Columns("COL_SATSUEI_DIV").DisplayIndex = .COL_SATSUEI_DIV_DISP_INDX
                dtgList.Columns("COL_BODY_PART").DisplayIndex = .COL_BODY_PART_DISP_INDX
                dtgList.Columns("COL_MODALITY").DisplayIndex = .COL_MODALITY_DISP_INDX
                dtgList.Columns("COL_DEL_FLG").DisplayIndex = .COL_DEL_FLG_DISP_INDX
                'dtgList.Columns("COL_STUDY_INSTANCE_UID").DisplayIndex = .COL_STUDY_INSTANCE_UID_DISP_INDX
                dtgList.Columns("COL_ORDER_ANO").DisplayIndex = .COL_ORDER_ANO_DISP_INDX
                dtgList.Columns("COL_ORDER_NO").DisplayIndex = .COL_ORDER_NO_DISP_INDX
                dtgList.Columns("COL_PATIENT_ANO").DisplayIndex = .COL_PATIENT_ANO_DISP_INDX
                dtgList.Columns("COL_ORDER_COMMENT").DisplayIndex = .COL_ORDER_COMMENT_DISP_INDX
                dtgList.Columns("COL_PATIENT_COMMENT").DisplayIndex = .COL_PATIENT_COMMENT_DISP_INDX
                dtgList.Columns("COL_COMMENT_FLG").DisplayIndex = .COL_COMMENT_FLG_DISP_INDX
                dtgList.Columns("COL_INS_DATE").DisplayIndex = .COL_INS_DATE_DISP_INDX
                dtgList.Columns("COL_INS_TIME").DisplayIndex = .COL_INS_TIME_DISP_INDX
                dtgList.Columns("COL_UPD_DATE").DisplayIndex = .COL_UPD_DATE_DISP_INDX
                dtgList.Columns("COL_UPD_TIME").DisplayIndex = .COL_UPD_TIME_DISP_INDX
                dtgList.Columns("COL_AGE").DisplayIndex = .COL_AGE_DISP_INDX

                dtgList.Columns("COL_WARD").DisplayIndex = .COL_WARD_DISP_INDX
                dtgList.Columns("COL_ROOM").DisplayIndex = .COL_ROOM_DISP_INDX
                dtgList.Columns("COL_REQ_DOC").DisplayIndex = .COL_REQ_DOC_DISP_INDX
                dtgList.Columns("COL_DEPT").DisplayIndex = .COL_DEPT_DISP_INDX
                dtgList.Columns("COL_NYUGAI").DisplayIndex = .COL_NYUGAI_DISP_INDX

                dtgList.Columns("COL_STATE").HeaderText = .COL_STATE_HEADER
                dtgList.Columns("COL_STUDY_NAME").HeaderText = .COL_STUDY_NAME_HEADER
                dtgList.Columns("COL_ORDER_DATE").HeaderText = .COL_ORDER_DATE_HEADER
                dtgList.Columns("COL_ORDER_TIME").HeaderText = .COL_ORDER_TIME_HEADER
                dtgList.Columns("COL_STUDY_DATE").HeaderText = .COL_STUDY_DATE_HEADER
                dtgList.Columns("COL_STUDY_TIME").HeaderText = .COL_STUDY_TIME_HEADER
                dtgList.Columns("COL_PATIENT_ID").HeaderText = .COL_PATIENT_ID_HEADER
                dtgList.Columns("COL_PATIENT_KANJI").HeaderText = .COL_PATIENT_KANJI_HEADER
                dtgList.Columns("COL_PATIENT_KANA").HeaderText = .COL_PATIENT_KANA_HEADER
                dtgList.Columns("COL_PATIENT_EIJI").HeaderText = .COL_PATIENT_EIJI_HEADER
                dtgList.Columns("COL_PATIENT_BIRTH").HeaderText = .COL_PATIENT_BIRTH_HEADER
                dtgList.Columns("COL_PATIENT_SEX").HeaderText = .COL_PATIENT_SEX_HEADER
                'dtgList.Columns("COL_SATSUEI_DIV").HeaderText = .COL_SATSUEI_DIV_HEADER
                dtgList.Columns("COL_BODY_PART").HeaderText = .COL_BODY_PART_HEADER
                dtgList.Columns("COL_MODALITY").HeaderText = .COL_MODALITY_HEADER
                dtgList.Columns("COL_DEL_FLG").HeaderText = .COL_DEL_FLG_HEADER
                'dtgList.Columns("COL_STUDY_INSTANCE_UID").HeaderText = .COL_STUDY_INSTANCE_UID_HEADER
                dtgList.Columns("COL_ORDER_ANO").HeaderText = .COL_ORDER_ANO_HEADER
                dtgList.Columns("COL_ORDER_NO").HeaderText = .COL_ORDER_NO_HEADER
                dtgList.Columns("COL_PATIENT_ANO").HeaderText = .COL_PATIENT_ANO_HEADER
                dtgList.Columns("COL_ORDER_COMMENT").HeaderText = .COL_ORDER_COMMENT_HEADER
                dtgList.Columns("COL_PATIENT_COMMENT").HeaderText = .COL_PATIENT_COMMENT_HEADER
                dtgList.Columns("COL_COMMENT_FLG").HeaderText = .COL_COMMENT_FLG_HEADER
                dtgList.Columns("COL_INS_DATE").HeaderText = .COL_INS_DATE_HEADER
                dtgList.Columns("COL_INS_TIME").HeaderText = .COL_INS_TIME_HEADER
                dtgList.Columns("COL_UPD_DATE").HeaderText = .COL_UPD_DATE_HEADER
                dtgList.Columns("COL_UPD_TIME").HeaderText = .COL_UPD_TIME_HEADER
                'dtgList.Columns("COL_AGE").DisplayIndex = .COL_AGE_DISP_INDX

                dtgList.Columns("COL_WARD").HeaderText = .COL_WARD_HEADER
                dtgList.Columns("COL_ROOM").HeaderText = .COL_ROOM_HEADER
                dtgList.Columns("COL_REQ_DOC").HeaderText = .COL_REQ_DOC_HEADER
                dtgList.Columns("COL_DEPT").HeaderText = .COL_DEPT_HEADER
                dtgList.Columns("COL_NYUGAI").HeaderText = .COL_NYUGAI_HEADER
            End With
        Catch ex As Exception
            Call subOutLog("subSetColWidth エラー：" & ex.Message, 0)
        End Try


        '       Call subOutLog("一覧項目幅設定 終了 [frmList.subSetColWidth]", 0)

    End Sub
#End Region

#Region "オーダ情報登録(受付済オーダの登録)(fncInsOrder)"
    '********************************************************************************
    '* 機能概要　：オーダ情報登録処理(受付済オーダの登録)
    '* 関数名称　：fncInsOrder
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/03/01
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncInsOrder(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer
        Dim strWork As String = vbNullString
        Dim strDate As String()
        Dim dsData As New DataSet
        Dim strOrderDate As String = vbNullString
        Dim strOrderNo As String = vbNullString
        Dim strStudyDiv As String = vbNullString

        Try
            '///過去日付が選択された場合、本日日付に置き換える ADD By Watanabe 2012.04.03
            If Me.dtStudyDate.Value.ToString("yyyyMMdd") < Now.ToString("yyyyMMdd") Then
                strOrderDate = Now.ToString("yyyyMMdd")
            Else
                strOrderDate = Me.dtStudyDate.Value.ToString("yyyyMMdd")
            End If

            strWork = fncGetServerDate(2, strMsg)
            strDate = strWork.Split(",")
            '///健診番号がある場合、OrderNoをYYMMDDHHMM+健診番号とし、無い場合YYYYMMDDHHMMSSとする。2014.02.15
            If g_JushinNo = "" Then
                '///OrderNoをYYYYMMDDHHMMSS
                strOrderNo = strDate(0).Trim & strDate(1).Trim
            Else
                '///OrderNoをYYMMDDHHMM+健診番号
                strOrderNo = strDate(0).Trim.Substring(2, 6) & strDate(1).Trim.Substring(0, 4) & g_JushinNo
            End If
            strStudyDiv = 0
            'If G_STUDY_DIV = 0 Then
            '    strOrderNo = "S" & strDate(0).Trim.Substring(2, 6) & strDate(1).Trim & aryModality(Me.cmbModality.SelectedIndex).MODALITY_CD
            '    strStudyDiv = 0
            'Else
            '    strOrderNo = "K" & strDate(0).Trim.Substring(2, 6) & strDate(1).Trim & aryModality(Me.cmbModality.SelectedIndex).MODALITY_CD
            '    strStudyDiv = 1
            'End If
            strSQL.Clear()
            strSQL.AppendLine("INSERT INTO TRAN_ORDER ")
            strSQL.AppendLine("(")
            strSQL.AppendLine("ORDER_NO,")
            strSQL.AppendLine("ORDER_DATE,")
            strSQL.AppendLine("ORDER_TIME,")
            strSQL.AppendLine("PATIENT_ANO,")
            strSQL.AppendLine("STUDY_DATE,")
            strSQL.AppendLine("STUDY_TIME,")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("BODY_PART,")
            strSQL.AppendLine("ORDER_COMMENT,")
            strSQL.AppendLine("STATE,")
            strSQL.AppendLine("STUDY_INSTANCE_UID,")
            strSQL.AppendLine("TERMINAL,")
            strSQL.AppendLine("INS_DATE,")
            strSQL.AppendLine("INS_TIME,")
            strSQL.AppendLine("INS_USER,")
            strSQL.AppendLine("STUDY_DIV,")
            strSQL.AppendLine("DEL_FLG ")
            strSQL.AppendLine(")")
            strSQL.AppendLine("VALUES ")
            strSQL.AppendLine("(")
            strSQL.AppendLine("'" & strOrderNo & "',")
            '           strSQL.AppendLine("'" & strDate(0).Trim & strDate(1).Trim & "',")
            '///過去日付が選択された場合、本日日付に置き換える ADD By Watanabe 2012.04.03
            'strSQL.AppendLine("'" & Me.dtStudyDate.Value.ToString("yyyyMMdd") & "',")
            strSQL.AppendLine("'" & strOrderDate & "',")
            '---END

            strSQL.AppendLine("'" & strDate(1).Trim & "',")
            strSQL.AppendLine(typPatient.PATIENT_ANO & ",")
            '///過去日付が選択された場合、本日日付に置き換える ADD By Watanabe 2012.04.03
            'strSQL.AppendLine("'" & Me.dtStudyDate.Value.ToString("yyyyMMdd") & "',")
            strSQL.AppendLine("'" & strOrderDate & "',")
            '---END

            strSQL.AppendLine("'" & strDate(1).Trim & "',")
            strSQL.AppendLine("'" & aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO & "',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'',")
            '///未来日オーダを受付済みとしない。UpDated By Watanabe 2013.02.14
            '---START 2013.02.14
            If Me.dtStudyDate.Value.ToString("yyyyMMdd") > Now.ToString("yyyyMMdd") Then
                strSQL.AppendLine("'0',")
            Else
                strSQL.AppendLine("'1',")
            End If
            'strSQL.AppendLine("'1',")
            '---END 2013.02.14
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'" & My.Settings.TERMINAL & "',")
            strSQL.AppendLine("'" & strDate(0).Trim & "',")
            strSQL.AppendLine("'" & strDate(1).Trim & "',")
            strSQL.AppendLine("'" & typUserInf.USER_ID & "',")
            strSQL.AppendLine("'" & strStudyDiv & "',")
            strSQL.AppendLine("'0'")
            strSQL.AppendLine(")")

            '///SQL文発行
            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Function
#End Region

#Region "医事側DB参照(fncChkIjiDB)"
    '********************************************************************************
    '* 機能概要　：医事側DB参照
    '* 関数名称　：fncChkIjiDB
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/12/02
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncChkIjiDB(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim K2R As New clsKana2Ro
        Dim strWkPatient As String

        Try
            '///DB Connection
            Call fncDBConnect(strMsg)

            strWkPatient = CLng(Me.txtPatientID.Text)

            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine(" F001 AS PATIENT_ID,")
            strSQL.AppendLine(" ISNULL(F002,'') AS PATIENT_KANA_NM,")
            strSQL.AppendLine(" ISNULL(F003,'') AS PATIENT_KANJI_NM,")
            strSQL.AppendLine(" ISNULL(F004,0) AS PATIENT_SEX,")
            strSQL.AppendLine(" ISNULL(F005,'') AS PATIENT_BIRTH ")
            strSQL.AppendLine("FROM TTID01 ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("F001 = '" & strWkPatient.PadLeft(Me.txtPatientID.MaxLength) & "' ")
            strSQL.AppendLine("ORDER BY UPDDT DESC ")

            '///SQL文発行
            dsData = fncAdptSQL(objConnect_IjiDB, strSQL.ToString, strMsg)

            '///ﾃﾞｰﾀ未存在時
            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            '///取得ﾃﾞｰﾀ構造体退避
            With typIjiPatient
                .PATIENT_ID = dsData.Tables(0).Rows(0).Item("PATIENT_ID")
                .PATIENT_ID = .PATIENT_ID.Trim
                .PATIENT_KANA = dsData.Tables(0).Rows(0).Item("PATIENT_KANA_NM")
                .PATIENT_EIJI = StrConv(K2R.kana2ro(StrConv(.PATIENT_KANA, VbStrConv.Narrow)), VbStrConv.Uppercase)
                .PATIENT_KANJI = dsData.Tables(0).Rows(0).Item("PATIENT_KANJI_NM")
                .PATIENT_SEX = dsData.Tables(0).Rows(0).Item("PATIENT_SEX")
                .PATIENT_BIRTH = dsData.Tables(0).Rows(0).Item("PATIENT_BIRTH")
            End With

            '///DB DisConnect
            Call fncDBDisConnect(strMsg)

            Return RET_NORMAL

        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog(strMsg, 1)
            Return RET_ERROR
        End Try
    End Function
#End Region

#Region "患者情報登録(fncInsPatient)"
    '********************************************************************************
    '* 機能概要　：患者情報登録
    '* 関数名称　：fncInsPatient
    '* 引　数　　：strMsg　・・・ｴﾗｰﾒｯｾｰｼﾞ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2013/01/10
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncInsPatient(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer = 0
        Dim strDate() As String
        Dim strWork As String = vbNullString

        Try
            strWork = fncGetServerDate(2, strMsg)
            strDate = strWork.Split(",")

            strSQL.Clear()
            strSQL.AppendLine("INSERT INTO TRAN_PATIENT ")
            strSQL.AppendLine("(")
            strSQL.AppendLine("PATIENT_ID,")
            strSQL.AppendLine("PATIENT_KANA,")
            strSQL.AppendLine("PATIENT_KANJI,")
            strSQL.AppendLine("PATIENT_EIJI,")
            strSQL.AppendLine("PATIENT_BIRTH,")
            strSQL.AppendLine("PATIENT_SEX,")
            strSQL.AppendLine("INS_USER,")
            strSQL.AppendLine("INS_DATE,")
            strSQL.AppendLine("INS_TIME,")
            strSQL.AppendLine("DEL_FLG ")
            strSQL.AppendLine(") ")
            strSQL.AppendLine("VALUES ")
            strSQL.AppendLine("(")
            strSQL.AppendLine("'" & typIjiPatient.PATIENT_ID & "',")
            strSQL.AppendLine("'" & typIjiPatient.PATIENT_KANA & "',")
            strSQL.AppendLine("'" & typIjiPatient.PATIENT_KANJI & "',")
            strSQL.AppendLine("'" & typIjiPatient.PATIENT_EIJI & "',")
            strSQL.AppendLine("'" & typIjiPatient.PATIENT_BIRTH.Replace("/", "") & "',")
            strSQL.AppendLine("'" & typIjiPatient.PATIENT_SEX & "',")
            strSQL.AppendLine("'PRIM_MWM_FORM',")
            strSQL.AppendLine("'" & strDate(0).Trim & "',")
            strSQL.AppendLine("'" & strDate(1).Trim & "',")
            strSQL.AppendLine("'0'")
            strSQL.AppendLine(")")
            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("患者情報登録にてエラー:" & strMsg, 1)
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Function
#End Region

#Region "患者情報更新(fncUpdPatient)"
    '********************************************************************************
    '* 機能概要　：患者情報更新
    '* 関数名称　：fncUpdPatient
    '* 引　数　　：strMsg　・・・ｴﾗｰﾒｯｾｰｼﾞ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2013/01/10
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncUpdPatient(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer = 0
        Dim strDate() As String
        Dim strWork As String = vbNullString

        Try
            strWork = fncGetServerDate(2, strMsg)
            strDate = strWork.Split(",")

            strSQL.Clear()
            strSQL.AppendLine("UPDATE TRAN_PATIENT ")
            strSQL.AppendLine("SET PATIENT_KANA = '" & typIjiPatient.PATIENT_KANA & "',")
            strSQL.AppendLine("PATIENT_KANJI = '" & typIjiPatient.PATIENT_KANJI & "',")
            strSQL.AppendLine("PATIENT_EIJI = '" & typIjiPatient.PATIENT_EIJI & "',")
            strSQL.AppendLine("PATIENT_BIRTH = '" & typIjiPatient.PATIENT_BIRTH.Replace("/", "") & "',")
            strSQL.AppendLine("PATIENT_SEX = '" & typIjiPatient.PATIENT_SEX & "',")
            strSQL.AppendLine("BEFOR_KANA = '" & typPatient.PATIENT_KANA & "',")
            strSQL.AppendLine("BEFOR_KANJI = '" & typPatient.PATIENT_KANJI & "',")
            strSQL.AppendLine("BEFOR_EIJI = '" & typPatient.PATIENT_EIJI & "',")
            strSQL.AppendLine("BEFOR_BIRTH = '" & typPatient.PATIENT_BIRTH & "',")
            strSQL.AppendLine("BEFOR_SEX = '" & typPatient.PATIENT_SEX & "',")
            strSQL.AppendLine("UPD_USER = 'PRIM_MWM_FORM',")
            strSQL.AppendLine("UPD_DATE = '" & strDate(0).Trim & "',")
            strSQL.AppendLine("UPD_TIME = '" & strDate(1).Trim & "' ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("PATIENT_ANO = " & typPatient.PATIENT_ANO)
            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("患者情報更新にてエラー：" & strMsg, 1)
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Function
#End Region

#Region "患者情報取得(fncGetPatient)"
    Private Function fncGetPatient(ByVal strPatientAno As String, ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine(" PATIENT_ANO,")
            strSQL.AppendLine(" KANJA_NO,")
            strSQL.AppendLine(" KANJI_NAME,")
            strSQL.AppendLine(" KANA_NAME,")
            strSQL.AppendLine(" ISNULL(EIJI_NAME,'') AS EIJI_NAME,")
            strSQL.AppendLine(" SEIBETSU,")
            strSQL.AppendLine(" SEIBETSU_NAME,")
            strSQL.AppendLine(" SHINCHO,")
            strSQL.AppendLine(" TAIJYU,")
            strSQL.AppendLine(" BIRTH_DATE,")
            strSQL.AppendLine(" BLOOD_TYPE_ABO,")
            strSQL.AppendLine(" BLOOD_TYPE_RH,")
            strSQL.AppendLine(" NINSHIN_UMU,")
            strSQL.AppendLine(" BUNBEN_YOTEI_DATE,")
            strSQL.AppendLine(" NINSHIN_SHUSU,")
            strSQL.AppendLine(" HOKOU,")
            strSQL.AppendLine(" HOKOU_NAME,")
            strSQL.AppendLine(" ANSEIDO,")
            strSQL.AppendLine(" ANSEIDO_NAME,")
            strSQL.AppendLine(" KANGODO ")
            strSQL.AppendLine("FROM TRAN_PATIENT ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine(" PATIENT_ANO = " & strPatientAno & " ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            With objPatient
                .PATIENT_ANO = dsData.Tables(0).Rows(0).Item("PATIENT_ANO")
                .KANJA_NO = dsData.Tables(0).Rows(0).Item("KANJA_NO")
                .KANJI_NAME = dsData.Tables(0).Rows(0).Item("KANJI_NAME")
                .KANA_NAME = dsData.Tables(0).Rows(0).Item("KANA_NAME")
                .EIJI_NAME = dsData.Tables(0).Rows(0).Item("EIJI_NAME")
                .SEIBETSU = dsData.Tables(0).Rows(0).Item("SEIBETSU")
                .SEIBETSU_NAME = dsData.Tables(0).Rows(0).Item("SEIBETSU_NAME")
                .BIRTH_DATE = dsData.Tables(0).Rows(0).Item("BIRTH_DATE")
                .SHINCHO = dsData.Tables(0).Rows(0).Item("SHINCHO")
                .TAIJYU = dsData.Tables(0).Rows(0).Item("TAIJYU")
                .BLOOD_TYPE_ABO = dsData.Tables(0).Rows(0).Item("BLOOD_TYPE_ABO")
                .BLOOD_TYPE_RH = dsData.Tables(0).Rows(0).Item("BLOOD_TYPE_RH")
                .NINSHIN_UMU = dsData.Tables(0).Rows(0).Item("NINSHIN_UMU")
                .BUNBEN_YOTEI_DATE = dsData.Tables(0).Rows(0).Item("BUNBEN_YOTEI_DATE")
                .NINSHIN_SHUSU = dsData.Tables(0).Rows(0).Item("NINSHIN_SHUSU")
                .HOKOU = dsData.Tables(0).Rows(0).Item("HOKOU")
                .HOKOU_NAME = dsData.Tables(0).Rows(0).Item("HOKOU_NAME")
                .ANSEIDO = dsData.Tables(0).Rows(0).Item("ANSEIDO")
                .ANSEIDO_NAME = dsData.Tables(0).Rows(0).Item("ANSEIDO_NAME")
                .KANGODO = dsData.Tables(0).Rows(0).Item("KANGODO")
            End With


            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
        End Try
    End Function
#End Region

#Region "部位情報取得(fncGetOrderPart)"
    Private Function fncGetOrderPart(ByRef typOrder As TYPE_ORDER, ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim lngBodyPartCount As Long = 0
        Dim lngCounter As Long = 0

        Try
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine(" KOUMKU_HIMBAN,")
            strSQL.AppendLine(" KOUMKU_NAME ")
            strSQL.AppendLine("FROM ")

            Select Case typOrder.MODALITY_CD
                Case "CR"
                    strSQL.AppendLine("TRAN_ORDER_XRAY_BUI_CR ")
                Case "CT"
                    strSQL.AppendLine("TRAN_ORDER_XRAY_BUI_CT ")
                Case "RF"
                    strSQL.AppendLine("TRAN_ORDER_XRAY_BUI_RF ")
                Case "ES"
                    strSQL.AppendLine("TRAN_ORDER_ES_DIV ")
                Case "US"
                    strSQL.AppendLine("TRAN_ORDER_US_DIV ")
                Case Else
                    Return RET_NORMAL
            End Select

            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine(" ORDER_ANO = " & typOrder.ORDER_ANO & " ")
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND
            lngBodyPartCount = dsData.Tables(0).Rows.Count

            For lngCounter = 0 To lngBodyPartCount - 1
                typOrder.BODY_PART &= dsData.Tables(0).Rows(lngCounter).Item("KOUMKU_NAME") & ","
            Next lngCounter
            typOrder.BODY_PART = typOrder.BODY_PART.Substring(0, typOrder.BODY_PART.Length - 1)

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("部位情報取得(fncGetOrderPart) エラー：" & strMsg, 1)
            Return RET_ERROR
        Finally
            strSQL.Clear()
            dsData.Dispose()
            dsData = Nothing
        End Try
    End Function
#End Region

#Region "カードリーダ情報監視"
    Private Sub tmrProc_Tick(sender As System.Object, e As System.EventArgs) Handles tmrProc.Tick
        tmrProc.Enabled = False
        Dim lngCounter As Long = 0
        Dim strFiles As String() = System.IO.Directory.GetFiles( _
            My.Settings.OUT_PATH, "*.txt", System.IO.SearchOption.TopDirectoryOnly)
        Dim strGetData As String = vbNullString
        Dim strWork() As String
        Dim k2r As New clsKana2Ro
        Dim strEiji As String = vbNullString
        Dim lngCounter2 As Long = 0
        Dim bolMatchFlg As Boolean = False
        Dim strPatientID As String = vbNullString
        Dim strState As String = vbNullString
        Dim strPatientCard As String = vbNullString

        '///各種タイマーのOFF ADD By Watanabe 2014.05.19
        Timer1.Enabled = False
        tmrProc.Enabled = False

        '///取得ファイルのデータを読込む
        For lngCounter = 0 To strFiles.Length - 1
            Dim objsw As New System.IO.StreamReader(strFiles(lngCounter), _
                                        System.Text.Encoding.GetEncoding(932))
            strGetData = objsw.ReadToEnd
            objsw.Close()
            strWork = strGetData.Split(",")
            g_Card = True

            With objPatient
                '.PATIENT_ID = Strings.Right(strWork(0).Trim, 6)
                .KANJA_NO = strWork(0).Trim

                Me.txtPatientID.Text = .KANJA_NO & Chr(Keys.Enter)
                '///改行コード付与していない患者IDを代入 2014.03.31 UpDated By Watanabe
                strPatientCard = .KANJA_NO.Trim

                'Call cmdEditPatient_Click(sender, e)
                For lngCounter2 = 0 To Me.dtgList.RowCount - 1
                    strPatientID = Me.dtgList.Rows(lngCounter2).Cells("COL_PATIENT_ID").Value
                    strState = Me.dtgList.Rows(lngCounter2).Cells("COL_STATE").Value

                    If (strPatientID.Trim = strPatientCard.Trim) And (strState.Trim = "未受付") Then

                        dtgList.Rows(lngCounter2).Selected = True
                        bolMatchFlg = True
                        lngSelRow = lngCounter2
                        Call cmdAccept.PerformClick()
                        '///1件のみ処理するよう処理後にループを抜ける ADD By Watanabe 2014.04.11
                        Exit For
                    End If
                Next lngCounter2

                System.IO.File.Delete(strFiles(lngCounter))
            End With

            If bolMatchFlg = False Then
                Call MsgBox("該当患者のデータは一覧にありませんでした。" & vbCrLf & "もう一度診察券をご確認下さい。", MsgBoxStyle.Exclamation)
                Call subOutLog("該当患者のデータは一覧にありませんでした。" & vbCrLf & "もう一度診察券をご確認下さい。", 2)
            End If
        Next lngCounter

        '///各種タイマーのON ADD By Watanabe 2014.05.19
        tmrProc.Enabled = True
        Timer1.Enabled = True

    End Sub
#End Region

#Region "subReadMain"
    '*******************************************************************
    '* 関数名称：subReadMain
    '* 機能概要：カードリーダ読取
    '* 引　数　：無
    '* 戻り値　：無
    '* 作成日　：2011/07/12
    '* 作成者　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 変更履歴：
    '*******************************************************************
    Private Sub subReadMain()
        Dim lngResult As Long
        Dim intReadFlg As Integer
        Dim lngTrakRet1 As Long
        Dim lngTrakRet2 As Long
        Dim lngTrakRet3 As Long
        Dim lngTrak1 As Long
        Dim lngTrak2 As Long
        Dim lngTrak3 As Long
        Dim bytData() As Byte
        Dim lngTrakLen As Long
        Dim lngCounter As Long
        Dim strData As String = vbNullString
        Dim strMsg As String = vbNullString
        Dim bytGetData() As Byte

        Try

            lngResult = GetStatus(intReadFlg)

            If intReadFlg > 0 Then

                Timer1.Enabled = False

                '///読取結果の取得
                lngResult = 0
                lngResult = GetCard_Result(lngTrakRet1, lngTrakRet2, lngTrakRet3, lngTrak1, lngTrak2, lngTrak3)

                '///取得結果を結合
                If lngTrakRet1 = 0 Then

                    Call subOutLog("診察券データ読取(Trak1)", 0)

                    ReDim bytData(lngTrak1)

                    lngResult = GetTrk1Data(lngTrakLen, bytData(0))

                    For lngCounter = 0 To bytData.Length - 1
                        strData &= Strings.Chr(bytData(lngCounter))
                    Next lngCounter

                    bytGetData = bytData
                End If

                If lngTrakRet2 = 0 Then

                    Call subOutLog("診察券データ読取(Trak2)", 0)

                    ReDim bytData(lngTrak2)

                    lngResult = GetTrk2Data(lngTrakLen, bytData(0))

                    For lngCounter = 0 To lngTrakLen - 1
                        strData &= Strings.Chr(bytData(lngCounter))
                    Next lngCounter
                End If

                If lngTrakRet3 = 0 Then
                    Call subOutLog("診察券データ読取(Trak3)", 0)

                    ReDim bytData(lngTrak3)

                    lngResult = GetTrk3Data(lngTrakLen, bytData(0))

                    For lngCounter = 0 To lngTrakLen - 1
                        strData &= Strings.Chr(bytData(lngCounter))
                    Next lngCounter
                End If

                Call subOutLog("診察券読取データ(" & strData & ")", 0)

                Call subSeparateData(strData, bytGetData)

                'g_Card = True

            End If

        Catch ex As Exception
            strMsg = ex.Message
            MsgBox(strMsg, MsgBoxStyle.Critical)
        End Try
    End Sub
#End Region

#Region "subSeparateData"
    '*******************************************************************
    '* 関数名称：subSeparateData
    '* 機能概要：データを変数に展開する
    '* 引　数　：strData　・・・展開データ
    '* 戻り値　：無
    '* 作成日　：2011/07/12
    '* 作成者　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 変更履歴：
    '*******************************************************************
    Private Sub subSeparateData(strData As String, bytbufWork() As Byte)
        Dim enc As System.Text.Encoding = System.Text.Encoding.GetEncoding("iso-2022-jp")
        Dim enc2 As System.Text.Encoding = System.Text.Encoding.GetEncoding("SHIFT-JIS")
        Dim bytWork() As Byte
        Dim strWork As String = String.Empty
        Dim lngPos As Long
        Dim strInName As New System.Text.StringBuilder
        Dim strOutName As New System.Text.StringBuilder
        Dim K2R As New clsKana2Ro
        Dim strOption As String = vbNullString
        Dim bytEditKanji() As Byte
        Dim strRet As New System.Text.StringBuilder

        '**********************
        '* 文字列をByteに変換 *
        '**********************
        bytWork = enc.GetBytes(strData)
        strWork = enc2.GetString(bytWork.ToArray, 0, bytWork.Length - 1)

        '<<<<<<<<<< 本番用 >>>>>>>>>>
        '///患者ID
        g_PatientID = enc.GetString(bytWork.ToArray, 1, 10)
        g_PatientID = CLng(g_PatientID)
        g_PatientID = g_PatientID.PadLeft(10)
        lngPos = 10

        '///患者カナ氏名
        lngPos += 1
        g_Kana = enc.GetString(bytWork.ToArray, lngPos, 20)
        g_Kana = g_Kana.Replace("・", Space(1))
        g_Kana = g_Kana.Trim
        lngPos += 20

        '///患者生年月日
        'lngPos += 1
        g_Birth = enc.GetString(bytWork.ToArray, lngPos, 7)
        lngPos += 7

        '///患者性別
        g_Sex = enc.GetString(bytWork.ToArray, lngPos, 1)
        lngPos += 1

        '/////漢字変換・・・・
        '///患者漢字氏名
        'lngPos += 2
        Dim i As Integer
        Erase bytEditKanji
        ReDim Preserve bytEditKanji(2)
        bytEditKanji(0) = CByte(Keys.Escape)
        bytEditKanji(1) = CByte(Asc("$"))
        bytEditKanji(2) = CByte(Asc("B"))

        Dim lngCounter As Long = 3
        For i = lngPos + 2 To bytbufWork.Length - 4
            ReDim Preserve bytEditKanji(lngCounter)

            If bytbufWork(i) <> CByte(Keys.Escape) And bytbufWork(i) <> 15 And bytbufWork(i) <> 32 Then
                bytEditKanji(lngCounter) = bytbufWork(i)
                lngCounter += 1
            End If
        Next
        ReDim Preserve bytEditKanji(bytEditKanji.Length + 3)

        bytEditKanji(bytEditKanji.Length - 3) = CByte(Keys.Escape)
        bytEditKanji(bytEditKanji.Length - 2) = CByte(Asc("("))
        bytEditKanji(bytEditKanji.Length - 1) = CByte(Asc("J"))

        g_Kanji = enc.GetString(bytEditKanji.ToArray, 0, bytEditKanji.Length - 8)

        strInName.Clear()
        strOutName.Clear()
        strInName.Append(g_Kanji)
        Call nkf32.NkfConvert(strOutName, strInName)
        g_Kanji = strOutName.ToString

        'With G_CARD_PATIENT
        With typPatient
            .PATIENT_ID = g_PatientID
            .PATIENT_KANA = g_Kana
            .PATIENT_KANJI = g_Kanji
            .PATIENT_EIJI = K2R.kana2ro(g_Kana)
            .PATIENT_EIJI = .PATIENT_EIJI.ToUpper
            '.PATIENT_NAME = .PATIENT_EIJI & "=" & .PATIENT_KANJI & "=" & .PATIENT_KANA
            .PATIENT_BIRTH = g_Birth
            .PATIENT_SEX = g_Sex
            '.NYUUGAI = ""
            '.WARD = ""
            '.BED_ROOM = ""
            '.COMMENT = ""
            .PATIENT_COMMENT = ""
        End With
        Me.txtPatientID.Text = g_PatientID
        Me.txtPatientKana.Text = g_Kana

        '///Enter押下時は患者情報登録処理(オーダ登録)を行う。
        Dim sender As Object
        Dim e As System.EventArgs
        Call cmdEditPatient_Click(sender, e)

    End Sub
#End Region

#Region "subPortOpen"
    '*******************************************************************
    '* 関数名称：subPortOpen
    '* 機能概要：USB PortのOpen
    '* 引　数　：無
    '* 戻り値　：無
    '* 作成日　：2011/07/12
    '* 作成者　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 変更履歴：
    '*******************************************************************
    Private Sub subPortOpen()
        Dim lngRet As Long                                       'DLLの戻り値格納
        Dim strMsg As String = vbNullString
        Try

            '///COM PortのOpen
            lngRet = USB_Device_Open
            Call subOutLog("CardReader Com Port Open", 0)

            '///接続カードリーダを指定
            Call Connection_Model_Setup(0)
            Call subOutLog("接続カードリーダ指定", 0)

            tmrProc.Enabled = True

        Catch ex As Exception
            strMsg = ex.Message
        End Try
    End Sub
#End Region

#Region "subPortClose"
    '*******************************************************************
    '* 関数名称：subPortClose
    '* 機能概要：USB PortのClose
    '* 引　数　：無
    '* 戻り値　：無
    '* 作成日　：2011/07/12
    '* 作成者　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 変更履歴：
    '*******************************************************************
    Private Sub subPortClose()
        tmrProc.Enabled = False

        '///COM Port Close
        Call USB_Device_Close()

        Call subOutLog("CardReader Com Port Close", 0)
    End Sub
#End Region

#Region "所定フォルダファイル名取得"
    '*********************************************************************
    '* 関数名　　：subGetAllFiles
    '* 関数概要　：所定フォルダのファイル名取得
    '* 引　数　　：strFolder ・・・対象フォルダPath
    '* 　　　　　：strPattern・・・取得ファイルのパターン
    '* 　　　　　：aryFiles　・・・ファイル名退避用
    '* 　　　　　：intProc　 ・・・処理フラグ(0=所定フォルダ内のみ、1=サブフォルダも含める)
    '* 戻り値　　：DataSet
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Sub subGetAllFiles(ByVal strFolder As String, _
        ByVal strPattern As String, ByRef aryFiles As ArrayList, ByVal intProc As Integer)

        '**********************************
        '* Folderにあるファイルを取得する *
        '**********************************
        Dim fs As String() = _
            System.IO.Directory.GetFiles(strFolder, strPattern)

        '***********************
        '* ArrayListに追加する *
        '***********************
        aryFiles.AddRange(fs)

        If intProc = 0 Then Exit Sub

        '**************************
        '* サブフォルダを取得する *
        '**************************
        Dim ds As String() = System.IO.Directory.GetDirectories(strFolder)

        '**************************************
        '* サブフォルダにあるファイルも調べる *
        '**************************************
        Dim d As String
        For Each d In ds
            subGetAllFiles(d, strPattern, aryFiles, 1)
        Next d

    End Sub
#End Region

#Region "オーダ単位でのModality情報を取得"
    ''' <summary>
    ''' オーダ単位でのModality情報を取得
    ''' </summary>
    ''' <param name="strTerminal">端末名</param>
    Private Sub subGetModalityInfo(ByVal strTerminal As String)
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim strMsg As String = vbNullString
        Dim intCounter As Integer = 0

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("ISNULL(MODALITY_CD,'') AS MODALITY_CD,")
            strSQL.AppendLine("ISNULL(SCP_AE,'') AS SCP_AE,")
            strSQL.AppendLine("ISNULL(SCU_AE,'') AS SCU_AE,")
            strSQL.AppendLine("ISNULL(SCU_IP,'') AS SCU_IP,")
            strSQL.AppendLine("SCU_PORT,")
            strSQL.AppendLine("ISNULL(HOME_PATH,'') AS HOME_PATH,")
            strSQL.AppendLine("ISNULL(CHARCTERSET,'') AS CHARCTERSET ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_MODALITY ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("TERMINAL = '" & strTerminal & "' ")
            strSQL.AppendLine("AND")
            strSQL.AppendLine("DEL_FLG = 'False'")
            strSQL.AppendLine("ORDER BY MODALITY_SORT")


            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            '///構造体クラスの初期化
            Erase aryModality

            For intCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryModality(intCounter)
                aryModality(intCounter) = New clsModality

                '///取得データを構造体クラスへ退避
                With aryModality(intCounter)
                    .MODALITY_NO = dsData.Tables(0).Rows(intCounter).Item("MODALITY_NO")
                    .MODALITY_CD = dsData.Tables(0).Rows(intCounter).Item("MODALITY_CD")
                    .SCP_AE = dsData.Tables(0).Rows(intCounter).Item("SCP_AE")
                    .SCU_AE = dsData.Tables(0).Rows(intCounter).Item("SCU_AE")
                    .SCU_IP = dsData.Tables(0).Rows(intCounter).Item("SCU_IP")
                    .SCU_PORT = dsData.Tables(0).Rows(intCounter).Item("SCU_PORT")
                    .CHARCTERSET = dsData.Tables(0).Rows(intCounter).Item("CHARCTERSET")
                    .HOME_PATH = dsData.Tables(0).Rows(intCounter).Item("HOME_PATH")
                End With
            Next intCounter
        Catch ex As Exception
            strMsg = ex.Message
        Finally
            dsData.Dispose()
            dsData = Nothing
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Sub

    Private Sub dtgList_ColumnSortModeChanged(sender As Object, e As DataGridViewColumnEventArgs) Handles dtgList.ColumnSortModeChanged
        g_e = e
    End Sub

    Private Sub cmdAccept_QueryAccessibilityHelp(sender As Object, e As QueryAccessibilityHelpEventArgs) Handles cmdAccept.QueryAccessibilityHelp

    End Sub

#End Region

#End Region

End Class
