﻿
Public Class frmInformation

    Private Sub frmInformation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim strHostName As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(strHostName)
        Dim hostadd As System.Net.IPAddress = iphe.AddressList(1)
        Dim mc As New System.Management.ManagementClass("Win32_OperatingSystem")
        Dim moc As System.Management.ManagementObjectCollection = mc.GetInstances()
        Dim mo As System.Management.ManagementObject

        For Each mo In moc
            Me.lblMem.Text = fGetFormatByteSize(mo("TotalVisibleMemorySize") * 1024)
        Next

        Dim baseKeyName As String = "HARDWARE\DESCRIPTION\System\CentralProcessor\0\"
        Dim rParentKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(baseKeyName)
        Dim keyValue As String = rParentKey.GetValue("ProcessorNameString")
        rParentKey.Close()


        Me.lblUserNM.Text = My.Settings.HOSPITAL


        Me.lblMachineNM.Text = Environment.MachineName
        Me.lblCPU.Text = keyValue
        Me.lblIP.Text = hostadd.ToString

    End Sub


    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked

        Me.Close()
        Me.Dispose()

    End Sub

    'バイト数（数値）を単位変換（バイト・KB・MB・GB 付きの文字列）するAPI関数の宣言
    Private Declare Function StrFormatByteSize64A Lib "SHLWAPI.DLL" ( _
        ByVal qdw As Int64, ByVal pszBuf As String, ByVal uiBufSize As Integer) As Integer
    'バイト数（数値）を自動単位変換（バイト・KB・MB・GB 付きの文字列）する自作関数
    Private Function fGetFormatByteSize(ByVal myFileSize As Decimal) As String
        Dim Ret As Integer
        Dim Buf As String
        Buf = New String(ControlChars.NullChar, 64)
        Ret = StrFormatByteSize64A(myFileSize, Buf, Buf.Length)
        Return Buf.TrimEnd(ControlChars.NullChar)
    End Function
End Class