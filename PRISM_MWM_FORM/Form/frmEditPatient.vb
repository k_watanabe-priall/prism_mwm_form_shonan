﻿Public Class frmEditPatient

#Region "定義"
    Private k2r As New clsKana2Ro
    Private aryModality() As clsModality
    Private clsResizer As New AutoResizer
    Public intInputError As Integer = 0
#End Region

#Region "Property"
    Public Property PATIENT_ID As String
        Get
            Return Me.txtPatientID.Text
        End Get
        Set(value As String)
            Me.txtPatientID.Text = value
        End Set
    End Property
    Public Property KENSHIN_PATIENT_ID As String
        Get
            Return Me.txtKenshinID.Text
        End Get
        Set(value As String)
            Me.txtKenshinID.Text = value
        End Set
    End Property

    Public Property PATIENT_KANJI As String
        Get
            Return Me.txtKanji.Text
        End Get
        Set(value As String)
            Me.txtKanji.Text = value
        End Set
    End Property
    Public Property PATIENT_KANA As String
        Get
            Return Me.txtKana.Text
        End Get
        Set(value As String)
            Me.txtKana.Text = value
        End Set
    End Property
    Public Property PATIENT_EIJI As String
        Get
            Return Me.txtEiji.Text
        End Get
        Set(value As String)
            Me.txtEiji.Text = value
        End Set
    End Property
    Public Property PATIENT_BIRTH As String
        Get
            Return Me.txtBirth.Text
        End Get
        Set(value As String)
            Me.txtBirth.Text = value
        End Set
    End Property
    Public Property PATIENT_SEX As String
        Get
            Select Case Me.cmbSex.SelectedIndex
                Case 0
                    Return 1
                Case 2
                    Return 2
                Case 3
                    Return 9
                Case Else
                    Return 0
            End Select
            '            Return Me.cmbSex.SelectedIndex
        End Get
        Set(value As String)
            If value = "" Then value = -1
            Me.txtSex.Text = value
            'If value = "9" Then
            '    Me.cmbSex.SelectedIndex = CInt(value)
            'End If
        End Set
    End Property
    Public Property PATIENT_COMMENT As String
        Get
            Return Me.txtPatientComment.Text
        End Get
        Set(value As String)
            Me.txtPatientComment.Text = value
        End Set
    End Property
    Public Property PATIENT_ANO As String
        Get
            Return Me.txtPatientAno.Text
        End Get
        Set(value As String)
            Me.txtPatientAno.Text = value
        End Set
    End Property
    Public Property SELECTED_MODALITY As String
        Get
            Return Me.txtModality.Text
        End Get
        Set(value As String)
            Me.txtModality.Text = value
        End Set
    End Property
    Public Property UPD_FLG As String
        Get
            Return Me.txtUpdFlg.Text
        End Get
        Set(value As String)
            Me.txtUpdFlg.Text = value
            If value = "1" Then
                Me.cmdAddOrder.Enabled = False
                Me.cmdEdit.Text = "患者情報更新"
            Else
                Me.cmdAddOrder.Enabled = True
                Me.cmdEdit.Text = "患者情報のみ登録"
            End If
        End Set
    End Property
    Public Property STUDY_DATE As String
        Get
            Return Me.txtStudyDate.Text
        End Get
        Set(value As String)
            Me.txtStudyDate.Text = value
        End Set
    End Property
    Public Property STUDY_DIV As Integer
        Get
            If Me.txtStudyDiv.Text = "" Then
                Return 0
            End If
            Return Me.txtStudyDiv.Text
        End Get
        Set(value As Integer)
            Me.txtStudyDiv.Text = value
        End Set
    End Property
    Public Property CANCEL_FLG As String
        Get
            Return Me.txtCancel.Text
        End Get
        Set(value As String)
            Me.txtCancel.Text = value
        End Set
    End Property

#End Region

#Region "FormEvents"

#Region "FormLoad"
    Private Sub frmEditPatient_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim strMsg As String = vbNullString

        '///現在設定されているフォントをデフォルトとして退避
        G_FONT = My.Settings.FORM_FONT

        '///フォーム及び各コントロールの初期情報退避
        clsResizer = New AutoResizer(Me)

        Me.Width = My.Settings.FORM_EDIT_PATIENT_WIDTH
        Me.Height = My.Settings.FORM_EDIT_PATIENT_HEIGHT

        '///モダリティマスタ取得
        If fncGetModality(strMsg) = RET_ERROR Then
            MsgBox(strMsg & Space(1) & "FormLoad", MsgBoxStyle.Critical, "画面初期処理")
            Exit Sub
        End If

        '///画面初期処理
        Call subFormInit()

    End Sub

#End Region

#Region "FormResize"
    Private Sub frmEditPatient_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        '///フォームと各コントロールのリサイズ
        clsResizer.subResize()
    End Sub
#End Region

#Region "FormResizeEnd"
    Private Sub frmEditPatient_ResizeEnd(sender As Object, e As System.EventArgs) Handles Me.ResizeEnd
        My.Settings.FORM_EDIT_PATIENT_HEIGHT = Me.Height
        My.Settings.FORM_EDIT_PATIENT_WIDTH = Me.Width
        My.Settings.Save()
    End Sub
#End Region

#End Region

#Region "ControlEvents"

#Region "登録ボタン押下"
    Private Sub cmdEdit_Click(sender As System.Object, e As System.EventArgs) Handles cmdEdit.Click
        Dim strMsg As String = vbNullString

        '////////// 入力チェック //////////
        '///患者ID
        If Me.txtPatientID.Text = "" Then
            intInputError = 1
            MsgBox("患者IDが未入力です。" & vbCrLf & "入力して下さい。", vbExclamation, "入力チェック")
            Me.txtPatientID.Focus()
            Exit Sub
        End If
        If STUDY_DIV = 1 Then
            If Me.txtKenshinID.Text = "" Then
                intInputError = 1
                MsgBox("健診IDが未入力です。" & vbCrLf & "入力して下さい。", vbExclamation, "入力チェック")
                Me.txtKenshinID.Focus()
                Exit Sub
            End If
        End If
        ''///患者漢字氏名
        'If Me.txtKanji.Text = "" Then
        '    MsgBox("漢字氏名が未入力です。" & vbCrLf & "入力して下さい。", vbExclamation, "入力チェック")
        '    Me.txtKanji.Focus()
        '    Exit Sub
        'End If
        '///カナ氏名
        If Me.txtKana.Text = "" Then
            intInputError = 1
            MsgBox("カナ氏名が未入力です。" & vbCrLf & "入力して下さい。", vbExclamation, "入力チェック")
            Me.txtKana.Focus()
            Exit Sub
        End If
        '///英字氏名
        If Me.txtEiji.Text = "" Then
            intInputError = 1
            MsgBox("英字氏名が未入力です。" & vbCrLf & "入力して下さい。", vbExclamation, "入力チェック")
            Me.txtEiji.Focus()
            Exit Sub
        End If
        '///性別
        If Me.cmbSex.SelectedIndex = -1 Then
            intInputError = 1
            MsgBox("性別が未選択です。" & vbCrLf & "選択して下さい。", vbExclamation, "入力チェック")
            Me.cmbSex.Focus()
            Exit Sub
        End If
        '///生年月日
        If Me.txtBirth.Text = "" Then
            intInputError = 1
            MsgBox("生年月日が未入力です。" & vbCrLf & "入力して下さい。", vbExclamation, "入力チェック")
            Me.txtBirth.Focus()
            Exit Sub
        Else
            If Me.txtBirth.TextLength = 8 Then
                Dim strBirth As String = Me.txtBirth.Text.Substring(0, 4) & "/" & Me.txtBirth.Text.Substring(4, 2) & "/" & Me.txtBirth.Text.Substring(6, 2)
                If IsDate(strBirth) = False Then
                    intInputError = 1
                    MsgBox("入力された生年月日に妥当性がありません。" & vbCrLf & "再度入力して下さい。", vbExclamation, "入力チェック")
                    Me.txtBirth.Focus()
                    Exit Sub
                End If
            ElseIf Me.txtBirth.TextLength = 10 Then
                If IsDate(Me.txtBirth.Text) = False Then
                    intInputError = 1
                    MsgBox("入力された生年月日に妥当性がありません。" & vbCrLf & "再度入力して下さい。", vbExclamation, "入力チェック")
                    Me.txtBirth.Focus()
                    Exit Sub
                End If
            Else
                intInputError = 1
                MsgBox("入力された生年月日に妥当性がありません。" & vbCrLf & "再度入力して下さい。", vbExclamation, "入力チェック")
                Me.txtBirth.Focus()
                Exit Sub
            End If
        End If

        Dim strBirth2 As String
        If Me.txtBirth.TextLength = 8 Then
            strBirth2 = Me.txtBirth.Text.Substring(0, 4) & "/" & Me.txtBirth.Text.Substring(4, 2) & "/" & Me.txtBirth.Text.Substring(6, 2)
        Else
            strBirth2 = Me.txtBirth.Text.Trim
        End If
        '///確認メッセージ表示
        If Me.txtUpdFlg.Text = "" Or Me.txtUpdFlg.Text = "0" Then
            If MsgBox("以下の内容で患者情報を登録します。" & vbCrLf & "宜しいですか。" & _
                      vbCrLf & _
                      "--------------------------------------------------" & vbCrLf & _
                      "患者ID" & vbTab & vbTab & Me.txtPatientID.Text.Trim & vbCrLf & _
                      "健診ID" & vbTab & vbTab & Me.txtKenshinID.Text.Trim & vbCrLf & _
                      "患者カナ氏名" & vbTab & Me.txtKana.Text.Trim & vbCrLf & _
                      "患者英字氏名" & vbTab & Me.txtEiji.Text.Trim & vbCrLf & _
                      "性別" & vbTab & vbTab & Me.cmbSex.SelectedItem & vbCrLf & _
                      "生年月日" & vbTab & vbTab & strBirth2 & vbCrLf & _
                      "患者コメント" & vbTab & Me.txtPatientComment.Text, _
                      MsgBoxStyle.Question + MsgBoxStyle.YesNo, "患者情報登録") = MsgBoxResult.No Then
                Exit Sub
            End If

            '///患者情報登録処理
            If fncInsPatient(strMsg) = RET_ERROR Then
                MsgBox(strMsg, MsgBoxStyle.Critical, "患者情報登録処理")
                Me.cmdAddOrder.Enabled = False
                Exit Sub
            Else
                MsgBox("患者情報を正常に登録しました。", MsgBoxStyle.Information, "患者情報登録処理")
                'Me.cmdEdit.Enabled = False
                Me.cmdAddOrder.Enabled = True
                Me.cmdClose.Enabled = True
            End If
        Else
            If MsgBox("以下の内容で患者情報を更新します。" & vbCrLf & "宜しいですか。" & _
                     vbCrLf & _
                     "--------------------------------------------------" & vbCrLf & _
                     "患者ID" & vbTab & vbTab & Me.txtPatientID.Text.Trim & vbCrLf & _
                     "健診ID" & vbTab & vbTab & Me.txtKenshinID.Text.Trim & vbCrLf & _
                     "患者カナ氏名" & vbTab & Me.txtKana.Text.Trim & vbCrLf & _
                     "患者英字氏名" & vbTab & Me.txtEiji.Text.Trim & vbCrLf & _
                     "性別" & vbTab & vbTab & Me.cmbSex.SelectedItem & vbCrLf & _
                     "生年月日" & vbTab & vbTab & strBirth2 & vbCrLf & _
                     "患者コメント" & vbTab & Me.txtPatientComment.Text, _
                     MsgBoxStyle.Question + MsgBoxStyle.YesNo, "患者情報更新") = MsgBoxResult.No Then
                Exit Sub
            End If
            '///患者情報更新処理
            Dim intRet As Integer
            intRet = fncUpdPatient(strMsg)
            Select Case intRet
                Case RET_ERROR
                    MsgBox(strMsg, MsgBoxStyle.Critical, "患者情報登録処理")
                    Me.cmdAddOrder.Enabled = False
                    Exit Sub
                Case RET_NOTFOUND
                    '///患者情報登録処理
                    If fncInsPatient(strMsg) = RET_ERROR Then
                        MsgBox(strMsg, MsgBoxStyle.Critical, "患者情報登録処理")
                        Me.cmdAddOrder.Enabled = False
                        Exit Sub
                    Else
                        'MsgBox("患者情報を正常に登録しました。", MsgBoxStyle.Information, "患者情報登録処理")
                        Me.cmdEdit.Enabled = False
                        Me.cmdAddOrder.Enabled = True
                    End If
                Case RET_NORMAL
                    'MsgBox("患者情報を正常に更新しました。", MsgBoxStyle.Information, "患者情報更新処理")
                    'Me.cmdEdit.Enabled = False
                    Me.cmdAddOrder.Enabled = True
                    Me.cmdClose.Enabled = True
            End Select

            Me.cmdClose.Enabled = True

        End If
    End Sub
#End Region

#Region "閉じるボタン押下"
    Private Sub cmdClose_Click(sender As System.Object, e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub
#End Region

#Region "カナ入力エリアLostFocus"
    Private Sub txtKana_LostFocus(sender As Object, e As System.EventArgs) Handles txtKana.LostFocus
        Me.txtEiji.Text = StrConv(k2r.kana2ro(StrConv(txtKana.Text, VbStrConv.Narrow)), VbStrConv.Uppercase)
    End Sub
#End Region

#Region "生年月日 KeyPress"
    Private Sub txtBirth_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtBirth.KeyPress
        If (e.KeyChar < "0"c Or e.KeyChar > "9"c) Then
            If e.KeyChar <> vbBack And e.KeyChar <> Chr(13) Then
                e.Handled = True
            End If
        End If

    End Sub
#End Region

#Region "オーダ情報登録ボタン押下"
    Private Sub cmdAddOrder_Click(sender As System.Object, e As System.EventArgs) Handles cmdAddOrder.Click
        Dim strMsg As String = vbNullString
        'If Me.cmbModality.SelectedIndex = -1 Then
        '    MsgBox("撮影室を選択して下さい。", MsgBoxStyle.Exclamation, "入力チェック")
        '    Exit Sub
        'End If
        'frmEditOrder.PATIENT_ID = Me.txtPatientID.Text.Trim
        'frmEditOrder.SELECTED_MODALITY = aryModality(Me.cmbModality.SelectedIndex).MODALITY_CD
        'frmEditOrder.SELECTED_MODALITY_NO = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO
        'frmEditOrder.ORDER_ANO = ""
        'frmEditOrder.ShowDialog()
        intInputError = 0

        Call cmdEdit_Click(sender, e)
        If intInputError = 0 Then
            If fncGetPatientAno(Me.txtPatientID.Text, "0", strMsg) <> RET_NORMAL Then

            End If
            If fncInsOrder(strMsg) = RET_ERROR Then
                Call MsgBox(strMsg & Space(1) & "オーダ情報登録", MsgBoxStyle.Critical, "オーダ情報登録")
                Exit Sub
            End If
        End If
    End Sub
#End Region

#Region "モダリティ変更時"
    Private Sub cmbModality_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbModality.SelectedIndexChanged
        Me.txtModality.Text = aryModality(Me.cmbModality.SelectedIndex).MODALITY_CD
        Me.txtModalityNo.Text = aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO
    End Sub
#End Region

#Region "患者ID KeyDown"
    Private Sub txtPatientID_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtPatientID.KeyDown
        If e.KeyCode = Keys.Enter Then
            '///職員(先頭1桁"K")以外は先頭0埋め
            If Me.txtPatientID.Text <> "" And Me.txtPatientID.TextLength > 2 And IsNumeric(Me.txtPatientID.Text) = False Then
                If Not Me.txtPatientID.Text.Substring(0, 1).ToUpper = "K" And _
                   Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "SA" And _
                   Not Me.txtPatientID.Text.Substring(0, 2).ToUpper = "TE" Then
                    Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength, "0"c)
                End If
            ElseIf Me.txtPatientID.Text <> "" And IsNumeric(Me.txtPatientID.Text) = True Then
                Me.txtPatientID.Text = Me.txtPatientID.Text.PadLeft(Me.txtPatientID.MaxLength, "0"c)
            Else
                Me.txtPatientID.Text = Me.txtPatientID.Text.ToUpper
            End If
        End If
    End Sub
#End Region

#Region "患者ID KeyPress"
    Private Sub txtPatientID_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPatientID.KeyPress
        'If (e.KeyChar < "0"c Or e.KeyChar > "9"c) Then
        '    If e.KeyChar <> vbBack And e.KeyChar <> Chr(13) Then
        '        If e.KeyChar = "k"c Or e.KeyChar = "K"c Then Exit Sub
        '        e.Handled = True
        '    End If
        'End If

    End Sub
#End Region

#Region "生年月日フォーカス移動時"
    Private Sub txtBirth_LostFocus(sender As Object, e As System.EventArgs) Handles txtBirth.LostFocus
        Dim lngBirth As Long
        If PATIENT_BIRTH <> "" Then
            If PATIENT_BIRTH = "" Then
                Me.lblAge.Text = ""
                Exit Sub
            ElseIf PATIENT_BIRTH.Length <> 8 Then
                Me.lblAge.Text = ""
                Exit Sub
            ElseIf PATIENT_BIRTH.Length = 8 Then
                lngBirth = CLng(PATIENT_BIRTH)
            Else
                Me.lblAge.Text = ""
                Exit Sub
            End If
        Else
            If Me.txtBirth.Text = "" Then
                Me.lblAge.Text = ""
                Exit Sub
            ElseIf Me.txtBirth.TextLength <> 8 Then
                Me.lblAge.Text = ""
                Exit Sub
            ElseIf Me.txtBirth.TextLength = 8 Then
                lngBirth = CLng(Me.txtBirth.Text)
            Else
                Me.lblAge.Text = ""
                Exit Sub
            End If
        End If
        Dim strAge As String = (CLng(Now.ToString("yyyyMMdd")) - CLng(PATIENT_BIRTH)) / 10000
        Dim intPos As Integer = InStr(strAge, ".")
        If intPos = 0 Then
            strAge = strAge
        Else
            strAge = strAge.Substring(0, intPos - 1)
        End If
        Me.lblAge.Text = strAge

    End Sub
#End Region

#End Region

#Region "Sub Routine"

#Region "患者情報登録(fncInsPatient)"
    '********************************************************************************
    '* 機能概要　：患者情報登録
    '* 関数名称　：fncInsPatient
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・0=正常、-1=データ無、-9=エラー
    '* 作成日　　：2011/02/09
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncInsPatient(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer = 0
        Dim strWork As String
        Dim strDate As String()

        Try
            '///サーバ日付の取得
            strWork = fncGetServerDate(2, strMsg)
            strDate = strWork.Split(",")

            Dim strSex As String = vbNullString

            Select Case Me.cmbSex.SelectedIndex
                Case 0
                    strSex = 1
                Case 1
                    strSex = 2
                Case 2
                    strSex = 9
            End Select
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("INSERT INTO TRAN_PATIENT ")
            strSQL.AppendLine("(")
            strSQL.AppendLine("PATIENT_ID,")
            strSQL.AppendLine("KENSHIN_PATIENT_ID,")
            strSQL.AppendLine("PATIENT_KANA,")
            strSQL.AppendLine("PATIENT_KANJI,")
            strSQL.AppendLine("PATIENT_EIJI,")
            strSQL.AppendLine("PATIENT_BIRTH,")
            strSQL.AppendLine("PATIENT_SEX,")
            strSQL.AppendLine("BEFOR_KANA,")
            strSQL.AppendLine("BEFOR_KANJI,")
            strSQL.AppendLine("BEFOR_EIJI,")
            strSQL.AppendLine("BEFOR_BIRTH,")
            strSQL.AppendLine("BEFOR_SEX,")
            strSQL.AppendLine("PATIENT_COMMENT,")
            strSQL.AppendLine("INS_DATE,")
            strSQL.AppendLine("INS_TIME,")
            strSQL.AppendLine("INS_USER,")
            strSQL.AppendLine("DEL_FLG ")
            strSQL.AppendLine(")")
            strSQL.AppendLine("VALUES")
            strSQL.AppendLine("(")
            strSQL.AppendLine("'" & Me.txtPatientID.Text.Trim & "',")
            strSQL.AppendLine("'" & Me.txtKenshinID.Text.Trim & "',")
            strSQL.AppendLine("'" & StrConv(Me.txtKana.Text.Trim, VbStrConv.Narrow) & "',")
            strSQL.AppendLine("'" & StrConv(Me.txtKana.Text, VbStrConv.Wide) & "',")
            strSQL.AppendLine("'" & Me.txtEiji.Text.Trim & "',")
            strSQL.AppendLine("'" & Me.txtBirth.Text.Trim.Replace("/", "") & "',")
            strSQL.AppendLine("'" & strSex & "',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'" & Me.txtPatientComment.Text.Replace("'", "''") & "',")
            strSQL.AppendLine("'" & strDate(0) & "',")
            strSQL.AppendLine("'" & strDate(1) & "',")
            strSQL.AppendLine("'" & typUserInf.USER_ID & "',")
            strSQL.AppendLine("'0'")
            strSQL.AppendLine(")")

            '///SQL文発行
            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            Return RET_NORMAL

        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog(strMsg & Space(1) & "患者情報登録(fncInsPatient)", 1)
            Return RET_ERROR

        Finally
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Function
#End Region

#Region "患者情報更新(fncUpdPatient)"
    '********************************************************************************
    '* 機能概要　：患者情報更新
    '* 関数名称　：fncUpdPatient
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・0=正常、-1=データ無、-9=エラー
    '* 作成日　　：2011/02/09
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncUpdPatient(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim intRet As Integer = 0
        Dim strWork As String
        Dim strDate As String()

        Try
            '///サーバ日付の取得
            strWork = fncGetServerDate(2, strMsg)
            strDate = strWork.Split(",")

            '///存在チェック用SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("COUNT(PATIENT_ANO) AS CNT")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("TRAN_PATIENT ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("PATIENT_ANO = " & Me.txtPatientAno.Text & " ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables(0).Rows(0).Item("CNT") = 0 Then Return RET_NOTFOUND

            '///更新用SQL文生成
            Dim strSex As String = vbNullString

            Select Case Me.cmbSex.SelectedIndex
                Case 0
                    strSex = 1
                Case 1
                    strSex = 2
                Case 2
                    strSex = 9
            End Select
            strSQL.Clear()
            strSQL.AppendLine("UPDATE TRAN_PATIENT ")
            strSQL.AppendLine("SET ")
            strSQL.AppendLine("PATIENT_ID = '" & Me.txtPatientID.Text & "',")
            strSQL.AppendLine("KENSHIN_PATIENT_ID = '" & Me.txtKenshinID.Text & "',")
            strSQL.AppendLine("PATIENT_KANJI = '" & StrConv(Me.txtKana.Text, VbStrConv.Wide) & "',")
            strSQL.AppendLine("PATIENT_KANA = '" & Me.txtKana.Text & "',")
            strSQL.AppendLine("PATIENT_EIJI = '" & Me.txtEiji.Text & "',")
            strSQL.AppendLine("PATIENT_SEX = '" & strSex & "',")
            strSQL.AppendLine("PATIENT_BIRTH = '" & Me.txtBirth.Text.Replace("/", "") & "',")
            strSQL.AppendLine("PATIENT_COMMENT = '" & Me.txtPatientComment.Text.Replace("'", "''") & "',")
            strSQL.AppendLine("BEFOR_KANA = '" & Me.PATIENT_KANA & "',")
            strSQL.AppendLine("BEFOR_KANJI = '" & Me.PATIENT_KANJI & "',")
            strSQL.AppendLine("BEFOR_EIJI = '" & Me.PATIENT_EIJI & "',")
            strSQL.AppendLine("BEFOR_BIRTH = '" & Me.PATIENT_BIRTH.Replace("/", "") & "',")
            strSQL.AppendLine("BEFOR_SEX = '" & Me.PATIENT_SEX & "',")
            strSQL.AppendLine("UPD_DATE = '" & strDate(0).Trim & "',")
            strSQL.AppendLine("UPD_TIME = '" & strDate(1).Trim & "',")
            strSQL.AppendLine("UPD_USER = '" & typUserInf.USER_ID & "' ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("PATIENT_ANO = " & Me.txtPatientAno.Text & " ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")

            '///SQL発行
            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try
    End Function
#End Region

#Region "モダリティ情報取得(subGetModality)"
    '********************************************************************************
    '* 機能概要　：モダリティ情報取得
    '* 関数名称　：subGetModality
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/01/16
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetModality(ByRef strMsg As String, Optional intProc As Integer = 0) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try

            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("MODALITY_CD,")
            strSQL.AppendLine("MODALITY_NAME,")
            strSQL.AppendLine("TERMINAL,")
            strSQL.AppendLine("ROOM_NAME ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_MODALITY ")

            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            Dim lngCounter As Integer = 0
            Erase aryModality
            Me.cmbModality.Items.Clear()

            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryModality(lngCounter)

                aryModality(lngCounter) = New clsModality

                With aryModality(lngCounter)
                    .MODALITY_NO = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_NO")
                    .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")
                    .MODALITY_NAME = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_NAME")
                    .TERMINAL = dsData.Tables(0).Rows(lngCounter).Item("TERMINAL")
                    .ROOM_NAME = dsData.Tables(0).Rows(lngCounter).Item("ROOM_NAME")

                    Me.cmbModality.Items.Add(.ROOM_NAME)
                End With
            Next lngCounter

            Return RET_NORMAL
        Catch ex As Exception
            Call subOutLog(ex.Message & Space(1) & "モダリティ情報取得(subGetModality)", 1)
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try

    End Function

#End Region

#Region "画面初期処理(subFormInit)"
    '********************************************************************************
    '* 機能概要　：画面初期処理
    '* 関数名称　：subFormInit
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日　　：2012/01/20
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Sub subFormInit()

        Me.txtCancel.Text = "0"

        If Me.txtUpdFlg.Text <> "1" Then
            If PATIENT_ID = "" Then
                Me.txtPatientID.Text = vbNullString         '患者ID
            End If

            Me.txtKanji.Text = vbNullString             '漢字氏名
            If PATIENT_KANA = "" Then
                Me.txtKana.Text = vbNullString              'カナ氏名
            End If
            'Me.txtKana.Text = vbNullString              'カナ氏名
            Me.txtEiji.Text = vbNullString              '英字氏名
            Me.cmbSex.SelectedIndex = -1                '性別
            Me.txtBirth.Text = vbNullString             '生年月日
            Me.lblAge.Text = vbNullString
            Me.txtPatientComment.Text = vbNullString    '患者コメント
            'Me.txtModality.Text = vbNullString          'モダリティCD(隠しコントロール)
            'Me.txtModalityNo.Text = vbNullString        'モダリティNo(隠しコントロール)
            Me.cmdEdit.Enabled = True

        Else
            Me.txtPatientID.Text = PATIENT_ID
            Me.txtKanji.Text = PATIENT_KANJI
            Me.txtKana.Text = PATIENT_KANA
            Me.txtEiji.Text = PATIENT_EIJI
            Me.txtBirth.Text = PATIENT_BIRTH
            If Me.txtBirth.Text = "" Then
                Me.lblAge.Text = ""
            End If

            Dim sender As New Object
            Dim e As New EventArgs
            Call txtBirth_LostFocus(sender, e)
            Me.cmbSex.Text = Me.txtSex.Text
            'Select Case Me.txtSex.Text
            '    Case "1"
            '        Me.cmbSex.SelectedText = "男"
            '    Case "2"
            '        Me.cmbSex.SelectedText = "女"
            '    Case "9"
            '        Me.cmbSex.SelectedText = "不明"
            '    Case Else
            '        Me.cmbSex.SelectedIndex = -1
            'End Select
            Select Case Me.txtSex.Text
                Case "1", "M", "男"
                    Me.cmbSex.SelectedIndex = 0
                Case "2", "F", "女"
                    Me.cmbSex.SelectedIndex = 1
                Case "9", "O", "不明"
                    Me.cmbSex.SelectedIndex = 2
                Case Else
                    Me.cmbSex.SelectedIndex = -1
            End Select

            Me.txtUpdFlg.Text = UPD_FLG
            Me.txtPatientComment.Text = PATIENT_COMMENT
            Me.cmdEdit.Enabled = True
        End If

        If STUDY_DIV = 1 Then
            Me.txtKenshinID.Visible = False
        Else
            Me.txtKenshinID.Visible = False
        End If
        If Me.txtUpdFlg.Text = "0" Then
            Me.cmdClose.Enabled = False
        Else
            Me.cmdClose.Enabled = True
        End If
        Me.cmbModality.SelectedIndex = CInt(Me.txtModality.Text) - 1
    End Sub
#End Region

#Region "オーダ情報登録(受付済オーダの登録)(fncInsOrder)"
    '********************************************************************************
    '* 機能概要　：オーダ情報登録処理(受付済オーダの登録)
    '* 関数名称　：fncInsOrder
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・RET_NORMAL=正常、RET_NOTFOUND=データ無、RET_ERROR=異常終了
    '* 作成日　　：2012/03/01
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncInsOrder(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer
        Dim strWork As String = vbNullString
        Dim strDate As String()
        Dim dsData As New DataSet
        Dim strOrderNo As String = vbNullString

        Try
            strWork = fncGetServerDate(2, strMsg)
            strDate = strWork.Split(",")
            strOrderNo = strDate(0).Trim & strDate(1).Trim
            'If G_STUDY_DIV = 0 Then
            '    strOrderNo = "S" & strDate(0).Trim.Substring(2, 6) & strDate(1).Trim & aryModality(Me.cmbModality.SelectedIndex).MODALITY_CD
            'Else
            '    strOrderNo = "K" & strDate(0).Trim.Substring(2, 6) & strDate(1).Trim & aryModality(Me.cmbModality.SelectedIndex).MODALITY_CD
            'End If

            strSQL.Clear()
            strSQL.AppendLine("INSERT INTO TRAN_ORDER ")
            strSQL.AppendLine("(")
            strSQL.AppendLine("ORDER_NO,")
            strSQL.AppendLine("ORDER_DATE,")
            strSQL.AppendLine("ORDER_TIME,")
            strSQL.AppendLine("PATIENT_ANO,")
            strSQL.AppendLine("STUDY_DATE,")
            strSQL.AppendLine("STUDY_TIME,")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("BODY_PART,")
            strSQL.AppendLine("ORDER_COMMENT,")
            strSQL.AppendLine("STATE,")
            strSQL.AppendLine("STUDY_INSTANCE_UID,")
            strSQL.AppendLine("TERMINAL,")
            strSQL.AppendLine("INS_DATE,")
            strSQL.AppendLine("INS_TIME,")
            strSQL.AppendLine("INS_USER,")
            strSQL.AppendLine("DEL_FLG ")
            strSQL.AppendLine(")")
            strSQL.AppendLine("VALUES ")
            strSQL.AppendLine("(")
            strSQL.AppendLine("'" & strOrderNo & "',")
            strSQL.AppendLine("'" & Me.txtStudyDate.Text & "',")
            strSQL.AppendLine("'" & strDate(1).Trim & "',")
            strSQL.AppendLine(txtPatientAno.Text & ",")
            strSQL.AppendLine("'" & Me.txtStudyDate.Text & "',")
            strSQL.AppendLine("'" & strDate(1).Trim & "',")
            strSQL.AppendLine("'" & aryModality(Me.cmbModality.SelectedIndex).MODALITY_NO & "',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'0',")
            strSQL.AppendLine("'',")
            strSQL.AppendLine("'" & My.Settings.TERMINAL & "',")
            strSQL.AppendLine("'" & strDate(0).Trim & "',")
            strSQL.AppendLine("'" & strDate(1).Trim & "',")
            strSQL.AppendLine("'" & typUserInf.USER_ID & "',")
            strSQL.AppendLine("'0'")
            strSQL.AppendLine(")")

            '///SQL文発行
            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Function
#End Region

#Region "患者登録番号取得(fncGetPatientAno)"
    Private Function fncGetPatientAno(ByVal strPatientID As String, ByVal intProc As Integer, ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("PATIENT_ANO ")
            strSQL.AppendLine("FROM TRAN_PATIENT ")
            strSQL.AppendLine("WHERE ")

            If intProc = 0 Then
                strSQL.AppendLine("PATIENT_ID =  '" & Me.txtPatientID.Text & "' ")
            Else
                strSQL.AppendLine("KENSHIN_PATIENT_ID =  '" & Me.txtKenshinID.Text & "' ")
            End If

            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0'")

            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            txtPatientAno.Text = dsData.Tables(0).Rows(0).Item("PATIENT_ANO")

            Return RET_NORMAL
        Catch ex As Exception
            Return RET_ERROR
        Finally
        End Try
    End Function
#End Region

#End Region

    Private Sub cmdCancel_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancel.Click
        Me.txtCancel.Text = "1"
        Me.Close()
    End Sub
End Class