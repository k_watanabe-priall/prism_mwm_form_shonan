﻿Public Class frmSelectBodyPart

#Region "定義"
    Private aryModality() As clsModality
    Private aryBodyPart() As clsBodyPart
    Private clsResizer As New AutoResizer
#End Region

#Region "プロパティ"
    '///モダリティ
    Public Property SELECTED_MODALITY As String
        Get
            Return Me.txtModality.Text
        End Get
        Set(value As String)
            Me.txtModality.Text = value
        End Set
    End Property
#End Region

#Region "FormEvents"

#Region "FormLoad"
    Private Sub frmSelectBodyPart_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim strMsg As String = vbNullString

        '///現在設定されているフォントをデフォルトとして退避
        G_FONT = My.Settings.FORM_FONT

        '///フォーム及び各コントロールの初期情報退避
        clsResizer = New AutoResizer(Me)

        Me.Width = My.Settings.FORM_BODY_PART_WIDTH
        Me.Height = My.Settings.FORM_BODY_PART_HEGHT

        Me.dtgList.AllowUserToAddRows = False

        '///モダリティマスタ取得
        If fncGetModality(strMsg) = RET_ERROR Then
            MsgBox(strMsg & Space(1) & "FormLoad", MsgBoxStyle.Critical, "モダリティマスタ取得")
            Call subOutLog(strMsg & Space(1) & "frmSelectBodyPart.FormLoad", 1)
            End
        End If
    End Sub
#End Region

#Region "FormResize"
    Private Sub frmSelectBodyPart_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        '///フォームと各コントロールのリサイズ
        clsResizer.subResize()
    End Sub
#End Region

#Region "FormResizeEnd"
    Private Sub frmSelectBodyPart_ResizeEnd(sender As Object, e As System.EventArgs) Handles Me.ResizeEnd
        My.Settings.FORM_BODY_PART_HEGHT = Me.Height
        My.Settings.FORM_BODY_PART_WIDTH = Me.Width
        My.Settings.Save()
    End Sub
#End Region

#End Region

#Region "ControlEvent"

#Region "検査室名選択時"
    Private Sub cmbModality_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbModality.SelectedIndexChanged
        Dim strMsg As String = vbNullString
        Dim lngCounter As Long = 0
        Dim lngMstCounter As Long = 0

        'If My.Settings.TERMINAL = aryModality(Me.cmbModality.SelectedIndex).TERMINAL Then
        If aryBodyPartDetail IsNot Nothing Then
            'Me.chkLstBodyPart.Items.Clear()
            '///部位マスタ取得
            If fncGetMstBodyPart(strMsg) = RET_ERROR Then
                MsgBox(strMsg & Space(1) & "cmbModality_SelectedIndexChanged", MsgBoxStyle.Critical, "部位マスタ取得")
                Call subOutLog(strMsg & Space(1) & "cmbModality_SelectedIndexChanged", 1)
                Exit Sub
            End If

            For lngMstCounter = 0 To aryBodyPart.Length - 1
                For lngCounter = 0 To aryBodyPartDetail.Length - 1
                    If aryBodyPart(lngMstCounter).BODY_PART_NO = aryBodyPartDetail(lngCounter).BODY_PART_NO Then
                        'Me.chkLstBodyPart.SetItemChecked(lngCounter, True)
                        Me.dtgList.Rows(lngCounter).Cells("COL_SELECT").Value = True
                        Select Case aryBodyPartDetail(lngCounter).STUDY_DIV
                            Case "0"
                                Me.dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = "【単】"
                            Case "1"
                                Me.dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = "【造】"
                            Case "2"
                                Me.dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = "【単+造】"
                            Case "3"
                                Me.dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = "【Dynamic】"
                            Case Else
                                Me.dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = ""
                        End Select
                    End If
                    'Me.chkLstBodyPart.Items.Add(aryBodyPartDetail(lngCounter).BODY_PART_NM)
                Next lngCounter
            Next lngMstCounter
        Else
            '///部位マスタ取得
            If fncGetMstBodyPart(strMsg) = RET_ERROR Then
                MsgBox(strMsg & Space(1) & "cmbModality_SelectedIndexChanged", MsgBoxStyle.Critical, "部位マスタ取得")
                Call subOutLog(strMsg & Space(1) & "cmbModality_SelectedIndexChanged", 1)
                Exit Sub
            End If
        End If
        'Else
        ''///部位マスタ取得
        'If fncGetMstBodyPart(strMsg) = RET_ERROR Then
        '    MsgBox(strMsg & Space(1) & "cmbModality_SelectedIndexChanged", MsgBoxStyle.Critical, "部位マスタ取得")
        '    Call subOutLog(strMsg & Space(1) & "cmbModality_SelectedIndexChanged", 1)
        '    Exit Sub
        'End If
        'End If

    End Sub
#End Region

#Region "キャンセルボタン押下時"
    Private Sub cmdCancel_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub
#End Region

#Region "OKボタン押下時"
    Private Sub cmdOK_Click(sender As System.Object, e As System.EventArgs) Handles cmdOK.Click
        Dim lngCounter As Long = 0
        Dim lngChkCnt As Long = 0


        Erase aryBodyPartDetail

        'For lngCounter = 0 To Me.chkLstBodyPart.CheckedIndices.Count - 1
        '    ReDim Preserve aryBodyPartDetail(lngCounter)
        '    aryBodyPartDetail(lngCounter) = New clsBodyPart

        '    With aryBodyPartDetail()
        '        .BODY_PART_NM = Me.chkLstBodyPart.Items.Item(lngCounter)
        '    End With
        'Next lngCounter

        For lngCounter = 0 To Me.dtgList.Rows.Count - 1

            If dtgList.Rows(lngCounter).Cells("COL_SELECT").Value = True Then
                ReDim Preserve aryBodyPartDetail(lngChkCnt)
                aryBodyPartDetail(lngChkCnt) = New clsBodyPart

                With aryBodyPartDetail(lngChkCnt)
                    .BODY_PART_NM = dtgList.Rows(lngCounter).Cells("COL_BODY_PART").Value
                    .BODY_PART_NO = dtgList.Rows(lngCounter).Cells("COL_BODY_PART_NO").Value

                    Select Case dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value
                        Case "【単】"
                            .STUDY_DIV = 0
                        Case "【造】"
                            .STUDY_DIV = 1
                        Case "【単+造】"
                            .STUDY_DIV = 2
                        Case "【Dynamic】"
                            .STUDY_DIV = 3
                    End Select
                    lngChkCnt += 1
                End With
            End If
        Next

        For lngCounter = 0 To aryBodyPart.Length - 1

            If aryBodyPartDetail Is Nothing Then
                Exit For
            End If

            For lngChkCnt = 0 To aryBodyPartDetail.Length - 1

                If aryBodyPart(lngCounter).BODY_PART_NM = aryBodyPartDetail(lngChkCnt).BODY_PART_NM Then
                    With aryBodyPartDetail(lngChkCnt)
                        .BODY_PART_NO = aryBodyPart(lngCounter).BODY_PART_NO
                        .MODALITY_CD = aryBodyPart(lngCounter).MODALITY_CD
                        '.STUDY_DIV = aryBodyPart(lngCounter).STUDY_DIV
                    End With
                End If
            Next lngChkCnt
        Next lngCounter

        Me.Close()
    End Sub
#End Region

#Region "部位一覧CellEnter"
    Private Sub dtgList_CellEnter(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dtgList.CellEnter
        Dim wkDataGridView As DataGridView
        wkDataGridView = CType(sender, DataGridView)

        If TypeOf wkDataGridView.Columns(e.ColumnIndex) _
          Is DataGridViewComboBoxColumn Then

            SendKeys.Send("{F4}")
            'ElseIf TypeOf wkDataGridView.Columns(e.ColumnIndex) _
            '  Is DataGridViewCheckBoxColumn Then

            '    SendKeys.Send("{F4}")
        End If
    End Sub
#End Region

#End Region

#Region "Sub Routine"

#Region "部位マスタ取得(fncGetMstBodyPart)"
    '********************************************************************************
    '* 機能概要　：部位マスタ取得
    '* 関数名称　：fncGetMstBodyPart
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・0=正常、-1=データ無、-9=エラー
    '* 作成日　　：2012/01/25
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetMstBodyPart(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim lngCounter As Long = 0

        Try
            Erase aryBodyPart
            Me.dtgList.Rows.Clear()

            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("BODY_PART_NO,")
            strSQL.AppendLine("BODY_PART_NM,")
            strSQL.AppendLine("MODALITY_CD ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_BODY_PART ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("MODALITY_CD = '" & aryModality(Me.cmbModality.SelectedIndex).MODALITY_CD & "' ")
            strSQL.AppendLine("ORDER BY DISP_SORT ")

            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND

            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryBodyPart(lngCounter)
                aryBodyPart(lngCounter) = New clsBodyPart

                With aryBodyPart(lngCounter)
                    .BODY_PART_NO = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NO")
                    .BODY_PART_NM = dsData.Tables(0).Rows(lngCounter).Item("BODY_PART_NM")
                    .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")

                    'Me.chkLstBodyPart.Items.Add(.BODY_PART_NM)
                    Me.dtgList.Rows.Add()
                    dtgList.Rows(lngCounter).Cells("COL_BODY_PART").Value = .BODY_PART_NM
                    dtgList.Rows(lngCounter).Cells("COL_BODY_PART_NO").Value = .BODY_PART_NO
                    dtgList.Rows(lngCounter).Cells("COL_SELECT").Value = False
                    dtgList.Rows(lngCounter).Cells("COL_STUDY_DIV").Value = ""
                End With

            Next lngCounter

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try
    End Function
#End Region

#Region "モダリティマスタ取得(fncGetModality)"
    '********************************************************************************
    '* 機能概要　：モダリティマスタ取得
    '* 関数名称　：fncGetModality
    '* 引　数　　：strMsg・・・エラーメッセージ退避用
    '* 戻り値　　：Integer・・・0=正常、-1=データ無、-9=エラー
    '* 作成日　　：2012/01/25
    '* 作成者　　：Created By Watanabe
    '* 更新履歴
    '********************************************************************************
    Private Function fncGetModality(ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim lngCounter As Long = 0

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT ")
            strSQL.AppendLine("MODALITY_NO,")
            strSQL.AppendLine("MODALITY_CD,")
            strSQL.AppendLine("MODALITY_NAME,")
            strSQL.AppendLine("TERMINAL,")
            strSQL.AppendLine("ROOM_NAME ")
            strSQL.AppendLine("FROM ")
            strSQL.AppendLine("MST_MODALITY ")
            'strSQL.AppendLine("GROUP BY MODALITY_CD ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            Erase aryModality
            Me.cmbModality.Items.Clear()

            '///取得データを構造体へ退避
            For lngCounter = 0 To dsData.Tables(0).Rows.Count - 1
                ReDim Preserve aryModality(lngCounter)
                aryModality(lngCounter) = New clsModality

                With aryModality(lngCounter)
                    .MODALITY_NO = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_NO")
                    .MODALITY_CD = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_CD")
                    .MODALITY_NAME = dsData.Tables(0).Rows(lngCounter).Item("MODALITY_NAME")
                    .TERMINAL = dsData.Tables(0).Rows(lngCounter).Item("TERMINAL")
                    .ROOM_NAME = dsData.Tables(0).Rows(lngCounter).Item("ROOM_NAME")

                    Me.cmbModality.Items.Add(.ROOM_NAME)
                End With
            Next lngCounter

            '///
            If Me.txtModality.Text <> "" Then
                For lngCounter = 0 To aryModality.Length - 1
                    If Me.txtModality.Text = aryModality(lngCounter).MODALITY_NO Then
                        Me.cmbModality.SelectedIndex = lngCounter
                        Exit For
                    End If
                Next lngCounter
            Else
                Me.cmbModality.SelectedIndex = -1
            End If

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Return RET_ERROR
        Finally

        End Try
    End Function
#End Region

#End Region

End Class