﻿Public NotInheritable Class frmSplash

    Private lngTimerCnt As Long

    'TODO: このフォームは、プロジェクト デザイナ ([プロジェクト] メニューの下の [プロパティ]) の [アプリケーション] タブを使用して、
    '  アプリケーションのスプラッシュ スクリーンとして簡単に設定することができます

#Region "Form Events"

#Region "Form Load"
    Private Sub frmSplash_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strMsg As String = vbNullString

        Me.Cursor = Cursors.WaitCursor
        Me.StartPosition = FormStartPosition.CenterScreen

        'アプリケーションのアセンブリ情報に従って、ランタイムにダイアログ テキストを設定します。  

        'TODO: [プロジェクト] メニューの下にある [プロジェクト プロパティ] ダイアログの [アプリケーション] ペインで、アプリケーションのアセンブリ情報を 
        '  カスタマイズします

        'アプリケーション タイトル
        'If My.Application.Info.Title <> "" Then
        '    ApplicationTitle.Text = My.Application.Info.Title
        'Else
        '    'アプリケーション タイトルがない場合は、拡張子なしのアプリケーション名を使用します
        '    ApplicationTitle.Text = System.IO.Path.GetFileNameWithoutExtension(My.Application.Info.AssemblyName)
        'End If

        'デザイン時に書式設定文字列としてバージョン管理で設定されたテキストを使用して、バージョン情報の書式を
        '  設定します。これにより効率的なローカリゼーションが可能になります。
        '  ビルドおよび リビジョン情報は、次のコードを使用したり、バージョン管理のデザイン時のテキストを 
        '  "Version {0}.{1:00}.{2}.{3}" のように変更したりすることによって含めることができます。
        '  詳細については、ヘルプの String.Format() を参照してください。
        '
        '    Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build, My.Application.Info.Version.Revision)

        'Version.Text = System.String.Format(Version.Text, My.Application.Info.Version.Major, My.Application.Info.Version.Minor)

        ''著作権情報
        'Copyright.Text = My.Application.Info.Copyright

        If g_SystemStart = 0 Then
            Me.Opacity = 0
            Timer1.Enabled = True
        End If
        Label4.Text = "Ver." & Application.ProductVersion.ToString
        Label4.Visible = True
        Me.Cursor = Cursors.Default

    End Sub
#End Region

#End Region

#Region "Control Events"
    Private Sub LinkLabel1_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Me.Hide()
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        Dim objfrmInformation As New frmInformation

        '******************
        '* 情報画面を表示 *
        '******************
        objfrmInformation.ShowDialog()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lngTimerCnt += 1

        Me.Opacity = lngTimerCnt / 100
        If lngTimerCnt = 130 Then
            Me.Cursor = Cursors.Default
            Dim frmLogIn As New frmLogin

            Call subOutLog("ログイン画面表示 開始 [frmSplash.Timer1_Tick]", 0)


            Timer1.Enabled = False
            Me.TopMost = True

            g_SystemStart = 1
            frmLogIn.Show()

            Me.TopMost = False
            Me.Hide()

        End If

    End Sub

#End Region

#Region "Sub Routine"

#Region "特定のアドレスにPingを送信(fncSendPing)"
    '********************************************************************************
    '* 関数名称　：fncSendPing
    '* 機能概要　：特定のアドレスにPingを送信
    '* 引　数　　：strAdress=疎通を確認するIPアドレス
    '* 　　　　　：strMsg　 =エラーメッセージ退避用
    '* 戻り値　　：Boolean・・・正常=True
    '* 　　　　　：       　　　異常=False
    '* 作成日　　：2010/12/22
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '********************************************************************************
    Private Function fncSendPing(ByVal strAdress As String, ByRef strMsg As String) As Boolean

        Try

            'Pingオブジェクトの作成
            Dim objProcess As New System.Net.NetworkInformation.Ping()

            '"www.yahoo.com"にPingを送信する
            Dim reply As System.Net.NetworkInformation.PingReply = objProcess.Send(strAdress)

            '結果を取得
            If reply.Status = System.Net.NetworkInformation.IPStatus.Success Then
                Return True
            Else
                Return False
            End If

            objProcess.Dispose()

        Catch ex As Exception
            strMsg = ex.Message
            Return False
        End Try
    End Function
#End Region

#End Region

End Class
