﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditOrder
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditOrder))
        Me.grpPatient = New System.Windows.Forms.GroupBox()
        Me.txtPatientComment = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cmdPatientClear = New VIBlend.WinForms.Controls.vButton()
        Me.txtBirth = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtSex = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtEiji = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtKana = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtKanji = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPatientID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grpOrder = New System.Windows.Forms.GroupBox()
        Me.txtBodyPart = New System.Windows.Forms.TextBox()
        Me.cmdBodyPart = New VIBlend.WinForms.Controls.vButton()
        Me.lstBodyPart = New System.Windows.Forms.ListBox()
        Me.txtOrderAno = New System.Windows.Forms.TextBox()
        Me.txtModalityNo = New System.Windows.Forms.TextBox()
        Me.txtOrderTime = New System.Windows.Forms.TextBox()
        Me.txtOrderDate = New System.Windows.Forms.TextBox()
        Me.txtModality = New System.Windows.Forms.TextBox()
        Me.cmbBodyPart = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmbStudy_T = New System.Windows.Forms.ComboBox()
        Me.cmbStudy_H = New System.Windows.Forms.ComboBox()
        Me.cmdOrderClear = New VIBlend.WinForms.Controls.vButton()
        Me.txtOrderComment = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dtOrderDate = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmbModality = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.grpProc = New System.Windows.Forms.GroupBox()
        Me.cmdAddOrder = New VIBlend.WinForms.Controls.vButton()
        Me.cmdClose = New VIBlend.WinForms.Controls.vButton()
        Me.txtStudyDiv = New System.Windows.Forms.TextBox()
        Me.txtKenshinID = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.grpPatient.SuspendLayout()
        Me.grpOrder.SuspendLayout()
        Me.grpProc.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpPatient
        '
        Me.grpPatient.Controls.Add(Me.txtKenshinID)
        Me.grpPatient.Controls.Add(Me.Label15)
        Me.grpPatient.Controls.Add(Me.txtPatientComment)
        Me.grpPatient.Controls.Add(Me.Label14)
        Me.grpPatient.Controls.Add(Me.cmdPatientClear)
        Me.grpPatient.Controls.Add(Me.txtBirth)
        Me.grpPatient.Controls.Add(Me.Label7)
        Me.grpPatient.Controls.Add(Me.txtSex)
        Me.grpPatient.Controls.Add(Me.Label6)
        Me.grpPatient.Controls.Add(Me.txtEiji)
        Me.grpPatient.Controls.Add(Me.Label5)
        Me.grpPatient.Controls.Add(Me.txtKana)
        Me.grpPatient.Controls.Add(Me.Label3)
        Me.grpPatient.Controls.Add(Me.txtKanji)
        Me.grpPatient.Controls.Add(Me.Label2)
        Me.grpPatient.Controls.Add(Me.txtPatientID)
        Me.grpPatient.Controls.Add(Me.Label1)
        Me.grpPatient.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.grpPatient.ForeColor = System.Drawing.Color.DodgerBlue
        Me.grpPatient.Location = New System.Drawing.Point(9, 12)
        Me.grpPatient.Name = "grpPatient"
        Me.grpPatient.Size = New System.Drawing.Size(749, 177)
        Me.grpPatient.TabIndex = 2
        Me.grpPatient.TabStop = False
        Me.grpPatient.Text = "患者情報"
        '
        'txtPatientComment
        '
        Me.txtPatientComment.Location = New System.Drawing.Point(161, 107)
        Me.txtPatientComment.Multiline = True
        Me.txtPatientComment.Name = "txtPatientComment"
        Me.txtPatientComment.ReadOnly = True
        Me.txtPatientComment.Size = New System.Drawing.Size(481, 46)
        Me.txtPatientComment.TabIndex = 13
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label14.Location = New System.Drawing.Point(19, 107)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(80, 16)
        Me.Label14.TabIndex = 12
        Me.Label14.Text = "患者コメント"
        '
        'cmdPatientClear
        '
        Me.cmdPatientClear.BackColor = System.Drawing.Color.Transparent
        Me.cmdPatientClear.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdPatientClear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdPatientClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdPatientClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPatientClear.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdPatientClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPatientClear.Location = New System.Drawing.Point(660, 72)
        Me.cmdPatientClear.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdPatientClear.Name = "cmdPatientClear"
        Me.cmdPatientClear.PressedTextColor = System.Drawing.Color.Black
        Me.cmdPatientClear.RoundedCornersMask = CType(15, Byte)
        Me.cmdPatientClear.RoundedCornersRadius = 8
        Me.cmdPatientClear.Size = New System.Drawing.Size(81, 34)
        Me.cmdPatientClear.StretchImage = True
        Me.cmdPatientClear.StyleKey = "Button"
        Me.cmdPatientClear.TabIndex = 14
        Me.cmdPatientClear.Text = "クリア"
        Me.cmdPatientClear.UseThemeTextColor = False
        Me.cmdPatientClear.UseVisualStyleBackColor = False
        Me.cmdPatientClear.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'txtBirth
        '
        Me.txtBirth.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtBirth.Location = New System.Drawing.Point(364, 82)
        Me.txtBirth.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtBirth.Name = "txtBirth"
        Me.txtBirth.ReadOnly = True
        Me.txtBirth.Size = New System.Drawing.Size(125, 22)
        Me.txtBirth.TabIndex = 11
        Me.txtBirth.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(241, 85)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 16)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "生年月日"
        '
        'txtSex
        '
        Me.txtSex.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtSex.Location = New System.Drawing.Point(161, 82)
        Me.txtSex.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtSex.Name = "txtSex"
        Me.txtSex.ReadOnly = True
        Me.txtSex.Size = New System.Drawing.Size(65, 22)
        Me.txtSex.TabIndex = 9
        Me.txtSex.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(19, 85)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 16)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "性　別"
        '
        'txtEiji
        '
        Me.txtEiji.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtEiji.Location = New System.Drawing.Point(161, 60)
        Me.txtEiji.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtEiji.Name = "txtEiji"
        Me.txtEiji.ReadOnly = True
        Me.txtEiji.Size = New System.Drawing.Size(328, 22)
        Me.txtEiji.TabIndex = 7
        Me.txtEiji.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(19, 63)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "患者英字氏名"
        '
        'txtKana
        '
        Me.txtKana.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtKana.Location = New System.Drawing.Point(161, 34)
        Me.txtKana.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtKana.Name = "txtKana"
        Me.txtKana.ReadOnly = True
        Me.txtKana.Size = New System.Drawing.Size(328, 22)
        Me.txtKana.TabIndex = 5
        Me.txtKana.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(19, 37)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "患者半角カナ氏名"
        '
        'txtKanji
        '
        Me.txtKanji.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtKanji.Location = New System.Drawing.Point(161, 34)
        Me.txtKanji.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtKanji.Name = "txtKanji"
        Me.txtKanji.ReadOnly = True
        Me.txtKanji.Size = New System.Drawing.Size(328, 22)
        Me.txtKanji.TabIndex = 3
        Me.txtKanji.TabStop = False
        Me.txtKanji.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(19, 37)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "患者漢字氏名"
        Me.Label2.Visible = False
        '
        'txtPatientID
        '
        Me.txtPatientID.BackColor = System.Drawing.SystemColors.Window
        Me.txtPatientID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtPatientID.Location = New System.Drawing.Point(161, 12)
        Me.txtPatientID.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtPatientID.MaxLength = 8
        Me.txtPatientID.Name = "txtPatientID"
        Me.txtPatientID.Size = New System.Drawing.Size(212, 22)
        Me.txtPatientID.TabIndex = 1
        Me.txtPatientID.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(19, 15)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "患者ID"
        '
        'grpOrder
        '
        Me.grpOrder.Controls.Add(Me.txtStudyDiv)
        Me.grpOrder.Controls.Add(Me.txtBodyPart)
        Me.grpOrder.Controls.Add(Me.cmdBodyPart)
        Me.grpOrder.Controls.Add(Me.lstBodyPart)
        Me.grpOrder.Controls.Add(Me.txtOrderAno)
        Me.grpOrder.Controls.Add(Me.txtModalityNo)
        Me.grpOrder.Controls.Add(Me.txtOrderTime)
        Me.grpOrder.Controls.Add(Me.txtOrderDate)
        Me.grpOrder.Controls.Add(Me.txtModality)
        Me.grpOrder.Controls.Add(Me.cmbBodyPart)
        Me.grpOrder.Controls.Add(Me.Label13)
        Me.grpOrder.Controls.Add(Me.cmbStudy_T)
        Me.grpOrder.Controls.Add(Me.cmbStudy_H)
        Me.grpOrder.Controls.Add(Me.cmdOrderClear)
        Me.grpOrder.Controls.Add(Me.txtOrderComment)
        Me.grpOrder.Controls.Add(Me.Label12)
        Me.grpOrder.Controls.Add(Me.Label11)
        Me.grpOrder.Controls.Add(Me.Label10)
        Me.grpOrder.Controls.Add(Me.Label9)
        Me.grpOrder.Controls.Add(Me.dtOrderDate)
        Me.grpOrder.Controls.Add(Me.Label8)
        Me.grpOrder.Controls.Add(Me.cmbModality)
        Me.grpOrder.Controls.Add(Me.Label4)
        Me.grpOrder.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.grpOrder.ForeColor = System.Drawing.Color.DodgerBlue
        Me.grpOrder.Location = New System.Drawing.Point(10, 195)
        Me.grpOrder.Name = "grpOrder"
        Me.grpOrder.Size = New System.Drawing.Size(746, 241)
        Me.grpOrder.TabIndex = 1
        Me.grpOrder.TabStop = False
        Me.grpOrder.Text = "オーダ情報"
        '
        'txtBodyPart
        '
        Me.txtBodyPart.Location = New System.Drawing.Point(647, 34)
        Me.txtBodyPart.Name = "txtBodyPart"
        Me.txtBodyPart.Size = New System.Drawing.Size(43, 21)
        Me.txtBodyPart.TabIndex = 22
        Me.txtBodyPart.Visible = False
        '
        'cmdBodyPart
        '
        Me.cmdBodyPart.BackColor = System.Drawing.Color.Transparent
        Me.cmdBodyPart.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdBodyPart.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdBodyPart.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdBodyPart.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdBodyPart.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdBodyPart.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBodyPart.Location = New System.Drawing.Point(658, 136)
        Me.cmdBodyPart.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdBodyPart.Name = "cmdBodyPart"
        Me.cmdBodyPart.PressedTextColor = System.Drawing.Color.Black
        Me.cmdBodyPart.RoundedCornersMask = CType(15, Byte)
        Me.cmdBodyPart.RoundedCornersRadius = 8
        Me.cmdBodyPart.Size = New System.Drawing.Size(81, 34)
        Me.cmdBodyPart.StretchImage = True
        Me.cmdBodyPart.StyleKey = "Button"
        Me.cmdBodyPart.TabIndex = 21
        Me.cmdBodyPart.Text = "部位選択"
        Me.cmdBodyPart.UseThemeTextColor = False
        Me.cmdBodyPart.UseVisualStyleBackColor = False
        Me.cmdBodyPart.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'lstBodyPart
        '
        Me.lstBodyPart.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lstBodyPart.FormattingEnabled = True
        Me.lstBodyPart.ItemHeight = 15
        Me.lstBodyPart.Location = New System.Drawing.Point(159, 59)
        Me.lstBodyPart.Name = "lstBodyPart"
        Me.lstBodyPart.Size = New System.Drawing.Size(481, 124)
        Me.lstBodyPart.TabIndex = 20
        '
        'txtOrderAno
        '
        Me.txtOrderAno.Location = New System.Drawing.Point(647, 9)
        Me.txtOrderAno.Name = "txtOrderAno"
        Me.txtOrderAno.Size = New System.Drawing.Size(43, 21)
        Me.txtOrderAno.TabIndex = 14
        Me.txtOrderAno.Visible = False
        '
        'txtModalityNo
        '
        Me.txtModalityNo.Location = New System.Drawing.Point(696, 84)
        Me.txtModalityNo.Name = "txtModalityNo"
        Me.txtModalityNo.Size = New System.Drawing.Size(43, 21)
        Me.txtModalityNo.TabIndex = 18
        Me.txtModalityNo.Visible = False
        '
        'txtOrderTime
        '
        Me.txtOrderTime.Location = New System.Drawing.Point(696, 59)
        Me.txtOrderTime.Name = "txtOrderTime"
        Me.txtOrderTime.Size = New System.Drawing.Size(43, 21)
        Me.txtOrderTime.TabIndex = 17
        Me.txtOrderTime.Visible = False
        '
        'txtOrderDate
        '
        Me.txtOrderDate.Location = New System.Drawing.Point(696, 34)
        Me.txtOrderDate.Name = "txtOrderDate"
        Me.txtOrderDate.Size = New System.Drawing.Size(43, 21)
        Me.txtOrderDate.TabIndex = 16
        Me.txtOrderDate.Visible = False
        '
        'txtModality
        '
        Me.txtModality.Location = New System.Drawing.Point(696, 9)
        Me.txtModality.Name = "txtModality"
        Me.txtModality.Size = New System.Drawing.Size(43, 21)
        Me.txtModality.TabIndex = 15
        Me.txtModality.Visible = False
        '
        'cmbBodyPart
        '
        Me.cmbBodyPart.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.cmbBodyPart.FormattingEnabled = True
        Me.cmbBodyPart.Location = New System.Drawing.Point(307, 65)
        Me.cmbBodyPart.Name = "cmbBodyPart"
        Me.cmbBodyPart.Size = New System.Drawing.Size(91, 24)
        Me.cmbBodyPart.TabIndex = 11
        Me.cmbBodyPart.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(17, 68)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(68, 16)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "検査部位"
        '
        'cmbStudy_T
        '
        Me.cmbStudy_T.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.cmbStudy_T.FormattingEnabled = True
        Me.cmbStudy_T.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.cmbStudy_T.Items.AddRange(New Object() {"00", "15", "30", "45"})
        Me.cmbStudy_T.Location = New System.Drawing.Point(465, 36)
        Me.cmbStudy_T.Name = "cmbStudy_T"
        Me.cmbStudy_T.Size = New System.Drawing.Size(50, 24)
        Me.cmbStudy_T.TabIndex = 7
        '
        'cmbStudy_H
        '
        Me.cmbStudy_H.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.cmbStudy_H.FormattingEnabled = True
        Me.cmbStudy_H.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.cmbStudy_H.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"})
        Me.cmbStudy_H.Location = New System.Drawing.Point(389, 36)
        Me.cmbStudy_H.Name = "cmbStudy_H"
        Me.cmbStudy_H.Size = New System.Drawing.Size(50, 24)
        Me.cmbStudy_H.TabIndex = 5
        '
        'cmdOrderClear
        '
        Me.cmdOrderClear.BackColor = System.Drawing.Color.Transparent
        Me.cmdOrderClear.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdOrderClear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdOrderClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdOrderClear.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOrderClear.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdOrderClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOrderClear.Location = New System.Drawing.Point(657, 176)
        Me.cmdOrderClear.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdOrderClear.Name = "cmdOrderClear"
        Me.cmdOrderClear.PressedTextColor = System.Drawing.Color.Black
        Me.cmdOrderClear.RoundedCornersMask = CType(15, Byte)
        Me.cmdOrderClear.RoundedCornersRadius = 8
        Me.cmdOrderClear.Size = New System.Drawing.Size(81, 34)
        Me.cmdOrderClear.StretchImage = True
        Me.cmdOrderClear.StyleKey = "Button"
        Me.cmdOrderClear.TabIndex = 19
        Me.cmdOrderClear.Text = "クリア"
        Me.cmdOrderClear.UseThemeTextColor = False
        Me.cmdOrderClear.UseVisualStyleBackColor = False
        Me.cmdOrderClear.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'txtOrderComment
        '
        Me.txtOrderComment.ImeMode = System.Windows.Forms.ImeMode.Hiragana
        Me.txtOrderComment.Location = New System.Drawing.Point(159, 193)
        Me.txtOrderComment.Multiline = True
        Me.txtOrderComment.Name = "txtOrderComment"
        Me.txtOrderComment.Size = New System.Drawing.Size(481, 42)
        Me.txtOrderComment.TabIndex = 13
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label12.Location = New System.Drawing.Point(17, 195)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(84, 16)
        Me.Label12.TabIndex = 12
        Me.Label12.Text = "オーダコメント"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label11.Location = New System.Drawing.Point(514, 40)
        Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(23, 16)
        Me.Label11.TabIndex = 8
        Me.Label11.Text = "分"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(438, 40)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(23, 16)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "時"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(304, 40)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 16)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "検査時刻"
        '
        'dtOrderDate
        '
        Me.dtOrderDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.dtOrderDate.Location = New System.Drawing.Point(159, 36)
        Me.dtOrderDate.MaxDate = New Date(2050, 12, 31, 0, 0, 0, 0)
        Me.dtOrderDate.MinDate = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.dtOrderDate.Name = "dtOrderDate"
        Me.dtOrderDate.Size = New System.Drawing.Size(138, 22)
        Me.dtOrderDate.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(17, 40)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 16)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "検査日"
        '
        'cmbModality
        '
        Me.cmbModality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbModality.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.cmbModality.FormattingEnabled = True
        Me.cmbModality.Location = New System.Drawing.Point(159, 12)
        Me.cmbModality.Name = "cmbModality"
        Me.cmbModality.Size = New System.Drawing.Size(138, 24)
        Me.cmbModality.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(17, 15)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "検査室"
        '
        'grpProc
        '
        Me.grpProc.Controls.Add(Me.cmdAddOrder)
        Me.grpProc.Controls.Add(Me.cmdClose)
        Me.grpProc.Location = New System.Drawing.Point(11, 442)
        Me.grpProc.Name = "grpProc"
        Me.grpProc.Size = New System.Drawing.Size(745, 66)
        Me.grpProc.TabIndex = 0
        Me.grpProc.TabStop = False
        '
        'cmdAddOrder
        '
        Me.cmdAddOrder.BackColor = System.Drawing.Color.Transparent
        Me.cmdAddOrder.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdAddOrder.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdAddOrder.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdAddOrder.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAddOrder.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdAddOrder.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddOrder.Location = New System.Drawing.Point(436, 18)
        Me.cmdAddOrder.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdAddOrder.Name = "cmdAddOrder"
        Me.cmdAddOrder.PressedTextColor = System.Drawing.Color.Black
        Me.cmdAddOrder.RoundedCornersMask = CType(15, Byte)
        Me.cmdAddOrder.RoundedCornersRadius = 8
        Me.cmdAddOrder.Size = New System.Drawing.Size(147, 34)
        Me.cmdAddOrder.StretchImage = True
        Me.cmdAddOrder.StyleKey = "Button"
        Me.cmdAddOrder.TabIndex = 0
        Me.cmdAddOrder.Text = "オーダ登録"
        Me.cmdAddOrder.UseThemeTextColor = False
        Me.cmdAddOrder.UseVisualStyleBackColor = False
        Me.cmdAddOrder.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'cmdClose
        '
        Me.cmdClose.BackColor = System.Drawing.Color.Transparent
        Me.cmdClose.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClose.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdClose.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClose.Location = New System.Drawing.Point(591, 18)
        Me.cmdClose.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.PressedTextColor = System.Drawing.Color.Black
        Me.cmdClose.RoundedCornersMask = CType(15, Byte)
        Me.cmdClose.RoundedCornersRadius = 8
        Me.cmdClose.Size = New System.Drawing.Size(147, 34)
        Me.cmdClose.StretchImage = True
        Me.cmdClose.StyleKey = "Button"
        Me.cmdClose.TabIndex = 1
        Me.cmdClose.Text = "閉じる"
        Me.cmdClose.UseThemeTextColor = False
        Me.cmdClose.UseVisualStyleBackColor = False
        Me.cmdClose.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'txtStudyDiv
        '
        Me.txtStudyDiv.Location = New System.Drawing.Point(647, 61)
        Me.txtStudyDiv.Name = "txtStudyDiv"
        Me.txtStudyDiv.Size = New System.Drawing.Size(43, 21)
        Me.txtStudyDiv.TabIndex = 23
        Me.txtStudyDiv.Visible = False
        '
        'txtKenshinID
        '
        Me.txtKenshinID.BackColor = System.Drawing.SystemColors.Window
        Me.txtKenshinID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtKenshinID.Location = New System.Drawing.Point(442, 12)
        Me.txtKenshinID.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtKenshinID.MaxLength = 8
        Me.txtKenshinID.Name = "txtKenshinID"
        Me.txtKenshinID.Size = New System.Drawing.Size(212, 22)
        Me.txtKenshinID.TabIndex = 16
        Me.txtKenshinID.TabStop = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(380, 15)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(51, 16)
        Me.Label15.TabIndex = 15
        Me.Label15.Text = "健診ID"
        '
        'frmEditOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(771, 515)
        Me.Controls.Add(Me.grpProc)
        Me.Controls.Add(Me.grpOrder)
        Me.Controls.Add(Me.grpPatient)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmEditOrder"
        Me.Text = "オーダ情報登録/更新"
        Me.grpPatient.ResumeLayout(False)
        Me.grpPatient.PerformLayout()
        Me.grpOrder.ResumeLayout(False)
        Me.grpOrder.PerformLayout()
        Me.grpProc.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpPatient As System.Windows.Forms.GroupBox
    Friend WithEvents txtBirth As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtSex As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtEiji As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtKana As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtKanji As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPatientID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents grpOrder As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbModality As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dtOrderDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtOrderComment As System.Windows.Forms.TextBox
    Friend WithEvents grpProc As System.Windows.Forms.GroupBox
    Friend WithEvents cmdClose As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdPatientClear As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdOrderClear As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdAddOrder As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmbStudy_T As System.Windows.Forms.ComboBox
    Friend WithEvents cmbStudy_H As System.Windows.Forms.ComboBox
    Friend WithEvents cmbBodyPart As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtModality As System.Windows.Forms.TextBox
    Friend WithEvents txtOrderTime As System.Windows.Forms.TextBox
    Friend WithEvents txtOrderDate As System.Windows.Forms.TextBox
    Friend WithEvents txtPatientComment As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtModalityNo As System.Windows.Forms.TextBox
    Friend WithEvents txtOrderAno As System.Windows.Forms.TextBox
    Friend WithEvents lstBodyPart As System.Windows.Forms.ListBox
    Friend WithEvents cmdBodyPart As VIBlend.WinForms.Controls.vButton
    Friend WithEvents txtBodyPart As System.Windows.Forms.TextBox
    Friend WithEvents txtStudyDiv As System.Windows.Forms.TextBox
    Friend WithEvents txtKenshinID As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
End Class
