﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbModality = New System.Windows.Forms.ComboBox()
        Me.cmdToDay = New VIBlend.WinForms.Controls.vButton()
        Me.cmdNextMonth = New VIBlend.WinForms.Controls.vButton()
        Me.cmdNextDate = New VIBlend.WinForms.Controls.vButton()
        Me.cmdBeforMonth = New VIBlend.WinForms.Controls.vButton()
        Me.cmdBeforDate = New VIBlend.WinForms.Controls.vButton()
        Me.dtStudyDate = New System.Windows.Forms.DateTimePicker()
        Me.dtgList = New System.Windows.Forms.DataGridView()
        Me.COL_PATIENT_COMMENT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_DEL_FLG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_ORDER_COMMENT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_STUDY_COMMENT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_STATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_STUDY_DIV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_PATIENT_ANO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_PATIENT_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_KENSHIN_PATIENT_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_PATIENT_KANJI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_PATIENT_EIJI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_PATIENT_KANA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_PATIENT_SEX = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_PATIENT_BIRTH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_AGE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_DEPT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_NYUGAI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_WARD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_ROOM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_REQ_DOC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_ORDER_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_ORDER_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_ORDER_TIME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_STUDY_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_STUDY_TIME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_MODALITY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_STUDY_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_BODY_PART = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_ORDER_ANO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_INS_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_INS_TIME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_UPD_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_UPD_TIME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_COMMENT_FLG = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ctxtProcMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.画像表示ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.受付ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.受付取消ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.削除ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.削除取消ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.実施ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.実施取消ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.患者情報修正ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.部位情報修正ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.オーダ明細ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.txtPatientID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdPortable = New VIBlend.WinForms.Controls.vButton()
        Me.cmdCanJisshi = New VIBlend.WinForms.Controls.vButton()
        Me.cmdJisshi = New VIBlend.WinForms.Controls.vButton()
        Me.cmdOutCSV = New VIBlend.WinForms.Controls.vButton()
        Me.cmdEditOrder = New VIBlend.WinForms.Controls.vButton()
        Me.cmdOrderDetail = New VIBlend.WinForms.Controls.vButton()
        Me.cmdCanDelete = New VIBlend.WinForms.Controls.vButton()
        Me.cmdCanAccept = New VIBlend.WinForms.Controls.vButton()
        Me.cmdDelete = New VIBlend.WinForms.Controls.vButton()
        Me.cmdAccept = New VIBlend.WinForms.Controls.vButton()
        Me.cmdLogOut = New VIBlend.WinForms.Controls.vButton()
        Me.msSysMenu = New System.Windows.Forms.MenuStrip()
        Me.システムToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ログアウトToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.終了ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.データToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditOrder = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEditPatient = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.Interval = New System.Windows.Forms.ToolStripMenuItem()
        Me.Interval1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Interval2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Interval3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Interval4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Interval5 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Interval6 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.受付ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.受付取消ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.削除ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.削除取消ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.実施ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.実施取消ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.オーダ明細ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.画像表示ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.情報ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.バージョン情報ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.設定ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.一覧表示列ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmdEditPatient = New VIBlend.WinForms.Controls.vButton()
        Me.txtPatientKana = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblStudyEnd = New System.Windows.Forms.Label()
        Me.chkStudyEnd = New System.Windows.Forms.CheckBox()
        Me.lblStudyAccept = New System.Windows.Forms.Label()
        Me.chkStudyAccept = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblDelete = New System.Windows.Forms.Label()
        Me.lblNonDelete = New System.Windows.Forms.Label()
        Me.lblAccept = New System.Windows.Forms.Label()
        Me.lblNonAccept = New System.Windows.Forms.Label()
        Me.chkNonAccept = New System.Windows.Forms.CheckBox()
        Me.chkDelete = New System.Windows.Forms.CheckBox()
        Me.chkAccept = New System.Windows.Forms.CheckBox()
        Me.chkNonDelete = New System.Windows.Forms.CheckBox()
        Me.lblEnd = New System.Windows.Forms.Label()
        Me.chkEnd = New System.Windows.Forms.CheckBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.saveCSV = New System.Windows.Forms.SaveFileDialog()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblPatientCnt = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tmrProc = New System.Windows.Forms.Timer(Me.components)
        Me.chkPorterble = New System.Windows.Forms.CheckBox()
        Me.chkModality1 = New System.Windows.Forms.CheckBox()
        Me.chkModality2 = New System.Windows.Forms.CheckBox()
        Me.chkTmscPorterble = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dtgList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctxtProcMenu.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.msSysMenu.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbModality)
        Me.GroupBox1.Controls.Add(Me.cmdToDay)
        Me.GroupBox1.Controls.Add(Me.cmdNextMonth)
        Me.GroupBox1.Controls.Add(Me.cmdNextDate)
        Me.GroupBox1.Controls.Add(Me.cmdBeforMonth)
        Me.GroupBox1.Controls.Add(Me.cmdBeforDate)
        Me.GroupBox1.Controls.Add(Me.dtStudyDate)
        Me.GroupBox1.Location = New System.Drawing.Point(21, 32)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(1865, 67)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'cmbModality
        '
        Me.cmbModality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbModality.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbModality.FormattingEnabled = True
        Me.cmbModality.Location = New System.Drawing.Point(9, 14)
        Me.cmbModality.Name = "cmbModality"
        Me.cmbModality.Size = New System.Drawing.Size(241, 54)
        Me.cmbModality.TabIndex = 0
        '
        'cmdToDay
        '
        Me.cmdToDay.BackColor = System.Drawing.Color.Transparent
        Me.cmdToDay.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdToDay.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdToDay.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdToDay.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdToDay.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdToDay.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdToDay.Location = New System.Drawing.Point(1460, 14)
        Me.cmdToDay.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdToDay.Name = "cmdToDay"
        Me.cmdToDay.PressedTextColor = System.Drawing.Color.Black
        Me.cmdToDay.RoundedCornersMask = CType(15, Byte)
        Me.cmdToDay.RoundedCornersRadius = 8
        Me.cmdToDay.Size = New System.Drawing.Size(165, 45)
        Me.cmdToDay.StretchImage = True
        Me.cmdToDay.StyleKey = "Button"
        Me.cmdToDay.TabIndex = 6
        Me.cmdToDay.Text = "本日"
        Me.cmdToDay.UseThemeTextColor = False
        Me.cmdToDay.UseVisualStyleBackColor = False
        Me.cmdToDay.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ECOGREEN
        '
        'cmdNextMonth
        '
        Me.cmdNextMonth.BackColor = System.Drawing.Color.Transparent
        Me.cmdNextMonth.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdNextMonth.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdNextMonth.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdNextMonth.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNextMonth.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdNextMonth.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNextMonth.Location = New System.Drawing.Point(1292, 13)
        Me.cmdNextMonth.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdNextMonth.Name = "cmdNextMonth"
        Me.cmdNextMonth.PressedTextColor = System.Drawing.Color.Black
        Me.cmdNextMonth.RoundedCornersMask = CType(15, Byte)
        Me.cmdNextMonth.RoundedCornersRadius = 8
        Me.cmdNextMonth.Size = New System.Drawing.Size(145, 45)
        Me.cmdNextMonth.StretchImage = True
        Me.cmdNextMonth.StyleKey = "Button"
        Me.cmdNextMonth.TabIndex = 5
        Me.cmdNextMonth.Text = "＞＞"
        Me.cmdNextMonth.UseThemeTextColor = False
        Me.cmdNextMonth.UseVisualStyleBackColor = False
        Me.cmdNextMonth.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ECOGREEN
        '
        'cmdNextDate
        '
        Me.cmdNextDate.BackColor = System.Drawing.Color.Transparent
        Me.cmdNextDate.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdNextDate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdNextDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdNextDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdNextDate.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdNextDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNextDate.Location = New System.Drawing.Point(1135, 13)
        Me.cmdNextDate.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdNextDate.Name = "cmdNextDate"
        Me.cmdNextDate.PressedTextColor = System.Drawing.Color.Black
        Me.cmdNextDate.RoundedCornersMask = CType(15, Byte)
        Me.cmdNextDate.RoundedCornersRadius = 8
        Me.cmdNextDate.Size = New System.Drawing.Size(145, 45)
        Me.cmdNextDate.StretchImage = True
        Me.cmdNextDate.StyleKey = "Button"
        Me.cmdNextDate.TabIndex = 4
        Me.cmdNextDate.Text = "＞"
        Me.cmdNextDate.UseThemeTextColor = False
        Me.cmdNextDate.UseVisualStyleBackColor = False
        Me.cmdNextDate.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ECOGREEN
        '
        'cmdBeforMonth
        '
        Me.cmdBeforMonth.BackColor = System.Drawing.Color.Transparent
        Me.cmdBeforMonth.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdBeforMonth.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdBeforMonth.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdBeforMonth.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdBeforMonth.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdBeforMonth.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBeforMonth.Location = New System.Drawing.Point(427, 14)
        Me.cmdBeforMonth.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdBeforMonth.Name = "cmdBeforMonth"
        Me.cmdBeforMonth.PressedTextColor = System.Drawing.Color.Black
        Me.cmdBeforMonth.RoundedCornersMask = CType(15, Byte)
        Me.cmdBeforMonth.RoundedCornersRadius = 8
        Me.cmdBeforMonth.Size = New System.Drawing.Size(145, 45)
        Me.cmdBeforMonth.StretchImage = True
        Me.cmdBeforMonth.StyleKey = "Button"
        Me.cmdBeforMonth.TabIndex = 1
        Me.cmdBeforMonth.Text = "＜＜"
        Me.cmdBeforMonth.UseThemeTextColor = False
        Me.cmdBeforMonth.UseVisualStyleBackColor = False
        Me.cmdBeforMonth.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ECOGREEN
        '
        'cmdBeforDate
        '
        Me.cmdBeforDate.BackColor = System.Drawing.Color.Transparent
        Me.cmdBeforDate.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdBeforDate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdBeforDate.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdBeforDate.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdBeforDate.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdBeforDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBeforDate.Location = New System.Drawing.Point(584, 14)
        Me.cmdBeforDate.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdBeforDate.Name = "cmdBeforDate"
        Me.cmdBeforDate.PressedTextColor = System.Drawing.Color.Black
        Me.cmdBeforDate.RoundedCornersMask = CType(15, Byte)
        Me.cmdBeforDate.RoundedCornersRadius = 8
        Me.cmdBeforDate.Size = New System.Drawing.Size(145, 45)
        Me.cmdBeforDate.StretchImage = True
        Me.cmdBeforDate.StyleKey = "Button"
        Me.cmdBeforDate.TabIndex = 2
        Me.cmdBeforDate.Text = "＜"
        Me.cmdBeforDate.UseThemeTextColor = False
        Me.cmdBeforDate.UseVisualStyleBackColor = False
        Me.cmdBeforDate.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ECOGREEN
        '
        'dtStudyDate
        '
        Me.dtStudyDate.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtStudyDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtStudyDate.Location = New System.Drawing.Point(741, 13)
        Me.dtStudyDate.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.dtStudyDate.MaxDate = New Date(2030, 12, 31, 0, 0, 0, 0)
        Me.dtStudyDate.MinDate = New Date(2000, 1, 1, 0, 0, 0, 0)
        Me.dtStudyDate.Name = "dtStudyDate"
        Me.dtStudyDate.Size = New System.Drawing.Size(382, 60)
        Me.dtStudyDate.TabIndex = 3
        '
        'dtgList
        '
        Me.dtgList.AllowUserToOrderColumns = True
        Me.dtgList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_PATIENT_COMMENT, Me.COL_DEL_FLG, Me.COL_ORDER_COMMENT, Me.COL_STUDY_COMMENT, Me.COL_STATE, Me.COL_STUDY_DIV, Me.COL_PATIENT_ANO, Me.COL_PATIENT_ID, Me.COL_KENSHIN_PATIENT_ID, Me.COL_PATIENT_KANJI, Me.COL_PATIENT_EIJI, Me.COL_PATIENT_KANA, Me.COL_PATIENT_SEX, Me.COL_PATIENT_BIRTH, Me.COL_AGE, Me.COL_DEPT, Me.COL_NYUGAI, Me.COL_WARD, Me.COL_ROOM, Me.COL_REQ_DOC, Me.COL_ORDER_NO, Me.COL_ORDER_DATE, Me.COL_ORDER_TIME, Me.COL_STUDY_DATE, Me.COL_STUDY_TIME, Me.COL_MODALITY, Me.COL_STUDY_NAME, Me.COL_BODY_PART, Me.COL_ORDER_ANO, Me.COL_INS_DATE, Me.COL_INS_TIME, Me.COL_UPD_DATE, Me.COL_UPD_TIME, Me.COL_COMMENT_FLG})
        Me.dtgList.ContextMenuStrip = Me.ctxtProcMenu
        Me.dtgList.Location = New System.Drawing.Point(22, 267)
        Me.dtgList.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.dtgList.MultiSelect = False
        Me.dtgList.Name = "dtgList"
        Me.dtgList.RowHeadersWidth = 51
        Me.dtgList.RowTemplate.Height = 21
        Me.dtgList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgList.Size = New System.Drawing.Size(1867, 652)
        Me.dtgList.TabIndex = 11
        '
        'COL_PATIENT_COMMENT
        '
        Me.COL_PATIENT_COMMENT.HeaderText = "患者コメント"
        Me.COL_PATIENT_COMMENT.MinimumWidth = 6
        Me.COL_PATIENT_COMMENT.Name = "COL_PATIENT_COMMENT"
        Me.COL_PATIENT_COMMENT.ReadOnly = True
        Me.COL_PATIENT_COMMENT.Visible = False
        Me.COL_PATIENT_COMMENT.Width = 125
        '
        'COL_DEL_FLG
        '
        Me.COL_DEL_FLG.HeaderText = "削除フラグ"
        Me.COL_DEL_FLG.MinimumWidth = 6
        Me.COL_DEL_FLG.Name = "COL_DEL_FLG"
        Me.COL_DEL_FLG.ReadOnly = True
        Me.COL_DEL_FLG.Visible = False
        Me.COL_DEL_FLG.Width = 125
        '
        'COL_ORDER_COMMENT
        '
        Me.COL_ORDER_COMMENT.HeaderText = "オーダコメント"
        Me.COL_ORDER_COMMENT.MinimumWidth = 6
        Me.COL_ORDER_COMMENT.Name = "COL_ORDER_COMMENT"
        Me.COL_ORDER_COMMENT.ReadOnly = True
        Me.COL_ORDER_COMMENT.Visible = False
        Me.COL_ORDER_COMMENT.Width = 125
        '
        'COL_STUDY_COMMENT
        '
        Me.COL_STUDY_COMMENT.HeaderText = "実施コメント"
        Me.COL_STUDY_COMMENT.MinimumWidth = 6
        Me.COL_STUDY_COMMENT.Name = "COL_STUDY_COMMENT"
        Me.COL_STUDY_COMMENT.ReadOnly = True
        Me.COL_STUDY_COMMENT.Visible = False
        Me.COL_STUDY_COMMENT.Width = 125
        '
        'COL_STATE
        '
        Me.COL_STATE.HeaderText = "進捗"
        Me.COL_STATE.MinimumWidth = 6
        Me.COL_STATE.Name = "COL_STATE"
        Me.COL_STATE.ReadOnly = True
        Me.COL_STATE.Width = 125
        '
        'COL_STUDY_DIV
        '
        Me.COL_STUDY_DIV.HeaderText = "検査区分"
        Me.COL_STUDY_DIV.MinimumWidth = 6
        Me.COL_STUDY_DIV.Name = "COL_STUDY_DIV"
        Me.COL_STUDY_DIV.Width = 125
        '
        'COL_PATIENT_ANO
        '
        Me.COL_PATIENT_ANO.HeaderText = "患者番号"
        Me.COL_PATIENT_ANO.MinimumWidth = 6
        Me.COL_PATIENT_ANO.Name = "COL_PATIENT_ANO"
        Me.COL_PATIENT_ANO.ReadOnly = True
        Me.COL_PATIENT_ANO.Visible = False
        Me.COL_PATIENT_ANO.Width = 125
        '
        'COL_PATIENT_ID
        '
        Me.COL_PATIENT_ID.HeaderText = "患者ID"
        Me.COL_PATIENT_ID.MinimumWidth = 6
        Me.COL_PATIENT_ID.Name = "COL_PATIENT_ID"
        Me.COL_PATIENT_ID.ReadOnly = True
        Me.COL_PATIENT_ID.Width = 125
        '
        'COL_KENSHIN_PATIENT_ID
        '
        Me.COL_KENSHIN_PATIENT_ID.HeaderText = "健診ID"
        Me.COL_KENSHIN_PATIENT_ID.MinimumWidth = 6
        Me.COL_KENSHIN_PATIENT_ID.Name = "COL_KENSHIN_PATIENT_ID"
        Me.COL_KENSHIN_PATIENT_ID.ReadOnly = True
        Me.COL_KENSHIN_PATIENT_ID.Visible = False
        Me.COL_KENSHIN_PATIENT_ID.Width = 125
        '
        'COL_PATIENT_KANJI
        '
        Me.COL_PATIENT_KANJI.HeaderText = "患者氏名"
        Me.COL_PATIENT_KANJI.MinimumWidth = 6
        Me.COL_PATIENT_KANJI.Name = "COL_PATIENT_KANJI"
        Me.COL_PATIENT_KANJI.ReadOnly = True
        Me.COL_PATIENT_KANJI.Visible = False
        Me.COL_PATIENT_KANJI.Width = 125
        '
        'COL_PATIENT_EIJI
        '
        Me.COL_PATIENT_EIJI.HeaderText = "患者氏名"
        Me.COL_PATIENT_EIJI.MinimumWidth = 6
        Me.COL_PATIENT_EIJI.Name = "COL_PATIENT_EIJI"
        Me.COL_PATIENT_EIJI.ReadOnly = True
        Me.COL_PATIENT_EIJI.Visible = False
        Me.COL_PATIENT_EIJI.Width = 125
        '
        'COL_PATIENT_KANA
        '
        Me.COL_PATIENT_KANA.HeaderText = "患者氏名"
        Me.COL_PATIENT_KANA.MinimumWidth = 6
        Me.COL_PATIENT_KANA.Name = "COL_PATIENT_KANA"
        Me.COL_PATIENT_KANA.ReadOnly = True
        Me.COL_PATIENT_KANA.Width = 125
        '
        'COL_PATIENT_SEX
        '
        Me.COL_PATIENT_SEX.HeaderText = "性別"
        Me.COL_PATIENT_SEX.MinimumWidth = 6
        Me.COL_PATIENT_SEX.Name = "COL_PATIENT_SEX"
        Me.COL_PATIENT_SEX.ReadOnly = True
        Me.COL_PATIENT_SEX.Width = 125
        '
        'COL_PATIENT_BIRTH
        '
        Me.COL_PATIENT_BIRTH.HeaderText = "生年月日"
        Me.COL_PATIENT_BIRTH.MinimumWidth = 6
        Me.COL_PATIENT_BIRTH.Name = "COL_PATIENT_BIRTH"
        Me.COL_PATIENT_BIRTH.ReadOnly = True
        Me.COL_PATIENT_BIRTH.Width = 125
        '
        'COL_AGE
        '
        Me.COL_AGE.HeaderText = "年齢"
        Me.COL_AGE.MinimumWidth = 6
        Me.COL_AGE.Name = "COL_AGE"
        Me.COL_AGE.ReadOnly = True
        Me.COL_AGE.Width = 125
        '
        'COL_DEPT
        '
        Me.COL_DEPT.HeaderText = "依頼科"
        Me.COL_DEPT.MinimumWidth = 6
        Me.COL_DEPT.Name = "COL_DEPT"
        Me.COL_DEPT.Width = 125
        '
        'COL_NYUGAI
        '
        Me.COL_NYUGAI.HeaderText = "入外"
        Me.COL_NYUGAI.MinimumWidth = 6
        Me.COL_NYUGAI.Name = "COL_NYUGAI"
        Me.COL_NYUGAI.Width = 125
        '
        'COL_WARD
        '
        Me.COL_WARD.HeaderText = "病棟"
        Me.COL_WARD.MinimumWidth = 6
        Me.COL_WARD.Name = "COL_WARD"
        Me.COL_WARD.Width = 125
        '
        'COL_ROOM
        '
        Me.COL_ROOM.HeaderText = "病室"
        Me.COL_ROOM.MinimumWidth = 6
        Me.COL_ROOM.Name = "COL_ROOM"
        Me.COL_ROOM.Width = 125
        '
        'COL_REQ_DOC
        '
        Me.COL_REQ_DOC.HeaderText = "依頼医"
        Me.COL_REQ_DOC.MinimumWidth = 6
        Me.COL_REQ_DOC.Name = "COL_REQ_DOC"
        Me.COL_REQ_DOC.Width = 125
        '
        'COL_ORDER_NO
        '
        Me.COL_ORDER_NO.HeaderText = "オーダ番号"
        Me.COL_ORDER_NO.MinimumWidth = 6
        Me.COL_ORDER_NO.Name = "COL_ORDER_NO"
        Me.COL_ORDER_NO.ReadOnly = True
        Me.COL_ORDER_NO.Visible = False
        Me.COL_ORDER_NO.Width = 150
        '
        'COL_ORDER_DATE
        '
        Me.COL_ORDER_DATE.HeaderText = "検査予定日"
        Me.COL_ORDER_DATE.MinimumWidth = 6
        Me.COL_ORDER_DATE.Name = "COL_ORDER_DATE"
        Me.COL_ORDER_DATE.ReadOnly = True
        Me.COL_ORDER_DATE.Width = 125
        '
        'COL_ORDER_TIME
        '
        Me.COL_ORDER_TIME.HeaderText = "検査予定時刻"
        Me.COL_ORDER_TIME.MinimumWidth = 6
        Me.COL_ORDER_TIME.Name = "COL_ORDER_TIME"
        Me.COL_ORDER_TIME.ReadOnly = True
        Me.COL_ORDER_TIME.Width = 125
        '
        'COL_STUDY_DATE
        '
        Me.COL_STUDY_DATE.HeaderText = "検査実施日"
        Me.COL_STUDY_DATE.MinimumWidth = 6
        Me.COL_STUDY_DATE.Name = "COL_STUDY_DATE"
        Me.COL_STUDY_DATE.ReadOnly = True
        Me.COL_STUDY_DATE.Visible = False
        Me.COL_STUDY_DATE.Width = 125
        '
        'COL_STUDY_TIME
        '
        Me.COL_STUDY_TIME.HeaderText = "検査実施時刻"
        Me.COL_STUDY_TIME.MinimumWidth = 6
        Me.COL_STUDY_TIME.Name = "COL_STUDY_TIME"
        Me.COL_STUDY_TIME.ReadOnly = True
        Me.COL_STUDY_TIME.Width = 125
        '
        'COL_MODALITY
        '
        Me.COL_MODALITY.HeaderText = "モダリティ"
        Me.COL_MODALITY.MinimumWidth = 6
        Me.COL_MODALITY.Name = "COL_MODALITY"
        Me.COL_MODALITY.ReadOnly = True
        Me.COL_MODALITY.Visible = False
        Me.COL_MODALITY.Width = 125
        '
        'COL_STUDY_NAME
        '
        Me.COL_STUDY_NAME.HeaderText = "検査名"
        Me.COL_STUDY_NAME.MinimumWidth = 6
        Me.COL_STUDY_NAME.Name = "COL_STUDY_NAME"
        Me.COL_STUDY_NAME.ReadOnly = True
        Me.COL_STUDY_NAME.Width = 125
        '
        'COL_BODY_PART
        '
        Me.COL_BODY_PART.HeaderText = "検査部位"
        Me.COL_BODY_PART.MinimumWidth = 6
        Me.COL_BODY_PART.Name = "COL_BODY_PART"
        Me.COL_BODY_PART.ReadOnly = True
        Me.COL_BODY_PART.Width = 125
        '
        'COL_ORDER_ANO
        '
        Me.COL_ORDER_ANO.HeaderText = "オーダ登録番号"
        Me.COL_ORDER_ANO.MinimumWidth = 6
        Me.COL_ORDER_ANO.Name = "COL_ORDER_ANO"
        Me.COL_ORDER_ANO.ReadOnly = True
        Me.COL_ORDER_ANO.Visible = False
        Me.COL_ORDER_ANO.Width = 125
        '
        'COL_INS_DATE
        '
        Me.COL_INS_DATE.HeaderText = "オーダ登録日"
        Me.COL_INS_DATE.MinimumWidth = 6
        Me.COL_INS_DATE.Name = "COL_INS_DATE"
        Me.COL_INS_DATE.ReadOnly = True
        Me.COL_INS_DATE.Visible = False
        Me.COL_INS_DATE.Width = 125
        '
        'COL_INS_TIME
        '
        Me.COL_INS_TIME.HeaderText = "オーダ登録時刻"
        Me.COL_INS_TIME.MinimumWidth = 6
        Me.COL_INS_TIME.Name = "COL_INS_TIME"
        Me.COL_INS_TIME.ReadOnly = True
        Me.COL_INS_TIME.Visible = False
        Me.COL_INS_TIME.Width = 125
        '
        'COL_UPD_DATE
        '
        Me.COL_UPD_DATE.HeaderText = "オーダ更新日"
        Me.COL_UPD_DATE.MinimumWidth = 6
        Me.COL_UPD_DATE.Name = "COL_UPD_DATE"
        Me.COL_UPD_DATE.ReadOnly = True
        Me.COL_UPD_DATE.Visible = False
        Me.COL_UPD_DATE.Width = 125
        '
        'COL_UPD_TIME
        '
        Me.COL_UPD_TIME.HeaderText = "オーダ更新時刻"
        Me.COL_UPD_TIME.MinimumWidth = 6
        Me.COL_UPD_TIME.Name = "COL_UPD_TIME"
        Me.COL_UPD_TIME.ReadOnly = True
        Me.COL_UPD_TIME.Visible = False
        Me.COL_UPD_TIME.Width = 125
        '
        'COL_COMMENT_FLG
        '
        Me.COL_COMMENT_FLG.HeaderText = "コメント"
        Me.COL_COMMENT_FLG.MinimumWidth = 6
        Me.COL_COMMENT_FLG.Name = "COL_COMMENT_FLG"
        Me.COL_COMMENT_FLG.ReadOnly = True
        Me.COL_COMMENT_FLG.Visible = False
        Me.COL_COMMENT_FLG.Width = 125
        '
        'ctxtProcMenu
        '
        Me.ctxtProcMenu.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ctxtProcMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.画像表示ToolStripMenuItem, Me.ToolStripSeparator1, Me.受付ToolStripMenuItem1, Me.受付取消ToolStripMenuItem1, Me.ToolStripSeparator2, Me.削除ToolStripMenuItem1, Me.削除取消ToolStripMenuItem1, Me.ToolStripSeparator3, Me.実施ToolStripMenuItem1, Me.実施取消ToolStripMenuItem1, Me.ToolStripSeparator4, Me.患者情報修正ToolStripMenuItem, Me.部位情報修正ToolStripMenuItem1, Me.オーダ明細ToolStripMenuItem1})
        Me.ctxtProcMenu.Name = "ctxtProcMenu"
        Me.ctxtProcMenu.Size = New System.Drawing.Size(169, 268)
        '
        '画像表示ToolStripMenuItem
        '
        Me.画像表示ToolStripMenuItem.Name = "画像表示ToolStripMenuItem"
        Me.画像表示ToolStripMenuItem.Size = New System.Drawing.Size(168, 24)
        Me.画像表示ToolStripMenuItem.Text = "画像表示"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(165, 6)
        '
        '受付ToolStripMenuItem1
        '
        Me.受付ToolStripMenuItem1.Name = "受付ToolStripMenuItem1"
        Me.受付ToolStripMenuItem1.Size = New System.Drawing.Size(168, 24)
        Me.受付ToolStripMenuItem1.Text = "受　付"
        '
        '受付取消ToolStripMenuItem1
        '
        Me.受付取消ToolStripMenuItem1.Name = "受付取消ToolStripMenuItem1"
        Me.受付取消ToolStripMenuItem1.Size = New System.Drawing.Size(168, 24)
        Me.受付取消ToolStripMenuItem1.Text = "受付取消"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(165, 6)
        '
        '削除ToolStripMenuItem1
        '
        Me.削除ToolStripMenuItem1.Name = "削除ToolStripMenuItem1"
        Me.削除ToolStripMenuItem1.Size = New System.Drawing.Size(168, 24)
        Me.削除ToolStripMenuItem1.Text = "削　除"
        '
        '削除取消ToolStripMenuItem1
        '
        Me.削除取消ToolStripMenuItem1.Name = "削除取消ToolStripMenuItem1"
        Me.削除取消ToolStripMenuItem1.Size = New System.Drawing.Size(168, 24)
        Me.削除取消ToolStripMenuItem1.Text = "削除取消"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(165, 6)
        '
        '実施ToolStripMenuItem1
        '
        Me.実施ToolStripMenuItem1.Name = "実施ToolStripMenuItem1"
        Me.実施ToolStripMenuItem1.Size = New System.Drawing.Size(168, 24)
        Me.実施ToolStripMenuItem1.Text = "実施"
        '
        '実施取消ToolStripMenuItem1
        '
        Me.実施取消ToolStripMenuItem1.Name = "実施取消ToolStripMenuItem1"
        Me.実施取消ToolStripMenuItem1.Size = New System.Drawing.Size(168, 24)
        Me.実施取消ToolStripMenuItem1.Text = "実施取消"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(165, 6)
        '
        '患者情報修正ToolStripMenuItem
        '
        Me.患者情報修正ToolStripMenuItem.Name = "患者情報修正ToolStripMenuItem"
        Me.患者情報修正ToolStripMenuItem.Size = New System.Drawing.Size(168, 24)
        Me.患者情報修正ToolStripMenuItem.Text = "患者情報修正"
        Me.患者情報修正ToolStripMenuItem.Visible = False
        '
        '部位情報修正ToolStripMenuItem1
        '
        Me.部位情報修正ToolStripMenuItem1.Name = "部位情報修正ToolStripMenuItem1"
        Me.部位情報修正ToolStripMenuItem1.Size = New System.Drawing.Size(168, 24)
        Me.部位情報修正ToolStripMenuItem1.Text = "部位情報修正"
        Me.部位情報修正ToolStripMenuItem1.Visible = False
        '
        'オーダ明細ToolStripMenuItem1
        '
        Me.オーダ明細ToolStripMenuItem1.Name = "オーダ明細ToolStripMenuItem1"
        Me.オーダ明細ToolStripMenuItem1.Size = New System.Drawing.Size(168, 24)
        Me.オーダ明細ToolStripMenuItem1.Text = "オーダ明細"
        Me.オーダ明細ToolStripMenuItem1.Visible = False
        '
        'txtPatientID
        '
        Me.txtPatientID.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPatientID.Location = New System.Drawing.Point(15, 37)
        Me.txtPatientID.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.txtPatientID.MaxLength = 10
        Me.txtPatientID.Name = "txtPatientID"
        Me.txtPatientID.Size = New System.Drawing.Size(256, 57)
        Me.txtPatientID.TabIndex = 1
        Me.txtPatientID.Text = "1234567890"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 17)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "患者ID"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdPortable)
        Me.GroupBox2.Controls.Add(Me.cmdCanJisshi)
        Me.GroupBox2.Controls.Add(Me.cmdJisshi)
        Me.GroupBox2.Controls.Add(Me.cmdOutCSV)
        Me.GroupBox2.Controls.Add(Me.cmdEditOrder)
        Me.GroupBox2.Controls.Add(Me.cmdOrderDetail)
        Me.GroupBox2.Controls.Add(Me.cmdCanDelete)
        Me.GroupBox2.Controls.Add(Me.cmdCanAccept)
        Me.GroupBox2.Controls.Add(Me.cmdDelete)
        Me.GroupBox2.Controls.Add(Me.cmdAccept)
        Me.GroupBox2.Controls.Add(Me.cmdLogOut)
        Me.GroupBox2.Location = New System.Drawing.Point(22, 927)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(1864, 72)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        '
        'cmdPortable
        '
        Me.cmdPortable.BackColor = System.Drawing.Color.Transparent
        Me.cmdPortable.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdPortable.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdPortable.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdPortable.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPortable.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdPortable.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPortable.Location = New System.Drawing.Point(1139, 19)
        Me.cmdPortable.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdPortable.Name = "cmdPortable"
        Me.cmdPortable.PressedTextColor = System.Drawing.Color.Black
        Me.cmdPortable.RoundedCornersMask = CType(15, Byte)
        Me.cmdPortable.RoundedCornersRadius = 8
        Me.cmdPortable.Size = New System.Drawing.Size(184, 45)
        Me.cmdPortable.StretchImage = True
        Me.cmdPortable.StyleKey = "Button"
        Me.cmdPortable.TabIndex = 6
        Me.cmdPortable.Text = "ポータブル一括実施"
        Me.cmdPortable.UseThemeTextColor = False
        Me.cmdPortable.UseVisualStyleBackColor = False
        Me.cmdPortable.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ORANGEFRESH
        Me.cmdPortable.Visible = False
        '
        'cmdCanJisshi
        '
        Me.cmdCanJisshi.BackColor = System.Drawing.Color.Transparent
        Me.cmdCanJisshi.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdCanJisshi.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdCanJisshi.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdCanJisshi.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCanJisshi.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdCanJisshi.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCanJisshi.Location = New System.Drawing.Point(1139, 19)
        Me.cmdCanJisshi.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdCanJisshi.Name = "cmdCanJisshi"
        Me.cmdCanJisshi.PressedTextColor = System.Drawing.Color.Black
        Me.cmdCanJisshi.RoundedCornersMask = CType(15, Byte)
        Me.cmdCanJisshi.RoundedCornersRadius = 8
        Me.cmdCanJisshi.Size = New System.Drawing.Size(150, 45)
        Me.cmdCanJisshi.StretchImage = True
        Me.cmdCanJisshi.StyleKey = "Button"
        Me.cmdCanJisshi.TabIndex = 9
        Me.cmdCanJisshi.Text = "実施取消"
        Me.cmdCanJisshi.UseThemeTextColor = False
        Me.cmdCanJisshi.UseVisualStyleBackColor = False
        Me.cmdCanJisshi.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ORANGEFRESH
        Me.cmdCanJisshi.Visible = False
        '
        'cmdJisshi
        '
        Me.cmdJisshi.BackColor = System.Drawing.Color.Transparent
        Me.cmdJisshi.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdJisshi.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdJisshi.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdJisshi.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdJisshi.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdJisshi.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdJisshi.Location = New System.Drawing.Point(980, 19)
        Me.cmdJisshi.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdJisshi.Name = "cmdJisshi"
        Me.cmdJisshi.PressedTextColor = System.Drawing.Color.Black
        Me.cmdJisshi.RoundedCornersMask = CType(15, Byte)
        Me.cmdJisshi.RoundedCornersRadius = 8
        Me.cmdJisshi.Size = New System.Drawing.Size(150, 45)
        Me.cmdJisshi.StretchImage = True
        Me.cmdJisshi.StyleKey = "Button"
        Me.cmdJisshi.TabIndex = 5
        Me.cmdJisshi.Text = "実　施"
        Me.cmdJisshi.UseThemeTextColor = False
        Me.cmdJisshi.UseVisualStyleBackColor = False
        Me.cmdJisshi.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ULTRABLUE
        Me.cmdJisshi.Visible = False
        '
        'cmdOutCSV
        '
        Me.cmdOutCSV.BackColor = System.Drawing.Color.Transparent
        Me.cmdOutCSV.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdOutCSV.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdOutCSV.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdOutCSV.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOutCSV.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdOutCSV.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOutCSV.Location = New System.Drawing.Point(1510, 18)
        Me.cmdOutCSV.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdOutCSV.Name = "cmdOutCSV"
        Me.cmdOutCSV.PressedTextColor = System.Drawing.Color.Black
        Me.cmdOutCSV.RoundedCornersMask = CType(15, Byte)
        Me.cmdOutCSV.RoundedCornersRadius = 8
        Me.cmdOutCSV.Size = New System.Drawing.Size(165, 45)
        Me.cmdOutCSV.StretchImage = True
        Me.cmdOutCSV.StyleKey = "Button"
        Me.cmdOutCSV.TabIndex = 8
        Me.cmdOutCSV.Text = "CSV出力"
        Me.cmdOutCSV.UseThemeTextColor = False
        Me.cmdOutCSV.UseVisualStyleBackColor = False
        Me.cmdOutCSV.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'cmdEditOrder
        '
        Me.cmdEditOrder.BackColor = System.Drawing.Color.Transparent
        Me.cmdEditOrder.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdEditOrder.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdEditOrder.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdEditOrder.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdEditOrder.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdEditOrder.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEditOrder.Location = New System.Drawing.Point(12, 19)
        Me.cmdEditOrder.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdEditOrder.Name = "cmdEditOrder"
        Me.cmdEditOrder.PressedTextColor = System.Drawing.Color.Black
        Me.cmdEditOrder.RoundedCornersMask = CType(15, Byte)
        Me.cmdEditOrder.RoundedCornersRadius = 8
        Me.cmdEditOrder.Size = New System.Drawing.Size(116, 45)
        Me.cmdEditOrder.StretchImage = True
        Me.cmdEditOrder.StyleKey = "Button"
        Me.cmdEditOrder.TabIndex = 0
        Me.cmdEditOrder.Text = "オーダ登録"
        Me.cmdEditOrder.UseThemeTextColor = False
        Me.cmdEditOrder.UseVisualStyleBackColor = False
        Me.cmdEditOrder.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ULTRABLUE
        Me.cmdEditOrder.Visible = False
        '
        'cmdOrderDetail
        '
        Me.cmdOrderDetail.BackColor = System.Drawing.Color.Transparent
        Me.cmdOrderDetail.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdOrderDetail.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdOrderDetail.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdOrderDetail.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOrderDetail.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdOrderDetail.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOrderDetail.Location = New System.Drawing.Point(1335, 18)
        Me.cmdOrderDetail.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdOrderDetail.Name = "cmdOrderDetail"
        Me.cmdOrderDetail.PressedTextColor = System.Drawing.Color.Black
        Me.cmdOrderDetail.RoundedCornersMask = CType(15, Byte)
        Me.cmdOrderDetail.RoundedCornersRadius = 8
        Me.cmdOrderDetail.Size = New System.Drawing.Size(165, 45)
        Me.cmdOrderDetail.StretchImage = True
        Me.cmdOrderDetail.StyleKey = "Button"
        Me.cmdOrderDetail.TabIndex = 7
        Me.cmdOrderDetail.Text = "オーダ明細"
        Me.cmdOrderDetail.UseThemeTextColor = False
        Me.cmdOrderDetail.UseVisualStyleBackColor = False
        Me.cmdOrderDetail.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ULTRABLUE
        Me.cmdOrderDetail.Visible = False
        '
        'cmdCanDelete
        '
        Me.cmdCanDelete.BackColor = System.Drawing.Color.Transparent
        Me.cmdCanDelete.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdCanDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdCanDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdCanDelete.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCanDelete.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdCanDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCanDelete.Location = New System.Drawing.Point(806, 19)
        Me.cmdCanDelete.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdCanDelete.Name = "cmdCanDelete"
        Me.cmdCanDelete.PressedTextColor = System.Drawing.Color.Black
        Me.cmdCanDelete.RoundedCornersMask = CType(15, Byte)
        Me.cmdCanDelete.RoundedCornersRadius = 8
        Me.cmdCanDelete.Size = New System.Drawing.Size(150, 45)
        Me.cmdCanDelete.StretchImage = True
        Me.cmdCanDelete.StyleKey = "Button"
        Me.cmdCanDelete.TabIndex = 4
        Me.cmdCanDelete.Text = "削除取消"
        Me.cmdCanDelete.UseThemeTextColor = False
        Me.cmdCanDelete.UseVisualStyleBackColor = False
        Me.cmdCanDelete.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ORANGEFRESH
        '
        'cmdCanAccept
        '
        Me.cmdCanAccept.BackColor = System.Drawing.Color.Transparent
        Me.cmdCanAccept.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdCanAccept.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdCanAccept.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdCanAccept.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCanAccept.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdCanAccept.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCanAccept.Location = New System.Drawing.Point(465, 19)
        Me.cmdCanAccept.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdCanAccept.Name = "cmdCanAccept"
        Me.cmdCanAccept.PressedTextColor = System.Drawing.Color.Black
        Me.cmdCanAccept.RoundedCornersMask = CType(15, Byte)
        Me.cmdCanAccept.RoundedCornersRadius = 8
        Me.cmdCanAccept.Size = New System.Drawing.Size(150, 45)
        Me.cmdCanAccept.StretchImage = True
        Me.cmdCanAccept.StyleKey = "Button"
        Me.cmdCanAccept.TabIndex = 2
        Me.cmdCanAccept.Text = "受付取消"
        Me.cmdCanAccept.UseThemeTextColor = False
        Me.cmdCanAccept.UseVisualStyleBackColor = False
        Me.cmdCanAccept.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ORANGEFRESH
        '
        'cmdDelete
        '
        Me.cmdDelete.BackColor = System.Drawing.Color.Transparent
        Me.cmdDelete.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdDelete.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdDelete.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(648, 19)
        Me.cmdDelete.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.PressedTextColor = System.Drawing.Color.Black
        Me.cmdDelete.RoundedCornersMask = CType(15, Byte)
        Me.cmdDelete.RoundedCornersRadius = 8
        Me.cmdDelete.Size = New System.Drawing.Size(150, 45)
        Me.cmdDelete.StretchImage = True
        Me.cmdDelete.StyleKey = "Button"
        Me.cmdDelete.TabIndex = 3
        Me.cmdDelete.Text = "削　除"
        Me.cmdDelete.UseThemeTextColor = False
        Me.cmdDelete.UseVisualStyleBackColor = False
        Me.cmdDelete.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ULTRABLUE
        '
        'cmdAccept
        '
        Me.cmdAccept.BackColor = System.Drawing.Color.Transparent
        Me.cmdAccept.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdAccept.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdAccept.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdAccept.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdAccept.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdAccept.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAccept.Location = New System.Drawing.Point(303, 18)
        Me.cmdAccept.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdAccept.Name = "cmdAccept"
        Me.cmdAccept.PressedTextColor = System.Drawing.Color.Black
        Me.cmdAccept.RoundedCornersMask = CType(15, Byte)
        Me.cmdAccept.RoundedCornersRadius = 8
        Me.cmdAccept.Size = New System.Drawing.Size(150, 45)
        Me.cmdAccept.StretchImage = True
        Me.cmdAccept.StyleKey = "Button"
        Me.cmdAccept.TabIndex = 1
        Me.cmdAccept.Text = "受　付"
        Me.cmdAccept.UseThemeTextColor = False
        Me.cmdAccept.UseVisualStyleBackColor = False
        Me.cmdAccept.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ULTRABLUE
        '
        'cmdLogOut
        '
        Me.cmdLogOut.BackColor = System.Drawing.Color.Transparent
        Me.cmdLogOut.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdLogOut.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdLogOut.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdLogOut.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdLogOut.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdLogOut.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLogOut.Location = New System.Drawing.Point(1687, 19)
        Me.cmdLogOut.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdLogOut.Name = "cmdLogOut"
        Me.cmdLogOut.PressedTextColor = System.Drawing.Color.Black
        Me.cmdLogOut.RoundedCornersMask = CType(15, Byte)
        Me.cmdLogOut.RoundedCornersRadius = 8
        Me.cmdLogOut.Size = New System.Drawing.Size(165, 45)
        Me.cmdLogOut.StretchImage = True
        Me.cmdLogOut.StyleKey = "Button"
        Me.cmdLogOut.TabIndex = 9
        Me.cmdLogOut.Text = "ログアウト"
        Me.cmdLogOut.UseThemeTextColor = False
        Me.cmdLogOut.UseVisualStyleBackColor = False
        Me.cmdLogOut.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'msSysMenu
        '
        Me.msSysMenu.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.msSysMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.システムToolStripMenuItem, Me.データToolStripMenuItem, Me.情報ToolStripMenuItem, Me.設定ToolStripMenuItem})
        Me.msSysMenu.Location = New System.Drawing.Point(0, 0)
        Me.msSysMenu.Name = "msSysMenu"
        Me.msSysMenu.Padding = New System.Windows.Forms.Padding(11, 3, 0, 3)
        Me.msSysMenu.Size = New System.Drawing.Size(1902, 30)
        Me.msSysMenu.TabIndex = 0
        Me.msSysMenu.Text = "MenuStrip1"
        '
        'システムToolStripMenuItem
        '
        Me.システムToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ログアウトToolStripMenuItem, Me.終了ToolStripMenuItem})
        Me.システムToolStripMenuItem.Name = "システムToolStripMenuItem"
        Me.システムToolStripMenuItem.Size = New System.Drawing.Size(69, 24)
        Me.システムToolStripMenuItem.Text = "システム"
        '
        'ログアウトToolStripMenuItem
        '
        Me.ログアウトToolStripMenuItem.Name = "ログアウトToolStripMenuItem"
        Me.ログアウトToolStripMenuItem.Size = New System.Drawing.Size(148, 26)
        Me.ログアウトToolStripMenuItem.Text = "ログアウト"
        '
        '終了ToolStripMenuItem
        '
        Me.終了ToolStripMenuItem.Name = "終了ToolStripMenuItem"
        Me.終了ToolStripMenuItem.Size = New System.Drawing.Size(148, 26)
        Me.終了ToolStripMenuItem.Text = "終　了"
        '
        'データToolStripMenuItem
        '
        Me.データToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEditOrder, Me.mnuEditPatient, Me.ToolStripSeparator9, Me.Interval, Me.ToolStripSeparator8, Me.受付ToolStripMenuItem, Me.受付取消ToolStripMenuItem, Me.ToolStripSeparator5, Me.削除ToolStripMenuItem, Me.削除取消ToolStripMenuItem, Me.ToolStripSeparator6, Me.実施ToolStripMenuItem, Me.実施取消ToolStripMenuItem, Me.ToolStripSeparator7, Me.オーダ明細ToolStripMenuItem, Me.画像表示ToolStripMenuItem1})
        Me.データToolStripMenuItem.Name = "データToolStripMenuItem"
        Me.データToolStripMenuItem.Size = New System.Drawing.Size(56, 24)
        Me.データToolStripMenuItem.Text = "データ"
        '
        'mnuEditOrder
        '
        Me.mnuEditOrder.Name = "mnuEditOrder"
        Me.mnuEditOrder.Size = New System.Drawing.Size(182, 26)
        Me.mnuEditOrder.Text = "オーダ登録"
        Me.mnuEditOrder.Visible = False
        '
        'mnuEditPatient
        '
        Me.mnuEditPatient.Name = "mnuEditPatient"
        Me.mnuEditPatient.Size = New System.Drawing.Size(182, 26)
        Me.mnuEditPatient.Text = "患者情報登録"
        Me.mnuEditPatient.Visible = False
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(179, 6)
        '
        'Interval
        '
        Me.Interval.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Interval1, Me.Interval2, Me.Interval3, Me.Interval4, Me.Interval5, Me.Interval6})
        Me.Interval.Name = "Interval"
        Me.Interval.Size = New System.Drawing.Size(182, 26)
        Me.Interval.Text = "自動更新"
        '
        'Interval1
        '
        Me.Interval1.Name = "Interval1"
        Me.Interval1.Size = New System.Drawing.Size(146, 26)
        Me.Interval1.Text = "5秒"
        '
        'Interval2
        '
        Me.Interval2.Name = "Interval2"
        Me.Interval2.Size = New System.Drawing.Size(146, 26)
        Me.Interval2.Text = "10秒"
        '
        'Interval3
        '
        Me.Interval3.Name = "Interval3"
        Me.Interval3.Size = New System.Drawing.Size(146, 26)
        Me.Interval3.Text = "15秒"
        '
        'Interval4
        '
        Me.Interval4.Name = "Interval4"
        Me.Interval4.Size = New System.Drawing.Size(146, 26)
        Me.Interval4.Text = "30秒"
        '
        'Interval5
        '
        Me.Interval5.Name = "Interval5"
        Me.Interval5.Size = New System.Drawing.Size(146, 26)
        Me.Interval5.Text = "1分"
        '
        'Interval6
        '
        Me.Interval6.Name = "Interval6"
        Me.Interval6.Size = New System.Drawing.Size(146, 26)
        Me.Interval6.Text = "1分30秒"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(179, 6)
        '
        '受付ToolStripMenuItem
        '
        Me.受付ToolStripMenuItem.Name = "受付ToolStripMenuItem"
        Me.受付ToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.受付ToolStripMenuItem.Text = "受　付"
        '
        '受付取消ToolStripMenuItem
        '
        Me.受付取消ToolStripMenuItem.Name = "受付取消ToolStripMenuItem"
        Me.受付取消ToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.受付取消ToolStripMenuItem.Text = "受付取消"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(179, 6)
        '
        '削除ToolStripMenuItem
        '
        Me.削除ToolStripMenuItem.Name = "削除ToolStripMenuItem"
        Me.削除ToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.削除ToolStripMenuItem.Text = "削　除"
        '
        '削除取消ToolStripMenuItem
        '
        Me.削除取消ToolStripMenuItem.Name = "削除取消ToolStripMenuItem"
        Me.削除取消ToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.削除取消ToolStripMenuItem.Text = "削除取消"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(179, 6)
        '
        '実施ToolStripMenuItem
        '
        Me.実施ToolStripMenuItem.Name = "実施ToolStripMenuItem"
        Me.実施ToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.実施ToolStripMenuItem.Text = "実施"
        '
        '実施取消ToolStripMenuItem
        '
        Me.実施取消ToolStripMenuItem.Name = "実施取消ToolStripMenuItem"
        Me.実施取消ToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.実施取消ToolStripMenuItem.Text = "実施取消"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(179, 6)
        '
        'オーダ明細ToolStripMenuItem
        '
        Me.オーダ明細ToolStripMenuItem.Name = "オーダ明細ToolStripMenuItem"
        Me.オーダ明細ToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.オーダ明細ToolStripMenuItem.Text = "オーダ明細"
        Me.オーダ明細ToolStripMenuItem.Visible = False
        '
        '画像表示ToolStripMenuItem1
        '
        Me.画像表示ToolStripMenuItem1.Name = "画像表示ToolStripMenuItem1"
        Me.画像表示ToolStripMenuItem1.Size = New System.Drawing.Size(182, 26)
        Me.画像表示ToolStripMenuItem1.Text = "画像表示"
        '
        '情報ToolStripMenuItem
        '
        Me.情報ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.バージョン情報ToolStripMenuItem})
        Me.情報ToolStripMenuItem.Name = "情報ToolStripMenuItem"
        Me.情報ToolStripMenuItem.Size = New System.Drawing.Size(53, 24)
        Me.情報ToolStripMenuItem.Text = "情報"
        '
        'バージョン情報ToolStripMenuItem
        '
        Me.バージョン情報ToolStripMenuItem.Name = "バージョン情報ToolStripMenuItem"
        Me.バージョン情報ToolStripMenuItem.Size = New System.Drawing.Size(176, 26)
        Me.バージョン情報ToolStripMenuItem.Text = "バージョン情報"
        '
        '設定ToolStripMenuItem
        '
        Me.設定ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.一覧表示列ToolStripMenuItem})
        Me.設定ToolStripMenuItem.Name = "設定ToolStripMenuItem"
        Me.設定ToolStripMenuItem.Size = New System.Drawing.Size(53, 24)
        Me.設定ToolStripMenuItem.Text = "設定"
        '
        '一覧表示列ToolStripMenuItem
        '
        Me.一覧表示列ToolStripMenuItem.Name = "一覧表示列ToolStripMenuItem"
        Me.一覧表示列ToolStripMenuItem.Size = New System.Drawing.Size(167, 26)
        Me.一覧表示列ToolStripMenuItem.Text = "一覧表示列"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmdEditPatient)
        Me.GroupBox3.Controls.Add(Me.txtPatientKana)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.txtPatientID)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Location = New System.Drawing.Point(21, 106)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.GroupBox3.Size = New System.Drawing.Size(773, 98)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        '
        'cmdEditPatient
        '
        Me.cmdEditPatient.BackColor = System.Drawing.Color.Transparent
        Me.cmdEditPatient.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdEditPatient.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdEditPatient.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdEditPatient.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdEditPatient.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdEditPatient.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEditPatient.Location = New System.Drawing.Point(611, 27)
        Me.cmdEditPatient.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdEditPatient.Name = "cmdEditPatient"
        Me.cmdEditPatient.PressedTextColor = System.Drawing.Color.Black
        Me.cmdEditPatient.RoundedCornersMask = CType(15, Byte)
        Me.cmdEditPatient.RoundedCornersRadius = 8
        Me.cmdEditPatient.Size = New System.Drawing.Size(121, 45)
        Me.cmdEditPatient.StretchImage = True
        Me.cmdEditPatient.StyleKey = "Button"
        Me.cmdEditPatient.TabIndex = 4
        Me.cmdEditPatient.Text = "患者情報登録/更新"
        Me.cmdEditPatient.UseThemeTextColor = False
        Me.cmdEditPatient.UseVisualStyleBackColor = False
        Me.cmdEditPatient.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ULTRABLUE
        Me.cmdEditPatient.Visible = False
        '
        'txtPatientKana
        '
        Me.txtPatientKana.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPatientKana.Location = New System.Drawing.Point(283, 36)
        Me.txtPatientKana.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.txtPatientKana.Name = "txtPatientKana"
        Me.txtPatientKana.ReadOnly = True
        Me.txtPatientKana.Size = New System.Drawing.Size(478, 57)
        Me.txtPatientKana.TabIndex = 3
        Me.txtPatientKana.Text = "1234567890"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.Location = New System.Drawing.Point(279, 17)
        Me.Label2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(128, 25)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "患者カナ氏名"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblStudyEnd)
        Me.GroupBox4.Controls.Add(Me.chkStudyEnd)
        Me.GroupBox4.Controls.Add(Me.lblStudyAccept)
        Me.GroupBox4.Controls.Add(Me.chkStudyAccept)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.lblTotal)
        Me.GroupBox4.Controls.Add(Me.lblDelete)
        Me.GroupBox4.Controls.Add(Me.lblNonDelete)
        Me.GroupBox4.Controls.Add(Me.lblAccept)
        Me.GroupBox4.Controls.Add(Me.lblNonAccept)
        Me.GroupBox4.Controls.Add(Me.chkNonAccept)
        Me.GroupBox4.Controls.Add(Me.chkDelete)
        Me.GroupBox4.Controls.Add(Me.chkAccept)
        Me.GroupBox4.Controls.Add(Me.chkNonDelete)
        Me.GroupBox4.Controls.Add(Me.lblEnd)
        Me.GroupBox4.Controls.Add(Me.chkEnd)
        Me.GroupBox4.Location = New System.Drawing.Point(993, 107)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.GroupBox4.Size = New System.Drawing.Size(893, 97)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        '
        'lblStudyEnd
        '
        Me.lblStudyEnd.AutoSize = True
        Me.lblStudyEnd.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStudyEnd.Location = New System.Drawing.Point(506, 40)
        Me.lblStudyEnd.Name = "lblStudyEnd"
        Me.lblStudyEnd.Size = New System.Drawing.Size(44, 31)
        Me.lblStudyEnd.TabIndex = 8
        Me.lblStudyEnd.Text = "99"
        '
        'chkStudyEnd
        '
        Me.chkStudyEnd.AutoSize = True
        Me.chkStudyEnd.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkStudyEnd.Location = New System.Drawing.Point(468, 13)
        Me.chkStudyEnd.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.chkStudyEnd.Name = "chkStudyEnd"
        Me.chkStudyEnd.Size = New System.Drawing.Size(118, 31)
        Me.chkStudyEnd.TabIndex = 7
        Me.chkStudyEnd.Text = "検査済"
        Me.chkStudyEnd.UseVisualStyleBackColor = True
        '
        'lblStudyAccept
        '
        Me.lblStudyAccept.AutoSize = True
        Me.lblStudyAccept.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStudyAccept.Location = New System.Drawing.Point(349, 40)
        Me.lblStudyAccept.Name = "lblStudyAccept"
        Me.lblStudyAccept.Size = New System.Drawing.Size(44, 31)
        Me.lblStudyAccept.TabIndex = 5
        Me.lblStudyAccept.Text = "99"
        '
        'chkStudyAccept
        '
        Me.chkStudyAccept.AutoSize = True
        Me.chkStudyAccept.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkStudyAccept.Location = New System.Drawing.Point(293, 13)
        Me.chkStudyAccept.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.chkStudyAccept.Name = "chkStudyAccept"
        Me.chkStudyAccept.Size = New System.Drawing.Size(174, 31)
        Me.chkStudyAccept.TabIndex = 4
        Me.chkStudyAccept.Text = "検査受付済"
        Me.chkStudyAccept.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(376, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 31)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "合計"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(483, 65)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(41, 31)
        Me.Label10.TabIndex = 16
        Me.Label10.Text = "件"
        '
        'lblTotal
        '
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(428, 65)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(49, 20)
        Me.lblTotal.TabIndex = 15
        Me.lblTotal.Text = "99"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDelete
        '
        Me.lblDelete.AutoSize = True
        Me.lblDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDelete.Location = New System.Drawing.Point(768, 40)
        Me.lblDelete.Name = "lblDelete"
        Me.lblDelete.Size = New System.Drawing.Size(44, 31)
        Me.lblDelete.TabIndex = 12
        Me.lblDelete.Text = "99"
        '
        'lblNonDelete
        '
        Me.lblNonDelete.AutoSize = True
        Me.lblNonDelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNonDelete.Location = New System.Drawing.Point(635, 40)
        Me.lblNonDelete.Name = "lblNonDelete"
        Me.lblNonDelete.Size = New System.Drawing.Size(44, 31)
        Me.lblNonDelete.TabIndex = 10
        Me.lblNonDelete.Text = "99"
        Me.lblNonDelete.Visible = False
        '
        'lblAccept
        '
        Me.lblAccept.AutoSize = True
        Me.lblAccept.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAccept.Location = New System.Drawing.Point(199, 40)
        Me.lblAccept.Name = "lblAccept"
        Me.lblAccept.Size = New System.Drawing.Size(44, 31)
        Me.lblAccept.TabIndex = 3
        Me.lblAccept.Text = "99"
        '
        'lblNonAccept
        '
        Me.lblNonAccept.AutoSize = True
        Me.lblNonAccept.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNonAccept.Location = New System.Drawing.Point(69, 40)
        Me.lblNonAccept.Name = "lblNonAccept"
        Me.lblNonAccept.Size = New System.Drawing.Size(44, 31)
        Me.lblNonAccept.TabIndex = 1
        Me.lblNonAccept.Text = "99"
        '
        'chkNonAccept
        '
        Me.chkNonAccept.AutoSize = True
        Me.chkNonAccept.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkNonAccept.Location = New System.Drawing.Point(31, 13)
        Me.chkNonAccept.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.chkNonAccept.Name = "chkNonAccept"
        Me.chkNonAccept.Size = New System.Drawing.Size(118, 31)
        Me.chkNonAccept.TabIndex = 0
        Me.chkNonAccept.Text = "未受付"
        Me.chkNonAccept.UseVisualStyleBackColor = True
        '
        'chkDelete
        '
        Me.chkDelete.AutoSize = True
        Me.chkDelete.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkDelete.Location = New System.Drawing.Point(730, 13)
        Me.chkDelete.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.chkDelete.Name = "chkDelete"
        Me.chkDelete.Size = New System.Drawing.Size(118, 31)
        Me.chkDelete.TabIndex = 11
        Me.chkDelete.Text = "削除済"
        Me.chkDelete.UseVisualStyleBackColor = True
        '
        'chkAccept
        '
        Me.chkAccept.AutoSize = True
        Me.chkAccept.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkAccept.Location = New System.Drawing.Point(162, 13)
        Me.chkAccept.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.chkAccept.Name = "chkAccept"
        Me.chkAccept.Size = New System.Drawing.Size(118, 31)
        Me.chkAccept.TabIndex = 2
        Me.chkAccept.Text = "受付済"
        Me.chkAccept.UseVisualStyleBackColor = True
        '
        'chkNonDelete
        '
        Me.chkNonDelete.AutoSize = True
        Me.chkNonDelete.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkNonDelete.Location = New System.Drawing.Point(599, 13)
        Me.chkNonDelete.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.chkNonDelete.Name = "chkNonDelete"
        Me.chkNonDelete.Size = New System.Drawing.Size(118, 31)
        Me.chkNonDelete.TabIndex = 9
        Me.chkNonDelete.Text = "未削除"
        Me.chkNonDelete.UseVisualStyleBackColor = True
        '
        'lblEnd
        '
        Me.lblEnd.AutoSize = True
        Me.lblEnd.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnd.Location = New System.Drawing.Point(433, 49)
        Me.lblEnd.Name = "lblEnd"
        Me.lblEnd.Size = New System.Drawing.Size(44, 31)
        Me.lblEnd.TabIndex = 14
        Me.lblEnd.Text = "99"
        Me.lblEnd.Visible = False
        '
        'chkEnd
        '
        Me.chkEnd.AutoSize = True
        Me.chkEnd.Font = New System.Drawing.Font("ＭＳ ゴシック", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.chkEnd.Location = New System.Drawing.Point(377, 22)
        Me.chkEnd.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.chkEnd.Name = "chkEnd"
        Me.chkEnd.Size = New System.Drawing.Size(174, 31)
        Me.chkEnd.TabIndex = 6
        Me.chkEnd.Text = "検査受付済"
        Me.chkEnd.UseVisualStyleBackColor = True
        Me.chkEnd.Visible = False
        '
        'Timer1
        '
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(803, 156)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 31)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "患者数"
        Me.Label4.Visible = False
        '
        'lblPatientCnt
        '
        Me.lblPatientCnt.AutoSize = True
        Me.lblPatientCnt.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPatientCnt.Location = New System.Drawing.Point(898, 156)
        Me.lblPatientCnt.Name = "lblPatientCnt"
        Me.lblPatientCnt.Size = New System.Drawing.Size(59, 31)
        Me.lblPatientCnt.TabIndex = 4
        Me.lblPatientCnt.Text = "999"
        Me.lblPatientCnt.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(948, 156)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(42, 31)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "人"
        Me.Label6.Visible = False
        '
        'tmrProc
        '
        Me.tmrProc.Interval = 2000
        '
        'chkPorterble
        '
        Me.chkPorterble.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkPorterble.AutoSize = True
        Me.chkPorterble.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPorterble.Location = New System.Drawing.Point(331, 219)
        Me.chkPorterble.Name = "chkPorterble"
        Me.chkPorterble.Size = New System.Drawing.Size(129, 41)
        Me.chkPorterble.TabIndex = 10
        Me.chkPorterble.Text = "ポータブル"
        Me.chkPorterble.UseVisualStyleBackColor = True
        '
        'chkModality1
        '
        Me.chkModality1.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkModality1.AutoSize = True
        Me.chkModality1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkModality1.Location = New System.Drawing.Point(255, 219)
        Me.chkModality1.Name = "chkModality1"
        Me.chkModality1.Size = New System.Drawing.Size(84, 41)
        Me.chkModality1.TabIndex = 7
        Me.chkModality1.Text = "Aplio"
        Me.chkModality1.UseVisualStyleBackColor = True
        Me.chkModality1.Visible = False
        '
        'chkModality2
        '
        Me.chkModality2.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkModality2.AutoSize = True
        Me.chkModality2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkModality2.Location = New System.Drawing.Point(92, 219)
        Me.chkModality2.Name = "chkModality2"
        Me.chkModality2.Size = New System.Drawing.Size(96, 41)
        Me.chkModality2.TabIndex = 8
        Me.chkModality2.Text = "VIVID"
        Me.chkModality2.UseVisualStyleBackColor = True
        '
        'chkTmscPorterble
        '
        Me.chkTmscPorterble.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkTmscPorterble.AutoSize = True
        Me.chkTmscPorterble.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTmscPorterble.Location = New System.Drawing.Point(177, 219)
        Me.chkTmscPorterble.Name = "chkTmscPorterble"
        Me.chkTmscPorterble.Size = New System.Drawing.Size(87, 41)
        Me.chkTmscPorterble.TabIndex = 9
        Me.chkTmscPorterble.Text = "Xario"
        Me.chkTmscPorterble.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1902, 1003)
        Me.Controls.Add(Me.chkTmscPorterble)
        Me.Controls.Add(Me.chkModality2)
        Me.Controls.Add(Me.chkModality1)
        Me.Controls.Add(Me.chkPorterble)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lblPatientCnt)
        Me.Controls.Add(Me.dtgList)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.msSysMenu)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.msSysMenu
        Me.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "検査一覧 Ver 1.4.0"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dtgList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctxtProcMenu.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.msSysMenu.ResumeLayout(False)
        Me.msSysMenu.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdNextMonth As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdNextDate As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdBeforMonth As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdBeforDate As VIBlend.WinForms.Controls.vButton
    Friend WithEvents dtStudyDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtgList As System.Windows.Forms.DataGridView
    Friend WithEvents txtPatientID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdToDay As VIBlend.WinForms.Controls.vButton
    Friend WithEvents ctxtProcMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents 受付ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 受付取消ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 削除ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 削除取消ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdDelete As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdAccept As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdLogOut As VIBlend.WinForms.Controls.vButton
    Friend WithEvents msSysMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents システムToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ログアウトToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 終了ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents データToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 受付ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 受付取消ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 削除ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 削除取消ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents オーダ明細ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 情報ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents バージョン情報ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents オーダ明細ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmdOrderDetail As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdCanDelete As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdCanAccept As VIBlend.WinForms.Controls.vButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents chkDelete As System.Windows.Forms.CheckBox
    Friend WithEvents chkAccept As System.Windows.Forms.CheckBox
    Friend WithEvents Interval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Interval1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Interval2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Interval3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Interval4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Interval5 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Interval6 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents chkNonDelete As System.Windows.Forms.CheckBox
    Friend WithEvents chkNonAccept As System.Windows.Forms.CheckBox
    Friend WithEvents 画像表示ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 画像表示ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmdEditOrder As VIBlend.WinForms.Controls.vButton
    Friend WithEvents mnuEditOrder As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEditPatient As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmdEditPatient As VIBlend.WinForms.Controls.vButton
    Friend WithEvents txtPatientKana As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbModality As System.Windows.Forms.ComboBox
    Friend WithEvents 部位情報修正ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 患者情報修正ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmdOutCSV As VIBlend.WinForms.Controls.vButton
    Friend WithEvents saveCSV As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblDelete As System.Windows.Forms.Label
    Friend WithEvents lblNonDelete As System.Windows.Forms.Label
    Friend WithEvents lblAccept As System.Windows.Forms.Label
    Friend WithEvents lblNonAccept As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblPatientCnt As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmdCanJisshi As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdJisshi As VIBlend.WinForms.Controls.vButton
    Friend WithEvents 実施ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 実施取消ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents 実施ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 実施取消ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblStudyAccept As System.Windows.Forms.Label
    Friend WithEvents chkStudyAccept As System.Windows.Forms.CheckBox
    Friend WithEvents lblStudyEnd As System.Windows.Forms.Label
    Friend WithEvents chkStudyEnd As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tmrProc As System.Windows.Forms.Timer
    Friend WithEvents 設定ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents 一覧表示列ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkPorterble As System.Windows.Forms.CheckBox
    Friend WithEvents chkModality1 As System.Windows.Forms.CheckBox
    Friend WithEvents chkModality2 As System.Windows.Forms.CheckBox
    Friend WithEvents lblEnd As System.Windows.Forms.Label
    Friend WithEvents chkEnd As System.Windows.Forms.CheckBox
    Friend WithEvents cmdPortable As VIBlend.WinForms.Controls.vButton
    Friend WithEvents chkTmscPorterble As System.Windows.Forms.CheckBox
    Friend WithEvents COL_PATIENT_COMMENT As DataGridViewTextBoxColumn
    Friend WithEvents COL_DEL_FLG As DataGridViewTextBoxColumn
    Friend WithEvents COL_ORDER_COMMENT As DataGridViewTextBoxColumn
    Friend WithEvents COL_STUDY_COMMENT As DataGridViewTextBoxColumn
    Friend WithEvents COL_STATE As DataGridViewTextBoxColumn
    Friend WithEvents COL_STUDY_DIV As DataGridViewTextBoxColumn
    Friend WithEvents COL_PATIENT_ANO As DataGridViewTextBoxColumn
    Friend WithEvents COL_PATIENT_ID As DataGridViewTextBoxColumn
    Friend WithEvents COL_KENSHIN_PATIENT_ID As DataGridViewTextBoxColumn
    Friend WithEvents COL_PATIENT_KANJI As DataGridViewTextBoxColumn
    Friend WithEvents COL_PATIENT_EIJI As DataGridViewTextBoxColumn
    Friend WithEvents COL_PATIENT_KANA As DataGridViewTextBoxColumn
    Friend WithEvents COL_PATIENT_SEX As DataGridViewTextBoxColumn
    Friend WithEvents COL_PATIENT_BIRTH As DataGridViewTextBoxColumn
    Friend WithEvents COL_AGE As DataGridViewTextBoxColumn
    Friend WithEvents COL_DEPT As DataGridViewTextBoxColumn
    Friend WithEvents COL_NYUGAI As DataGridViewTextBoxColumn
    Friend WithEvents COL_WARD As DataGridViewTextBoxColumn
    Friend WithEvents COL_ROOM As DataGridViewTextBoxColumn
    Friend WithEvents COL_REQ_DOC As DataGridViewTextBoxColumn
    Friend WithEvents COL_ORDER_NO As DataGridViewTextBoxColumn
    Friend WithEvents COL_ORDER_DATE As DataGridViewTextBoxColumn
    Friend WithEvents COL_ORDER_TIME As DataGridViewTextBoxColumn
    Friend WithEvents COL_STUDY_DATE As DataGridViewTextBoxColumn
    Friend WithEvents COL_STUDY_TIME As DataGridViewTextBoxColumn
    Friend WithEvents COL_MODALITY As DataGridViewTextBoxColumn
    Friend WithEvents COL_STUDY_NAME As DataGridViewTextBoxColumn
    Friend WithEvents COL_BODY_PART As DataGridViewTextBoxColumn
    Friend WithEvents COL_ORDER_ANO As DataGridViewTextBoxColumn
    Friend WithEvents COL_INS_DATE As DataGridViewTextBoxColumn
    Friend WithEvents COL_INS_TIME As DataGridViewTextBoxColumn
    Friend WithEvents COL_UPD_DATE As DataGridViewTextBoxColumn
    Friend WithEvents COL_UPD_TIME As DataGridViewTextBoxColumn
    Friend WithEvents COL_COMMENT_FLG As DataGridViewTextBoxColumn
End Class
