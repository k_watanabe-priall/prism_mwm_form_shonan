﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCrMenu
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdMobile = New System.Windows.Forms.Button()
        Me.cmdCR = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cmdMobile
        '
        Me.cmdMobile.Font = New System.Drawing.Font("MS UI Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdMobile.Location = New System.Drawing.Point(5, 91)
        Me.cmdMobile.Name = "cmdMobile"
        Me.cmdMobile.Size = New System.Drawing.Size(350, 83)
        Me.cmdMobile.TabIndex = 0
        Me.cmdMobile.Text = "モバイル"
        Me.cmdMobile.UseVisualStyleBackColor = True
        '
        'cmdCR
        '
        Me.cmdCR.Font = New System.Drawing.Font("MS UI Gothic", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdCR.Location = New System.Drawing.Point(5, 2)
        Me.cmdCR.Name = "cmdCR"
        Me.cmdCR.Size = New System.Drawing.Size(350, 83)
        Me.cmdCR.TabIndex = 1
        Me.cmdCR.Text = "コンソール"
        Me.cmdCR.UseVisualStyleBackColor = True
        '
        'frmCrMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(358, 177)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdCR)
        Me.Controls.Add(Me.cmdMobile)
        Me.Name = "frmCrMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cmdMobile As Button
    Friend WithEvents cmdCR As Button
End Class
