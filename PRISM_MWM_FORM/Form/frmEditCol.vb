﻿Public Class frmEditCol

#Region "変数定義"
    Private bolUpd As Boolean = False   '適用ボタン押下フラグ
    Private intSelRow As Integer     '選択行位置退避用
#End Region

#Region "定数定義"
    Private DISP_VISIBLE_TURE As String = "表示する"
    Private DISP_VISIBLE_FALSE As String = "表示しない"
    Private DISP_VISIBLE_NON As String = "未設定"
#End Region

#Region "FormEvents"

#Region "FormLoad"
    Private Sub frmEditCol_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        bolUpd = False

        '///設定情報読込
        Call subGetConfig()
    End Sub
#End Region

#End Region

#Region "ControlEvents"

#Region "一覧クリック時"
    Private Sub dtgListInf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtgListInf.Click

        '///選択データを各テキストに展開
        For Each RowData As DataGridViewRow In dtgListInf.SelectedRows
            intSelRow = RowData.Index
            Me.txtDispColName.Text = dtgListInf.Rows(intSelRow).Cells("COL_LIST_HEADER").Value
            Me.cmbVisible.SelectedItem = dtgListInf.Rows(intSelRow).Cells("COL_LIST_VISIBLE").Value
        Next RowData

    End Sub
#End Region

#Region "閉じるボタン押下時"
    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        If bolUpd = False Then
            If MsgBox("適用未実施の場合、変更内容が破棄されます。" & vbCrLf & "終了しても宜しいですか。", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, "終了確認") = MsgBoxResult.No Then
                Exit Sub
            End If
        End If

        Me.Close()
    End Sub
#End Region

#Region "適用ボタン押下時"
    Private Sub cmdUpd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpd.Click
        '///表示名未入力チェック
        If Me.txtDispColName.Text.Trim = "" Then
            MsgBox("表示名が入力されていません。 入力して下さい。", MsgBoxStyle.Exclamation, "一覧列設定")
            Me.txtDispColName.Focus()
            Exit Sub
        End If

        '///表示可否未選択チェック
        If Me.cmbVisible.SelectedItem = "" Then
            MsgBox("表示可否が選択されていません。 選択して下さい。", MsgBoxStyle.Exclamation, "一覧列設定")
            Me.cmbVisible.Focus()
            Exit Sub
        End If

        '///一覧に表示
        Me.dtgListInf.Rows(intSelRow).Cells("COL_LIST_HEADER").Value = Me.txtDispColName.Text.Trim
        Me.dtgListInf.Rows(intSelRow).Cells("COL_LIST_VISIBLE").Value = Me.cmbVisible.SelectedItem

        '///設定保存
        Call subSaveConfig()

        bolUpd = True
    End Sub
#End Region

#End Region

#Region "Sub Routine"

#Region "設定ファイル読込(subGetConfig)"
    '**********************************************************************
    '* 関数名称　：subGetConfig
    '* 機能概要　：設定ファイル読込
    '* 引　数　　：
    '* 戻り値　　：無
    '* 作成日　　：2011/03/03
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************
    Private Sub subGetConfig()

        Settings.LoadFromXmlFile()

        With Settings.Instance
            Me.dtgListInf.Rows.Add()
 
            '///進捗
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(0).Cells("COL_DISP_INDX").Value = .COL_STATE_DISP_INDX
            Me.dtgListInf.Rows(0).Cells("COL_LIST_HEADER").Value = .COL_STATE_HEADER

            If .COL_STATE_VISIBLE = True Then
                Me.dtgListInf.Rows(0).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_STATE_VISIBLE = False Then
                Me.dtgListInf.Rows(0).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(0).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(0).Cells("COL_LIST_WIDTH").Value = .COL_STATE_WIDTH

            '///検査区分
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(1).Cells("COL_DISP_INDX").Value = .COL_STUDY_DIV_DISP_INDX
            Me.dtgListInf.Rows(1).Cells("COL_LIST_HEADER").Value = .COL_STUDY_DIV_HEADER

            If .COL_STUDY_DIV_VISIBLE = True Then
                Me.dtgListInf.Rows(1).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_STUDY_DIV_VISIBLE = False Then
                Me.dtgListInf.Rows(1).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(1).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(1).Cells("COL_LIST_WIDTH").Value = .COL_STUDY_DIV_WIDTH

            '///検査名
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(2).Cells("COL_DISP_INDX").Value = .COL_STUDY_NAME_DISP_INDX
            Me.dtgListInf.Rows(2).Cells("COL_LIST_HEADER").Value = .COL_STUDY_NAME_HEADER

            If .COL_STUDY_NAME_VISIBLE = True Then
                Me.dtgListInf.Rows(2).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_STUDY_NAME_VISIBLE = False Then
                Me.dtgListInf.Rows(2).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(2).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(2).Cells("COL_LIST_WIDTH").Value = .COL_STUDY_NAME_WIDTH

            '///検査予定日
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(3).Cells("COL_DISP_INDX").Value = .COL_ORDER_DATE_DISP_INDX
            Me.dtgListInf.Rows(3).Cells("COL_LIST_HEADER").Value = .COL_ORDER_DATE_HEADER

            If .COL_ORDER_DATE_VISIBLE = True Then
                Me.dtgListInf.Rows(3).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_ORDER_DATE_VISIBLE = False Then
                Me.dtgListInf.Rows(3).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(3).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(3).Cells("COL_LIST_WIDTH").Value = .COL_ORDER_DATE_WIDTH

            '///検査予定時刻
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(4).Cells("COL_DISP_INDX").Value = .COL_ORDER_TIME_DISP_INDX
            Me.dtgListInf.Rows(4).Cells("COL_LIST_HEADER").Value = .COL_ORDER_TIME_HEADER

            If .COL_ORDER_TIME_VISIBLE = True Then
                Me.dtgListInf.Rows(4).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_ORDER_TIME_VISIBLE = False Then
                Me.dtgListInf.Rows(4).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(4).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(4).Cells("COL_LIST_WIDTH").Value = .COL_ORDER_TIME_WIDTH

            '///検査実施日
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(5).Cells("COL_DISP_INDX").Value = .COL_STUDY_DATE_DISP_INDX
            Me.dtgListInf.Rows(5).Cells("COL_LIST_HEADER").Value = .COL_STUDY_DATE_HEADER

            If .COL_STUDY_DATE_VISIBLE = True Then
                Me.dtgListInf.Rows(5).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_STUDY_DATE_VISIBLE = False Then
                Me.dtgListInf.Rows(5).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(5).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(5).Cells("COL_LIST_WIDTH").Value = .COL_STUDY_DATE_WIDTH

            '///検査時刻
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(6).Cells("COL_DISP_INDX").Value = .COL_STUDY_TIME_DISP_INDX
            Me.dtgListInf.Rows(6).Cells("COL_LIST_HEADER").Value = .COL_STUDY_TIME_HEADER

            If .COL_STUDY_TIME_VISIBLE = True Then
                Me.dtgListInf.Rows(6).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_STUDY_TIME_VISIBLE = False Then
                Me.dtgListInf.Rows(6).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(6).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(6).Cells("COL_LIST_WIDTH").Value = .COL_STUDY_TIME_WIDTH

            '///患者ID
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(7).Cells("COL_DISP_INDX").Value = .COL_PATIENT_ID_DISP_INDX
            Me.dtgListInf.Rows(7).Cells("COL_LIST_HEADER").Value = .COL_PATIENT_ID_HEADER

            If .COL_PATIENT_ID_VISIBLE = True Then
                Me.dtgListInf.Rows(7).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_PATIENT_ID_VISIBLE = False Then
                Me.dtgListInf.Rows(7).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(7).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(7).Cells("COL_LIST_WIDTH").Value = .COL_PATIENT_ID_WIDTH

            '///患者名(漢字)
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(8).Cells("COL_DISP_INDX").Value = .COL_PATIENT_KANJI_DISP_INDX
            Me.dtgListInf.Rows(8).Cells("COL_LIST_HEADER").Value = .COL_PATIENT_KANJI_HEADER

            If .COL_PATIENT_KANJI_VISIBLE = True Then
                Me.dtgListInf.Rows(8).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_PATIENT_KANJI_VISIBLE = False Then
                Me.dtgListInf.Rows(8).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(8).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(8).Cells("COL_LIST_WIDTH").Value = .COL_PATIENT_KANJI_WIDTH

            '///患者名(カナ)
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(9).Cells("COL_DISP_INDX").Value = .COL_PATIENT_KANA_DISP_INDX
            Me.dtgListInf.Rows(9).Cells("COL_LIST_HEADER").Value = .COL_PATIENT_KANA_HEADER

            If .COL_PATIENT_KANA_VISIBLE = True Then
                Me.dtgListInf.Rows(9).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_PATIENT_KANA_VISIBLE = False Then
                Me.dtgListInf.Rows(9).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(9).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(9).Cells("COL_LIST_WIDTH").Value = .COL_PATIENT_KANA_WIDTH

            '///患者名(英字)
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(10).Cells("COL_DISP_INDX").Value = .COL_PATIENT_EIJI_DISP_INDX
            Me.dtgListInf.Rows(10).Cells("COL_LIST_HEADER").Value = .COL_PATIENT_EIJI_HEADER

            If .COL_PATIENT_EIJI_VISIBLE = True Then
                Me.dtgListInf.Rows(10).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_PATIENT_EIJI_VISIBLE = False Then
                Me.dtgListInf.Rows(10).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(10).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(10).Cells("COL_LIST_WIDTH").Value = .COL_PATIENT_EIJI_WIDTH

            '///生年月日
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(11).Cells("COL_DISP_INDX").Value = .COL_PATIENT_BIRTH_DISP_INDX
            Me.dtgListInf.Rows(11).Cells("COL_LIST_HEADER").Value = .COL_PATIENT_BIRTH_HEADER

            If .COL_PATIENT_BIRTH_VISIBLE = True Then
                Me.dtgListInf.Rows(11).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_PATIENT_EIJI_VISIBLE = False Then
                Me.dtgListInf.Rows(11).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(11).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(11).Cells("COL_LIST_WIDTH").Value = .COL_PATIENT_BIRTH_WIDTH

            '///性別
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(12).Cells("COL_DISP_INDX").Value = .COL_PATIENT_SEX_DISP_INDX
            Me.dtgListInf.Rows(12).Cells("COL_LIST_HEADER").Value = .COL_PATIENT_SEX_HEADER

            If .COL_PATIENT_SEX_VISIBLE = True Then
                Me.dtgListInf.Rows(12).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_PATIENT_SEX_VISIBLE = False Then
                Me.dtgListInf.Rows(12).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(12).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(12).Cells("COL_LIST_WIDTH").Value = .COL_PATIENT_SEX_WIDTH

            '///検査部位
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(13).Cells("COL_DISP_INDX").Value = .COL_BODY_PART_DISP_INDX
            Me.dtgListInf.Rows(13).Cells("COL_LIST_HEADER").Value = .COL_BODY_PART_HEADER

            If .COL_BODY_PART_VISIBLE = True Then
                Me.dtgListInf.Rows(13).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_BODY_PART_VISIBLE = False Then
                Me.dtgListInf.Rows(13).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(13).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(13).Cells("COL_LIST_WIDTH").Value = .COL_BODY_PART_WIDTH

            '///モダリティ
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(14).Cells("COL_DISP_INDX").Value = .COL_MODALITY_DISP_INDX
            Me.dtgListInf.Rows(14).Cells("COL_LIST_HEADER").Value = .COL_MODALITY_HEADER

            If .COL_MODALITY_VISIBLE = True Then
                Me.dtgListInf.Rows(14).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_MODALITY_VISIBLE = False Then
                Me.dtgListInf.Rows(14).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(14).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(14).Cells("COL_LIST_WIDTH").Value = .COL_MODALITY_WIDTH

            '///依頼科
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(15).Cells("COL_DISP_INDX").Value = .COL_DEPT_DISP_INDX
            Me.dtgListInf.Rows(15).Cells("COL_LIST_HEADER").Value = .COL_DEPT_HEADER

            If .COL_DEPT_VISIBLE = True Then
                Me.dtgListInf.Rows(15).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_DEPT_VISIBLE = False Then
                Me.dtgListInf.Rows(15).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(15).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(15).Cells("COL_LIST_WIDTH").Value = .COL_DEPT_WIDTH

            '///入外
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(16).Cells("COL_DISP_INDX").Value = .COL_NYUGAI_DISP_INDX
            Me.dtgListInf.Rows(16).Cells("COL_LIST_HEADER").Value = .COL_NYUGAI_HEADER
            Me.dtgListInf.Rows(16).Cells("COL_LIST_VISIBLE").Value = .COL_NYUGAI_VISIBLE

            If .COL_NYUGAI_VISIBLE = True Then
                Me.dtgListInf.Rows(16).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_NYUGAI_VISIBLE = False Then
                Me.dtgListInf.Rows(16).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(16).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(16).Cells("COL_LIST_WIDTH").Value = .COL_NYUGAI_WIDTH

            '///病棟
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(17).Cells("COL_DISP_INDX").Value = .COL_WARD_DISP_INDX
            Me.dtgListInf.Rows(17).Cells("COL_LIST_HEADER").Value = .COL_WARD_HEADER

            If .COL_WARD_VISIBLE = True Then
                Me.dtgListInf.Rows(17).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_WARD_VISIBLE = False Then
                Me.dtgListInf.Rows(17).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(17).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(17).Cells("COL_LIST_WIDTH").Value = .COL_WARD_WIDTH

            '///病室
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(18).Cells("COL_DISP_INDX").Value = .COL_ROOM_DISP_INDX
            Me.dtgListInf.Rows(18).Cells("COL_LIST_HEADER").Value = .COL_ROOM_HEADER

            If .COL_ROOM_VISIBLE = True Then
                Me.dtgListInf.Rows(18).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_ROOM_VISIBLE = False Then
                Me.dtgListInf.Rows(18).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(18).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(18).Cells("COL_LIST_WIDTH").Value = .COL_ROOM_WIDTH

            '///依頼医
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(19).Cells("COL_DISP_INDX").Value = .COL_REQ_DOC_DISP_INDX
            Me.dtgListInf.Rows(19).Cells("COL_LIST_HEADER").Value = .COL_REQ_DOC_HEADER

            If .COL_REQ_DOC_VISIBLE = True Then
                Me.dtgListInf.Rows(19).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_REQ_DOC_VISIBLE = False Then
                Me.dtgListInf.Rows(19).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(19).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(19).Cells("COL_LIST_WIDTH").Value = .COL_REQ_DOC_WIDTH

            '///削除フラグ
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(20).Cells("COL_DISP_INDX").Value = .COL_DEL_FLG_DISP_INDX
            Me.dtgListInf.Rows(20).Cells("COL_LIST_HEADER").Value = .COL_DEL_FLG_HEADER

            If .COL_DEL_FLG_VISIBLE = True Then
                Me.dtgListInf.Rows(20).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_DEL_FLG_VISIBLE = False Then
                Me.dtgListInf.Rows(20).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(20).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(20).Cells("COL_LIST_WIDTH").Value = .COL_DEL_FLG_WIDTH

            '///StudyInstanceUID
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(21).Cells("COL_DISP_INDX").Value = .COL_STUDY_INSTANCE_UID_DISP_INDX
            Me.dtgListInf.Rows(21).Cells("COL_LIST_HEADER").Value = .COL_STUDY_INSTANCE_UID_HEADER

            If .COL_STUDY_INSTANCE_UID_VISIBLE = True Then
                Me.dtgListInf.Rows(21).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_STUDY_INSTANCE_UID_VISIBLE = False Then
                Me.dtgListInf.Rows(21).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(21).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(21).Cells("COL_LIST_WIDTH").Value = .COL_STUDY_INSTANCE_UID_WIDTH

            '///オーダ登録番号
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(22).Cells("COL_DISP_INDX").Value = .COL_ORDER_ANO_DISP_INDX
            Me.dtgListInf.Rows(22).Cells("COL_LIST_HEADER").Value = .COL_ORDER_ANO_HEADER

            If .COL_ORDER_ANO_VISIBLE = True Then
                Me.dtgListInf.Rows(22).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_ORDER_ANO_VISIBLE = False Then
                Me.dtgListInf.Rows(22).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(22).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(22).Cells("COL_LIST_WIDTH").Value = .COL_ORDER_ANO_WIDTH

            '///オーダ番号
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(23).Cells("COL_DISP_INDX").Value = .COL_ORDER_NO_DISP_INDX
            Me.dtgListInf.Rows(23).Cells("COL_LIST_HEADER").Value = .COL_ORDER_NO_HEADER

            If .COL_ORDER_NO_VISIBLE = True Then
                Me.dtgListInf.Rows(23).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_ORDER_NO_VISIBLE = False Then
                Me.dtgListInf.Rows(23).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(23).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(23).Cells("COL_LIST_WIDTH").Value = .COL_ORDER_NO_WIDTH

            '///患者登録番号
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(24).Cells("COL_DISP_INDX").Value = .COL_PATIENT_ANO_DISP_INDX
            Me.dtgListInf.Rows(24).Cells("COL_LIST_HEADER").Value = .COL_PATIENT_ANO_HEADER

            If .COL_PATIENT_ANO_VISIBLE = True Then
                Me.dtgListInf.Rows(24).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_PATIENT_ANO_VISIBLE = False Then
                Me.dtgListInf.Rows(24).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(24).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(24).Cells("COL_LIST_WIDTH").Value = .COL_PATIENT_ANO_WIDTH

            '///オーダコメント
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(25).Cells("COL_DISP_INDX").Value = .COL_ORDER_COMMENT_DISP_INDX
            Me.dtgListInf.Rows(25).Cells("COL_LIST_HEADER").Value = .COL_ORDER_COMMENT_HEADER

            If .COL_ORDER_COMMENT_VISIBLE = True Then
                Me.dtgListInf.Rows(25).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_ORDER_COMMENT_VISIBLE = False Then
                Me.dtgListInf.Rows(25).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(25).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(25).Cells("COL_LIST_WIDTH").Value = .COL_ORDER_COMMENT_WIDTH

            '///患者コメント
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(26).Cells("COL_DISP_INDX").Value = .COL_PATIENT_COMMENT_DISP_INDX
            Me.dtgListInf.Rows(26).Cells("COL_LIST_HEADER").Value = .COL_PATIENT_COMMENT_HEADER

            If .COL_PATIENT_COMMENT_VISIBLE = True Then
                Me.dtgListInf.Rows(26).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_PATIENT_COMMENT_VISIBLE = False Then
                Me.dtgListInf.Rows(26).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(26).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(26).Cells("COL_LIST_WIDTH").Value = .COL_PATIENT_COMMENT_WIDTH

            '///検査コメント
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(27).Cells("COL_DISP_INDX").Value = .COL_STUDY_COMMENT_DISP_INDX
            Me.dtgListInf.Rows(27).Cells("COL_LIST_HEADER").Value = .COL_STUDY_COMMENT_HEADER

            If .COL_STUDY_COMMENT_VISIBLE = True Then
                Me.dtgListInf.Rows(27).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_STUDY_COMMENT_VISIBLE = False Then
                Me.dtgListInf.Rows(27).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(27).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(27).Cells("COL_LIST_WIDTH").Value = .COL_STUDY_COMMENT_WIDTH

            '///コメントフラグ
            Me.dtgListInf.Rows.Add()
            Me.dtgListInf.Rows(28).Cells("COL_DISP_INDX").Value = .COL_COMMENT_FLG_DISP_INDX
            Me.dtgListInf.Rows(28).Cells("COL_LIST_HEADER").Value = .COL_COMMENT_FLG_HEADER

            If .COL_COMMENT_FLG_VISIBLE = True Then
                Me.dtgListInf.Rows(28).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE
            ElseIf .COL_COMMENT_FLG_VISIBLE = False Then
                Me.dtgListInf.Rows(28).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_FALSE
            Else
                Me.dtgListInf.Rows(28).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_NON
            End If

            Me.dtgListInf.Rows(28).Cells("COL_LIST_WIDTH").Value = .COL_COMMENT_FLG_WIDTH

        End With
    End Sub
#End Region

#Region "設定ファイル保存(subSaveConfig)"
    '**********************************************************************
    '* 関数名称　：subSaveConfig
    '* 機能概要　：設定ファイル保存
    '* 引　数　　：
    '* 戻り値　　：無
    '* 作成日　　：2011/03/03
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************
    Private Sub subSaveConfig()
        With Settings.Instance
 
            '///進捗
            .COL_STATE_DISP_INDX = Me.dtgListInf.Rows(0).Cells("COL_DISP_INDX").Value
            .COL_STATE_HEADER = Me.dtgListInf.Rows(0).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(0).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_STATE_VISIBLE = True
            Else
                .COL_STATE_VISIBLE = False
            End If

            .COL_STATE_WIDTH = Me.dtgListInf.Rows(0).Cells("COL_LIST_WIDTH").Value

            '///検査区分
            .COL_STUDY_DIV_DISP_INDX = Me.dtgListInf.Rows(1).Cells("COL_DISP_INDX").Value
            .COL_STUDY_DIV_HEADER = Me.dtgListInf.Rows(1).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(1).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_STUDY_DIV_VISIBLE = True
            Else
                .COL_STUDY_DIV_VISIBLE = False
            End If

            .COL_STUDY_DIV_WIDTH = Me.dtgListInf.Rows(1).Cells("COL_LIST_WIDTH").Value

            '///検査名
            .COL_STUDY_NAME_DISP_INDX = Me.dtgListInf.Rows(2).Cells("COL_DISP_INDX").Value
            .COL_STUDY_NAME_HEADER = Me.dtgListInf.Rows(2).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(2).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_STUDY_NAME_VISIBLE = True
            Else
                .COL_STUDY_NAME_VISIBLE = False
            End If

            .COL_STUDY_NAME_WIDTH = Me.dtgListInf.Rows(2).Cells("COL_LIST_WIDTH").Value

            '///検査予定日
            .COL_ORDER_DATE_DISP_INDX = Me.dtgListInf.Rows(3).Cells("COL_DISP_INDX").Value
            .COL_ORDER_DATE_HEADER = Me.dtgListInf.Rows(3).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(3).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_ORDER_DATE_VISIBLE = True
            Else
                .COL_ORDER_DATE_VISIBLE = False
            End If

            .COL_ORDER_DATE_WIDTH = Me.dtgListInf.Rows(3).Cells("COL_LIST_WIDTH").Value

            '///検査予定時刻
            .COL_ORDER_TIME_DISP_INDX = Me.dtgListInf.Rows(4).Cells("COL_DISP_INDX").Value
            .COL_ORDER_TIME_HEADER = Me.dtgListInf.Rows(4).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(4).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_ORDER_TIME_VISIBLE = True
            Else
                .COL_ORDER_TIME_VISIBLE = False
            End If

            .COL_ORDER_TIME_WIDTH = Me.dtgListInf.Rows(4).Cells("COL_LIST_WIDTH").Value

            '///検査実施日
            .COL_STUDY_DATE_DISP_INDX = Me.dtgListInf.Rows(5).Cells("COL_DISP_INDX").Value
            .COL_STUDY_DATE_HEADER = Me.dtgListInf.Rows(5).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(5).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_STUDY_DATE_VISIBLE = True
            Else
                .COL_STUDY_DATE_VISIBLE = False
            End If

            .COL_STUDY_DATE_WIDTH = Me.dtgListInf.Rows(5).Cells("COL_LIST_WIDTH").Value


            '///検査実施時刻
            .COL_STUDY_TIME_DISP_INDX = Me.dtgListInf.Rows(6).Cells("COL_DISP_INDX").Value
            .COL_STUDY_TIME_HEADER = Me.dtgListInf.Rows(6).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(6).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_STUDY_TIME_VISIBLE = True
            Else
                .COL_STUDY_TIME_VISIBLE = False
            End If

            .COL_STUDY_TIME_WIDTH = Me.dtgListInf.Rows(6).Cells("COL_LIST_WIDTH").Value

            '///患者ID
            .COL_PATIENT_ID_DISP_INDX = Me.dtgListInf.Rows(7).Cells("COL_DISP_INDX").Value
            .COL_PATIENT_ID_HEADER = Me.dtgListInf.Rows(7).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(7).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_PATIENT_ID_VISIBLE = True
            Else
                .COL_PATIENT_ID_VISIBLE = False
            End If

            .COL_PATIENT_ID_WIDTH = Me.dtgListInf.Rows(7).Cells("COL_LIST_WIDTH").Value

            '///患者氏名(漢字)
            .COL_PATIENT_KANJI_DISP_INDX = Me.dtgListInf.Rows(8).Cells("COL_DISP_INDX").Value
            .COL_PATIENT_KANJI_HEADER = Me.dtgListInf.Rows(8).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(8).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_PATIENT_KANJI_VISIBLE = True
            Else
                .COL_PATIENT_KANJI_VISIBLE = False
            End If

            .COL_PATIENT_KANJI_WIDTH = Me.dtgListInf.Rows(8).Cells("COL_LIST_WIDTH").Value

            '///患者氏名(カナ)
            .COL_PATIENT_KANA_DISP_INDX = Me.dtgListInf.Rows(9).Cells("COL_DISP_INDX").Value
            .COL_PATIENT_KANA_HEADER = Me.dtgListInf.Rows(9).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(9).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_PATIENT_KANA_VISIBLE = True
            Else
                .COL_PATIENT_KANA_VISIBLE = False
            End If

            .COL_PATIENT_KANA_WIDTH = Me.dtgListInf.Rows(9).Cells("COL_LIST_WIDTH").Value

            '///患者氏名(英字)
            .COL_PATIENT_EIJI_DISP_INDX = Me.dtgListInf.Rows(10).Cells("COL_DISP_INDX").Value
            .COL_PATIENT_EIJI_HEADER = Me.dtgListInf.Rows(10).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(10).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_PATIENT_EIJI_VISIBLE = True
            Else
                .COL_PATIENT_EIJI_VISIBLE = False
            End If

            .COL_PATIENT_EIJI_WIDTH = Me.dtgListInf.Rows(10).Cells("COL_LIST_WIDTH").Value

            '///生年月日
            .COL_PATIENT_BIRTH_DISP_INDX = Me.dtgListInf.Rows(11).Cells("COL_DISP_INDX").Value
            .COL_PATIENT_BIRTH_HEADER = Me.dtgListInf.Rows(11).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(11).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_PATIENT_BIRTH_VISIBLE = True
            Else
                .COL_PATIENT_BIRTH_VISIBLE = False
            End If

            .COL_PATIENT_BIRTH_WIDTH = Me.dtgListInf.Rows(11).Cells("COL_LIST_WIDTH").Value

            '///性別
            .COL_PATIENT_SEX_DISP_INDX = Me.dtgListInf.Rows(12).Cells("COL_DISP_INDX").Value
            .COL_PATIENT_SEX_HEADER = Me.dtgListInf.Rows(12).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(12).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_PATIENT_SEX_VISIBLE = True
            Else
                .COL_PATIENT_SEX_VISIBLE = False
            End If

            .COL_PATIENT_SEX_WIDTH = Me.dtgListInf.Rows(12).Cells("COL_LIST_WIDTH").Value

            '///検査部位
            .COL_BODY_PART_DISP_INDX = Me.dtgListInf.Rows(13).Cells("COL_DISP_INDX").Value
            .COL_BODY_PART_HEADER = Me.dtgListInf.Rows(13).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(13).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_BODY_PART_VISIBLE = True
            Else
                .COL_BODY_PART_VISIBLE = False
            End If

            .COL_BODY_PART_WIDTH = Me.dtgListInf.Rows(13).Cells("COL_LIST_WIDTH").Value

            '///モダリティ
            .COL_MODALITY_DISP_INDX = Me.dtgListInf.Rows(14).Cells("COL_DISP_INDX").Value
            .COL_MODALITY_HEADER = Me.dtgListInf.Rows(14).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(14).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_MODALITY_VISIBLE = True
            Else
                .COL_MODALITY_VISIBLE = False
            End If

            .COL_MODALITY_WIDTH = Me.dtgListInf.Rows(14).Cells("COL_LIST_WIDTH").Value

            '///依頼科
            .COL_DEPT_DISP_INDX = Me.dtgListInf.Rows(15).Cells("COL_DISP_INDX").Value
            .COL_DEPT_HEADER = Me.dtgListInf.Rows(15).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(15).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_DEPT_VISIBLE = True
            Else
                .COL_DEPT_VISIBLE = False
            End If

            .COL_DEPT_WIDTH = Me.dtgListInf.Rows(15).Cells("COL_LIST_WIDTH").Value

            '///入外
            .COL_NYUGAI_DISP_INDX = Me.dtgListInf.Rows(16).Cells("COL_DISP_INDX").Value
            .COL_NYUGAI_HEADER = Me.dtgListInf.Rows(16).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(16).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_NYUGAI_VISIBLE = True
            Else
                .COL_NYUGAI_VISIBLE = False
            End If

            .COL_NYUGAI_WIDTH = Me.dtgListInf.Rows(16).Cells("COL_LIST_WIDTH").Value

            '///病棟
            .COL_WARD_DISP_INDX = Me.dtgListInf.Rows(17).Cells("COL_DISP_INDX").Value
            .COL_WARD_HEADER = Me.dtgListInf.Rows(17).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(17).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_WARD_VISIBLE = True
            Else
                .COL_WARD_VISIBLE = False
            End If

            .COL_WARD_WIDTH = Me.dtgListInf.Rows(17).Cells("COL_LIST_WIDTH").Value

            '///病室
            .COL_ROOM_DISP_INDX = Me.dtgListInf.Rows(18).Cells("COL_DISP_INDX").Value
            .COL_ROOM_HEADER = Me.dtgListInf.Rows(18).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(18).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_ROOM_VISIBLE = True
            Else
                .COL_ROOM_VISIBLE = False
            End If

            .COL_ROOM_WIDTH = Me.dtgListInf.Rows(18).Cells("COL_LIST_WIDTH").Value

            '///依頼医
            .COL_REQ_DOC_DISP_INDX = Me.dtgListInf.Rows(20).Cells("COL_DISP_INDX").Value
            .COL_REQ_DOC_HEADER = Me.dtgListInf.Rows(20).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(20).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_REQ_DOC_VISIBLE = True
            Else
                .COL_REQ_DOC_VISIBLE = False
            End If

            .COL_REQ_DOC_WIDTH = Me.dtgListInf.Rows(20).Cells("COL_LIST_WIDTH").Value

            '///削除フラグ
            .COL_DEL_FLG_DISP_INDX = Me.dtgListInf.Rows(21).Cells("COL_DISP_INDX").Value
            .COL_DEL_FLG_HEADER = Me.dtgListInf.Rows(21).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(21).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_DEL_FLG_VISIBLE = True
            Else
                .COL_DEL_FLG_VISIBLE = False
            End If

            .COL_DEL_FLG_WIDTH = Me.dtgListInf.Rows(21).Cells("COL_LIST_WIDTH").Value

            '///オーダ登録番号
            .COL_ORDER_ANO_DISP_INDX = Me.dtgListInf.Rows(22).Cells("COL_DISP_INDX").Value
            .COL_ORDER_ANO_HEADER = Me.dtgListInf.Rows(22).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(22).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_ORDER_ANO_VISIBLE = True
            Else
                .COL_ORDER_ANO_VISIBLE = False
            End If

            .COL_ORDER_ANO_WIDTH = Me.dtgListInf.Rows(22).Cells("COL_LIST_WIDTH").Value

            '///オーダ番号
            .COL_ORDER_NO_DISP_INDX = Me.dtgListInf.Rows(23).Cells("COL_DISP_INDX").Value
            .COL_ORDER_NO_HEADER = Me.dtgListInf.Rows(23).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(23).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_ORDER_NO_VISIBLE = True
            Else
                .COL_ORDER_NO_VISIBLE = False
            End If

            .COL_ORDER_NO_WIDTH = Me.dtgListInf.Rows(23).Cells("COL_LIST_WIDTH").Value

            '///患者登録番号
            .COL_PATIENT_ANO_DISP_INDX = Me.dtgListInf.Rows(24).Cells("COL_DISP_INDX").Value
            .COL_PATIENT_ANO_HEADER = Me.dtgListInf.Rows(24).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(24).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_PATIENT_ANO_VISIBLE = True
            Else
                .COL_PATIENT_ANO_VISIBLE = False
            End If

            .COL_PATIENT_ANO_WIDTH = Me.dtgListInf.Rows(24).Cells("COL_LIST_WIDTH").Value

            '///オーダコメント
            .COL_ORDER_COMMENT_DISP_INDX = Me.dtgListInf.Rows(25).Cells("COL_DISP_INDX").Value
            .COL_ORDER_COMMENT_HEADER = Me.dtgListInf.Rows(25).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(25).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_ORDER_COMMENT_VISIBLE = True
            Else
                .COL_ORDER_COMMENT_VISIBLE = False
            End If

            .COL_ORDER_COMMENT_WIDTH = Me.dtgListInf.Rows(25).Cells("COL_LIST_WIDTH").Value

            '///患者コメント
            .COL_PATIENT_COMMENT_DISP_INDX = Me.dtgListInf.Rows(26).Cells("COL_DISP_INDX").Value
            .COL_PATIENT_COMMENT_HEADER = Me.dtgListInf.Rows(26).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(26).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_PATIENT_COMMENT_VISIBLE = True
            Else
                .COL_PATIENT_COMMENT_VISIBLE = False
            End If

            .COL_PATIENT_COMMENT_WIDTH = Me.dtgListInf.Rows(26).Cells("COL_LIST_WIDTH").Value

            '///検査コメント
            .COL_STUDY_COMMENT_DISP_INDX = Me.dtgListInf.Rows(27).Cells("COL_DISP_INDX").Value
            .COL_STUDY_COMMENT_HEADER = Me.dtgListInf.Rows(27).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(27).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_STUDY_COMMENT_VISIBLE = True
            Else
                .COL_STUDY_COMMENT_VISIBLE = False
            End If

            .COL_STUDY_COMMENT_WIDTH = Me.dtgListInf.Rows(27).Cells("COL_LIST_WIDTH").Value

            '///コメント
            .COL_COMMENT_FLG_DISP_INDX = Me.dtgListInf.Rows(28).Cells("COL_DISP_INDX").Value
            .COL_COMMENT_FLG_HEADER = Me.dtgListInf.Rows(28).Cells("COL_LIST_HEADER").Value

            If Me.dtgListInf.Rows(28).Cells("COL_LIST_VISIBLE").Value = DISP_VISIBLE_TURE Then
                .COL_COMMENT_FLG_VISIBLE = True
            Else
                .COL_COMMENT_FLG_VISIBLE = False
            End If

            .COL_COMMENT_FLG_WIDTH = Me.dtgListInf.Rows(28).Cells("COL_LIST_WIDTH").Value

        End With
        Settings.SaveToXmlFile()

    End Sub
#End Region

#End Region

 
End Class