﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOrderDetails
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrderDetails))
        Me.grpPatientInfo = New System.Windows.Forms.GroupBox()
        Me.cmdPatientCommentSave = New VIBlend.WinForms.Controls.vButton()
        Me.cmdPatientCommentEdit = New VIBlend.WinForms.Controls.vButton()
        Me.txtPatientComment = New System.Windows.Forms.RichTextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtAge = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtBirth = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtSex = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtPatientName_Eiji = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPatientName_Half_Kana = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPatientName_Kanji = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtPatientID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grpOrderInfo = New System.Windows.Forms.GroupBox()
        Me.lblState = New System.Windows.Forms.Label()
        Me.txtOrderComment = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtBodyPart = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cmdOrderCommentSave = New VIBlend.WinForms.Controls.vButton()
        Me.cmdExcuteCommentEdit = New VIBlend.WinForms.Controls.vButton()
        Me.txtExcuteComment = New System.Windows.Forms.RichTextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lstHist = New System.Windows.Forms.ListBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtModality = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtExcuteTime = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtExcuteDate = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtOrderTime = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtOrderDate = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.cmdClose = New VIBlend.WinForms.Controls.vButton()
        Me.cmdEditOrder = New VIBlend.WinForms.Controls.vButton()
        Me.toolchipDetail = New System.Windows.Forms.ToolTip(Me.components)
        Me.grpPatientInfo.SuspendLayout()
        Me.grpOrderInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpPatientInfo
        '
        Me.grpPatientInfo.Controls.Add(Me.cmdPatientCommentSave)
        Me.grpPatientInfo.Controls.Add(Me.cmdPatientCommentEdit)
        Me.grpPatientInfo.Controls.Add(Me.txtPatientComment)
        Me.grpPatientInfo.Controls.Add(Me.Label22)
        Me.grpPatientInfo.Controls.Add(Me.txtAge)
        Me.grpPatientInfo.Controls.Add(Me.Label8)
        Me.grpPatientInfo.Controls.Add(Me.txtBirth)
        Me.grpPatientInfo.Controls.Add(Me.Label7)
        Me.grpPatientInfo.Controls.Add(Me.txtSex)
        Me.grpPatientInfo.Controls.Add(Me.Label6)
        Me.grpPatientInfo.Controls.Add(Me.txtPatientName_Eiji)
        Me.grpPatientInfo.Controls.Add(Me.Label5)
        Me.grpPatientInfo.Controls.Add(Me.txtPatientName_Half_Kana)
        Me.grpPatientInfo.Controls.Add(Me.Label3)
        Me.grpPatientInfo.Controls.Add(Me.txtPatientName_Kanji)
        Me.grpPatientInfo.Controls.Add(Me.Label2)
        Me.grpPatientInfo.Controls.Add(Me.txtPatientID)
        Me.grpPatientInfo.Controls.Add(Me.Label1)
        Me.grpPatientInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.grpPatientInfo.ForeColor = System.Drawing.Color.DodgerBlue
        Me.grpPatientInfo.Location = New System.Drawing.Point(4, 12)
        Me.grpPatientInfo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.grpPatientInfo.Name = "grpPatientInfo"
        Me.grpPatientInfo.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.grpPatientInfo.Size = New System.Drawing.Size(849, 181)
        Me.grpPatientInfo.TabIndex = 0
        Me.grpPatientInfo.TabStop = False
        Me.grpPatientInfo.Text = "患者情報"
        '
        'cmdPatientCommentSave
        '
        Me.cmdPatientCommentSave.BackColor = System.Drawing.Color.Transparent
        Me.cmdPatientCommentSave.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdPatientCommentSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdPatientCommentSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdPatientCommentSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPatientCommentSave.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdPatientCommentSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPatientCommentSave.Location = New System.Drawing.Point(748, 18)
        Me.cmdPatientCommentSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdPatientCommentSave.Name = "cmdPatientCommentSave"
        Me.cmdPatientCommentSave.PressedTextColor = System.Drawing.Color.Black
        Me.cmdPatientCommentSave.RoundedCornersMask = CType(15, Byte)
        Me.cmdPatientCommentSave.RoundedCornersRadius = 8
        Me.cmdPatientCommentSave.Size = New System.Drawing.Size(74, 20)
        Me.cmdPatientCommentSave.StretchImage = True
        Me.cmdPatientCommentSave.StyleKey = "Button"
        Me.cmdPatientCommentSave.TabIndex = 16
        Me.cmdPatientCommentSave.Text = "保　存"
        Me.cmdPatientCommentSave.UseThemeTextColor = False
        Me.cmdPatientCommentSave.UseVisualStyleBackColor = False
        Me.cmdPatientCommentSave.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'cmdPatientCommentEdit
        '
        Me.cmdPatientCommentEdit.BackColor = System.Drawing.Color.Transparent
        Me.cmdPatientCommentEdit.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdPatientCommentEdit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdPatientCommentEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdPatientCommentEdit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdPatientCommentEdit.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdPatientCommentEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPatientCommentEdit.Location = New System.Drawing.Point(666, 18)
        Me.cmdPatientCommentEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdPatientCommentEdit.Name = "cmdPatientCommentEdit"
        Me.cmdPatientCommentEdit.PressedTextColor = System.Drawing.Color.Black
        Me.cmdPatientCommentEdit.RoundedCornersMask = CType(15, Byte)
        Me.cmdPatientCommentEdit.RoundedCornersRadius = 8
        Me.cmdPatientCommentEdit.Size = New System.Drawing.Size(74, 20)
        Me.cmdPatientCommentEdit.StretchImage = True
        Me.cmdPatientCommentEdit.StyleKey = "Button"
        Me.cmdPatientCommentEdit.TabIndex = 15
        Me.cmdPatientCommentEdit.Text = "編　集"
        Me.cmdPatientCommentEdit.UseThemeTextColor = False
        Me.cmdPatientCommentEdit.UseVisualStyleBackColor = False
        Me.cmdPatientCommentEdit.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'txtPatientComment
        '
        Me.txtPatientComment.BackColor = System.Drawing.Color.LightYellow
        Me.txtPatientComment.Enabled = False
        Me.txtPatientComment.ImeMode = System.Windows.Forms.ImeMode.Hiragana
        Me.txtPatientComment.Location = New System.Drawing.Point(564, 44)
        Me.txtPatientComment.Name = "txtPatientComment"
        Me.txtPatientComment.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Horizontal
        Me.txtPatientComment.Size = New System.Drawing.Size(275, 128)
        Me.txtPatientComment.TabIndex = 17
        Me.txtPatientComment.Text = ""
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label22.Location = New System.Drawing.Point(561, 24)
        Me.Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(80, 16)
        Me.Label22.TabIndex = 14
        Me.Label22.Text = "患者コメント"
        '
        'txtAge
        '
        Me.txtAge.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtAge.Location = New System.Drawing.Point(401, 113)
        Me.txtAge.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtAge.Name = "txtAge"
        Me.txtAge.ReadOnly = True
        Me.txtAge.Size = New System.Drawing.Size(65, 22)
        Me.txtAge.TabIndex = 11
        Me.txtAge.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(272, 116)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(83, 16)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "検査時年齢"
        '
        'txtBirth
        '
        Me.txtBirth.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtBirth.Location = New System.Drawing.Point(173, 135)
        Me.txtBirth.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtBirth.Name = "txtBirth"
        Me.txtBirth.ReadOnly = True
        Me.txtBirth.Size = New System.Drawing.Size(125, 22)
        Me.txtBirth.TabIndex = 13
        Me.txtBirth.TabStop = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label7.Location = New System.Drawing.Point(31, 138)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 16)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "生年月日"
        '
        'txtSex
        '
        Me.txtSex.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtSex.Location = New System.Drawing.Point(173, 113)
        Me.txtSex.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtSex.Name = "txtSex"
        Me.txtSex.ReadOnly = True
        Me.txtSex.Size = New System.Drawing.Size(65, 22)
        Me.txtSex.TabIndex = 9
        Me.txtSex.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(31, 116)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 16)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "性　別"
        '
        'txtPatientName_Eiji
        '
        Me.txtPatientName_Eiji.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtPatientName_Eiji.Location = New System.Drawing.Point(173, 91)
        Me.txtPatientName_Eiji.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtPatientName_Eiji.Name = "txtPatientName_Eiji"
        Me.txtPatientName_Eiji.ReadOnly = True
        Me.txtPatientName_Eiji.Size = New System.Drawing.Size(328, 22)
        Me.txtPatientName_Eiji.TabIndex = 7
        Me.txtPatientName_Eiji.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(31, 94)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(98, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "患者英字氏名"
        '
        'txtPatientName_Half_Kana
        '
        Me.txtPatientName_Half_Kana.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtPatientName_Half_Kana.ImeMode = System.Windows.Forms.ImeMode.KatakanaHalf
        Me.txtPatientName_Half_Kana.Location = New System.Drawing.Point(173, 65)
        Me.txtPatientName_Half_Kana.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtPatientName_Half_Kana.Name = "txtPatientName_Half_Kana"
        Me.txtPatientName_Half_Kana.ReadOnly = True
        Me.txtPatientName_Half_Kana.Size = New System.Drawing.Size(328, 22)
        Me.txtPatientName_Half_Kana.TabIndex = 5
        Me.txtPatientName_Half_Kana.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(31, 68)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 16)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "患者半角カナ氏名"
        '
        'txtPatientName_Kanji
        '
        Me.txtPatientName_Kanji.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtPatientName_Kanji.Location = New System.Drawing.Point(173, 43)
        Me.txtPatientName_Kanji.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtPatientName_Kanji.Name = "txtPatientName_Kanji"
        Me.txtPatientName_Kanji.ReadOnly = True
        Me.txtPatientName_Kanji.Size = New System.Drawing.Size(328, 22)
        Me.txtPatientName_Kanji.TabIndex = 3
        Me.txtPatientName_Kanji.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(31, 46)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(98, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "患者漢字氏名"
        '
        'txtPatientID
        '
        Me.txtPatientID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtPatientID.Location = New System.Drawing.Point(173, 21)
        Me.txtPatientID.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtPatientID.Name = "txtPatientID"
        Me.txtPatientID.ReadOnly = True
        Me.txtPatientID.Size = New System.Drawing.Size(212, 22)
        Me.txtPatientID.TabIndex = 1
        Me.txtPatientID.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(31, 24)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "患者ID"
        '
        'grpOrderInfo
        '
        Me.grpOrderInfo.Controls.Add(Me.lblState)
        Me.grpOrderInfo.Controls.Add(Me.txtOrderComment)
        Me.grpOrderInfo.Controls.Add(Me.Label9)
        Me.grpOrderInfo.Controls.Add(Me.txtBodyPart)
        Me.grpOrderInfo.Controls.Add(Me.Label4)
        Me.grpOrderInfo.Controls.Add(Me.cmdOrderCommentSave)
        Me.grpOrderInfo.Controls.Add(Me.cmdExcuteCommentEdit)
        Me.grpOrderInfo.Controls.Add(Me.txtExcuteComment)
        Me.grpOrderInfo.Controls.Add(Me.Label24)
        Me.grpOrderInfo.Controls.Add(Me.lstHist)
        Me.grpOrderInfo.Controls.Add(Me.Label23)
        Me.grpOrderInfo.Controls.Add(Me.txtModality)
        Me.grpOrderInfo.Controls.Add(Me.Label19)
        Me.grpOrderInfo.Controls.Add(Me.txtExcuteTime)
        Me.grpOrderInfo.Controls.Add(Me.Label17)
        Me.grpOrderInfo.Controls.Add(Me.txtExcuteDate)
        Me.grpOrderInfo.Controls.Add(Me.Label18)
        Me.grpOrderInfo.Controls.Add(Me.txtOrderTime)
        Me.grpOrderInfo.Controls.Add(Me.Label16)
        Me.grpOrderInfo.Controls.Add(Me.txtOrderDate)
        Me.grpOrderInfo.Controls.Add(Me.Label15)
        Me.grpOrderInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.grpOrderInfo.ForeColor = System.Drawing.Color.DodgerBlue
        Me.grpOrderInfo.Location = New System.Drawing.Point(4, 199)
        Me.grpOrderInfo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.grpOrderInfo.Name = "grpOrderInfo"
        Me.grpOrderInfo.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.grpOrderInfo.Size = New System.Drawing.Size(849, 329)
        Me.grpOrderInfo.TabIndex = 1
        Me.grpOrderInfo.TabStop = False
        Me.grpOrderInfo.Text = "オーダ情報"
        '
        'lblState
        '
        Me.lblState.AutoSize = True
        Me.lblState.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblState.Location = New System.Drawing.Point(30, 112)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(0, 24)
        Me.lblState.TabIndex = 2
        '
        'txtOrderComment
        '
        Me.txtOrderComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtOrderComment.Location = New System.Drawing.Point(177, 272)
        Me.txtOrderComment.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtOrderComment.Multiline = True
        Me.txtOrderComment.Name = "txtOrderComment"
        Me.txtOrderComment.ReadOnly = True
        Me.txtOrderComment.Size = New System.Drawing.Size(377, 51)
        Me.txtOrderComment.TabIndex = 16
        Me.txtOrderComment.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(35, 272)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(84, 16)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "オーダコメント"
        '
        'txtBodyPart
        '
        Me.txtBodyPart.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtBodyPart.Location = New System.Drawing.Point(177, 213)
        Me.txtBodyPart.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtBodyPart.Multiline = True
        Me.txtBodyPart.Name = "txtBodyPart"
        Me.txtBodyPart.ReadOnly = True
        Me.txtBodyPart.Size = New System.Drawing.Size(377, 53)
        Me.txtBodyPart.TabIndex = 14
        Me.txtBodyPart.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(35, 213)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 16)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "検査部位"
        '
        'cmdOrderCommentSave
        '
        Me.cmdOrderCommentSave.BackColor = System.Drawing.Color.Transparent
        Me.cmdOrderCommentSave.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdOrderCommentSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdOrderCommentSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdOrderCommentSave.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOrderCommentSave.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdOrderCommentSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOrderCommentSave.Location = New System.Drawing.Point(748, 121)
        Me.cmdOrderCommentSave.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdOrderCommentSave.Name = "cmdOrderCommentSave"
        Me.cmdOrderCommentSave.PressedTextColor = System.Drawing.Color.Black
        Me.cmdOrderCommentSave.RoundedCornersMask = CType(15, Byte)
        Me.cmdOrderCommentSave.RoundedCornersRadius = 8
        Me.cmdOrderCommentSave.Size = New System.Drawing.Size(74, 20)
        Me.cmdOrderCommentSave.StretchImage = True
        Me.cmdOrderCommentSave.StyleKey = "Button"
        Me.cmdOrderCommentSave.TabIndex = 19
        Me.cmdOrderCommentSave.Text = "保　存"
        Me.cmdOrderCommentSave.UseThemeTextColor = False
        Me.cmdOrderCommentSave.UseVisualStyleBackColor = False
        Me.cmdOrderCommentSave.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'cmdExcuteCommentEdit
        '
        Me.cmdExcuteCommentEdit.BackColor = System.Drawing.Color.Transparent
        Me.cmdExcuteCommentEdit.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdExcuteCommentEdit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdExcuteCommentEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdExcuteCommentEdit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdExcuteCommentEdit.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdExcuteCommentEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdExcuteCommentEdit.Location = New System.Drawing.Point(666, 121)
        Me.cmdExcuteCommentEdit.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdExcuteCommentEdit.Name = "cmdExcuteCommentEdit"
        Me.cmdExcuteCommentEdit.PressedTextColor = System.Drawing.Color.Black
        Me.cmdExcuteCommentEdit.RoundedCornersMask = CType(15, Byte)
        Me.cmdExcuteCommentEdit.RoundedCornersRadius = 8
        Me.cmdExcuteCommentEdit.Size = New System.Drawing.Size(74, 20)
        Me.cmdExcuteCommentEdit.StretchImage = True
        Me.cmdExcuteCommentEdit.StyleKey = "Button"
        Me.cmdExcuteCommentEdit.TabIndex = 18
        Me.cmdExcuteCommentEdit.Text = "編　集"
        Me.cmdExcuteCommentEdit.UseThemeTextColor = False
        Me.cmdExcuteCommentEdit.UseVisualStyleBackColor = False
        Me.cmdExcuteCommentEdit.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'txtExcuteComment
        '
        Me.txtExcuteComment.BackColor = System.Drawing.Color.LightYellow
        Me.txtExcuteComment.Enabled = False
        Me.txtExcuteComment.ImeMode = System.Windows.Forms.ImeMode.Hiragana
        Me.txtExcuteComment.Location = New System.Drawing.Point(564, 143)
        Me.txtExcuteComment.Name = "txtExcuteComment"
        Me.txtExcuteComment.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Horizontal
        Me.txtExcuteComment.Size = New System.Drawing.Size(275, 180)
        Me.txtExcuteComment.TabIndex = 20
        Me.txtExcuteComment.Text = ""
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label24.Location = New System.Drawing.Point(561, 124)
        Me.Label24.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(95, 16)
        Me.Label24.TabIndex = 17
        Me.Label24.Text = "実施時コメント"
        '
        'lstHist
        '
        Me.lstHist.BackColor = System.Drawing.Color.LightYellow
        Me.lstHist.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lstHist.FormattingEnabled = True
        Me.lstHist.ItemHeight = 15
        Me.lstHist.Location = New System.Drawing.Point(34, 29)
        Me.lstHist.Name = "lstHist"
        Me.lstHist.Size = New System.Drawing.Size(805, 79)
        Me.lstHist.TabIndex = 1
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label23.Location = New System.Drawing.Point(31, 14)
        Me.Label23.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(68, 16)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "過去検査"
        '
        'txtModality
        '
        Me.txtModality.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtModality.Location = New System.Drawing.Point(177, 187)
        Me.txtModality.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtModality.Name = "txtModality"
        Me.txtModality.ReadOnly = True
        Me.txtModality.Size = New System.Drawing.Size(96, 22)
        Me.txtModality.TabIndex = 12
        Me.txtModality.TabStop = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label19.Location = New System.Drawing.Point(35, 190)
        Me.Label19.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(58, 16)
        Me.Label19.TabIndex = 11
        Me.Label19.Text = "モダリティ"
        '
        'txtExcuteTime
        '
        Me.txtExcuteTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtExcuteTime.Location = New System.Drawing.Point(459, 165)
        Me.txtExcuteTime.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtExcuteTime.Name = "txtExcuteTime"
        Me.txtExcuteTime.ReadOnly = True
        Me.txtExcuteTime.Size = New System.Drawing.Size(95, 22)
        Me.txtExcuteTime.TabIndex = 10
        Me.txtExcuteTime.TabStop = False
        Me.txtExcuteTime.Text = "10:00:00"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label17.Location = New System.Drawing.Point(316, 168)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(98, 16)
        Me.Label17.TabIndex = 9
        Me.Label17.Text = "検査実施時刻"
        '
        'txtExcuteDate
        '
        Me.txtExcuteDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtExcuteDate.Location = New System.Drawing.Point(177, 165)
        Me.txtExcuteDate.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtExcuteDate.Name = "txtExcuteDate"
        Me.txtExcuteDate.ReadOnly = True
        Me.txtExcuteDate.Size = New System.Drawing.Size(125, 22)
        Me.txtExcuteDate.TabIndex = 8
        Me.txtExcuteDate.TabStop = False
        Me.txtExcuteDate.Text = "2011/10/07"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label18.Location = New System.Drawing.Point(35, 168)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(83, 16)
        Me.Label18.TabIndex = 7
        Me.Label18.Text = "検査実施日"
        '
        'txtOrderTime
        '
        Me.txtOrderTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtOrderTime.Location = New System.Drawing.Point(459, 143)
        Me.txtOrderTime.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtOrderTime.Name = "txtOrderTime"
        Me.txtOrderTime.ReadOnly = True
        Me.txtOrderTime.Size = New System.Drawing.Size(95, 22)
        Me.txtOrderTime.TabIndex = 6
        Me.txtOrderTime.TabStop = False
        Me.txtOrderTime.Text = "10:00:00"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(316, 146)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(98, 16)
        Me.Label16.TabIndex = 5
        Me.Label16.Text = "検査予定時刻"
        '
        'txtOrderDate
        '
        Me.txtOrderDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.txtOrderDate.Location = New System.Drawing.Point(177, 143)
        Me.txtOrderDate.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtOrderDate.Name = "txtOrderDate"
        Me.txtOrderDate.ReadOnly = True
        Me.txtOrderDate.Size = New System.Drawing.Size(125, 22)
        Me.txtOrderDate.TabIndex = 4
        Me.txtOrderDate.TabStop = False
        Me.txtOrderDate.Text = "2011/10/07"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(35, 146)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(83, 16)
        Me.Label15.TabIndex = 3
        Me.Label15.Text = "検査予定日"
        '
        'cmdClose
        '
        Me.cmdClose.BackColor = System.Drawing.Color.Transparent
        Me.cmdClose.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClose.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdClose.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClose.Location = New System.Drawing.Point(706, 534)
        Me.cmdClose.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.PressedTextColor = System.Drawing.Color.Black
        Me.cmdClose.RoundedCornersMask = CType(15, Byte)
        Me.cmdClose.RoundedCornersRadius = 8
        Me.cmdClose.Size = New System.Drawing.Size(147, 34)
        Me.cmdClose.StretchImage = True
        Me.cmdClose.StyleKey = "Button"
        Me.cmdClose.TabIndex = 3
        Me.cmdClose.Text = "閉じる"
        Me.cmdClose.UseThemeTextColor = False
        Me.cmdClose.UseVisualStyleBackColor = False
        Me.cmdClose.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        '
        'cmdEditOrder
        '
        Me.cmdEditOrder.BackColor = System.Drawing.Color.Transparent
        Me.cmdEditOrder.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdEditOrder.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdEditOrder.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdEditOrder.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdEditOrder.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdEditOrder.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEditOrder.Location = New System.Drawing.Point(539, 534)
        Me.cmdEditOrder.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cmdEditOrder.Name = "cmdEditOrder"
        Me.cmdEditOrder.PressedTextColor = System.Drawing.Color.Black
        Me.cmdEditOrder.RoundedCornersMask = CType(15, Byte)
        Me.cmdEditOrder.RoundedCornersRadius = 8
        Me.cmdEditOrder.Size = New System.Drawing.Size(147, 34)
        Me.cmdEditOrder.StretchImage = True
        Me.cmdEditOrder.StyleKey = "Button"
        Me.cmdEditOrder.TabIndex = 2
        Me.cmdEditOrder.Text = "オーダ情報修正"
        Me.cmdEditOrder.UseThemeTextColor = False
        Me.cmdEditOrder.UseVisualStyleBackColor = False
        Me.cmdEditOrder.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.OFFICEBLUE
        Me.cmdEditOrder.Visible = False
        '
        'frmOrderDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(866, 580)
        Me.Controls.Add(Me.cmdEditOrder)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.grpOrderInfo)
        Me.Controls.Add(Me.grpPatientInfo)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOrderDetails"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "オーダ情報明細"
        Me.grpPatientInfo.ResumeLayout(False)
        Me.grpPatientInfo.PerformLayout()
        Me.grpOrderInfo.ResumeLayout(False)
        Me.grpOrderInfo.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpPatientInfo As System.Windows.Forms.GroupBox
    Friend WithEvents txtAge As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtBirth As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtSex As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPatientName_Eiji As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPatientName_Half_Kana As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPatientName_Kanji As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPatientID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents grpOrderInfo As System.Windows.Forms.GroupBox
    Friend WithEvents txtModality As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtExcuteTime As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtExcuteDate As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtOrderTime As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtOrderDate As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cmdClose As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdPatientCommentEdit As VIBlend.WinForms.Controls.vButton
    Friend WithEvents txtPatientComment As System.Windows.Forms.RichTextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents cmdExcuteCommentEdit As VIBlend.WinForms.Controls.vButton
    Friend WithEvents txtExcuteComment As System.Windows.Forms.RichTextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lstHist As System.Windows.Forms.ListBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cmdPatientCommentSave As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdOrderCommentSave As VIBlend.WinForms.Controls.vButton
    Friend WithEvents txtOrderComment As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtBodyPart As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmdEditOrder As VIBlend.WinForms.Controls.vButton
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents toolchipDetail As System.Windows.Forms.ToolTip
End Class
