﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectBodyPart
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmbModality = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grpProc = New System.Windows.Forms.GroupBox()
        Me.cmdCancel = New VIBlend.WinForms.Controls.vButton()
        Me.cmdOK = New VIBlend.WinForms.Controls.vButton()
        Me.txtModality = New System.Windows.Forms.TextBox()
        Me.dtgList = New System.Windows.Forms.DataGridView()
        Me.COL_SELECT = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.COL_STUDY_DIV = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.COL_BODY_PART = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.COL_BODY_PART_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grpProc.SuspendLayout()
        CType(Me.dtgList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmbModality
        '
        Me.cmbModality.Font = New System.Drawing.Font("MS UI Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmbModality.FormattingEnabled = True
        Me.cmbModality.Location = New System.Drawing.Point(71, 9)
        Me.cmbModality.Name = "cmbModality"
        Me.cmbModality.Size = New System.Drawing.Size(195, 23)
        Me.cmbModality.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "検査室名"
        '
        'grpProc
        '
        Me.grpProc.Controls.Add(Me.cmdCancel)
        Me.grpProc.Controls.Add(Me.cmdOK)
        Me.grpProc.Location = New System.Drawing.Point(3, 300)
        Me.grpProc.Name = "grpProc"
        Me.grpProc.Size = New System.Drawing.Size(578, 76)
        Me.grpProc.TabIndex = 3
        Me.grpProc.TabStop = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.Color.Transparent
        Me.cmdCancel.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdCancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdCancel.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(474, 19)
        Me.cmdCancel.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.PressedTextColor = System.Drawing.Color.Black
        Me.cmdCancel.RoundedCornersMask = CType(15, Byte)
        Me.cmdCancel.RoundedCornersRadius = 8
        Me.cmdCancel.Size = New System.Drawing.Size(88, 45)
        Me.cmdCancel.StretchImage = True
        Me.cmdCancel.StyleKey = "Button"
        Me.cmdCancel.TabIndex = 6
        Me.cmdCancel.Text = "キャンセル"
        Me.cmdCancel.UseThemeTextColor = False
        Me.cmdCancel.UseVisualStyleBackColor = False
        Me.cmdCancel.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ULTRABLUE
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.Color.Transparent
        Me.cmdOK.BorderStyle = VIBlend.WinForms.Controls.ButtonBorderStyle.SOLID
        Me.cmdOK.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cmdOK.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.cmdOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdOK.HighlightTextColor = System.Drawing.Color.Black
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(374, 19)
        Me.cmdOK.Margin = New System.Windows.Forms.Padding(6, 4, 6, 4)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.PressedTextColor = System.Drawing.Color.Black
        Me.cmdOK.RoundedCornersMask = CType(15, Byte)
        Me.cmdOK.RoundedCornersRadius = 8
        Me.cmdOK.Size = New System.Drawing.Size(88, 45)
        Me.cmdOK.StretchImage = True
        Me.cmdOK.StyleKey = "Button"
        Me.cmdOK.TabIndex = 5
        Me.cmdOK.Text = "OK"
        Me.cmdOK.UseThemeTextColor = False
        Me.cmdOK.UseVisualStyleBackColor = False
        Me.cmdOK.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.ULTRABLUE
        '
        'txtModality
        '
        Me.txtModality.Location = New System.Drawing.Point(538, 13)
        Me.txtModality.Name = "txtModality"
        Me.txtModality.Size = New System.Drawing.Size(43, 19)
        Me.txtModality.TabIndex = 16
        Me.txtModality.Visible = False
        '
        'dtgList
        '
        Me.dtgList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.COL_SELECT, Me.COL_STUDY_DIV, Me.COL_BODY_PART, Me.COL_BODY_PART_NO})
        Me.dtgList.Location = New System.Drawing.Point(12, 50)
        Me.dtgList.Name = "dtgList"
        Me.dtgList.RowTemplate.Height = 21
        Me.dtgList.Size = New System.Drawing.Size(569, 232)
        Me.dtgList.TabIndex = 18
        '
        'COL_SELECT
        '
        Me.COL_SELECT.HeaderText = "選択"
        Me.COL_SELECT.Name = "COL_SELECT"
        Me.COL_SELECT.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.COL_SELECT.Width = 35
        '
        'COL_STUDY_DIV
        '
        Me.COL_STUDY_DIV.HeaderText = "撮影区分"
        Me.COL_STUDY_DIV.Items.AddRange(New Object() {"【単】", "【造】", "【単+造】", "【Dynamic】"})
        Me.COL_STUDY_DIV.Name = "COL_STUDY_DIV"
        Me.COL_STUDY_DIV.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.COL_STUDY_DIV.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'COL_BODY_PART
        '
        Me.COL_BODY_PART.HeaderText = "部位名"
        Me.COL_BODY_PART.Name = "COL_BODY_PART"
        Me.COL_BODY_PART.ReadOnly = True
        Me.COL_BODY_PART.Width = 200
        '
        'COL_BODY_PART_NO
        '
        Me.COL_BODY_PART_NO.HeaderText = "部位番号"
        Me.COL_BODY_PART_NO.Name = "COL_BODY_PART_NO"
        Me.COL_BODY_PART_NO.Visible = False
        '
        'frmSelectBodyPart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 386)
        Me.ControlBox = False
        Me.Controls.Add(Me.dtgList)
        Me.Controls.Add(Me.txtModality)
        Me.Controls.Add(Me.grpProc)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmbModality)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmSelectBodyPart"
        Me.Text = "部位選択"
        Me.grpProc.ResumeLayout(False)
        CType(Me.dtgList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbModality As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents grpProc As System.Windows.Forms.GroupBox
    Friend WithEvents cmdCancel As VIBlend.WinForms.Controls.vButton
    Friend WithEvents cmdOK As VIBlend.WinForms.Controls.vButton
    Friend WithEvents txtModality As System.Windows.Forms.TextBox
    Friend WithEvents dtgList As System.Windows.Forms.DataGridView
    Friend WithEvents COL_SELECT As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents COL_STUDY_DIV As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents COL_BODY_PART As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents COL_BODY_PART_NO As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
