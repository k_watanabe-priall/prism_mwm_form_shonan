﻿Public NotInheritable Class frmLogin
    Private intAbortLogin As Integer = 0

#Region "FormEvents"

#Region "FormKeyPress"

    Private Sub frmLogin_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        Me.txtUserID.Text = My.Settings.LOGIN_USER
        If My.Settings.LOGIN_DIV = "1" Then
            Me.txtPassword.Text = My.Settings.LOGIN_PASS
            Me.cmdLogIn.PerformClick()
        End If
        If My.Settings.SAVE_PASS_DIV = "1" Then
            Me.txtPassword.Text = My.Settings.LOGIN_PASS
        End If
    End Sub

    Private Sub frmLogin_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        '///Form上でEnterを押下されたらログインボタンのClickEventsを呼び出す
        If (e.KeyChar = Chr(Keys.Enter)) Then
            Me.cmdLogIn.PerformClick()
        End If
    End Sub

#End Region

#Region "FormLoad"

    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim strMsg As String = ""

        'If g_SystemStart = 1 Then Me.Visible = False
        Try
            Call subOutLog("ログイン画面表示　[frmLogin.frmLogin_Load]", 0)

            '********************
            '* DataBase Connect *
            '********************
            Call subOutLog("データベース接続関数呼び出し(fncDBConnect)　[frmLogin.frmLogin_Load]", 0)

            If fncDBConnect(strMsg) = False Then
                '***********
                '* Log出力 *
                '***********
                Call subOutLog("データベース接続関数 失敗(fncDBConnect) (エラー内容 = " & strMsg & ") " & _
                               "[frmLogin.frmLogin_Load]", 1)

                End
            End If

            Me.Refresh()
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("ログイン画面表示　失敗 (エラー内容 = " & strMsg & ") " & _
                           "[frmLogin.frmLogin_Load]", 1)
        End Try
    End Sub

#End Region

#End Region

#Region "Control Events"

#Region "ログイン押下時"
    Private Sub cmdLogIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLogIn.Click
        Dim frmObjMain As New frmMain
        Dim intRet As Integer
        Dim strMsg As String = ""

        Call subOutLog("ログインボタン押下　[frmLogin.cmdLogIn_Click]", 0)

        Call subOutLog("ログイン認証関数呼び出し (ユーザＩＤ = " & txtUserID.Text & ") [frmLogin.cmdLogIn_Click]", 0)

        '///Login認証
        intRet = fncChkLogin(txtUserID.Text, txtPassword.Text, strMsg)

        '///戻り値による制御
        Select Case intRet
            Case 0          '認証OK
                '               frmObjMenu.Show()
                frmObjMain.Show()
                intAbortLogin = 0
                Me.Visible = False
            Case 1
                If intAbortLogin >= 3 Then
                    GoTo LoginWarining
                End If

                intAbortLogin += 1

                Call subOutLog("ログイン失敗 ユーザ未存在 (ユーザＩＤ = " & txtUserID.Text & ") [frmLogin.cmdLogIn_Click]", 2)

                MsgBox("入力されたユーザは存在しません。" & vbCrLf & "正しいユーザを入力して下さい。", MsgBoxStyle.Exclamation, "ログイン認証")
                Me.txtUserID.Focus()
                Exit Sub
            Case 2
                If intAbortLogin >= 3 Then
                    GoTo LoginWarining
                End If

                intAbortLogin += 1

                Call subOutLog("ログイン失敗 パスワード不一致 (ユーザＩＤ = " & txtUserID.Text & ") [frmLogin.cmdLogIn_Click]", 2)

                MsgBox("入力されたパスワードが誤っています。" & vbCrLf & "正しいパスワードを入力して下さい。", MsgBoxStyle.Exclamation, "ログイン認証")
                Me.txtPassword.Focus()
                Exit Sub
            Case 99
                If intAbortLogin >= 3 Then
                    GoTo LoginWarining
                End If

                intAbortLogin += 1

                Call subOutLog("ログイン失敗 (ユーザＩＤ = " & txtUserID.Text & ") (エラー内容 = " & strMsg & ") [frmLogin.cmdLogIn_Click]", 2)

                Call subOutLog("ログイン認証でエラーが発生しました。入力UserID = " & txtUserID.Text.Trim, 2)
                MsgBox("ログイン認証でエラーが発生しました。" & vbCrLf & "ErrMsg：" & strMsg & vbCrLf & "システム管理者へ連絡して下さい。", MsgBoxStyle.Critical, "ログイン認証")
                Exit Sub
        End Select

        Call subOutLog("ログイン成功 (ユーザＩＤ = " & txtUserID.Text & ") [frmLogin.cmdLogIn_Click]", 0)

        Exit Sub

        '///ログイン認証における強制終了
LoginWarining:
        MsgBox("ログイン認証に3回失敗しました。" & vbCrLf & "システムを強制終了します。" & vbCrLf & "ログイン情報をご確認の上、システムを再度起動して下さい。", MsgBoxStyle.Critical, "ログイン認証")
        Call subOutLog("ログイン認証に3回失敗しました。システムを強制終了します。入力UserID = " & txtUserID.Text.Trim, 1)
        End
    End Sub
#End Region

#Region "キャンセル押下時"
    Private Sub cmdQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdQuit.Click
        '///確認メッセージ
        Call subOutLog("キャンセルボタン押下 [frmLogin.cmdQuit_Click]", 0)

        If MsgBox("システムを終了します。" & vbCrLf & "宜しいですか。", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "ログイン") = vbNo Then
            Call subOutLog("キャンセルボタン押下 「いいえ」を応答 [frmLogin.cmdQuit_Click]", 0)
            Exit Sub
        End If

        Call subOutLog("キャンセルボタン押下 「はい」を応答 [frmLogin.cmdQuit_Click]", 0)

        Call subOutLog("！！！MWM Mammogram Edition END！！！", 0)

        Call subOutLog("==========================================================================", 0)

        End

    End Sub

#End Region

#Region "ユーザID GotFocus"

    Private Sub txtUserID_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserID.GotFocus
        '///ハイライト表示
        Call subHightLight(Me.txtUserID)
    End Sub
#End Region

#Region "ユーザID KeyPress"
    Private Sub txtUserID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUserID.KeyPress
        '///Enterを押下されたらログインボタンのClickEventsを呼び出す
        If (e.KeyChar = Chr(Keys.Enter)) Then
            If Me.txtPassword.Text <> "" Then
                Me.cmdLogIn.PerformClick()
            Else
                Me.txtPassword.Focus()
            End If

        End If

    End Sub
#End Region

#Region "パスワード GotFocus"

    Private Sub txtPassword_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPassword.GotFocus
        '///ハイライト表示
        Call subHightLight(Me.txtPassword)
    End Sub
#End Region

#Region "パスワード KeyPress"

    Private Sub txtPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        '///Enterを押下されたらログインボタンのClickEventsを呼び出す
        If (e.KeyChar = Chr(Keys.Enter)) Then
            Me.cmdLogIn.PerformClick()
        End If
    End Sub
#End Region

#Region "クリアボタン押下"

    Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClear.Click
        Call subOutLog("クリア押下 「はい」を応答 [frmLogin.cmdClear_Click]", 0)

        Me.txtUserID.Text = vbNullString
        Me.txtPassword.Text = vbNullString
    End Sub

#End Region

#End Region


End Class
