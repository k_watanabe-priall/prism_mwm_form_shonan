﻿'------------------------------------------------------------------------------
' <auto-generated>
'     このコードはツールによって生成されました。
'     ランタイム バージョン:4.0.30319.42000
'
'     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
'     コードが再生成されるときに損失したりします。
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace My
    
    <Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.8.1.0"),  _
     Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
    Partial Friend NotInheritable Class MySettings
        Inherits Global.System.Configuration.ApplicationSettingsBase
        
        Private Shared defaultInstance As MySettings = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New MySettings()),MySettings)
        
#Region "My.Settings 自動保存機能"
#If _MyType = "WindowsForms" Then
    Private Shared addedHandler As Boolean

    Private Shared addedHandlerLockObject As New Object

    <Global.System.Diagnostics.DebuggerNonUserCodeAttribute(), Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)> _
    Private Shared Sub AutoSaveSettings(sender As Global.System.Object, e As Global.System.EventArgs)
        If My.Application.SaveMySettingsOnExit Then
            My.Settings.Save()
        End If
    End Sub
#End If
#End Region
        
        Public Shared ReadOnly Property [Default]() As MySettings
            Get
                
#If _MyType = "WindowsForms" Then
               If Not addedHandler Then
                    SyncLock addedHandlerLockObject
                        If Not addedHandler Then
                            AddHandler My.Application.Shutdown, AddressOf AutoSaveSettings
                            addedHandler = True
                        End If
                    End SyncLock
                End If
#End If
                Return defaultInstance
            End Get
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("ModalityWorkListManage  Edition")>  _
        Public Property SYS_NAME() As String
            Get
                Return CType(Me("SYS_NAME"),String)
            End Get
            Set
                Me("SYS_NAME") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("127.0.0.1")>  _
        Public Property DATASOURCE() As String
            Get
                Return CType(Me("DATASOURCE"),String)
            End Get
            Set
                Me("DATASOURCE") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_MWM_SHONAN")>  _
        Public Property CATALOG() As String
            Get
                Return CType(Me("CATALOG"),String)
            End Get
            Set
                Me("CATALOG") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("priall")>  _
        Public Property DBUSER() As String
            Get
                Return CType(Me("DBUSER"),String)
            End Get
            Set
                Me("DBUSER") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("2336")>  _
        Public Property DBPASS() As String
            Get
                Return CType(Me("DBPASS"),String)
            End Get
            Set
                Me("DBPASS") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("C:\Users\k.watanabe\source\repos\PRISM_MWM_FORM_SHONAN\LOG")>  _
        Public Property LOG_PATH() As String
            Get
                Return CType(Me("LOG_PATH"),String)
            End Get
            Set
                Me("LOG_PATH") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_002")>  _
        Public Property TERMINAL() As String
            Get
                Return CType(Me("TERMINAL"),String)
            End Get
            Set
                Me("TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("True")>  _
        Public Property AUTO_REFRESH() As Boolean
            Get
                Return CType(Me("AUTO_REFRESH"),Boolean)
            End Get
            Set
                Me("AUTO_REFRESH") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("3000")>  _
        Public Property INTERVAL() As String
            Get
                Return CType(Me("INTERVAL"),String)
            End Get
            Set
                Me("INTERVAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("True")>  _
        Public Property NON_ACCEPT() As Boolean
            Get
                Return CType(Me("NON_ACCEPT"),Boolean)
            End Get
            Set
                Me("NON_ACCEPT") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("True")>  _
        Public Property ACCEPT() As Boolean
            Get
                Return CType(Me("ACCEPT"),Boolean)
            End Get
            Set
                Me("ACCEPT") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("False")>  _
        Public Property NON_DELETE() As Boolean
            Get
                Return CType(Me("NON_DELETE"),Boolean)
            End Get
            Set
                Me("NON_DELETE") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("False")>  _
        Public Property DELETE() As Boolean
            Get
                Return CType(Me("DELETE"),Boolean)
            End Get
            Set
                Me("DELETE") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("0")>  _
        Public Property VIEWER_DIV() As String
            Get
                Return CType(Me("VIEWER_DIV"),String)
            End Get
            Set
                Me("VIEWER_DIV") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property FORM_FONT() As String
            Get
                Return CType(Me("FORM_FONT"),String)
            End Get
            Set
                Me("FORM_FONT") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("787")>  _
        Public Property FORM_DETAIL_WIDTH() As Integer
            Get
                Return CType(Me("FORM_DETAIL_WIDTH"),Integer)
            End Get
            Set
                Me("FORM_DETAIL_WIDTH") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("520")>  _
        Public Property FORM_DETAIL_HEIGHT() As Integer
            Get
                Return CType(Me("FORM_DETAIL_HEIGHT"),Integer)
            End Get
            Set
                Me("FORM_DETAIL_HEIGHT") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("0")>  _
        Public Property FORM_SIZE_WIDTH() As Integer
            Get
                Return CType(Me("FORM_SIZE_WIDTH"),Integer)
            End Get
            Set
                Me("FORM_SIZE_WIDTH") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("0")>  _
        Public Property FORM_SIZE_HEIGHT() As Integer
            Get
                Return CType(Me("FORM_SIZE_HEIGHT"),Integer)
            End Get
            Set
                Me("FORM_SIZE_HEIGHT") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("湘南第一病院")>  _
        Public Property HOSPITAL() As String
            Get
                Return CType(Me("HOSPITAL"),String)
            End Get
            Set
                Me("HOSPITAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property VIEWER_END_URL() As String
            Get
                Return CType(Me("VIEWER_END_URL"),String)
            End Get
            Set
                Me("VIEWER_END_URL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("C:\Program Files (x86)\Internet Explorer\iexplore.exe")>  _
        Public Property VIEWER_EXEC() As String
            Get
                Return CType(Me("VIEWER_EXEC"),String)
            End Get
            Set
                Me("VIEWER_EXEC") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property VIEWER_URL() As String
            Get
                Return CType(Me("VIEWER_URL"),String)
            End Get
            Set
                Me("VIEWER_URL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("")>  _
        Public Property CORPORATE() As String
            Get
                Return CType(Me("CORPORATE"),String)
            End Get
            Set
                Me("CORPORATE") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("787")>  _
        Public Property FORM_EDIT_ORDER_WIDTH() As Long
            Get
                Return CType(Me("FORM_EDIT_ORDER_WIDTH"),Long)
            End Get
            Set
                Me("FORM_EDIT_ORDER_WIDTH") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("520")>  _
        Public Property FORM_EDIT_ORDER_HEIGHT() As Long
            Get
                Return CType(Me("FORM_EDIT_ORDER_HEIGHT"),Long)
            End Get
            Set
                Me("FORM_EDIT_ORDER_HEIGHT") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("702")>  _
        Public Property FORM_EDIT_PATIENT_WIDTH() As Integer
            Get
                Return CType(Me("FORM_EDIT_PATIENT_WIDTH"),Integer)
            End Get
            Set
                Me("FORM_EDIT_PATIENT_WIDTH") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("396")>  _
        Public Property FORM_EDIT_PATIENT_HEIGHT() As Integer
            Get
                Return CType(Me("FORM_EDIT_PATIENT_HEIGHT"),Integer)
            End Get
            Set
                Me("FORM_EDIT_PATIENT_HEIGHT") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("C:\Users\k.watanabe\source\repos\PRISM_MWM_FORM_SHONAN\CSV\")>  _
        Public Property CSV_PATH() As String
            Get
                Return CType(Me("CSV_PATH"),String)
            End Get
            Set
                Me("CSV_PATH") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("C:\Users\k.watanabe\source\repos\PRISM_MWM_FORM_SHONAN\PRISM_MWM_FORM\bin\appSett"& _ 
            "ings.config")>  _
        Public Property Setting_FILE_PATH() As String
            Get
                Return CType(Me("Setting_FILE_PATH"),String)
            End Get
            Set
                Me("Setting_FILE_PATH") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("shonan")>  _
        Public Property LOGIN_USER() As String
            Get
                Return CType(Me("LOGIN_USER"),String)
            End Get
            Set
                Me("LOGIN_USER") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("shonan")>  _
        Public Property LOGIN_PASS() As String
            Get
                Return CType(Me("LOGIN_PASS"),String)
            End Get
            Set
                Me("LOGIN_PASS") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("0")>  _
        Public Property SAVE_PASS_DIV() As String
            Get
                Return CType(Me("SAVE_PASS_DIV"),String)
            End Get
            Set
                Me("SAVE_PASS_DIV") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("1")>  _
        Public Property LOGIN_DIV() As String
            Get
                Return CType(Me("LOGIN_DIV"),String)
            End Get
            Set
                Me("LOGIN_DIV") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("618")>  _
        Public Property FORM_BODY_PART_HEGHT() As Integer
            Get
                Return CType(Me("FORM_BODY_PART_HEGHT"),Integer)
            End Get
            Set
                Me("FORM_BODY_PART_HEGHT") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("628")>  _
        Public Property FORM_BODY_PART_WIDTH() As Integer
            Get
                Return CType(Me("FORM_BODY_PART_WIDTH"),Integer)
            End Get
            Set
                Me("FORM_BODY_PART_WIDTH") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("True")>  _
        Public Property STUDY_ACCEPT() As Boolean
            Get
                Return CType(Me("STUDY_ACCEPT"),Boolean)
            End Get
            Set
                Me("STUDY_ACCEPT") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("True")>  _
        Public Property STUDY_END() As Boolean
            Get
                Return CType(Me("STUDY_END"),Boolean)
            End Get
            Set
                Me("STUDY_END") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("C:\Users\k.watanabe\source\repos\PRISM_MWM_FORM_SHONAN\ID")>  _
        Public Property OUT_PATH() As String
            Get
                Return CType(Me("OUT_PATH"),String)
            End Get
            Set
                Me("OUT_PATH") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("0")>  _
        Public Property XRAY_DIV() As String
            Get
                Return CType(Me("XRAY_DIV"),String)
            End Get
            Set
                Me("XRAY_DIV") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_002")>  _
        Public Property CR_TERMINAL() As String
            Get
                Return CType(Me("CR_TERMINAL"),String)
            End Get
            Set
                Me("CR_TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_003")>  _
        Public Property CT_TERMINAL() As String
            Get
                Return CType(Me("CT_TERMINAL"),String)
            End Get
            Set
                Me("CT_TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_004")>  _
        Public Property RF_TERMINAL() As String
            Get
                Return CType(Me("RF_TERMINAL"),String)
            End Get
            Set
                Me("RF_TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_006")>  _
        Public Property US1_TERMINAL() As String
            Get
                Return CType(Me("US1_TERMINAL"),String)
            End Get
            Set
                Me("US1_TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_005")>  _
        Public Property US2_TERMINAL() As String
            Get
                Return CType(Me("US2_TERMINAL"),String)
            End Get
            Set
                Me("US2_TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_007")>  _
        Public Property US3_TERMINAL() As String
            Get
                Return CType(Me("US3_TERMINAL"),String)
            End Get
            Set
                Me("US3_TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_008")>  _
        Public Property ES1_TERMINAL() As String
            Get
                Return CType(Me("ES1_TERMINAL"),String)
            End Get
            Set
                Me("ES1_TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_009")>  _
        Public Property ES2_TERMINAL() As String
            Get
                Return CType(Me("ES2_TERMINAL"),String)
            End Get
            Set
                Me("ES2_TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("False")>  _
        Public Property Finish() As Boolean
            Get
                Return CType(Me("Finish"),Boolean)
            End Get
            Set
                Me("Finish") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_011")>  _
        Public Property PORTA_TERMINAL() As String
            Get
                Return CType(Me("PORTA_TERMINAL"),String)
            End Get
            Set
                Me("PORTA_TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_011")>  _
        Public Property SITEHEALTH_TERMINAL() As String
            Get
                Return CType(Me("SITEHEALTH_TERMINAL"),String)
            End Get
            Set
                Me("SITEHEALTH_TERMINAL") = value
            End Set
        End Property
        
        <Global.System.Configuration.UserScopedSettingAttribute(),  _
         Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         Global.System.Configuration.DefaultSettingValueAttribute("PRIM_005_")>  _
        Public Property US4_TERMINAL() As String
            Get
                Return CType(Me("US4_TERMINAL"),String)
            End Get
            Set
                Me("US4_TERMINAL") = value
            End Set
        End Property
    End Class
End Namespace

Namespace My
    
    <Global.Microsoft.VisualBasic.HideModuleNameAttribute(),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Friend Module MySettingsProperty
        
        <Global.System.ComponentModel.Design.HelpKeywordAttribute("My.Settings")>  _
        Friend ReadOnly Property Settings() As Global.PRISM_MWM_FORM.My.MySettings
            Get
                Return Global.PRISM_MWM_FORM.My.MySettings.Default
            End Get
        End Property
    End Module
End Namespace
